#title Veba Günlüklerinde Gündelik Yaşam
#author Şeyma Çopur
#SORTtopics koranavirüs, kitap, veba günlükleri
#date 06.05.2020
#source Meydan Gazetesi
#lang tr
#pubdate 2020-06-23T13:02:54



<em>Yıl altmış beş, Londra / Dehşetengiz bir veba / Yüz bin can oldu heba / Ben ise hala hayatta!”</em>

Böyle son bulur Veba Yılı Günlüğü. Kapandığımız evlerin içinde birçoğumuzu huzursuzlukla baş başa bırakan korona virüs salgınını yaşarken uğradığı her yere ölümün kokusunu taşıyan veba salgınına tanık oluruz bu günlükle. Daniel Defoe Veba Yılı Günlüğü’nü 1722’de kaleme alır ancak günlükte bahsedilen yıl, kitabın sonundaki dörtlükte de yazdığı gibi, 1665’tir. Defoe, anlattığı dönemde henüz 5 yaşındadır ve bu yüzden birinci ağızdan anlattığı dönemin bir kurgudan ibaret olup olmadığı tam olarak bilinmez. Kimilerine göre Defoe’nun amcası Henry Foe’nun günlüğünden yola çıkılarak yazılmıştır.

Hangisinin gerçek olduğu da bilinmez ancak anlatılan her şey oldukça gerçekçidir. Okuyan, vebanın kol gezdiği sokaklarda bulur kendisini; görür, duyar, hisseder, koklar. Her gün öyle çok insan vebadan ölür ki ölüm, soğukluğunu kaybetmiştir artık. Sokak kenarlarında yığılmış bedenler olağanlaşır, gelişigüzel bir bezle bedeni sarmalanıp kazılan çukurlara “atılan” cesetler, korkuya ve acıya dayanamayıp mezara girmeye çalışanlar, sokaklarda sıkılı dişleri ve yumruklarıyla bağırarak dolaşanlar, günahlarının affolması için dua ederek merhamet dileyenler… Günlükte anlatıldığı haliyle veba, imkansız görünenin mümkün hale geldiği, düşünülemeyecek olanın gerçekleştiği bir salgındır.

Defoe kitaba, neden günlük yazdığını açıklayarak başlar. Londra’da yaşayan, eyer imalatıyla uğraşan anlatıcı, veba hastalığı yaşadığı topraklara uğradığı anda birçokları gibi vebadan nasıl kaçacağını düşünmeye başlar. Bir yandan kaçıp kurtulmayı bir yandan kalıp salgın boyunca kentte yaşananları yazmayı ister. Son kararı, çeşitli tesadüflerin de etkisiyle, kentte kalmak ve yazmak olur:

<em>“…Bakarsınız benden</em> <em>sonra da biri benzer bir sıkıntıya düştüğünde yine benzer bir seçim yapmak zorunda kalır. Aldığım bu notlar yapıp ettiklerimin tarihçesi değil kişinin eylemlerine rehber olsun diyedir. Yoksa şahsen yaşadıklarımın kimsenin gözünde zerre kadar önemi olmadığının farkındayım.”</em>

Anlatıcı kentte kalmayı “seçmiş” olsa da kentte kalmanın birçok kişi için zorunluluk olduğunu görürüz. Kent, her şeyden önce, kalabalık olduğu için terk edilesi bir yer olarak anlatılırken vebanın bulaştığı kimseler, birkaç iyileşme rivayeti dışında, günler içinde ölmektedir ve hastalıktan kurtulmanın tek yolu ondan kaçmaktır. Kaçanlar arasında kimler yoktur ki… Kentin ileri gelenleri bir yana, kral ve maiyeti kenti ilk terkedenlerden olur: <em>“…hastalık onların yanlarından bile geçmemişti.”</em> der anlatıcı. Kentte geriye kalanlar, kaçacak hiçbir yeri olmayanlardır: Evsizler, yoksullar, hizmetçiler…

Günlük, bir bakıma, kentin dış çeperlerindeki muhitlerde kalıp ölümün gelişini seyretmek zorunda bırakılan bu insanların anlatısıdır. Anlatıcının söylediği haliyle <em>“en umutsuz durumda olanlar”</em> buralarda yaşayanlardır. Vebayı korku içinde bekleyen ve günü gelip kendisine bulaştığında bir “kurban” gibi teslim olmaya hazırlanan yoksulların bekleyişi çoğunlukla “hayatta kalabilmek için iki lokma isterken” ya da evin içinden yükselen çığlık sesleriyle son bulur. <em>“Hastalandıklarında ne yiyecek bir şeyleri, ne ilaçları, ne onlara yol gösterecek hakim veya eczacı, ne de bakacak hemşireleri vardı.”</em> der anlatıcı, sefilliğin en uç noktasındaki bu insanları anlatırken.

Birikmiş parası olan, hastalığın yayıldığı haberini aldığında stok yapar ve salgının en can yakıcı bulaşıcılığa ulaştığı dönemleri evinde geçirebilir. Yoksullar ise stok yapamaz ve pazara gitmek için sürekli sokağa çıkmak zorundadır. Böylelikle hasta olan ya da hastalığının farkında olmayan birçok kişi pazara gelir, pazardan dönen birçok kişi sessiz sedasız, ölümü de kendisiyle taşır.

Hizmetçiler, hiç değilse ailenin diğer üyelerini yaşatabilmek için çalışmayı sürdürür ve çoğunlukla zengin ev sahibinin yazgısındaki son, onun da sonudur. Çoğunluğu kadınlardan oluşan bu hizmetçilerin en büyük korkusu vebaya yakalanmak değil işine son verilmesidir. Gerçekten birçoğunun işine, yoksul olduğu ve hastalığın yoksullar tarafından bulaştırıldığı düşüncesiyle son verilir. Ancak yoksulluk, yoksullara çalışmak dışında bir imkan tanımaz. Bu insanlar, hastalara bakmak ve ölüleri mezara taşımak gibi, hiç kimsenin yapmak istemeyeceği işleri bulduklarında bile hiç düşünmeden işe koyulur ve bu sayede para kazanırlar. Ne yazık ki bu insanlar arasında sahte ilaçla şifa dağıtan, büyüyle kendine inandıran, korkulardan beslenip umut satan fırsatçılara kazandığı paranın tümünü kaptıranların sayısı hiç az değildir.

Günlüğün sonuna gelecek olursak…

Bir yılın ardından veba Londra’yı terk eder, anlatıcının söylediğine göre son üç haftada bile 30.000 kişi ölmüştür ve veba salgını boyunca tam olarak kaç kişinin öldüğü asla gerçekten bilinemeyecektir. Çünkü anlatıcı bize verilerin eksik aktarıldığını, saklandığını ve gerçeği yansıtmadığını söyleyerek bitirir günlüğü. Yüz binlerce insanın yaşamını yitirmiş olmasının ardından her şeyin kolayca eski haline geldiğini söylemek zordur. İnsanların birbiriyle kurduğu ilişkide düşmanca tepkiler ortaya çıkar; mezar sayısının olağanüstü artışıyla evler ve mezarlar birbiriyle iç içe girer; yıkılmış, etkisizleşmiş, çökmüş bütün kurumlar yeniden kurulmaya başlanır. Ancak insanlar, hastalığın geçtiğine dair umutlanmaya başladığı andan itibaren, yaşadıkları “cehennemi” unutur ve tüm tedbirleri bir anda elden bırakarak tüm tehlike geçmişçesine eski yaşantısına dönmeye çalışır. Ölenler dünyayı terk etmişken kaçanlar kente döner; dindar olanlar Tanrı’ya insanlığı bu salgından kurtardığı için şükranlarını sunarken yoksullar vebayı atlatabildikleri için kendilerini şanslı sayarak yaşamayı sürdürür…

Aradan geçen üç yüz yıl “salgın” kavramına ve deneyimine dair birçok şeyi değiştirmiştirse de okuyanla kitap arasındaki gerçeklik ilişkisinin kurulabilmesini sağlayan en önemli etken, anlatılanların birçok yönüyle bugün de yaşanıyor olmasıdır. Değişmeyen şeylerden birisi de zenginler ve yoksulların salgından nasıl etkilendiğidir.



