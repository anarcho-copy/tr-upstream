#title İdam!
#author Gün Zileli
#SORTtopics idam, devlet, toplum, yasa
#date 31.07.2022
#source 02.08.2022 tarihinde şuradan alındı: [[https://www.gunzileli.net/2022/08/02/idam/][gunzileli.net]]
#lang tr
#pubdate 2022-08-02T20:02:24



Haber kanalları, 1 Şubat 2021’de darbeyle iktidara el koyan Burma (Mynmar) askeri cuntasının 4 muhalifi idam ettiğini duyurdu.

Türkiye uzak ve yakın tarihinden de çok iyi bildiğimiz, hatta kimi zaman yakından tanık olduğumuz (örneğin, sevgili arkadaşlarımız Deniz Gezmiş, Hüseyin İnan ve Yusuf Aslan’ın 5-6 Mayıs 1972 gecesi Mamak Cezaevi’nin mutfak kısmından çıkarılıp idam için Ankara Merkez Cezaevi’ne götürülüşlerine diğer arkadaşlarımla birlikte bizzat tanık olmuştum) idam denilen, devlet eliyle işlenen cinayet, cinayetlerin en korkuncudur. Çünkü diğer cinayet türlerinden farklı olarak idam devletin “yasal” cinayeti olmanın ötesinde, birçok insanın gönüllü desteğini alır. Bu cinayete verilen her destek aslında, bu devlet cinayetinin toplum cinayetine dönüşmesine ve kurbanın belki de milyonlarca defa öldürülmesine yol açar.

Çocukluğumda (1950’ler) halka açık (seyirlik) idamlar yapıldığını hatırlıyorum. Bu idamlar İstanbul’un hangi semtinde yapılırdı, şimdi yanlış bilgi vermeyeyim ama, ailelerin, çoluk çocuk, piknik yapar gibi, nevalelerini de yanlarına alarak idam seyretmeye gittikleri anlatılırdı. Bu seyrin, idam sehpasındaki kurbanın bedenine binlerce hançer saplanmasından ne farkı vardır? Pir Sultan Abdal, “ille dostun bir tek gülü yaralar beni” derken, biraz da idama giden insanın ne kadar hassas bir ruh hali içinde olduğunu anlatmamış mıdır? Tolstoy’un, gençlik yıllarında Paris’te bir giyotinle idamı seyretmesi, onun ruhsal dönüşümündeki en büyük amil olmuştur.

Cinayetler, spontane cinayetlerden taammüden cinayetlere doğru, cinayet failinin sorumluluğu açısından farklılıklar gösterir.

Birinci kategori, spontane cinayettir. Yani, katil önceden cinayet işleyeceğini bilmemektedir, buna yönelik hiçbir planı yoktur, hatta kimi durumlarda kurbanı tanımaz bile. Meydan kavgalarında işlenen cinayetler genellikle bu kategoriye girer. Katil, kavga anında bıçağını çekip rastgele sallamış ve birinin ölümüne yol açmıştır. Savunma cinayetleri de bu kategoriye girer. Sanık, cinayeti, kendini öldürmeye gelen birine karşı savunma amacıyla işlemiştir. Eğer kanıtlanırsa, bu tür cinayetleri işleyenler mahkemelerde beraat bile edebilirler.

İkinci kategori, ihmalden ya da dikkatsizlikten doğan iş ve “kaza” cinayetleridir. Bu cinayetlerin kurbanları işçiler ya da yolcular-sürücüler veya yayalardır.

Üçüncü kategoriyi oluşturan, hayatta bir kere bile yüz yüze gelmemiş, “asker” kılığına sokulmuş insanların birbirini öldürdüğü savaşta işlenen cinayetler ise, bırakın cezalandırılmayı, devletler tarafından “kahramanlık nişanı” ile ödüllendirilir.

Dördüncü kategori, taammüden cinayettir. Katil, cinayeti önceden planlamış ve gerçekleştirmiştir. Elbette bu, birinci kategorideki spontane cinayetle kıyaslanmayacak ölçüde gaddarca bir cinayettir. Uzun süreli tehditlerle birlikte gelen, yani “geliyorum” diyen kadın ve nefret cinayetleri bu kategoride yer alır. Birçok kadın her gün ölümü bekleyerek yaşar. Ölümü beklemek ölümden bile korkunçtur.

Beşinci kategori, suikast cinayetleridir. Siyasi nedenlerle ya da başka menfaatler nedeniyle kurban önceden belirlenir ve bir suikast sonucunda öldürülür. Devlete karşı silahlı mücadele veren gruplar olsun, devletin gizli istihbarat örgütleri olsun ya da mafya grupları olsun, genellikle bu yola başvururlar. Son zamanlarda TV kanallarında sık sık duyuyoruz, “bilmem hangi bölgenin sözde bölge sorumlusu etkisiz hale getirildi” diye. Bu, devlet eliyle yapılan suikastın haber formudur.

Altıncı kategori olan “rehine cinayetleri”, suikast cinayetlerine benzemekle birlikte, “soğukkanlılığı” açısından daha korkunçtur. Bu yola daha çok gizli silahlı mücadele örgütleri başvurur. Suikastten daha korkunçtur, çünkü suikastte kurban hiçbir şeyden habersizken aniden öldürülmüş, dolayısıyla bir ölüm korkusu yaşamamıştır. Oysa rehine cinayetinde kurban, eli kolu bağlı bir şekilde devletle yapılan pazarlıkların sonucunu beklemektedir. Bu pazarlıkların olumsuz sonuçlanacağı neredeyse kesindir. Çünkü hiçbir devlet, “önemli” bir mensubu için ödün vermeyi kabul etmez, kabul ederse, bunun, otoritesinde önemli bir gedik açacağını düşünür. Kısacası, kurban, bir anlamda rehineyi kaçıranların ve devletin ortak kararıyla katledilir. Yakın sayılabilecek bir tarihteki (1970’ler), İtalya Başbakanı Aldo Moro ve İsrail’in İstanbul Başkonsolosu Efraim Elrom cinayetleri (o zamanki Türkiye Hükümeti, Musevi topluluğunun, kaçıran örgüte yüklü bir bedel ödeme önerisini kaçıranlara iletmeyi bile gereksiz görerek ve baskıyı arttırarak aslında cinayete ortak, hatta önayak olmuştur) belleklere unutulmamacasına kazınmıştır.

Yedinci kategori ise, devletin “yasal” organlarının verdiği kararla işlenen cinayettir ki, buna idam denmektedir. Bu, tarihin gördüğü ve tanıdığı en “soğukkanlı” cinayet türüdür ve kararın hedefi olan insanı sadece ölüme değil, aynı zamanda kaçınılmaz ölümü beklemeye mahkûm ettiği için en acımasızıdır. Çünkü idama mahkûm edilen insan, öldürüleceğini bilmekte ve ölüm anına kadar her saniye ölüp ölüp dirilmektedir. İdam mahkûmu, sonunda bir gece yarısı ölüm hücresinden alınır ve bir tören havasında (eğer, Camus’nün romanındaki Meursault ya da Deniz, Hüseyin ve Yusuf vb gibi dini telkini reddetmemişse, bir din adamının duaları eşliğinde) idam sehpasına götürülür. Bu sahne, aslında, insana eziyetin zirvesidir. <strong>Örgütlerin, cezaevlerinde veya kendi hâkimiyet alanlarında gerçekleştirdikleri iç infazları da bu kategoride ele alınmalıdır.</strong>

Amerika’da Sovyetler Birliği casusu oldukları gerekçesiyle elektrikli sandalyede idam edilen karı-koca Rosenberglerin, göstermez camların ardında oturan “ilgililerin” önünde elektrikli sandalyelere oturtularak, uzun sürede öldüren elektrik akımına tabi tutulmaları “özgürlükler ülkesi” Amerikan devletinin en acımasız cinayeti olarak tarihe geçmiştir.

Sovyetler Birliği’nde siyasi idamlar, idamların çokluğu nedeniyle bu tür ritüellere pek yer vermemiştir. Örneğin I. Moskova Yargılaması’nda idama mahkûm edilen eski Bolşevikler Kamenev ve Zinovyev ve diğerleri, 72 saatlik temyiz süresi bile beklenmeden, uzun “törenlere” de gerek görülmeden, NKVD bodrumlarındaki hücrelerinden çıkarılıp enselerine sıkılarak anında öldürülmüşlerdir. Bir de, 1930’lu yıllarda, eski Bolşeviklere çocukları aracılığıyla tehdit oluşturmak için idam yaşının 12’ye indirilmesi vardır ki, “idamlar tarihi”nde, Sovyetler Birliği adlı, artık tarihe karışmış olan devlet bu konuda halen “rekoru” elinde tutmaktadır. Bu yasayla, sadece “itiraf” yapmaları istenen sanıklar, çocuklarının idam edilmesiyle tehdit edilmekle kalınmamış, açlık nedeniyle buğday taşıyan vagonlardan “hırsızlık” yapan çocuklar da idam edilebilmiştir (Yakında yayınlanması beklenen, Aleksander Orlov’un, <em>Stalin’in Suçlarının Gizli Tarihi</em>’nden).

Siyasi ya da değil, her türlü idamla, aslında insanlık bir kere bir kere daha ölüme gönderilir.


