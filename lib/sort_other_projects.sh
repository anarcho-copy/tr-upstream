#!/bin/bash

if [[ -z "${1}" ]]; then
        echo "Usage: $(basename "${BASH_SOURCE[0]}") <file>"
        exit 1
fi

sort -k2 "${1}"
