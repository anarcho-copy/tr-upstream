#!/bin/bash
log_base="$(date +%Y-%m-%d)"
function ia-save() { curl -s -I "https://wayback.anarsistkutuphane.org/$1" | egrep '^location:' | awk '{ print $2 }'; }

for url in $(grep -hrEo --include *.muse --exclude kitaplar.muse "(http|https)://[a-zA-Z0-9./?=_%:-]*" | sort -u  | grep -vE 'archive.org|archive.is|archive.ph|\.onion' |  sed -e 's/\.$//'); do
	save_url="$(ia-save "${url}")"
	if [[ ! -n "${save_url}" ]]; then
		save_url="?"
	fi
	echo  "archive: ${url} > ${save_url}" | gawk '{ print strftime("[%Y-%m-%d %H:%M:%S]"), $0 }' | tee -a "specials/archive-${log_base}.log"
done
