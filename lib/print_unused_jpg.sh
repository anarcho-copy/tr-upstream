#!/bin/bash
# to delete unused files execute:
#	rm `./print_unused_jpg.sh`

for file in $(find . -type f -name "*.jpg"); do
	base_file="$(basename "${file}")"
	grep -inr --include *.muse "${base_file}" &>/dev/null || echo "${file}"
done
