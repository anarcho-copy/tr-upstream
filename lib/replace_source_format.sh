#!/bin/bash
# tool used: https://github.com/dalance/amber

# if source url is not active
FILES=$(grep -r --include *.muse '#source' | grep "'dan alındı\|'den alındı" | grep -v '\[\[' | grep -v "şuradan" | cut -f1 -d":")

while IFS= read -r file; do
	date=$(grep '#source' "${file}" | awk '{print $2}')
	url=$(grep '#source' "${file}" | awk '{print $4}')
	host=$(lib/parse-url "${url}" host)

	BAR="$(grep '#source' "${file}")"
	FOO="$(echo "#source ${date} tarihinde şuradan alındı: [[${url}][${host}]]")"

	yes Y | ambr  "${BAR}" "${FOO}" "${file}"
done <<< "${FILES}"


# if source url is active
FILES=$(grep -r --include *.muse '#source' | grep "'dan alındı\|'den alındı" | grep '\[\[' | grep -v "şuradan" | cut -f1 -d":")

while IFS= read -r file; do
	date=$(grep '#source' "${file}" | awk '{print $2}')
	url=$(grep '#source' "${file}" | awk '{print $4}')

	BAR="$(grep '#source' "${file}")"
	FOO="$(echo "#source ${date} tarihinde şuradan alındı: ${url}")"

	yes Y | ambr "${BAR}" "${FOO}" "${file}"
done <<< "${FILES}"
