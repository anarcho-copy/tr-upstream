#title Antropolojide Kropotkin Etkisi
#author Zeynel Çuhadar
#SORTtopics antropoloji, anarşizm, kropotkin
#date 08.04.2020
#source Meydan Gazetesi
#lang tr
#pubdate 2020-04-07T22:00:00


İnsan bilimlerinin gelişiminde evrim düşüncesinin açtığı çatlak bugün bütün argümanların tartışıldığı bir zeminde ilerlerken Kropotkin’in Karşılıklı Yardımlaşma’da ifade ettiği görüşler, bu alanda çalışma yapanlarca geri dönülen bir kaynak olmaya başladı. Gazetemizin önceki sayılarında yer verdiğimiz Michael Tomasello, Matt Ridley gibi yazarların bazı eserlerinde olduğu gibi “Sosyal Antropoloji ve İnsanın Kökeni”nde de Alan Barnard, Kropotkin’in fikirlerinden ne denli etkilendiğini gösteriyor.

Güney Afrika antropolojisi üzerine yoğunlaşan, insanın ve dilin kökenine ilişkin çalışmalar üretmiş Alan Barnard’ın pek çok farklı çalışması olsa da şimdilik “Simgesel Düşüncenin Doğuşu” ve “Tarihöncesinde Dil” isimli eserleri Türkçe’ye kazandırıldı. Söz konusu araştırmalar bağlamında disiplinlerarası bir çalışmanın önemine vurgu yapan yazar, temelde insan kökeni araştırmalarında sosyal antropolojinin ne konularda işe yarayabileceğinden bahsediyor. Bunu yaparken de geçmişte insan kökenine ilişkin söz söylemiş yazarlardan teker teker bahsediyor ve söz konusu Kropotkin vurgusu tam da burada ortaya çıkıyor.


*** İnsan Bilimlerinde Anarşist Fikirlere Dönüş Nasıl Başladı?

Darwin’in bir bütün halinde şekillendirip bilimsel bir temele oturtarak dünyaya armağan ettiği evrim düşüncesi gelişirken özellikle dini topluluklar tarafından yoğun bir şiddetle karşılaştı. İnsanların kuyruksuz maymunlardan türediği görüşünü ilk ifade edenlerden biri olan Lucilio Vanini bu görüşü 1616’da açıkça ileri sürdüğü için 1619’da idam edilmişti.

Evrim gerçekliği zaman içerisinde dinin değil belki ancak devletlerin karşı koyamayacağı bir gerçeklik haline geldi. Özellikle Darwin takipçilerinin bir kısmı tarafından ileri sürülen Sosyal Darwinist fikirler akademide “evrimciliğin” tek gerçek yorumu olarak sunuldu. Aslen Darwin’e ait olmayan, Herbert Spencer’a ait olan “güçlü olanın hayatta kalması” fikri, Darwinciliğe yedirilmeye çalışıldı. Ancak yıllar öncesinde, Darwin hayatını kaybettikten sonraki erken süreçte olduğu gibi anarşistler evrimin esas devindiricisinin mücadele değil karşılıklı yardımlaşma olduğunu vurguladı.

Takip eden yıllarda, liberal teorisyenler tarafından çokça benimsenen Sosyal Darwinist fikirlerin faşist ideoloji vb. ile bütünleşmesi neticesinde yavaş yavaş bir geriye dönüş yaşanmaya başlandı. Kökleşmiş önyargılar kırıldı ve insan bilimlerinde yüz çevrilmiş eski kaynaklara geri dönülmeye başlandı, bu geriye dönüşte en çok başvurulan isimlerse “Karşılıklı Yardımlaşma” teorisini geliştiren Peter Kropotkin ve Elisee Reclus gibi anarşistler oldu.


*** Sosyal Antropoloji ve İnsanın Kökeni

Belki de Kropotkin’in de çokça yoğunlaştığı Buşmanlara ilişkin gözlemleriyle ilişkili olarak olacak ki Alan Barnard da bu isimlere rastlayanlardan biri olmuş. Kitabında “Kamusal Mülkiyet Trajedisi” başlıklı bölümde ele aldığı fikirler, vahşilerin “mülkiyetle” kurdukları ilişkiye yönelik dikkate değer yorumlara sahip. Barnard, insan kökeni araştırmalarında mülkiyet meselesini sorgularken sıkça liberal addedilen bir yazarın üstünde duruyor ve onun sosyalist olarak da anlaşılabilecek bir okumasını yapıyor. Bizim açımızdan önemli olan kısım ise bütün bu bakış açısının tümden hatalı olduğunu, sözkonusu arkaik toplulukları ya da vahşileri anlamak istiyorsak öncelikle yerel, komünal bir bakış açısı edinmemiz gerektiğini vurguladığı kısımda başlıyor.

İlkellerin ekonomisi hakkında düşünürken belli düşünme biçimlerimizi yeniden ele almamızı sağlayacak kavramlardan bahsediyor yazar. “Potlaç”a daha komünal bir alternatif olarak görebileceğimiz “hxano” denilen bir mekanizmayı değerlendiriyor ve onu bir “ilkel komünizm” düşüncesi olarak öne sürüyor. Barnard’ın anlatımıyla hxano, mülklerin yeniden dağıtımına uyarlanan etkin bir mekanizmadır ve bu mekanizmayı barındıran toplumu eşitlikçi kılar. Güney Afrika’daki kabilelerde gözlemlediği bu geleneğe göre tüketim malı olmayan her malzeme hxano’ya teslim edilir ve herkes buradan faydalanır.

Barnard’a göre “hxano bir ilkel komünizm düşüncesiyse potlaç da bir kök kapitalizm düşüncesidir.”


*** İlkellerin Bilinçli Bir Şekilde İlkel Kalışı

“Bizzat avcı toplayıcılar dünyayı çoğunlukla şu şekilde görür: Çevre, hayatın gereklerini yeterli miktarda içerir, böylece kişinin yaşam tarzı, karşılıklı yardımlaşma ve müşterek iyi niyet ilkelerini temel alır.”

Yakın zamanda çıkan kitabında James Scott’ın da üzerinde durduğu bir mesele daha Barnard’ın gündemine aldığı konulardan önemli bir tanesini oluşturuyor. İlkellerin avcı-toplayıcı yaşamda olan “bilinçli” ısrarı. Ünlü antropolog Sahlins’e gönderme yaparak ilkellerin yaşamında, pek çok anarşist tarafından üstünde durulan “boş zaman”a vurgu yapıyor. 1966’da Avcı İnsan Konferansı’nda Sahlins’ce dile getirilen “Eğer avcı toplayıcılar bir şeyi azamiye çıkaracaksa servetlerini değil boş zamanlarını azamiye çıkarır.” alıntısını yapıyor.

Alan Barnard, ekoloji bilincinin de aslında ilkeller arasındaki dünya kavrayışında mitsel ve ilişkisel olduğunu söyleyerek, bir anlamda bu bilinçli ilkel kalışı ekolojik uyumun garantörlerinden olarak görmemize katkı sağlıyor. Tıpkı Scott’ın yaptığı gibi Neolitik Döneme geçişin, bugünkü problemlerin temelinde yatan şey olabileceğini düşünüyor.

Barnard, toplum ve akrabalık arasındaki öncelik-sonralık konusunda Kropotkin’le hem fikir olmadığını söylese de kitabın sonlarına doğru evrensel akıl, toplum bilinci ne zaman başladı sorusu üzerine düşünürken yapısalcı geleneği tam anlamıyla reddedemediğini gözlemliyoruz.

Sözün özü, güncel tartışmalar çerçevesinde Alan Barnard’ın kitabı tıpkı diğer örneklerde olduğu gibi besleyici örnekler vermesinin yanında üzerine düşünülecek yeni sorulara da kapı aralıyor. Sosyal antropolojinin disiplinlerarası insanın kökeni araştırmalarının bir parçası olması gerektiği savunusunu ikna edici bir biçimde savunurken, ana akım kavrayışların ötesine uzanan çıkarımlara gerektiği değeri veren bir çalışma olarak karşımıza çıkıyor.



