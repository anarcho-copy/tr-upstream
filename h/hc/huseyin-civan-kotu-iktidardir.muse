#title Kötü İktidardır
#author Hüseyin Civan
#SORTtopics iktidar, devlet, özgürlük, anarşist ahlak, anarşist felsefe, insan doğası, kötülük
#date 22.09.2017
#source Meydan Gazetesi
#lang tr
#pubdate 2019-12-19T11:14:08



Felsefi önermeleri net bir şekilde ortaya koyabilmek; bu budur diyebilmek hiçbir zaman kolay olmamıştır. Genellikle doğru ya da yanlış olarak kabul gören önermeler dahi, doğru değil mi, yanlış değil mi sorgulamasından kaçınamaz. Bu felsefenin bir gerekliliğidir. Ortaya konulan önermenin doğruluğunu ya da yanlışlığını etraflıca tartışır. Sadece bilimsel verilere uygun ya da uygun değil demek, felsefe için tarihin hiçbir döneminde yeterli olmamıştır ve olmayacaktır. Bu düşünce ve düşünmenin kendisiyle ilgilidir. Daha önce ele alınan bağlamlardan farklı bağlamlar zaman içerisinde belirginleşebilir. Bu da bazen aynı meseleleri, aynı kavramları, aynı önermeleri hiç bitmeyen bir şekilde felsefi düşünce ve düşünmenin konusu yapmaktadır.

Felsefe düşünce tarihi boyunca bu şekilde ele alınan kavramlardan birisi de kötülüktür.


*** Neden Kötülüğü Tekrar Sorguluyoruz?

Kötülüğün kaynağını bulmaya yönelik sorgulama, içerisinde onunla mücadele etme ve böylelikle onu ortadan kaldırma isteğini de taşır. Özellikle aydınlanma etkisiyle, metafizik nedenlendirmelerden ziyade daha maddi temelde nedenlerle açıklanmaya çalışılan kavram, bu isteğin mümkün olduğundan hareket eder. Yani kötülük, bilimsel bir şekilde açıklanabilirse, onu oluşturan etmenler yok edilebilir. Ancak bu tarz düşünme sadece bireye odaklanır. Bu anlayış kötülüğü suçla eşitler, yok etmenin yolu iyi bir ceza sistemi yaratmaktır. Bugün suç ve ceza sisteminin, kötülüğün engellenmesinde işe yaradığının düşünülmesi bir yana, tam tersi bir etkiye sahip olduğu açıktır. Öte yandan, “eylemi” suç diye niteleyenin de ceza verme yetkisine sahip olanın da varlığı tartışılmadan kötülüğü bu basitlikte ele almak açık bir indirgemeciliktir. Ve tabi ki bunu yapmak birilerinin işine gelir!

Kötülüğe yönelik farklı felsefi yaklaşımlar, hep deneyimlenmiş bir kötülükten yola çıkar. “Kötü olan eylem” i eyleyen ve bu eylemden muzdarip olan… Dolayısıyla mesele (kendi eylemi üzerine düşünebilen) birey, eylemlerini gerçekleştirmesindeki iradesi ve eylemlerinin sorumluluğu üzerinden şekillenir.

Bu tarz örneklendirmelerin farklılığı, kötülüğü ele alırken sadece kötülüğü eyleyen ve bundan etkilenen ikiliğinden çıkarmaya yardımcı olmuş, meselenin daha geniş bir perspektiften ele alınmasını sağlamıştır. Ancak yine de bu daha geniş perspektif, toplumsal olan ile eşitlenmiş, kötülük ve buna kaynaklık eden nedenler etraflıca konuşulmadan bir oldu bittiye getirilmiştir. Ya da bu (ortaya çıkan kötü durumun toplumsal ve psikolojik değerlendirmeleriyle sağlanan) daha geniş perspektif kötülüğün kurumsallaşmış hallerini soyut bir biçimde ele almaya meyletmiştir.


*** Kötülük Probleminin Siyasallaştırılması

Kötülük probleminin siyaset ile ilişkilendirilmesindeki en büyük örneklerden birisi II. Dünya Savaşı olmuş; bu süreçte yapılan katliam benzeri toplu kötülük örneklerine referanslı birçok yazı kaleme alınmıştır. II. Dünya Savaşı’ndaki “Rasyonalize edilmiş katliamlar”, kötülük meselesinin sadece dinsel kökenlerinden kurtarılmasının yetmeyeceğini açık bir şekilde göstermiştir. Bu durumu açık bir şekilde ifade eden birçok düşünür, böylelikle kötülük problemini başka toplumsal durumlarla ilişkilendirmeye çalışmıştır.

Ancak burada bir başlangıç noktası sorunu vardır. Keza insanlık, II. Dünya Savaşı öncesinde de katliamlar yaşamıştır. II. Dünya Savaşı’nda yaşanan “büyük kötülükler” rasyonel bir şekilde nedenlendirilerek felsefi düşünmenin bir konusu yapılıyorken; bundan önceki “büyük kötülükleri” metafizik motivasyonlarla nedenlendirip, başka boyutlarla ele almamak bir çelişkidir. Dinsel ya da rasyonel nedenlendirmeler, açığa çıkan bu toplu kötülük durumlarını açıklamada güdük kalıyorsa; bunlar arasında bağ kurabilmek, durumu açıklayabilmek için başka bir dayanak noktasına ihtiyaç vardır.


*** Kropotkin ve Anarşist Ahlak

“Büyük kötülükler”in henüz toplumsal ve psikolojik ilişkilendirmelerle ele alınmadığı; ortada ne eleştirel teoricilerin ne postyapısalcıların olduğu; kötülüğe ilişkin örneklendirmelerin ne Arendt’in Nazi Subayı Eichmann örneğine ne de Foucault’nun Riviere örneğine referans verilerek yapıldığı bir dönemde; 19. yüzyılın sonlarında Pyotr Kropotkin Anarşist Ahlak’ı kaleme alıyordu. Ve sanıldığının aksine metafizik nedenlendirmelerle ya da dogmatik ilkelerle ahlaka konu olan kavramları değerlendirmiyordu.

Daha önce bahsettiğimiz gibi kötülüğe ilişkin yazılanlar deneyimlenmiş kötülük örneklerinden yararlanırlar. Anarşist Ahlak’ta bu örnek, dönemin en ünlü seri katillerinden Karındeşen Jack’tir.

İşlenen cinayetler üç ayrı çerçevede ele alınır. İlki Jack ve katlettiği kişi arasındadır. İlk değerlendirme doğrudan iki birey arasındaki eyleme odaklanır. Bir birey diğerinin yaşamına son vermiştir. Eylemde bulunanın davranışı diğer bireyin varlığını ortadan kaldırmaya yöneliktir.

İkincisi, Jack’i cinayetlere yönelten nedenlerdir. Bunda içinde yetiştiği kültürden okuduğu kitaplara, düşüncesini şekillendiren “dış etmenler” vardır. Bu noktada birey ve içinde bulunduğu nesnellik ve deneyimleri söz konusudur. Şüphesiz bu çerçeve önemlidir. Çünkü bireyin içerisinde yaşadığı sistem söz konusudur.

Ama tam da bu aşamada bir üçüncü çerçeve belirir. Bu üçüncü çerçeve yine “dış etmenler” ile ilişkilidir. Üçüncü çerçevede Kropotkin, “Jack ve bütün Jacklerin katlettiğinin on katından da fazla sayıda kadın, erkek ve çocuğu soğukkanlılıkla katletmiş bir yargıç” sorunsalını ortaya koyar.

Bireyin düşüncesini şekillendirebilen, hareketlerini belirleyebilen “dış etmenler”, Kropotkin’in Anarşist Ahlak’ında nesnel bir veri ya da kaçınılmaz bir gerçeklik değildir. Öte yandan birbirinden bağımsız da değildir. Sistematik bir şekilde işleyen kötülük söz konusudur üçüncü aşamada. Bu ilişkisellik “dış etmenler”in politik bir çözümlemesiyle anlaşılabilir.


*** Kötü Olan İktidardır, İktidarlıdır

Daha önce vurguladığımız gibi, bu alt başlıkta olduğu gibi iddialarda bulunmak o kadar kolay değildir. Anarşizm nasıl mevcut düzene ilişkin bir eleştiri, özgür bir toplumu gerçekleştirmek için bir ideoloji ve hareket, bu topluma ulaşmak için bir yöntem ve örgütlenme biçimiyse aynı zamanda felsefi bir bakış açısıdır. Ve bu bakış açısının temelinde iktidar ve iktidarlı olanı incelemek önemli yer tutar.

Kötülüğü sadece birey, iradesi ve sorumluluğu üzerinden ele almak bir alanı görünmez kılacaktır. O da bireyin içerisinde bulunduğu nesnel gerçekliktir. Ancak burada gözden kaçan, nesnel gerçekliğin içindeki iktidarın ya da kötülüğün kurumsallaşmış halleridir. Önemli olan, nesnel veri gibi kabul edilen kurumsallaşmış iktidara/kötülüğe odaklanmaktır. Kropotkin’in yapmaya çalıştığı, kötülük meselesinde, bireyin eylemlerini gerçekleştirdiği, eylemlerini gerçekleştirirken etkilendiği, eylemleriyle etkilediği, onun dışındaki dünyada doğal gibi anlaşılan, ancak hiç de öyle olmayan iktidar ve iktidar ilişkilerine odaklanmaktır.


*** Hobbesçu Varsayım

Ahlak felsefesinin konusu gibi görünen kötülük meselesi, aslında siyaset felsefesi açısından da önemlidir. Bir doğal durumda kaçınılmaz olarak ortaya çıkan Hobbesçu evrensel savaş düşüncesi, insanın özünde kötülük olduğu varsayımına dayanır. Seçimlerinde özgür olan bireyler, kötülüğe meyilli olduklarından ve böyle bir durumda birbirlerine kötülük yapacaklarından dolayı bu özgürlüğü kısıtlayacak bir kuruma ihtiyaçları vardır. Bu devlettir. “Kötü insan” oldukça devlete ihtiyaç vardır. Devlet toplumsal çatışma temelinde kurulduğu için, ontolojik olarak kötülüğe ihtiyaç duyar. Hem de bu kötülüğün ebedi olması lazımdır ki, devlet de varlığını sürdürebilsin. İnsan doğasına kötülük yüklenmesinin sebebi işte budur.

Sözde özünde kötü olanı kontrol altında alan bu kurumsallaşma, yine sözde meşruluğunu bireylerin “varlılığını” devam ettirebilme kaygısıyla kazanır. Ancak ne pahasına? Özgürlük! İktidar ve kurumsallaşmış bir iktidar biçimi olarak devlet “özgürlük” yiyen canavarlardır.

Anarşist felsefe, iktidarı, ister bireyler arasındaki ilişkilerde ortaya çıkmış olsun, isterse de kurumsallaşmış biçimleri olan devlet ve kapitalizm olsun; özgürlüğün tezatı olarak ele alır. İktidar ve iktidarın biçimlerinden kurtulmadan özgürlüğün gerçekleşebileceğini düşünmez.

Burada özgürlük meselesine ilişkin bir ayrımı ortaya koymak, hem “kötüyü eyleme özgürlüğü” gibi bir çelişkiyi fark etmeye olanak sağlayacaktır hem de kötüyü ısrarlıca insan doğasına yıkmaya çalışan bir iktidar fikrinin de altını boşaltacaktır. Bu ayrım özgürlüğün toplumsallığı fikridir.


*** Özgürlüğün Toplumsallığı

Bakunin’in “insanın sadece özgür insanlar arasında özgür olacağı” önermesi tam da bu duruma odaklanır. Bir kişinin bile özgür olmadığı bir toplumda iktidar ilişkileri devam ediyordur ve bu yüzden, doğrudan ya da dolaylı bu iktidar ilişkisinin sürdürülmesi sağlanıyordur. İşte, özgürlüğün tam da bu kolektif niteliği, onu önümüze “bireyin her istediğini yapma serbestliği” olarak değil de “kolektif bir sorumluluk” olarak çıkarır. Dolayısıyla, Hobbesçu doğal durumda birbirini öldürmeye odaklanan insanlar fikri, özgür olmayan insanları zorunlu kılar.

Bunun nedeni açıktır; özgürlüğün olduğu bir durumda iktidarın varlığı anlamını yitirir. Özgürlük gerçekleşmeden kötü ve kurumsallaşmış kötülükten kurtulunamaz.

Kötülük probleminde, Kropotkin’in üzerinde durduğu işte tam bu meseledir. İktidar ve iktidar ilişkilerinin var olduğu toplumda, dolayısıyla özgürlüğün olmadığı bir toplumda kötülük zorunludur. Ancak burada Kropotkin’in bireyi ve bireyin toplumsal olan ile ilişkisini es geçmeden üstünde durduğu mesele, iktidar kurumları meselesidir. Yöneticiler ve halk, mülk sahipleri ve mülksüzler gibi zorunlu olarak dayatılan ikiliğe dayalı siyasi ve ekonomik sistemler yok edilmediği takdirde “kötülüğün sürdürüleceği”ni savunur. Bu, tabi ki bir öncelik sonralık ilişkisi değildir. Bireyler arasındaki iktidarlı ilişkilerin değişmesi için mücadeleyi, kurumsallaşmış kötülüklerden kurtulma mücadelesiyle eş zamanlı kılar.

Kötü, kötülük gibi kavramlar tartışılırken; varlığının sebebini bu kötülükten alan kurumsallaşmaların es geçilmemesi; bu kavramların özgürlükle ilişkisinin açıkça ortaya konması; ve meselenin sadece psikolojik, toplumsal açılardan değil de siyasal bir perspektifle ele alınması, kötülüğün kaynağını sorgulamakta bize iyi bir çerçeve sağlayacaktır. Tabi anarşist bir bakış açısıyla bu üçünün ilişkisi kurulduğu sürece…



