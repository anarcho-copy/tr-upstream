#title Ermeni Soykırımı (1915–16): Genel Bakış
#author Holokost Ansiklopedisi
#SORTtopics ermeni, katliam, ermeni soykırımı
#source 04.07.2020 tarihinde şuradan alındı: [[https://encyclopedia.ushmm.org/content/tr/article/the-armenian-genocide-1915-16-overview][encyclopedia.ushmm.org]]
#lang tr
#pubdate 2020-07-04T18:45:54






[[h-a-holokost-ansiklopedisi-ermeni-soykirimi-1915-1-1.jpg 40 r][Osmanlı birlikleri sürgüne gönderilmekte olan Ermenileri koruyor. Osmanlı İmparatorluğu, 1915–16.
National Archives and Records Administration, College Park, MD]]



Bazen yirmici yüzyılın ilk soykırımı olarak da adlandırılan Ermeni soykırımı, Osmanlı İmparatorluğu’nda yaşayan Hristiyan Ermenilerin 1915 ilkbaharından 1916 sonbaharına kadarki dönemde fiziksel olarak yok edilmesini ifade eder. 1915 yılında çok etnikli Osmanlı İmparatorluğu’nda yaşayan yaklaşık 1,5 milyon Ermeni bulunmaktaydı. Soykırım sırasında gerek katliamlar ve münferit ölümler, gerekse sistematik kötü muamele, maruziyet ve açlık sonucu en az 664.000 ve muhtemelen de 1,2 milyon kişi hayatını kaybetmiştir.

<em>Soykırım</em> teriminin kökeni ve uluslararası hukukta kanunlaştırılması, 1915-16 yıllarında Ermenilerin toplu olarak öldürülmesine dayanmaktadır. Bu sözcüğü ilk kullanan ve daha sonra Birleşmiş Milletler’de savunucusu olan Avukat Raphael Lemkin, Osmanlıların Ermenilere karşı işlediği suçlar hakkında ilk çıkan gazete yazılarının grupların yasal korunma ihtiyacı (1948 tarihli BM Soykırım Sözleşmesi’nin ana unsuru) ile ilgili düşüncelerinin anahtarı olduğunu defalarca ifade etmiştir.

Osmanlı makamları, destek birliklerinin ve zaman zaman da sivillerin desteğiyle zulmün ve toplu cinayetlerin büyük kısmını işlemiştir. İttihat ve Terakki Cemiyeti’nin (İTC—Jön Türkler olarak da adlandırılır) kontrolündeki Osmanlı hükümeti, Orta ve Doğu Anadolu bölgelerinde oldukça büyük olan Ermenistan varlığını ortadan kaldırarak bu bölgedeki Müslüman Türk hâkimiyetini pekiştirmeyi hedeflemiştir.



[[h-a-holokost-ansiklopedisi-ermeni-soykirimi-1915-1-2.jpg 40 l][Bir grup Ermeni mülteci. 1915–20. National Archives and Records Administration, College Park, MD]]


Kitlesel mezalim ve soykırım suçları çoğu zaman savaş bağlamında işlenir. Ermenilerin yok edilmesi I. Dünya Savaşı’nın olaylarıyla yakından ilişkilidir. İşgalci düşman birliklerinin Ermenileri kendilerine katılmaya teşvik edeceği korkusuyla 1915 ilkbaharında Osmanlı hükümeti, Kuzeydoğu sınır bölgelerindeki Ermeni nüfusunu sürmeye başlamıştır. İzleyen aylarda Osmanlılar, kapsamı genişleterek—muharebe sahalarından uzaklığına bakılmaksızın—neredeyse tüm bölgelerden sürgünlere başlamıştır.

Ermeni soykırımının kurbanları, 1915 ilkbaharında başlayan yerel katliamlarda öldürülen insanları, tehcir sırasında açlık, susuzluk, maruziyet ve hastalık koşullarında ölenleri ve imparatorluğun güneyindeki çöl bölgelerinde (bugünkü Kuzey ve Doğu Suriye, Kuzey Suudi Arabistan ve Irak) ya da buraya giderken yolda ölenleri kapsamaktadır. Ayrıca on binlerce Ermeni çocuk zorla ailelerinden koparılmış ve İslam dinine geçirilmiştir.

ABD’nin İstanbul (Bizans) büyükelçisi Henry Morgenthau Sr., Ermenilere karşı işlenen mezalimden derin rahatsızlık duymuş ve tepki olarak dünyanın bilincini uyandırmanın yollarını arayan kişiler arasında yer almıştır. Ermenilerin içinde bulunduğu zor durum, Birleşik Devletler’de Başkan Woodrow Wilson, Hollywood ünlüleri ve halk tabanı düzeyinde hem yurtiçinde hem de yurtdışında gönüllü katılım sağlayıp Ermeni sığınmacıları ve yetimleri desteklemek üzere 110 milyon dolar (enflasyona göre ayarlama yapıldığında 1 milyar dolardan fazla) toplayan binlerce Amerikan vatandaşının rol oynadığı, benzeri görülmemiş bir umumi hayırseverlik tepkisini tetiklemiştir.



[[h-a-holokost-ansiklopedisi-ermeni-soykirimi-1915-1-3.jpg 40 r][Bir mülteci kampında çergilerin yanında duran Ermeni aileler. Osmanlı İmparatorluğu, 1915–16. Fotoğraf Armin T. Wegner tarafından çekilmiştir. Wegner, Alman Sıhhiye Kıtasında sağlık memuru olarak görev yapmıştır. 1915 ve 1916 yıllarında Wegner, Osmanlı İmparatorluğu’nu baştan başa gezmiş ve Ermeniler’e karşı yürütülen mezalimi belgelemiştir. (Armin T. Wegner’in kızı Sybil Stevens’ın izniyle. Wegner Collection, Deutsches Literaturarchiv, Marbach & United States Holocaust Memorial Museum.)<br><br> Armenian National Institute, Inc.]]



Ermeni soykırımı, Holokost (Yahudi Soykırımı) dönemine uzun bir gölge düşürmektedir. Büyükelçi Morgenthau’nun oğlu Henry Morgenthau Jr., Franklin D. Roosevelt’in idaresinde hazine bakanıydı. Morgenthau Jr., kısmen Ermeni soykırımı ile ilgili hatıraları nedeniyle, 200.000 gibi büyük bir sayıda Yahudiyi Nazi Avrupası’ndan kurtaran Savaş Mültecileri Kurulu’nun kurulmasında kilit bir müdafi olmuştur. Belki de en akıldan çıkmayan detay olarak Ermenilerin meşru müdafaası hakkında bir roman (Franz Werfel’in <em>Musa Dağında Kırk Gün</em> romanı), Yahudi Soykırımı sırasında azınlık mahallelerinde (getto) hapis olup içinde bulundukları zor durum ile ilham verici bir benzerlik ve bir direniş çağrısı olduğunu düşünen Yahudiler arasında gizlice elden ele dolaştırılmıştır.

<quote>

<strong>Okunabilecek Diğer Kaynaklar</strong>

Adalian, Rouben, ed. <em>The Armenian Genocide in U.S. Archives, 1915–1918</em>. Alexandria: Chadwick-Healey, 1991.

Akçam, Taner. <em>The Young Turks’ Crime against Humanity: The Armenian Genocide and Ethnic Cleansing in the Ottoman Empire</em>. Princeton: Princeton University Press, 2012.

Bloxham, Donald. <em>The Great Game of Genocide: Imperialism, Nationalism and the Destruction of the Ottoman Armenians</em>. Oxford: Oxford University Press, 2005.

Dündar, Fuat. <em>Crime of Numbers: The Role of Statistics in the Armenian Question</em>. New Brunswick, N. J.: Transaction, 2010.

Kaiser, Hilmar. “Genocide at the Twilight of the Ottoman Empire,” in <em>The Oxford Handbook of Genocid</em>e <em>Studies</em>, edited by Donald Bloxham and A. Dirk Moses. Oxford: Oxford University Press, 2010.

Kévorkian, Raymond. <em>The Armenian Genocide: A Complete History.</em> New York: I.B. Tauris, 2011.

Miller, Donald E. and Lorna Touryan Miller. <em>Survivors: An Oral History of the Armenian Genocide.</em> Berkeley-Los Angeles: University of California Press, 1993.

Morgenthau, Henry. <em>Ambassador Morgenthau’s Story.</em> Princeton: Gomidas Institute, 2.000.

Suny, Ronald Grigor et al., eds., <em>A Question of Genocide: Armenians and Turks at the End of the Ottoman Empire</em>. New York: Oxford University Press, 2011.

Üngör, Ugur Ümit. <em>The Making of Modern Turkey: Nation and State in Eastern Anatolia, 1913–1950</em>. New York: Oxford University Press, 2011.


</quote>


