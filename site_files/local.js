/* Empty by default, for local changes */

/* REPLACEMENTS */
$('a').each(function() {
    var link = $(this).attr('href');
    if (link) {
        /* replace '%40' to '@' */
        $(this).attr('href', link.replace('%40', '@'));
        /* replace '/category/topic/giris' to '/category/topic/giris?sort=pubdate_desc&rows=500'  */
        $(this).attr('href', link.replace('/category/topic/giris', '/category/topic/giris?sort=pubdate_desc&rows=500'));
    }
});
