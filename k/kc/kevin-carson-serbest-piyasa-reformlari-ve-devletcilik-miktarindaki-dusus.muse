#DELETED Reason for deletion: Machine translation (DeepL: after June 2022, Google Translate: before June 2022) has been detected. - https://mirror.anarsistkutuphane.org/c4ss.org/analyze/
#title Serbest Piyasa Reformları ve Devletçilik Miktarındaki Düşüş
#author Kevin Carson
#date 01.09.2008
#source 23.10.2022 tarihinde şuradan alındı: [[https://c4ss.org/content/57476][c4ss.org]]
#lang tr
#pubdate 2022-10-23T19:47:54
#topics sol piyasa anarşizmi, liberter, devlet
#notes Çeviri: Efsa <br>İngilizce Aslı: [[https://c4ss.org/content/36175][Free Market Reforms and the Reduction of Statism]]

Objektivist akademisyen Chris Sciabarra, <em>Total Freedom adlı</em> muhteşem kitabında “diyalektik liberteryanizm” çağrısında bulunmuştur. Sciabarra diyalektik analiz ile “bir parçanın doğasını onu sistematik olarak, yani içine gömülü olduğu sistemin bir uzantısı olarak görerek kavramayı” kastetmektedir. Tek tek parçalar karakterlerini parçası oldukları bütünden ve bu bütün içindeki işlevlerinden alırlar.

Bu, herhangi bir devlet müdahalesini, sistemin bütününde oynadığı rolü göz önünde bulundurmaksızın, tek başına ele almanın hata olduğu anlamına gelir. (Bkz. Sciabarra’nın “Dialectics and Liberty, <em>The Freeman,</em> Eylül 2005.)

Bir başka liberteryen, blog yazarı Arthur Silber, diyalektik liberteryenizmi “atomistik liberteryenizm” olarak adlandırdığı yaklaşımla karşılaştırmaktadır; bu yaklaşım “ilgili temel ilkelere odaklanır, ancak ilkelerin analiz edildiği genel bağlama çok az dikkat edilir (ya da hiç dikkat edilmez). Bu şekilde, bu yaklaşım ilkeleri Platon’un Formları gibi ele alır. . . .” Atomistik liberteryenler “sanki içinde yaşanılan toplum herhangi bir sorunun analiziyle tamamen alakasızmış gibi” tartışırlar.

Belirli bir devlet müdahalesi biçiminin devlet iktidarının yapısında hangi işlevi gördüğünü belirlemek için öncelikle devletin tarihsel amacının ne olduğunu sormalıyız. İşte özgürlükçü sınıf analizi burada devreye girer.

Özgürlükçü sınıf teorisi üzerine bildiğim en büyük çalışma Roderick Long’un “Toward a Libertarian Theory of Class” (<em>Sosyal Felsefe ve Politika,</em> Yaz 1998) başlıklı makalesidir. Long, yönetici sınıf teorilerini, yönetici sınıfın bileşenleri olarak devlet aygıtına ve plütokrasiye (devlet müdahalesinin zengin “özel sektör” yararlanıcıları) yaptıkları vurguya göre “devletçi” ya da “plütokratik” olarak kategorize eder.

Ana akım liberteryenizmin varsayılan eğilimi yüksek derecede devletçiliktir; bu eğilim sadece plütokrasi tarafından “yasal yağma “nın (Frédéric Bastiat’nın terimi) <em>mümkün kılınmasında devlet</em> zorunun gerekli rolünü vurgulamakla kalmaz, aynı zamanda plütokrasinin devletçiliğin <em>yararlanıcıları</em> olarak bile önemini küçümser. Bu, devletle ilişkili sınıf çıkarlarını geçici ve tesadüfi olarak ele almak anlamına gelir. Her ne kadar devletçi teori devleti (Franz Oppenheimer’ın ifadesiyle) zenginliğin örgütlü siyasi aracı olarak ele alsa da, yine de hükümeti sadece herhangi bir zamanda onu kontrol eden siyasi grupların sömürücü çıkarlarına hizmet eden bir kurum olarak görme eğilimindedir. Devletin nasıl işlediğine dair bu resim, herhangi bir zamanda onu kontrol eden çeşitli çıkar grupları arasında veya onlarla devlet arasında herhangi bir organik ilişki gerektirmez. Lisanslı profesyoneller, rant peşinde koşan şirketler, çiftçiler, düzenlenmiş kamu hizmetleri ve büyük emek dahil olmak üzere farklı çıkar grupları tarafından kontrol edilebilir; ortak olan tek şey, şu anda devlete en iyi yapışan grup olmalarıdır.

Murray Rothbard’ın pozisyonu çok daha farklıydı. Long’a göre Rothbard, devleti “yapısal hegemonya konumuna ulaşmış birincil bir grup, çağdaş politik ekonomide sınıf konsolidasyonu ve krizinin merkezinde yer alan bir grup tarafından kontrol ediliyor olarak görüyordu. Rothbard’ın bu soruna yaklaşımı, aslında, sınıfın tarihsel, siyasi, ekonomik ve sosyal dinamiklerini kavrayışında son derece diyalektiktir.”

Geçmişte şirket ekonomisinin devletin gücüyle o kadar yakından bağlantılı olduğunu savunmuştum ki, tıpkı Eski Rejim döneminde toprak ağalarının devletin bir <em>parçası olması gibi,</em> şirket yönetici sınıfını da devletin bir parçası olarak düşünmek daha mantıklı. Blog yazarı Brad Spangler bu ilişkiyi açıklamak için silahşör ve çantacı benzetmesini kullanmıştır:

İki tür soygun senaryosu varsayalım.

<quote>

Bir tanesinde, yalnız bir soyguncu size silah doğrultuyor ve paranızı alıyor. Tüm özgürlükçüler bunu, Devlet Sosyalizmine en çok benzeyen, iş başındaki her türlü hükümetin mikro bir örneği olarak kabul edecektir.

Devlet Kapitalizmini tasvir eden ikincisinde, soygunculardan biri (hükümetin gerçek aygıtı) sizi tabancayla korurken, ikincisi (Devletin müttefik şirketlerini temsil eden) sadece kol saatinizi, cüzdanınızı ve araba anahtarlarınızı bırakmanız gereken çantayı tutar. Çantacı ile etkileşiminizin “gönüllü bir işlem” olduğunu söylemek saçmalıktır. Böyle bir saçmalık tüm özgürlükçüler tarafından kınanmalıdır. Hem silahlı adam hem de çantacı birlikte gerçek Devlettir.

</quote>

Bu perspektif göz önüne alındığında, vergilerin ve düzenlemelerin devlet kapitalizminin genel yapısında oynadığı rol dikkate alınmaksızın, vergilerin azaltılması ya da serbest bırakılmasına yönelik belirli önerileri değerlendirmek pek mantıklı değildir. Özellikle de “serbest piyasa reformu” için ana akım önerilerin çoğunun şirket devletinden fayda sağlayan sınıfsal çıkarlar tarafından üretildiği düşünüldüğünde bu doğrudur.

Hiçbir politik-ekonomik sistem “yasak olmayan her şey zorunludur” anlamında tam devletçiliğe yaklaşmamıştır. Her sistemde zorunlu ve isteğe bağlı davranışların bir karışımı vardır. Yönetici sınıf, genel yapısı zorlayıcı devlet müdahalesi ile tanımlanan bir sistemin aralıklarında bir miktar gönüllü piyasa değişimine izin verir. Neyin zorunlu düzenlemeye tabi tutulacağı kadar hangi alanların gönüllü mübadeleye bırakılacağının seçimi de yönetici sınıfın genel stratejik resmini yansıtır. Devletçilik ve piyasa faaliyetlerinin toplam karışımı, yönetici sınıfın tahminine göre, siyasi araçlarla net sömürüyü en üst düzeye çıkarma olasılığı en yüksek olan şekilde seçilecektir.

*** Birincil ve İkincil Müdahaleler

Devlet müdahalesinin bazı biçimleri birincil niteliktedir. Bunlar, siyasi sistem aracılığıyla ekonomik sömürünün ayrıcalıklarını, sübvansiyonlarını ve diğer yapısal temellerini içerir. Bu, devletin birincil amacı olmuştur: belirli bir insan sınıfı tarafından ve onlar için uygulanan, zenginliğin örgütlü siyasi araçları. Ancak bazı müdahale biçimleri ikincildir. Bunların amacı dengeleyici ya da iyileştiricidir. Bunlar arasında refah devleti önlemleri, Keynesyen talep yönetimi ve benzerleri yer alır; bunların amacı ayrıcalığın en istikrarsızlaştırıcı yan etkilerini sınırlamak ve sistemin uzun vadede hayatta kalmasını sağlamaktır.

Ne yazık ki, şirket çıkarlarından kaynaklanan tipik “serbest piyasa reformu”, ayrıcalık ve sömürünün temel yapısını olduğu gibi bırakırken, müdahalenin yalnızca iyileştirici veya düzenleyici biçimlerini ortadan kaldırmayı içerir.

İlkeli özgürlükçülerin stratejik öncelikleri bunun tam tersi olmalıdır: öncelikle birincil etkisi sömürüyü mümkün kılmak olan devlet müdahalesinin temel, yapısal biçimlerini ortadan kaldırmak ve ancak bundan sonra devlet destekli sömürü sistemi altında yaşayan ortalama bir insan için hayatı katlanılabilir kılmaya hizmet eden ikincil, iyileştirici müdahale biçimlerini ortadan kaldırmak. Blog yazarı Jim Henley’in dediği gibi, koltuk değneklerinden önce prangaları çıkarın.

Sistemin genel işleyişi üzerindeki etkilerine bakmaksızın tipik “serbest piyasa” önerilerini “doğru yönde atılmış adımlar” olarak karşılamak, Romalıların Cannae’deki Pön merkezinin geri çekilmesini “doğru yönde atılmış bir adım” olarak karşılamasına benzer. Hannibal’ın savaş düzeni, Kartacalıların İtalya’dan genel bir çekilişinin ilk adımı değildi ve önerilen parça parça “özelleştirmelerin”, “deregülasyonların” ve “vergi indirimlerinin” siyasi yollarla elde edilen servet miktarını azaltmaya yönelik olmadığından emin olabilirsiniz.

*** Düzenlemeler ve Artan Devletçilik

Dahası, ayrıcalıkların kullanımını sınırlayan ve kısıtlayan düzenlemeler, doğru bir şekilde konuşmak gerekirse, devletçilikte net bir artış içermez. Bunlar sadece şirket devletinin kendi daha temel müdahale biçimleri üzerindeki dengeleyici kısıtlamalarıdır.

Silber, bu tür kısıtlamaların diyalektik doğasını, eczacıların vicdanlarını rahatsız eden ürünleri (“ertesi gün” hapları gibi) satmayı reddedip edemeyecekleri sorusuna atıfta bulunarak örneklendirmiştir. Atomistik-özgürlükçü yanıt şudur: “Elbette. Satma ya da satmama hakkı temel bir serbest piyasa özgürlüğüdür.” Silber’in de belirttiği gibi buradaki örtük varsayım, “bu anlaşmazlığın esasen özgür olan bir toplumda ortaya çıktığıdır.” Ancak eczacılar aslında, temel amacı rekabeti kısıtlamak ve hizmetleri için tekel fiyatı talep etmelerini sağlamak olan devletçi bir raket olan zorunlu mesleki ruhsatlandırmanın doğrudan yararlanıcılarıdır. Silber şöyle yazmış:

<quote>

Ana nokta çok basittir: eczacılık mesleği <em>devlet tarafından dayatılan bir tekeldir.</em> Başka bir deyişle: tüketici ve eczacı oyun alanında eşit rakipler değildir. Devlet başparmağını terazinin sadece bir tarafına sıkıca yerleştirmiştir. Bu, tüm diğer analizlerin kendisinden kaynaklanması gereken en önemli noktadır. . ..

. . . Devlet, lisanslı eczacılar için hükümet zoruyla bir tekel oluşturmuştur. Bu temel gerçek göz önüne alındığında, devletin yapabileceği en az şey, herkesin ihtiyaç duyduğu ilaçlara erişimini sağlamaktır- ve belirli bir ilacın ölüm kalım meselesi olup olmadığına eczacı değil, kesinlikle hükümet değil, bunu isteyen birey karar vermelidir.

</quote>

Devlet bir mesleğe, bir ticari firmaya veya bir sektöre özel bir ayrıcalık tanıdığında ve ardından bu ayrıcalığın kullanımına düzenleyici sınırlar koyduğunda, bu düzenleme devletçiliğin serbest piyasaya yeni bir müdahalesi değildir. Daha ziyade, devletin kendi temel devletçiliğinin sınırlandırılması ve nitelendirilmesidir. İkincil düzenleme devletçilikte net bir artış değil, net bir <em>azalmadır.</em>

Öte yandan, birincil ayrıcalık yürürlükten kaldırılmadan ikincil düzenlemenin yürürlükten kaldırılması, devletçilikte net bir <em>artış anlamına</em> gelecektir. Ayrıcalıktan yararlananlar devletin fiili bir kolu olduğundan, ayrıcalıklarını kötüye kullanmalarına yönelik düzenleyici kısıtlamaların ortadan kaldırılması, devletin kendi yetkilerini kullanmasına yönelik anayasal bir kısıtlamanın yürürlükten kaldırılmasıyla aynı pratik etkiye sahiptir.

Spangler’ın çantacı benzetmesini genişletecek olursak, sözde devletçiliğin büyük bir kısmı, kurbanın silah zoruyla cüzdanını teslim etmesinin ardından, silahlı adamın çantacıya kurbana taksi ücreti için yeterli parayı geri vermesini, böylece güvenli bir şekilde evine dönebileceğini ve soyulmak için para kazanmaya devam edebileceğini söylemesinden ibarettir.

Devlet “yasal yağmacılar” tarafından kontrol edildiğinde ve belirli bir durumda devlet müdahalesi lehine ya da aleyhine verilen her karar, onların ideal müdahale ve müdahale etmeme karışımına ilişkin stratejik değerlendirmelerini yansıttığında, gerçek bir devlet karşıtı hareketin “serbest piyasa reformu” önceliklerinin yağmacıların artık hangi müdahale biçimlerinin amaçlarına hizmet etmediğine ilişkin tahminlerine göre belirlenmesine izin vermesi bir hatadır. Eğer hükümetteki şirket temsilcileri belirli bir “serbest piyasa reformu” öneriyorlarsa, bunun nedeninin servetin net siyasi çıkarımını <em>artıracağına</em> inandıkları için olduğuna bahse girebilirsiniz.

Şirket egemen sınıfının “serbest piyasa reformu” yaklaşımı, “limon sosyalizmi “nin bir tür ayna görüntüsüdür. Limon sosyalizmi altında, siyasi kapitalistler (devlet aracılığıyla hareket ederek) şirket sermayesinin elinden alınmasından en çok fayda sağlayacağı endüstrileri kamulaştırmayı ve sermayenin maliyetini devletin üstlenmesini en çok tercih edeceği işlevleri toplumsallaştırmayı seçerler. Sistemin işleyişi için gerekli olduğu düşünülen ancak “özel sektör” himayesinde yürütülmesinin zahmetini haklı çıkaracak kadar karlı olmayan işlevleri özel sektörden devlet sektörüne kaydırırlar. Öte yandan, “limon piyasası reformu” altında, siyasi kapitalistler, devlet eyleminden tüm faydayı elde ettikten sonra müdahaleci politikaları tasfiye ederler.

İyi bir örnek: İngiliz sanayiciler, merkantilizm amacına ulaştıktan sonra, on dokuzuncu yüzyılın ortalarında “serbest ticareti” benimsemenin güvenli olduğunu düşündüler. Dünyanın yarısı İngiliz silah gücüyle tek bir pazar haline getirilmişti ve İngiliz ticaret filosu tarafından bir arada tutuluyordu. Britanya sömürge dünyasındaki rakip sanayiyi ortadan kaldırmıştı. Yerli halklardan muazzam miktarlarda toprak çalarak ve bunları imparatorluk pazarı için nakit ürünlere dönüştürerek Çitleme’yi küresel ölçekte yeniden canlandırmıştı. İngiliz sermayesinin hâkim konumu, geçmişteki merkantilizmin doğrudan sonucuydu; bu hâkim konumu tesis ettikten sonra, “serbest ticareti” göze alabilirdi.

Günümüz Amerika Birleşik Devletleri’ndeki sözde “serbest ticaret” hareketi de aynı modeli izlemektedir. Bir asır önce, yüksek gümrük vergileri Amerikan siyasi kapitalistlerinin çıkarlarına hizmet ediyordu. Bugün, Amerika’daki hâkim şirket çıkarları ulusötesi olduğunda, tarifeler artık onlar için yararlı değildir. Aslında tek bir küresel şirketin ulusal alt bölümleri arasında malların ve kısmen bitmiş ürünlerin transferini engellemektedirler.

Öte yandan, bugün “fikri mülkiyet” denilen şey, ulusötesi şirketler için, gümrük tarifelerinin bir asır önce eski ulusal şirketler için gördüğü korumacı işlevin aynısını görmektedir. Dolayısıyla siyasi kapitalistler, “fikri mülkiyet” yasasının yeni korumacılığını büyük ölçüde güçlendirirken, modası geçmiş tarife engellerini ortadan kaldırmayı içeren bir “serbest ticaret” versiyonunu teşvik etmektedir.

Unutmamalıyız ki devletçiliğin ölçüsü, ayrı parçalarının resmi devletçiliğinde değil, sistemin bütününün işleyişinde yatmaktadır. Devletçilerin stratejik önceliklerine uygun olarak seçilen bazı ayrı parçaların resmi devletçiliğindeki bir azalma, aslında genel devletçilik seviyesinde net bir <em>artışa</em> neden olabilir. Liberteryenler olarak devleti ortadan kaldırmaya yönelik stratejik gündemimiz, sistemin genel doğasına ilişkin anlayışımızı yansıtmalıdır.


