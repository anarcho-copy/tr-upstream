#cover k-g-katerina-gogou-1.jpg
#title Katerina Gogou: Atina'nın Anarşist Şairi, 1940-1993
#author libcom.org
#SORTtopics yunanistan, anarşist, biyografi, şair, şiir
#date 09.04.2010
#source 26.02.2021 tarihinde şuradan alındı: [[https://indir.anarcho-copy.org/pdf/gogou-katerina-atinanin-anarsist-sairi-1940-1993.pdf][indir.anarcho-copy.org]]
#lang tr
#pubdate 2021-02-26T18:46:24
#notes Çeviri: Anonim <br> İngilizce Aslı: [[https://libcom.org/history/katerina-gogou-athens-anarchist-poetess-1940-1993][Gogou, Katerina: Athens' anarchist poetess, 1940-1993]]



Çocukluğunun ilk yılları Nazi işgalinin doğurduğu kıtlık koşulları içinde geçen Katerina Gogou, 1 Haziran 1940'da doğdu. Çocukluk yıllarına dair anılarını ve aynı yıllarda verilen direnişi ve sivil savaşı, öldükten sonra yayınlanan, ölümüyle yarım kalmış otobiyografisi 'Benim Adım Odyssey' de konu edinmiştir:

<quote>

“Çetelerin savaşı. [1]
<br>
A aaa! Çetelerin savaşı bu,
<br>
Kendilerine cumhuriyetçi diyen, kocaman şapkalı yunanlılar vardı.
<br>
Uzun ceket ve montlanyla örümcek kafalı ve kocamanlardı,
<br>
Ceplerinde silah taşırlardı, ve daha fazlasını içlerinde.
<br>
Elleri ceplerinde, diğer yunanlıları vururlardı
<br>
Ve sanki, biri onların izindeymiş gibi telaşla kaçıp giderlerdi.
<br>
Ben dışarı çıkmak istiyordum, onlar buna iznim olmadığını
<br>
söylüyorlardı.
<br>
Oysa ben dışarıyı, 'yasak olan' ı istiyordum.
<br>
Sokağın köşesinde, yenmiş kedilerden tepecikler ve kıtlığın cesetleri
<br>
duruyordu -onlar, bunlara çöp diyordu- çocuklar ve onların anne babaları.
<br>
Gördüm, bir camın ardından, sol elimin avcunu delen o kurşunu, ve kanı,
<br>
ve nefes alıp veren o çöplüğü.
<br>
Annem mutfaktaydı, ve bdbam- babamın nerde olduğunu bile bilmiyordum.
<br>
Kapıyı açtım ve çöplüğe koştum
<br>
Ve orada, ister inanın ister inanmayın, hayatımdaki en güzel oğlanı gördüm.
<br>
Ortünmüştü
<br>
Ve elinde bir makineli tüfek tutuyordu,
<br>
Kısa sarı sakalları uzun sarı saçları vardı.
<br>
Gözleri.., anlatmak mümkün mü renklerini.
<br>
İsa'ya benziyordu, belki de O'ydu.
<br>
‘Uzaklaş buradan küçük kız, git’ dedi,
<br>
‘Benden uzaklaş, öldürecekler beni.'
<br>
Hızlıca koşabileyim diye derin bir nefes aldım.
<br>
‘Eğil ki bir kez öpeyim seni' dedi sonra bana.

Bense çoktan eve varmıştım.
<br>
İlk ve son aşık olduğum adam bir şehir gerillasıydı."

</quote>

İlk gençlik yılları, sivil savaş sonrası monarşiyi yaşayan Atina'nın şehir merkezinde geçti. Bu yıllar, ağır bir sansürün ve polis terörünün yaşandığı ve politik mahkumların sürgün kamplarına kapatıldığı yıllardı.'Benim Adım Odyssey' de, Gogou, Atina'nın en prestijli sinema salonlarından 'Pallas'ta, The Blackboad Jungle (1955) filmi gösterime girdiğinde, olup biten her şeye karşı duyduğu nefreti ilk kez binlerce yaşıtının yanında yer alarak ifade edebildiği zamanı anımsar: ' Film kasabaya geldiğinde, uzak mahallelerden ve şehir merkezinden gelen öfkeli gençler bir araya toplanmıştı. Filmin müziği, ünlü 'One Two Three O' clock Four O'clock Rock' duyulur duyulmaz kalabalığın yarısı çakmaklarını yakarken diğer yarısı jiletlerle sözde aristokrat Palas'ın kadife sinema koltuklarını delik deşik etmişti. Bense, iki grubun da içindeydim.' Aslında bu olay, Atina'da bir tür 'Teddy Boys' hareketinin doğuşuydu. Hemen sonra bu hareket 4000 numaralı maddeyle ağır bir şekilde bastırılmıştı.

Birkaç yıl sonra Katerina Gogou, liseden mezun olmuş ve Atina'daki bazı drama ve dans okuluna kayıt olmuş ve bir tiyatroda çalışıyordu. Dönemin sıkı sansür yasalarının içinde, geçimini sağlayabilecek tek alan zamanın kapitalist, ataerkil ve devletçi değerlerinin durmadan tazelendiği Yunanistan komedi endüstrisinde aktrislikti. Bu dönem, Katerina'ya işveren şirketi Finos Film tarafından verilen rollerin hemen hepsi, saf hizmetçi kızı, aptal kız kardeş ya da isyankar okul öğrencisi gibi klasik yan rollerdi. En bilindik ve aynı zamanda tanınmasını sağlayan rolü, 1959'da çekilen başrolde dönemin en meşhur aktrisi Aliki Vouyouklaki' nin olduğu "Beating Came From Paradise" isimli filmde, afacan bir okul öğrencisini canlandırdığı roldü. 1974'e, cunta rejiminin yıkılmasına dek, Gogou, daha birçok gişe rekoru kıran komedilerde öncekilere benzer sayısız rollerde oynadı. Spyratou'nunda dediği gibi 'sinema dünyası, film kahramanların ataerkil ve geleneksel aile yapısının izin verdiği ölçüde düzeni desteklediği ve toplumu bir tüketim toplumu olarak sunan kapitalist düşünceden ibaret bir dünyadır.' Bitmek bilmeyen tipik kadın rollerine rağmen Katerina, gerçek hayatında cunta sonrası beliren tutucu ve sözde feministlerinden çok uzakta radikal bir perspektif geliştirmiş ve 'Idionimo' isimli şiir derlemesinde yayınlanan 1980 yılında yazdığı bir şiirinde karşı durduklarına ağır bir şekilde seslenmiştir:

<quote>

‘'Öldürmek için ateş açıyorlar.
<br>
-Havaya ateş ediyorlar, diye bağırdılar [2]
<br>
Sonra, otobüs durağının önündeki çukur kanla doldu.
<br>
-Bunlar yalnızca plastik kurşun, dediler.
<br>
Adam düştü sonra.
<br>
-Adam bayıldı, diye bağırdılar.
<br>
Adam hareketsizdi.
<br>
;Ama, onlar çoktan kendi yollarına devam etmişlerdi.
<br>
Kıpırtısızdı adam,
<br>
;Ama, onlar çoktan tramvaya binip gitmişlerdi.
<br>
Gitmişlerdi.
<br>
Gitmişlerdi işte."

</quote>

Katerina Gogou'nun arkasında durduğu feminizm, demokratik ilerleme adına yapılan resmi ve cumhuriyetçi pratiklerden çok farklıydı. Erken dönem şiirlerinin birçoğu, 1970'lerde bile bile yok sayılan, yasaklanan fahişelik gerçekliğinin barındığı bir Yunanistan toplumunu betimliyordu. Aynı zamanda, Katerina'mn şiir dili çoğunlukla, şimdiye kadar kimsenin ifadesine o denli yaklaşamadığı Atina'nın karanlık ve kirli taraflarının vurucu bir anlatımına adanmıştı:

<quote>

" Acominatou sokağında
<br>
Kapının dışında plastik bir kapta yağlı bir yemek, Ağustos.
<br>
Orospular bir çarşaf kadar beyaz.
<br>
Öğleden sonra 4 ve gölgede 40 derece.
<br>
Ölü istiridyeler gibi
<br>
Kendiliğinden açılan bacaklar.
<br>
Sokak renkli iç çamaşırlarıyla dolu

Pakistanlılar[3], sivrisinek kovucu kimyasallar, topallayan kadınlar, muhbirler
<br>
Ve memelerine iğne yapan ibneler
<br>
Ve kanserlilerle dolu.
<br>
Sokak, ağzına dek
<br>
Bozulmuş dölyatağı tüpleri ve dışarı atılmış rahimlerle dolu
<br>
Ve gereksiz spermlerin şişirdiği karınlar.
<br>
- Burada bir bebeğe yer yok.
<br>
Magdalene ve Vanou işlerini bitirdiler
<br>
;Ama, tefeciler ve mahallenin kimi azizleri tüm işleri bozar.
<br>
Önce rüşvet verirler, sonra seni enseletirler.
<br>
Her şey böyle işler.
<br>
Yakıcı güneşin altında gölgesine sığınabilecekleri tek bir ağaç
<br>
Ya da yaslanabilecekleri tek bir kaya parçası olmaksızın
<br>
Tüm Metaxourgeio[4]' ya dağılan fahişeler. .
<br>
Şimdi, öfkeli yurttaşlar[5] ve dini gruplar aralarında anlaştılar,
<br>
Ve, Sizi fare gibi ıslatıp yakacağız, dediler,
<br>
Ellerinde petrol dolu şişeler.
<br>
Ağzına kadar polisle
<br>
Her biri bir iktidarsız röntgenci, ahlak tanrısı doktorlarla
<br>
Dolu zırhlı araçlar.
<br>
Ve, vinçler kafanızın üstünde gün boyu gezer
<br>
İslık çalan çocuklar uykunuzun üzerinde bir frengi.
<br>
‘'İşte, tüm cadıları ateşe verdik, becerdik bütün orospuları."
<br>
Karamanlis in büyük boy bir posteri
<br>
Gözleriniz resimde
<br>
Bir de gösterişli peruklar ve çürük meme, uçları.
<br>
Tutuklamalar saçlarınız ve boğazınız üzerine kapanır,

Ellerinizden ve ayağınızdan bir yatağa bağlarlar.
<br>
Sizi ve bizi
<br>
Nasıl ve neden
<br>
Nerde ya da kime yapıldığı fark etmez
<br>
Tüm bunların.
<br>
Şimdi, Larissa'da, 40 derecede,
<br>
Burada güneşin alnında. "

</quote>

Spyratou, Gogou'nun feminist düşüncesine dair şöyle yazar : 'Hayat kadınları toplumsal bir gerçeklik değil, toplumsal bir formdu ve aslında üreme mekanizmasının önemli bir parçasıydılar: erkek tarafından talep edildikçe bir fahişe sayılmalıydılar ve bir fahişe olmadıklarını ancak bir eş ya da anne olarak kanıtlamaları gerekliydi. Her iki durumda da bu erkek egemen bir düzenin kadınlık üzerine oluşturduğu ve tatmin edilmesini talep ettiği bir algıydı.' Gogou'nun "ideal kadın" fikrine bakılırsa, bu kadın tam bir devrimciydi, cumhuriyetçi dönemin sol eğilimli görüşleriyle uyumsuz gerçek bir devrimci:

<quote>

"Tehlikeliydi- tüm dünya dolu ve yağmurla sarsdıyorken, o sokağa çorapları bile olmadan çıkar, karşısına çıkan adamlara ıslık çalar, polis arabalarına taş atar, sincap gibi ağaçların üzerinde uzanır ve şimşeklerle sigarasını yakardı.

En son, yılın aynı tarihinde, aynı anda ve farklı yerlerde görülmüştü. Sağlam kaynaklara dayanarak, yıkılmış Manhattan köprüsü, anarşist oluşumlara aktarılan cephane gibi en gizli bilgilerin dolaşıma bile aynı kişiyle, onunla, Hintileniyordu. Kırmızı ya da siyah yün bir kazak giydiği ve saçlarında sedefşeritlerle elleri ödünç alınmış bir ceketin ceplerinde gezdiğine inanılıyordu.

Doğum yeri: Bilinmiyor
<br>
Cinsiyeti: Bilinmiyor
<br>
Mesleği: Bilinmiyor
<br>
Dini: Ateizm
<br>
Göz Rengi: B ilinmiyor
<br>
Adı: Sofla Viky Maria Olia Niki Anna Effie Argyro
<br>
Tüm devriye arabalarının dikkatine’
<br>
O silahlı, tehlikeli.
<br>
Silahlı ve tehlikeli.

Adı Sofla Viky Maria Olia Niki Anna Effle Argyro,
<br>
Ve tanrım! O güzel, güzel, güzel, çok güzel, “ (Idionimo, 1980)

</quote>

Gogou, 1978'de, ilk toplu şiirlerini "Three Clicks Left" adlı kitapta yayınladı ve bu kitap Atina'nın dönemin solcu ve cumhuriyetçi kahramanlık atmosferine hiç de uygun olmayan rahatız edici bir tasviriydi. Diğer yazarların ya da düşünürlerin tersine kendini şehrin en aşırı uçları ve varoş kesimiyle özdeşleştiren Gogou, Exarcheia'da yeni yeni belirmeye başlayan anarşist oluşum tarafından hemen benimsendi ve aralarında şairin ölümüne kadar hiç bozulmayacak bir bağ kuruldu. 1970'lerin sonları, cunta rejimin akabininde beliren devrimci modanın, yerini daha çeşitli ve muhalif bir kentsel kültüre bıraktığı, fabrikaların ve üniversitelerin işgal edildiği, ceza almayan cunta ajanlarına karşı silahlı mücadelelere girişildiği, karşılığında aşırı sağcıların sinemaları, sol eğilimli kurumlan bombaladığı yıllardı. Daha sonraları,1980'lerin anarşist hareketlenmesinin şiarı haline gelecek olan ve yıllardır yok sayılan varoşların, fahişelerin, keşlerin, tutukluların ve psikiyatrik hastaların politik önem kazanmasının önünde durmadan engel oluşturan geleneksel sol pratiklerinin ayırdına varan radikal Atina gençliğinin yükselişe geçtiği bir dönem başladı. Gogou, bu hiç de beklenmeyen sosyal ve politik gelişmelerin gerçek bir önsezicisiydi ve onun şiirleri özellikle de bunun için ayrı bir önemi hak ediyor:

<quote>

“Bizim hay at imiz çakılar,
<br>
Kirli ve çıkmaz sokaklarda çürümüş dişler arasında giderek zayıflayan sloganlar,
<br>
Sidik için kullanılan antiseptiklerin kokusu,
<br>
Ve, donmuş spermler.
<br>
Yırtıp atın posterleri!
<br>
Patission[6] da oradan oraya
<br>
Bir aşağı bir yukarı,
<br>
Hayatımız Patission!
<br>
Ve, koca bir deniz için bile zararsız tozlardan arındırarak
<br>
Mitropanos[7] hayatımıza girdi

;Ama, Dexamen[8] onu da koparıp aldı bizden,
<br>
Götleri yüksekte gezinen şu hanımefendiler gibi.
<br>
;Ama, biz buradayız hala.
<br>
Yaşam boyu, karnımız aç, dolaşıp duruyoruz.
<br>
Hep aynı hikaye:
<br>
Alay-yalnızlık-ümitsizlik, ve geri kalan.
<br>
Tamam, artık ağlamak yok, büyüdük.
<br>
Yalnızca yağmurlu havalarda
<br>
Emiyoruz gizlice başparmağımızı
<br>
Ve sonra, bir sigara içiyoruz.
<br>
Hayatımız,
<br>
Amaçsız bir soluk.
<br>
Organize grevlerde
<br>
Gammazlar ve devriyeler.
<br>
Size bu yüzden söylüyorum
<br>
B ize bir sonraki ateş edecekleri zaman
<br>
Kaçmayın, güvenin gücünüze.
<br>
Hadi! Bu kadar ucuza satmayalım etimizi,
<br>
Sakın!
<br>
Bak, yağmur yağıyor.
<br>
Bana bir sigara ver."

</quote>

1970'lerin sonlarında sinemaya dönüş yapan Gogou, bu kez kariyerinin ilk yıllarında oynadığı geleneksel kadın tiplemelerinden çok daha farklı rollerde oynadı. Dönüşü, 1977 yılında aynı zamanda kocası olan,Pavlos Tassios/un yönetmenliğinde çekilen ve Yunanistan yeni gerçekçiliğinin öncü filmlerinden sayılan "The Heavy Melon" adlı sinema filminde rol almasıyla gerçekleşti. Film, şehirlerde yeni yeni yeni oluşmaya başlayan, büyük çoğunluğunu sınıf kaybına uğramış bağnaz aristokratların oluşturduğu emekçi sınıfı tasvir edyordu. Gogou, filmde, parça başına çalışan ve sıklıkla kazancını arttırabilmek mesai yapmak zorunda kalan bir sanayi işçisini canlandırır. Kadın arzuladığı huzuru yalnızca aşkta arar; ama, sonunda bulduğu aşk onu gene eninde sonunda sömürünün bu kez ataerkil aile içinde işlediği kısmına maruz bırakır. Bu film Gogou' ya, Salonica Film Festivalinde en iyi kadın oyuncu ödülünü kazandırır. Birkaç yıl sonra,1980'de, Gogou, yine Tassios'un yönettiği "The Order" adlı filmde oynayacaktır. Bu seferki film, Nikos Koemtzis'in hayatını konu edinir: 1973 şubatında hapishaneden çıkan Koemtzis, kardeşi ve dostlarıyla beraber gece kulübü olan Nerajda'ya gider, kardeşi, Yunanistan'ın yerel oyunlarından Zeibekiko'yu oynamak için orkestradan Marcos Vamvakaris'in şarkısı 'Vergoules' i çalmasını ister. Orada bulunan diğer adamlar da dansa katılmak için kalktıklarında (,ki Zeibekiko esasen kökleri Küçük Asya'ya uzanan tek kişi ve genellikle erkekler tarafından edilen bir danstır), orkestradan biri, şarkının istek üzerine çalındığını söyleyerek dansa kalkan grubun oturmasını rica eder. Bunun üzerine tartışma çıkar ve Koemtsiz kardeşini öldürmeye çalıştıklarını düşündüğü üç polisi bıçaklayarak öldürür. Filmde geçen bu olay, Yunanistan'da 1970'lerin ikinci yarısında efsane haline gelmiş, tüm değerlerin yer değiştirdiği bir toplumda artık modası geçmiş kabul edilen 'onuru için yaşayan insan' modelinin sembolü sayılmıştı. Filmde aynı zamanda Gogou' nun "Three Clicks Left" adlı kitabından kimi şiirler, ileride Yunanistan'ın klasik müzik yayını yapan devlet radyosunda 'Third programme' adlı programı yapacak olan Kyriakos Sfetsas' ın öncü partisyonu altında okunur. İkinci kez Salonica Film Festivalinden film ödülle döner; ama, bu kez ödül, en iyi müzik dalında, filmdeki resital ve partisyon için verilir. Ardından filmin müzikleri plaklara basılır ve 1980'lerin radikal kültürü için bir nevi müzikal fetiş haline gelir. Bugün bile, film, müzik ve şiirin bir araya getirilmesi adına yapılan önemli girişimlerden bir tanesi olarak gösterilebilir. Filmde okunan ve dönemin cumhuriyetçi Yunanistanının durumuna dair doğrudan politik imalar sunan şiirlerden biri şöyledir:

<quote>

"Yalnızlık,
<br>
Onun gözlerinde
<br>
Karanlık bir metresin hüzünlü rengini taşımaz.
<br>
Ve, o kadın,
<br>
Konser salonlarında ve soğuk müzelerde

Kalçalarını titreterek
<br>
Halinden memnun ve gizemli edalarla gezinmez.
<br>
O, ne'mutlu' eski zamanların sarı bir çerçevesi,
<br>
Ne büyükannenin sandığındaki naftalin kokusu
<br>
Ve gülkurusu renginde kurdelalar
<br>
Ne de
<br>
Hasır şapkalar gibidir.
<br>
Bacakları küçük sahte gülüşler eşliğinde
<br>
Ve bir sığırın bitmek bilmeyen bakışları ve
<br>
İç çekişleri önünde açılmaz.
<br>
Ve, giymez çeşit çeşit iç çamaşırları.
<br>
Yalnızlık,
<br>
Onun gözlerinde PakistanlIların rengini taşır,
<br>
Bu yalnızlık.
<br>
Ve, bir ışık huzmesi altında
<br>
Diğerleriyle beraber
<br>
Karış karış sayılır.
<br>
Kuyrukta sabırla bekler.
<br>
Bournazi - Santa Barbara - Kokkinia
<br>
Touba -Stavroupoli - Kalamarla[9]
<br>
Kafalardan terin aktığı bir havada
<br>
Duyulur çığlıkları, ve
<br>
Elinde zincirler, karşı pencereleri kırar döker
<br>
Ve gasp eder fabrikalardaki makineleri.
<br>
Özel mülkiyeti ateşe verir.
<br>
O hapishanede bir Pazar ziyaretidir.
<br>
Tersane devrimcileri ve cezaevlerindeki tutuktular için
<br>
Olan ne ise, aynı şekilde,
<br>
O da
<br>
Yeryüzünün köle pazarında
<br>
Alınır ve satılır, her dakika, adım adım.
<br>
Kotzia[10] çok yakında!
<br>
Erken kalk

Uyan ve gör
<br>
O kadın çürümüş evlerde bir fahişe!
<br>
Ve, Almanlar asker toplarken,
<br>
Ve, Bulgaristan'dan taşman ve askılarda sallanan etler içinde
<br>
Şehrin merkezine dek uzanan
<br>
O sonsuz ulusal karayol.
<br>
Ve, kanı pıhtılaştığında,
<br>
Kadın artık dayanamayacak bu ucuz pazara,
<br>
Masalarda çıplak ayak Zelbekiko’sunu oynayacak,
<br>
Morarmış ellerinde bir balta tutacak!
<br>
Elleri, yara bere içinde.
<br>
Ve, biçimli bir balta ellerinde.
<br>
Yalnızlık,
<br>
Bizim yalnızlığımız diyorum!
<br>
Anlatılan bu yalnızlık bizim.
<br>
Ve, kafalarınızın üzerinde
<br>
Yeniden ve yeniden savrulan o balta
<br>
Bizim ellerimizde."

</quote>

Aynı yıl, 1980'de, Gogou, ikinci şiir derlemesi, "Idionimo" yu yayınladı. Kitabın ismi, protestocuların karşısında güvenlik güçlerine ve aynı zamanda grevlerin karşısında da rejimin kendisine kazanımlar sunan 410/1976 numaralı yasaya bir göndermeydi. Idionimo ismi bu yasaya dönemin anarşistleri ve solcuları tarafından 1920'lerin sonlarında dönemin liberal başbakanı Eleftherios Venizelos'un komünistlerin kurak ada kamplarına ihracını öngören yasasına ithafen takılan isimdi. Kitabında, Gogou, Yunanistan Komünist Partisi'ne mücadeleye ettiği ihanet yüzünden saldırır. Suçlama, partinin gençlik örgütünün (KNE) 'Düzenin Restorayonu İçin Komünist Gençlik (çelik kuvveti sembolize eden MAT kısaltmasıyla kafiyeli olarak, KNAT)' adı altında herhangi bir özerk girişimi ve özellikle 1979-1980'de ilk üniversite işgalinde yapılan polis karşıtı gösterileri hunharca bastırmak amacıyla yeni bir oluşuma gittiği üzerineydi. Hatta, dönemin "The Rooster Crows at Dawn" gibi anarşist dergilerinde, KNE' nin anarşistlere Politeknik' in özel odalarında işkence yaptıklarını yazıyordu. 1979-1980'de 815 numaralı eğitim yasasına karşı yapılan üniversite işgalleri Yunanistan'da yaşanan bir çeşit "68 Mayıs'ı" ydı ve gençlik, ilk defa,kitleler halinde dönemin sol ve komünist ortodoksisinden ve 1977'de anarşistlerle Exarcheia'da çatışmaya giren Maoistlerden hesap soruyordu.

1980'e kadar, Gogou, 1981'de Valtetsiou Sokağında ilk Atina gecekondusunun göründüğü Exarcheia'da filizlenen anarşist kültüre derinden bağlı kaldı. Bundan iki yıl önce, 1979'da, Sporting'de, polis baskılarına karşı organize edilen, birçok şarkıcının sahne aldığı büyük konserde büyük bir rol oynadı. Konserin taleplerinden biri, terörist olarak cezaevinde tutulan iki anarşist Philipos and Sofia Kiritsi'nin serbest bırakılmasıydı. Kritsi' lerin tutuklanması anarşist hareketinin ağırlığının ilk sembolik ifadesiydi ve yoldaşların özgürleşmesi için oluşturulan dayanışma hareketi için önemliydi. Doğal olarak, konser, sonunda 100 kişinin tutuklandığı bir çatışmayla sona erdi. Bu dönem, buna benzer olarak daha birçok, sonrasında çatışmalarının yaşandığı konser organizasyonları yapılmıştı ve bunların içinde en şiddetlisi, 1980 baharında Sporting'de verilen 'polis' konserlerinde yaşandı. Bu konser, cunta döneminde verilen ve Mick Jagger'ın seyircilere kırmızı karanfil attıktan sonra, ki polisler bunu komünist bir propaganda olarak yorumlamışlardı, sahneye doluşan polislerin grubun menajerini tartaklamasıyla son bulan Rolling Stones konserinden sonra verilen ilk Rock konseriydi. Çok hararetli geçen bu 1980 Mayıs'ı gecesi boyunca, tüm üniversiteler işgal altındayken, 2000 gencin biletleri olmadan stadyuma hücum etmesi üzerine Patission ve Aharnon caddeleri boyunca polisler ve gençler arasında büyük çatışmalar yaşandı.

1982'de, Gogou, üslubunun iyice belirginleştiği üçüncü şiir kitabı, "Wooden Overcoat" u çıkardı. 1984'de ise, senaryosunu kendisinin yazdığı "Ostria -Endgame" isimli filmde sinema yaşantısının son rolünü oynadı.Yine Tassios'un yönetmenliğini yaptığı bu film, bu kez küçük burjuva konformizmi uğruna devrimci fikirlerini terk eden üç çiftin öyküsünü aslında iğneleyici bir dille o zamana dek güç ve sömürüye alet olmuş tüm 'politeknik nesli' ni anlatıyordu.

1986'da, dördüncü şiir kitabı "The Absentees" i yayınlayan Gogou kitaptaki en tanındık şiirini öldürülen anarşist travesti Sonia'ya adamıştı:

<quote>

"Solgun yüzüyle bir kez iç çekti
<br>
Ve, sonra kafası yana düştü, hafifçe.
<br>
Sonsuz bir uykuya daldı.
<br>
Gökyüzü üzerinde uzanıyordu

Ve, dağlık ve kurak bir manzara -karanlık-,
<br>
Yalnızca taşların ve kayaların olduğu.
<br>
Yağmur bile yağmamıştı.
<br>
Kırmızıya boyanmış dudaklarınla
<br>
Bir gelindin sen,
<br>
Ve, ellerin sırmayla dokunmuş ve eskimiş dişleriydi
<br>
Taptaze yeryüzü için
<br>
Zambaklar diledin nazikçe.
<br>
Üzgün ve boyalı kız arkadaşlarınsa
<br>
İlgi ararken arzuyla,
<br>
Garip sesler çıkarıyordu.
<br>
Bazı filmlerde rol kapmak için,
<br>
Bu küçücük şiir bir sesleniş,
<br>
Onur sözü.
<br>
Zaman,
<br>
Gelecek olanların
<br>
Artık kartalların savaşını öğrendiği
<br>
Ve alnının gizli saklı bırakılanları
<br>
Gösterdiği bir zaman.
<br>
Zaman hep,
<br>
Kızıl bıçakların
<br>
Farklı olanları katlettikleri bir zaman'. "

</quote>

Şiirde geçen 'kızıl bıçak' , eşcinselliği devrimle ortadan kaldırılabilecek bir burjuva hastalığı gibi gören Yunanistan Komünist Partisi'nin eşcinselliğe karşı geliştirdiği bağnaz politikalara yapılan bir göndermedir. 1980'lerin ortaları, Yunanistan'da öfkeli eşcinsellerin özgürlük savaşı verdiği bir dönemdir. Tam da bundan sadece birkaç yıl önce, 1979'da, hükümetin sağ kanadı meclise eşcinsellerin ada kamplarına gönderilmesini talep eden bir yasa tasarısı sunmuştu. Yasa, Foucault ve Guattari gibi entelektüellerin uluslar arası baskısı ve ülke içinde yasaya karşı örgütlenen kitlesel hareketler sayesinde veto edilebilmişti. Bu asimilasyon yasasına karşı verilen mücadele 1980'lerde büyük çoğunluğu sol eğilimli bir grup olan AKOA liderliğinde oluşturulmuş eşcinsel özgürlük hareketlerinin beşiği oldu. Çevik kuvvetle çatışmaya giren travestiler (kendilerini böyle tanımlıyorlardı) hareketin en militan üyeleriydi. Anarşist ve aynı zamanda bir travesti olan Sonia' nın, yayınladığı Kraximo adlı travestiler için çıkarılan anarşist bir dergi nedeniyle sayısız tutuklamalara maruz kalan dönemin travesti ve anarşist figürü Paola'yla güçlü ilişkileri vardı. Sonia'nın Attiki sahillerindeki ıssız kayalar üzerinde bulunan, öldürülmeden önce vahşice işkence görmüş çıplak cesedinin bulunmasıyla açığa çıkan suikasti dönemin silahlı faşizan atmosferinin sembolü haline geldi.

Gogou da aynı dönemde çok defa polisin keyfi şiddetinin kurbanı oldu. 1986'da, zamanın sayısız anarşist yürüyüşlerinden birinde çevik kuvvet polisleri tarafından kötü bir şekilde dövüldükten sonra, Gogou, PASOK ( Panhelenik Sosyalist Hareket) 'un asayişten sorumlu başkanı General Droyannis' e birçok dava açmıştı. Özellikle, önde gelen anarşistlerden Katerina latropoulou ile arkadaşlığı ve politik yakınlığı nedeniyle Gogou'nun adı hep bakanlığın değişmeyen şüpheli listesinde kaldı. Baskılara karşı olan savaşımı en iyi şekilde belki de 'Some Times' adlı şiirinde anlatılır:

<quote>

"Bazen açılır kapı, usulca,
<br>
Bembeyaz elbiselerinle ve keten ayakkabılarınla
<br>
içeri girersin,
<br>
Eğilip, avuçlarıma nazikçe tam 72 tane madeni para bırakırsın
<br>
Ve, sonra gidersin.
<br>
Ben bıraktığın yerdeyim,
<br>
Ki bir dahaki gelişinde beni bulabilirsin.
<br>
Tırnaklarım çok uzadı
<br>
Demek ki çok zaman geçmiş,
<br>
Ve arkadaşlarım artık benden korkuyor.
<br>
Her gün patates pişiriyorum,
<br>
Ve, yit irdim hayal gücümü,
<br>
Ve, ismim söylendiğinde bir ürküntü,
<br>
Sanırım binlerini ihbar etmek zorundayım!
<br>
Kimi gazete küpürlerini saklıyorum
<br>
Hepsi de sen olduğunu söyledikleri bir adamla ilgili.
<br>
Yalan söylüyorlar, biliyorum
<br>
Çünkü ayaklarından vurulduğun yazıyor.
<br>
;Ama, biliyorum, onlar asla ayaklardan nişan almazlar bir insana.
<br>
Tek hedefleri akıldır.
<br>
Bu hep böyle olmadı mı?"

</quote>

1998'de Katerina "The Month of Frozen Grapes" isimli beşinci şiir kitabını yayımladı. Kitapta toplam 38 şiir vardı ve çoğu şiirler iki dizeden hatta bazıları tek satırdan oluşuyordu. Bu şiirler, daha çok, hayatının geri kalanında birçok kez kliniğe yatmasına neden olacak acıyla olan mücadelesinin ifadesiydi:

<quote>

"Bir ağaçtım ben,
<br>
Ve, kırıldım.
<br>
Kırdılar tüm dallarımı,
<br>
Çünkü tüm kayıp çoçukların
<br>
Dallarında oyunlar kurduğu bir ağaçtım,
<br>
Ve, hepsinin asılı kaldığı."

</quote>

Son olarak, 1990'da Katerina, yaşamı boyunca sürdürdüğü politik çizgiyi ve o durmadan duyduğu varoluş acısını anlattığı son şiir kitabı "The Return Journey" i yayınladı. Sayfaların birinde, içinde Yunanistan'daki anarşist hareketin doğuşunu simgeleyen beş kişinin isminin yazılı olduğu elle çizilmiş bir kutu vardır. Bu beş isim polis tarafından vurulmuş, şehir gerillalarıydı: Kasimis, Tsoutsouvis, Prekas, 1985'te Politeknik' in yıldönümü için yapılan yürüyüşte bir polisin vurduğu 15 yaşındaki Kaltezas ve dairesini özerk bir alan olarak ilan etmiş ve özel güçler tarafından vurulmuş devrimci ve sıra dışı doktor, Tsironis:

<quote>

“Dondurucu bir sessizlik ve sessiz bir korkunun terörü. Sessizlik usul usul ve aynı hizada hareket eden proplazmatik yüzlerle gittikçe tırmanıyor. Yakalanan o adam her şeyi biliyor. Kulaklarının ardında ve karnının derinliklerinde tarifsiz acılar. Yüzü değişiyor, gençleşiyor, artık daha yakışıklı, son çatışma için hazır, şereflice tanrısına yaklaşıyor. Dahafazla insan için adalet işkenceyi arttırıyor. Artık acı duymuyor. Şimdi, terör, avcılarının omzunda yükseliyor.

Şimdi nişan alacaklar
<br>
Ve cinayeti işleyecekler
<br>
Cinayeti.
<br>
İnsana benzeyen yüzleri bilincin tımarhanesindeki o ifadeyi takındı.
<br>
Sonsuzluğun ateşinde ve sonsuza kadar birbirlerini yok edecekler.

Ve çoğalacak melekler."

</quote>

3 Ekim 1993'te, Katerina Gogou, aşırı miktarda aldığı hap ve alkol yüzünden, 53 yaşında öldü. Cenazesinde binlerce insan toplandı. Katerina' mn son biyografisinde yayınlanan ve ölümünden sonra ortaya çıkan kayıp şiiri kuşkusuz onun anarşiye olan değişmez bağlılığının bir ifadesidir:

<quote>

“Bana engel olmasanıza! Hay al kuruyorum.
<br>
Yalnızlık ve adaletsizlik içinde yüzyıllar geçirdik.
<br>
Şimdi durun! Beni durduramazsınız.
<br>
Şimdi ve burada, sonsuza dek ve her yerde
<br>
Özgürlüğü düşlüyorum
<br>
Herkesin eşsiz güzelliklerini,
<br>
Evren için yepyeni bir uyum yaratacak olan.
<br>
Hadi oynayalım!
<br>
Mutluluktur bilgi,
<br>
Okullu olmayı gerektirmeyen.
<br>
Düşlüyorum çünkü bu duyduğum aşk,
<br>
Gökyüzündeki büyük hayaller için,
<br>
Ve, kendi fabrikalarında
<br>
Tüm dünya adına çikolata üreten işçiler için.
<br>
Düşlüyorum çünkü biliyorum ve buna gücüm var.
<br>
Bankalar hırsız eder insanı,
<br>
Ve hapishaneler terörist.
<br>
Tüm uyumsuzluk yalnızlıkla
<br>
İhtiyaç üretilenle
<br>
Ve, ordular sınırlarla
<br>
Var olur.
<br>
Hepsinin nedeni mülkiyet,
<br>
Ve, şiddetin nedeni şiddet.
<br>
Şimdi değil! Beni durduramazsınız!
<br>
Şimdi, ahlaklı dürüstlüğü
<br>
Yeniden kurmanın zamanı.
<br>
Yaşamı şiire,
<br>
Yaşamı pratiğe dönüştürme zamanı.
<br>
Bu yapabileceğimin hayali!

Sizi seviyorum.
<br>
Ne siz beni durdurabilirsiniz
<br>
Ne de ben artık hayallerle oyalanacağım.
<br>
Yaşıyorum ben'.
<br>
Ellerim dayanışma
<br>
Ve özgürlük için
<br>
Uzanıyor,
<br>
Ne kadar sürerse sürsün.
<br>
Hep yeni baştan,
<br>
Yaşasın anarşi!"

</quote>

[[k-g-katerina-gogou-2.jpg f]]


<br>

[1] "Çetelerin savaşı" (Symmoritopolemos) 1970 lerin ortalarına kadar süren sivil savaş için
<br>
monarşi yönetiminin kullandığı resmi ifadedir.

[2] Şair, Demokratik Kadın Hareketi adı altında örgütlenen feministlerden bahseder. Şiirin yazıldığı 1980 yılında, Poiteknik zin kuruluş yıldönümü üzerine düzenlenen yürüyüşte, bir işçi ve bir öğrenci, Koumis and Kanellopoulou, polis güçleri tarafından öldürülür. Demokratik Kadın Örgütünün bu cinayet karşısındaki tutumu sadece olayın üzerini kapatmaya çalışmak olmuştu.

[3] PakistanlIlar, 1990'da Yunanistan a olan ilk büyük göç dalgasından önce oluşan Yunanistan'daki ilk göçmen işçi sınıfıydı. İşçi hareketinin en güçlü döneminde, 1970'lerin ortasında PakistanlI işçiler büyük yankılar uyandıran bir grev başlatmışlardı.
<br>
[4] Atina'nın yoksul işçi ailelerin yaşadığı aşağı mahallelerinden biri. Zamanla, bölge terk edilmiş, eski ve kullanılmayan evlerin, araba mezarlıklarının ve genelevlerin bulunduğu bir mahalle haline gelmiştir.
<br>
[5] (Aganaktismenoi Polites) Faşitler ve sistemin kiralık katilleri için medya'mn kullandığı örtük ifade.

[6] Politeknik ve ASSOE üniversitesinin yakınında şehrin merkezi meydanlarını birbirine bağlayan cadde.

[7] İşçi kökenli ünlü şarkıcı. Zamanla, önce cunta sonrası cumhuriyetçi sol cenahta ve daha sonra da müzik endüstrisinde kapitalist sisteme entegre olmuştur.

[8] Parlamnet binasının hemen yakınındaki yönetici sınıfın bölgesi sayılabilecek bölge, Kolonaki' nin gösterişçiliğine olan gönderme. O dönem seçkinci sol kemsin sembolü olarak düşünülüyordu.

[9] Atina'nın işçi mahalleleri.

[10] Atina'daki inşaat işçilerinin inşaat şirketlerinde iş bulabilmek için tüm gün bekleştiği belediye meydanı.



