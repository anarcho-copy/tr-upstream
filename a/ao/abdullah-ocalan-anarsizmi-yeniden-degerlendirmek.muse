#title Anarşizmi Yeniden Değerlendirmek
#author Abdullah Öcalan
#SORTtopics demokratik konfederalizm
#date 2002
#source 18.05.2022 tarihinde şuradan alındı: [[https://www.lekolin.org/anarsizmi-yeniden-degerlendirmek/][lekolin.org]]
#lang tr
#pubdate 2022-05-18T21:10:42
#notes İngilizce Aslı: [[https://theanarchistlibrary.org/library/abdullah-ocalan-re-evaluating-anarchism][Re-evaluating Anarchism]]



Reel sosyalizmle yaşıt olan ve Fransız Devrimi’nde kök bulan anarşist hareketler reel sosyalizmin çözülüşü, daha doğrusu sistemle bütünleşmesi sonrasında yeniden değerlendirmeyi hak etmektedir. Ünlü temsilcileri Proudhon, Bakunin ve Kropotkin’in eleştirilerinde tümüyle haksız olmadıkları (sisteme ve reel sosyalizme ilişkin) bugün daha iyi anlaşılmaktadır. Kapitalizmi yalnız özel ve devlet tekeli olarak değil, modernite olarak da eleştirmekten geri kalmayan bir hareket olarak, sistemin en karşıt ucunda yer almayla dikkat çekerler. İktidara hem moralist, hem politik açıdan yönelttikleri eleştiriler önemli gerçeklik payı taşımaktadır. Geldikleri sosyal yapıların hareket üzerinde etkileri belirgindir. Kapitalizmin iktidardan düşürdüğü aristokrat kesimlerle eskiye göre göreceli olarak durumlarını daha da kötüleştirdiği şehir zanaatkârlarının sınıfsal tepkileri bu gerçeği yansıtır. Bireysel kalmaları, güçlü taban bulamamaları, karşıt sistem geliştirememeleri sosyal yapılarıyla yakından bağlantılıdır. Kapitalizmin ne yaptığını iyi biliyorlar, fakat neyi yapmaları gerektiğini iyi bilmiyorlar. Görüşlerini kısaca toparlarsak

 1. Kapitalist sistemi en soldan eleştirmektedirler. Ahlaki ve politik toplumu dağıttığını daha iyi kavrıyorlar. Marksistler gibi ilerici rol atfetmiyorlar. Dağıttığı toplumlara yaklaşımları daha olumludur. Gerici ve çürümeye mahkûm görmüyorlar. Ayakta kalmalarını daha ahlaki ve politik buluyorlar.

 1. İktidar ve devlet yaklaşımları Marksistlere göre daha kapsamlı ve gerçekçidir. İktidarın mutlak kötülük olduğunu söyleyen Bakunin’dir. Fakat her ne pahasına olursa olsun iktidar ve devletin hemen kaldırılmasını talep etmeleri ütopik olup, pratikte fazla gerçekleşme şansı olmayan yaklaşımlardır. Devlet ve iktidara dayalı sosyalizmin inşa edilemeyeceğini, belki de daha tehlikeli bürokratik bir kapitalizmle sonuçlanacağını öngörebilmişlerdir.

 1. Merkezi ulus-devlet inşasının tüm işçi sınıfı ve halk hareketleri için felaket olacağını ve umutlarına büyük darbe indireceğini öngörmeleri gerçekçidir. Almanya ve İtalya’nın birliği konusunda Marksistlerle giriştikleri eleştirilerde de haklı çıkmışlardır. Tarihin ulus-devlet lehinde gelişim göstermesinin eşitlik ve özgürlük ütopyaları için büyük kayıp anlamına geldiğini söylemeleri ve Marksistlerin ulus-devletten yana tavır almalarını şiddetle eleştirip ihanetle suçlamaları belirtilmesi gereken önemli hususlardır. Kendileri konfederalizmi savunmuştur.

 1. Bürokratizme, endüstriyalizme, kentleşmeye yönelik görüş ve eleştirileri de önemli oranda doğrulanmıştır. Erkenden anti-faşist ve ekolojik tavır geliştirmelerinde bu görüş ve eleştirilerinin önemli payı bulunmaktadır.

 1. Reel sosyalizme yönelttikleri eleştiriler de sistemin çözülmesiyle doğrulanmıştır. Kurulanın sosyalizm değil, bürokratik devlet kapitalizmi olduğunu en iyi teşhis eden kesimdir.

Oldukça önemli ve doğrulanmış bu görüş ve eleştirilerine rağmen, anarşist hareketin reel sosyalizme göre kitleselleşip pratik uygulama şansı bulamaması düşündürücüdür. Bu sanıyorum teorilerindeki ciddi bir eksiklik ve sakatlıktan kaynaklanmaktadır. Uygarlık çözümlemelerinin eksikliği ve uygulanabilir bir sistem geliştirememeleri bunda önemli rol oynamıştır. Tarihsel-toplum çözümlemeleri ve çözüm önerileri de pek geliştirilmemiştir. Ayrıca kendileri de pozitivist felsefenin etkisini taşımaktadır. Avrupa merkezli sosyal bilimin pek dışına çıktıkları söylenemez. En önemli eksiklikleri, bence demokratik siyaset ve modernite konusunda sistematik düşünce ve yapılanma içine girememeleridir. Görüş ve eleştirilerinin doğruluğuna ilişkin gösterdikleri titiz çabayı sistemleştirme ve uygulama konusunda sergileyememişlerdir. Belki de sınıfsal konumları buna engeldir. Diğer önemli bir engel, teorik görüşlerinde ve pratik yaşamlarında her türlü otoriteye duydukları tepkidir. İktidar ve devletin otoritesine duydukları haklı tepkiyi tüm otorite ve düzen biçimlenişine yansıtmaları, demokratik moderniteyi teorik ve pratik olarak gündemleştirmelerini etkilemiştir. En önemli özeleştiri konusunun demokratik otoritenin meşruiyetini ve demokratik modernitenin gerekliliğini görememeleri olduğu kanısındayım. Ayrıca ulus-devlet yerine demokratik ulus seçeneğini geliştirmeyişleri de önemli bir eksiklik ve özeleştiri konusudur.

Günümüzde reel sosyalizmin çözülüşü, ekolojik ve feminist hareketlerin gelişmesi, sivil toplumculuğun genel bir kabarma sergilemesi şüphesiz anarşistler üzerinde olumlu etki bırakmıştır. Fakat haklı çıktıklarını tekrarlamaları fazla anlam ifade etmiyor. Yanıtlamaları gereken soru, neden iddialı bir sistem eylemliliğini, inşasını geliştirmedikleridir. Bu da akla teori ile yaşamları arasındaki derin uçurumu getirmektedir. Çokça eleştirdikleri modern yaşamı acaba kendileri aşabilmiş midirler? Daha doğrusu, bu konuda ne kadar tutarlıdırlar? Avrupa merkezli yaşam tarzını bırakıp, gerçek bir küresel demokratik modernliğe adım atabilecekler mi?

Benzer soru ve eleştirileri çoğaltmak mümkündür. Önemli olan tarihte büyük fedakârlıklar göstermiş olan, önemli düşünürleri bağrında taşıyan, görüş ve eleştirileriyle entelektüel camiada önemli yer tutan bu hareketin ve mirasının tutarlı, gelişebilir bir sistem karşıtı sistem içinde toparlanabilmesidir. Anarşistlerin reel sosyalistlere göre daha rahat bir özeleştiri ile güncel pratiğe yönelmeleri beklenebilir. Ekonomik, sosyal, siyasal, entelektüel ve etik mücadelelerinde hak ettikleri yeri almaları önemini korumaktadır. Ortadoğu zemininde hızlanan uygarlık ve kültür boyutları da öne çıkmış bulunan mücadelelerde anarşistlerin hem kendilerini yenilemeleri, hem de güçlü katkılarda bulunmaları mümkündür. Demokratik modernite sisteminin yeniden inşa çalışmalarında ittifak geliştirilmesi gereken önemli güçlerden birisidir.

<right>
Abdullah Öcalan<br>
İmralı cezaevi, 2002
</right>


