#title Aramızda
#author Anonim
#SORTtopics teknoloji, iletişim, isyan, giriş, sosyal medya
#date Yaz, 2022
#source 18.07.2022 tarihinde şuradan çevrildi: [[https://theanarchistlibrary.org/library/anonymous-between-you-and-me][theanarchistlibrary.org]], [[https://thelocalkids.noblogs.org/files/2022/06/tlk08.pdf][The Local Kids, Sayı 8]]
#lang tr
#pubdate 2022-07-18T13:58:45
#notes Çeviri: Rojen Zaman <br>İngilizce Aslı: [[https://theanarchistlibrary.org/library/anonymous-between-you-and-me][Between You and Me]]



İkimizin arasında teknolojik bir engel olmamalı.

Aramızdaki ekranlar herhangi bir bağ oluşturmaz.

Önümüze aralarından seçim yapabileciğimiz fontları, kendimizi ifade etmek için sınırlı sayıda kelimeleri sürüyorlar. Kim olabileceğimize dair bir profil oluşturuyorlar. Sonsuz sayıda görüntüler, listeler, beğeniler, sergilenmiş/yayınlanmış görüşler akışta. Aşağıya kaydırarak birini tanıyabileceğini veya sağa kaydırarak yeni bir bağlantı kurabileceğini söylüyorlar.
<br>
Dijital zaman ve dijital dilde oluşmuş yalnızca ekranlar üzerinde var olabilen sosyal ağa mecbur bırakıldık. Üyelik zırhıyla çevrelenmiş, mahremiyetten ödün verilmesiyle mümkün olmuş, para kazanmayla beyni yıkanmış, davranışları pazara sunulmuş, istek ve arzuları manipüle edilmiş sosyal bağlantılara mecbur bırakıldık.
<br>
Elbette hayallerimiz listelerle, görüntülerle, elektronik bağlantılarla oluşmuş bir sosyal ağla sınırlı kalamaz değil mi?
<br>
Seçmek istemiyoruz,
<br>
Beraberce aramak ve –beraberce– bulmak istiyoruz.
<br>
Aralarından seçim yapabileceğimiz listelere veya bizi dizginleyecek çerçevelere ihtiyacımız yok değil mi?
<br>
Bırakıyoruz, hayatlarımızı geri alacağız.
<br>
Özgürlük seçebileceğin 1001 fonttan daha fazlasıdır.

Teknoloji tarafından kurgulanmış "bağlantı" ikimizin arasında bir duvardır. İkimizin arasında olan bu "bağlantı" bizi gözetlemek, izlemek ve yönlendirmek için var.
<br>
Teknoloji kapitalizmin suretinden oluşur – asıl amacı insandan elde ettiği kârdır. Bize baskı yapmak, dikkatimizi dağıtmak, bizi boktan şeylerle meşgul etmek, bizi kendimizden ve çevremizdeki dünyadan uzaklaştırmak ve yabancılaştırmak için yaratılmıştır.
<br>
Aramızdaki ekran güvenlik sağlamaz.
<br>
Fikirlemizin düşmanı tarafından yapılmış bir ekran o. Birçok dosta ve düşmana zarar veren bir ekran.

Aramızdaki bağ ancak ekran olmadığında, yok edildiğinde var olabilir.

Aramızdaki bağ için kendimizden başka şeye ihtiyacımız yok. Yalnızca sen ve ben varız, olsa olsa en fazla mekansal aralıktır aramızda olan.

Teknoloji, günlük hayatta attığımız her adımın en ince ayrıntısına kadar kelimenin tam anlamıyla nüfuz etmek ister. Teknoloji, hafızamızın ve iletişim becerilerimizin yerini almak ister; kendimizle, yaşamlarımızla, etrafımızdaki insanlarla, içerisinde yaşadığımız dünyayla nasıl bir ilişki kurduğumuzu belirlemek ister.

Aramızda kurduğumuz <em>doğrudan iletişim</em> – yönetenlerin ve kapitalistlerin sahip olduğu teknolojik sapmalar içermemeli.

Aramızdaki bağ kendi norm ve değerlerimizi yarattığımızda, düşünceyi eyleme dönüştürebildiğimizde ortaya çıkar.
<br>
Sen ve ben, ilişkilerimizi ve etkileşimlerimizi yaratabilir, etrafımızdaki dünyaya şekil verebilir ve çevremize müdahale edebiliriz.
<br>
Gerçek dünya,
<br>
Deneyimlediğimiz, hissettiğimiz, bütünün bir parçası haline geldiğimiz dünyadır.


Aramızdaki yakınlık.
<br>
Geldiğimiz ve oyalandığımız yeriyle, tanımak için zaman ayırdığımız yeriyle; tartışmaları, anlaşmazlıkları, anlayışları ve sabırlarıyla.
<br>
Bizi aşan bir dünyadır.


Özgürlük ve eşitlik, bizi ezenlere karşı savaş verdiğimiz, bize ait olanı geri aldığımız yerde ortaya çıkar.
<br>
Hayatımızın her yönüyle hem de.


Aramızda olan şey, başkaldırımız, isyanımız ve imkanlarımız için bir oyun alanıdır.

Aramızda, bizi yok eden şeyi yok etmek için sonsuz bir potansiyel var. Seçtiğimiz ve karar verdiğimiz, kendimizi eylediğimiz ve yarattığımız şeylerle birlikte.

Aramızda teknolojik bir engel olmamalı.
<br>
Aramızdan
<br>
güç,
<br>
ateş,
<br>
isyan yükselmeli.

Aramızdan yaşam doğacak böylece.



