#title Hz. İbrahim, Nietzsche ve İbn-ül Arabi Dolayısıyla Şirk Olarak Devlet
#author Alişan Şahin
#date 23.02.2020 - 03.10.2021
#source 31.08.2023 tarihinde şuradan alındı: [[https://itaatsiz.org/hz-ibrahim-nietzsche-ve-ibn-ul-arabi-dolayisiyla-sirk-olarak-devlet-alisan-sahin-1/][itaatsiz.org]]<sup>1</sup>, [[https://itaatsiz.org/hz-ibrahim-nietzsche-ve-ibn-ul-arabi-dolayisiyla-sirk-olarak-devlet-alisan-sahin-2/][itaatsiz.org]]<sup>2</sup>, [[https://itaatsiz.org/hz-ibrahim-nietzsche-ve-ibn-ul-arabi-dolayisiyla-sirk-olarak-devlet-alisan-sahin-3/][itaatsiz.org]]<sup>3</sup>, [[https://itaatsiz.org/hz-ibrahim-nietzsche-ve-ibn-ul-arabi-dolayisiyla-sirk-olarak-devlet-alisan-sahin-4/][itaatsiz.org]]<sup>4</sup>
#lang tr
#pubdate 2023-08-31T13:01:05
#topics islam, din, devlet



<quote>
<em>İbrâhîm/içimdeki putları devir/elindeki baltayla/kırılan putların yerine/yenilerini koyan kim</em>

<em>güneş buzdan evimi yıktı/koca buzlar düştü/putların boyunları kırıldı/ibrâhîm/güneşi evime sokan kim</em>

<em>asma bahçelerinde dolaşan güzelleri/buhtunnasır put yaptı/ben ki zamansız bahçeleri kucakladım/güzeller bende kaldı/ibrâhîm/gönlümü put sanıp da kıran kim</em>

<right>
Asaf Halet ÇELEBİ
</right>
</quote>

Asaf Halet’in bu şiiri bize Hz. İbrahim’i söyler. Hz. İbrahim’in rasyonel bir çıkarımla putları kırmasına gönderme yaparak, putları kırmasını hatırlatarak İbrahim’e tekrardan bir çağrıdır bu. İbrahim gelecek ve putları kıracaktır. Çünkü putların yerine yeni putlar konmuştur.

Bir taraftan putların kifayetsizliğini hissettiren/sezdiren şeyin ne olduğunu da imler. Bu sezdirmeyi sağlayan özne nedir/kimdir? Bir diğer taraftan da put gibi taptığımız, sevdiğimiz, uğruna ölümleri göze aldığımız en güzel şeyleri put sanıp da onu yok edenin (put sanıp da gönlümüzü kıran) kim ve ne gibi bir şey olduğunu anlatır. Harika dizelerdir. Ben, seni ve insanı artı-eksi yönleriyle resmetmeye çalışır.

Hayatımızın her anında put yerine koyduğumuz ve putlaştırdığımız onlarca belki de yüzlerce nesne, fenomen ve hatta özne başka bir deyişle öteki şey var ki…

<strong>Kur’an-ı Kerim</strong>’de put ve İbrahim kıssasına dair ayetler oldukça fazladır. Talmud’un tefsiri olan Midraş’ta Hz. İbrahim’in (Talmud onu Abram olarak anarken İncil’de Abraham’dır) putları reddedip, Rab’a yönelmesi anlatılır. Kur’an ayetlerinde ise Hz. İbrahim’in özellikle onun Yahudi ya da Hristiyan olmadığı ama ilk müminlerden olduğu (Kur’an tüm peygamberleri hak peygamber ve Hristiyan ve Yahudilerin mümin olduğunu kabul eder) fikrini savunulurken Hz. İbrahim’e dair anlatılar oldukça fazla yer tutar. Hz. İbrahim örnek peygamberlerden önde olanıdır. Bu ayetlerden sadece bir kısmı şöyledir:


<quote>
<em>“Kendi nefsini aşağılık kılandan başka İbrahim’in dininden kim yüz çevirir? Andolsun biz onu dünyada seçtik, gerçekten ahirette de O salihlerdendir.”</em> (Bakara, 2/130)

<em>“Rabbi ona: ‘Teslim ol.’ dediğinde (O:) ‘Alemlerin Rabbine teslim oldum.’ demişti.”</em> (Bakara, 2/131)

<em>“Dediler ki: ‘Yahudi veya Hristiyan olun ki hidayete eresiniz.’ De ki: ‘Hayır (doğru yol) Hanif (muvahhid) olan İbrahim’in dini(dir); O müşriklerden değildi.’”</em> (Bakara, 2/135)

<em>“Deyin ki: ‘Biz Allah’a; bize indirilene İbrahim, İsmail, İshak, Yakub ve torunlarına indirilene Musa ve İsa’ya verilen ile peygamberlere Rabbinden verilene iman ettik. Onlardan hiçbirini diğerinden ayırdetmeyiz ve biz O’na teslim olmuşlarız.’”</em> (Bakara, 2/136)

<em>“Allah kendisine mülk verdi diye Rabbi konusunda İbrahim’le tartışmaya gireni görmedin mi? Hani İbrahim: ‘Benim Rabbim diriltir ve öldürür.’ demişti; o da: ‘Ben de öldürür ve diriltirim.’ demişti. (O zaman) İbrahim: ‘Şüphe yok Allah güneşi doğudan getirir, (hadi) sen de onu batıdan getir.’ deyince o inkârcı böylece afallayıp kalmıştı. Allah zalimler topluluğunu hidayete erdirmez.”</em> (Bakara, 2/258)

<em>“Ey Kitap ehli, İbrahim konusunda ne diye çekişip tartışıyorsunuz? Tevrat da İncil de ancak ondan sonra indirilmiştir. Yine de akıl erdirmeyecek misiniz?”</em> (Âl-i İmran, 3/65)

<em>“İbrahim ne Yahudi idi ne de Hristiyandı: ancak O hanif (muvahhid) bir Müslümandı, müşriklerden de değildi.”</em> (Âl-i İmran, 3/67)
</quote>

Hz. İbrahim elle dokunulur, insan eliyle yapılabilen ya da doğada bulunan herhangi bir nesneye ibadet edilmesi fikrine karşı olarak putlara karşı bir duruşu simgeler. Buradan hareketle ona dair kıssalar bize, bizim tarafımızdan nesnelere atfettiğimiz şeylerin temelde bir yanılgı olduğunu iddia eder. İsteyen ona tapar ve ibadet eder, ediyor da! İnsanoğlunun varolduğu andan itibaren böyle bir tapınma biçimi varolmuştur ve bugün halen bu hal tek tanrılı inanca sahip kişiler tarafından dahi devam ettirilmektedir.

Bunu bir mesel olarak ele alırsak, mesele esas teşkil eden ve belki de bugünün insanının anlam dünyasını zenginleştirecek olan İbrahim’in babası Azer’in inandığı ve onun korumasını istediği putları kırma düşüncesi üstüne daima düşünmek gerektiği kanısındayım. Hz. İbrahim’in kendi kendine sorguladığı hal akli olanı simgeler. Putlardan kalıcılık ve yaratıcılık görmeyince ay ve yıldızlara yönelir. Onların haşmetinin; ayrı ayrı geceye ve gündüze sahip olmak gibi hallerin sınırlandırılmış olmasının sorgulanması ve idrak edilmesi sonucu hepsine hakim olana yönelir. Her şeye hakim olan, görülen ve gücü ve kuşatmışlığı ile öncesiz ve sonrasız olanın <strong>kendini göstermesinden</strong> kaynaklı bir idrak etme hali değildir. Bu bir sezgi halidir.[1]

Böyle bir sahip olmanın sınırlılığından hareketle sınırsız hakim olan ve her şeye hakim olanı arar ve ona tapmanın en doğru olan olduğuna kanaat getirir.

Ve Hz. İbrahim’in putları reddedip Allah’a yönelmesini anlatan en teferruatlı anlatım Kur’an’da mevcuttur. İncil’de ve Tevrat’ta bu tarz bir teferruatlı bir anlatımın olmadığını iddia eder Carol Barkos:

<quote>
<em>“İncil, Hz. İbrahim’in erken dönemine dair detayları kısa ve öz olarak verse de, onun özel doğumu, erken gelişmesi, Tanrı’nın birliğini kavraması ve Nemrut’a karşı korkusuz isyanı yüzyıllar boyunca ona popülarite kazandırmış ve Yahudi, Hristiyan ve İslam kaynaklarında çeşitli biçimler almıştır. Bir put kırıcı olarak oldukça bilindik Hz. İbrahim imajı İncil’den ziyade başka kaynaklarda ve Kur’an’da Hz. İbrahim’in öne çıkan tasviri dolayısıyladır. Yaratılış anlatısında hikâyenin olmaması pek çok İncil okuyucusunu şaşırtmaktadır.</em>

<em>Yaratılış Kitabı, Abraham’ın babasının putperestliğinden ya da Abraham’ın tektanrıcılığa bağlanmasının nasıl olduğundan bahsetmez. Orda sadece Tanrı’nın ona göstereceği ülkeye gitmesi ve ülkesini terk etmesine dair sözlerini okuruz.”</em>[2]
</quote>

Kur’an’daki Hz. İbrahim anlatısını ele alan ayetlerin bir kısmı şöyledir:

<quote>
<em>“Andolsun, Allah’a sizler arkanızı dönüp gittikten sonra ben sizin putlarınıza muhakkak bir tuzak kuracağım.”</em> (Enbiya, 21/57)

“<em>Böylece o yalnızca büyükleri hariç olmak üzere onları paramparça etti; belki ona başvururlar diye.”</em> (Enbiya, 21/58)
</quote>

Putları kırdıktan sonra ay ve yıldızı ilah benimsemeyi düşündükten sonra onlarda kalıcılık görmeyince <em>“Gerçek şu ki, ben bir muvahhid olarak yüzümü gökleri ve yeri yaratana çevirdim. Ve ben müşriklerden değilim.”</em> (En’âm, 6/79) diye düşünür. Daha sonra ona karşı tepkiler ve sürece dair anlatılar Kur’an’da uzun yer tutar.

<quote>
<em>“Bizim ilahlarımıza bunu kim yaptı? Şüphesiz o zalimlerden biridir, dediler.”</em> (Enbiya, 21/59)

<em>“Kendisine İbrahim denilen bir gencin bunları diline doladığını işittik, dediler.”</em> (Enbiya, 21/60)

<em>“Dediler ki: ‘Öyleyse onu insanların gözü önüne getirin ki ona (nasıl bir ceza vereceğimize) şahid olsunlar.’”</em> (Enbiya, 21/61)

<em>“Dediler ki: ‘Ey İbrahim, bunu ilahlarımıza sen mi yaptın?’”</em> (Enbiya, 21/62)

<em>“’Hayır’ dedi. ‘Bu yapmıştır, bu onların büyükleridir; eğer konuşabiliyorsa siz onlara soruverin.’”</em> (Enbiya, 21/63)

<em>“Bunun üzerine, kendi vicdanlarına dönüp (kendi kendilerine) ‘Zalimler sizlersiniz, sizler!’ dediler.”</em> (Enbiya, 21/64)

<em>“Sonra yine tepeleri üstüne ters döndüler: ‘Andolsun bunların konuşamayacaklarını sen de bilmektesin.’”</em> (Enbiya, 21/65)

<em>“Dedi ki: ‘O hâlde Allah’ı bırakıp da sizlere yararı olmayan ve zararı dokunmayan şeylere mi tapıyorsunuz?’”</em> (Enbiya, 21/66)

<em>“’Yuh size ve Allah’tan başka taptıklarınıza. Siz yine de akıllanmayacak mısınız?’”</em> (Enbiya, 21/67)

<em>“(İbrahim) Dedi ki: ‘Siz gerçekten Allah’ı bırakıp dünya hayatında aranızda bir sevgi bağı olarak putları (ilahlar) edindiniz.’”</em> (Ankebût, 29/25)

<em>“Hani o Rabbine arınmış (selim) bir kalp ile gelmişti.”</em> (Saffât, 37/84)

<em>“Hani İbrahim babası Azer’e (şöyle) demişti: ‘Sen putları ilahlar mı ediniyorsun? Doğrusu ben seni ve kavmini apaçık bir sapıklık içinde görüyorum.’”</em> (En’âm, 6/74)
</quote>

Meselenin esasına geldiğimizde sorular şöyle olabilir: Hz. İbrahim’in ilk tektanrıcı olarak yaşamış olduğu deneyim bugün bize neyi anlatır ya da bundan bizim ne dersler çıkarmamız gerekir? Aslında sorunun aslı bu olsa gerektir. Kur’an’ın özellikle Hz. İbrahim’in deneyimini bu kadar çok ele alması ve özellikle göstermek istemesi neye delalet eder?

Meseleyi <strong>put</strong> ve <strong>tapınma</strong> etrafında ele almak Kur’an’ın aktardığı Hz. İbrahim kıssalarına dair muradı da açık eder kanısındayım.

Put nedir? Türk Dil Kurumu’nun açıklaması aynen şöyledir: “<em>1. İsim: <strong>Bazı ilkel toplumlarda doğaüstü güç ve etkisi olduğuna inanılan canlı veya cansız nesne, tapıncak, sanem, fetiş:</strong> 2. isim, din bilgisi: <strong>Haç.</strong>”</em>

Buna göre ikonalar, resimler ve haç da dahil her şey put olarak adlandırılabilir ki öyledir. Bu adlandırmaya temel teşkil eden şey bu putlara tapınılması ve olağanüstü meziyetler daha doğrusu tanrısal meziyetler atfedilmesidir. Putun put olarak orada durması mesele değildir. Asıl mesele onun tapınılacak olması ve ona ilahlık vasfının yakıştırılmış olmasıdır. Kur’an’ın Hristiyanlığın Haç fikriyatına dair duruşu da bu minvaldedir. Yani teslisi tanrının yerine koymasıdır. Hristiyanların bir bakıma duruşunda bu teslis anlayışının tanrıya eş koşma yani haça tapma olarak tezahür etmesinden dolayı islam onu şirk olarak görme eğilimine girmiştir. Hz. İsa bir peygamberdir, o bir hak peygamberdir ama onun haç çıkarma ve haç figürü ile putlaştırılması İslam’ın ve Hak din inancının karşısında bir haldir. Kısacası o yaratılan bir imajın nesnesi ve tanrıya eş koşan bir figür haline getirilmiştir.

Gel gelelim Hz. Muhammed’in bugünün İslam anlayışları içindeki yerine. Kendisine bir hak peygamber diyen ve insan olmaktan öte bir üstünlük atfetmeyen insan-ı kamil bir kişilikle karşı karşıyayız. Selefi ve benzeri İslam anlayışlarınca uydurulan binlerce hadisle yaşamı ve sözleri üstüne yalanlar uydurulan selefilerin “Hz. Muhammed’i” ile kimi hard-core ateistlerin “Hz.Muhammed’i” birbirinin aynısı olarak tezahür ediyor bugün. Bu “Hz. Muhammed” Amerikalı neo-con evangelistlerin “Hz. Muhammed’i” ile benzerdir. Bu “Hz. Muhammed” imajı, putlaştırılan ve adeta bir puta dönüştürülen bir Hz. Muhammed’dir. Bugün fotoğrafına sahip olmadığımız Hz. Muhammed imgesi adeta putlaştırılmıştır. Fakirlerin fakiri Hz. Muhammed ve Kur’an fakirlerin ellerinden alınmış zenginlerin, kralların, imparatorların “Hz. Muhammed”i haline getirilmeye çalışılmıştır. Bugün gerek Kur’an gerekse peygamber Hz. Muhammed gerçek oluşundan ve varlığından çıkarılıp “şirk” malzemesi haline getirilmektedir.

Hz. Ayşe, onun “politik” vakalara müdahalesi ve Peygamberin ölümünün hemen ardından iktidar kavgası başladıktan bir süre sonraları onun, Hz. Osman konuşurken, Hz. Muhammed’in gömleğini göstererek:

<em>“Bu Hz. Peygamberin elbisesidir, daha eskimedi, ancak Osman Onun sünnetini eskitti”</em> (Yakubi, Tarih, II, 175) ya da <em>“Daha elbiselerinin üzerindeki kokun duruyor; fakat şimdiden şeriatın eskidi !”</em> dediği rivayet ediliyor. Hz. Peygamber elbiseleri, saçı ve sakalı tapınılacak put haline getirilmiştir. Doğduğu ve hakkın rahmetine kavuştuğu gün anılıyor ya da kutlanıyor. Peygamber peygamberliğinden çıkarılıp Kur’an’ın fikriyatının aksine putlaştırılmıştır.

Bunun yanında Kur’an nüshalarına kutsallık atfedip onu evin en müstesna yerine koymakta olanlar nüshaları para ile satılan bir nesne haline gelmesine ve milyonlarcasının para karşılığı satılmasına sessiz kalmışlar ve çeşitli baskılarını yaparak satışa sunmuşlardır. Bu itirazı o nüshalara yani kağıda kutsiyet atfettiğimden – kağıda kutsiyet atfedenler için daha vahim bir vaka olmasına rağmen satışını bizzat onlar yapmakta – değil Allah kelamının serbest ve engelsiz herkese ulaştırılması gayesinin çiğnenmiş olmasından dolayıdır. Oysa o bir kelam idi ve akar idi. Kulaktan kulağa aktarılan Allah’ın bir vahiyinin bu akış içinde bozulmasına engel olmak için idi. Manayı kelamla aktarmak her daim önemlidir fakat bu akış sırasında bozulmalar her zaman mümkündür ve bilinen bir şeydir. Onu sabitlemek yazı ile mümkündür ki o yapılmıştır. Hasbelkader bunu da satılan bir ticari meta haline getirmek zaman içinde olmuştur. Bugün bu nesnelere kutsiyet atfetmenin adı putlaştırmaktır.

Hz. İbrahim’den kalkan bir Kur’an ve İslam anlayışı selefiler ve benzeri anlayışlar sayesinde Kur’an ve Hz. Muhammed’in putlaştırılması aşamasına gelip dayanmıştır. Bu putlaştırma saiki sadece Hz. Muhammed ile sınırlı kalmamış İslam coğrafyasında– Hristiyan ya da başka dinlerde de farklı değildir bu – veli ya da aziz sıfatı atfedilen her “ermiş” kişi ve hatta tarikat şeyhleri putlaştırmadan nasibini almıştır. Bu hal seküler dünyaya da hakim bir haldir. Kemalistlerin K. Atatürk’ü seküler dünyanın putuna bir örnek olarak gösterilebilir.

*** İbn-ül Arabi’de Putlara Dair

Peki, TDK’nın <em>“Bazı ilkel toplumlarda doğaüstü güç ve etkisi olduğuna inanılan canlı veya cansız nesne, tapıncak, sanem, fetiş”</em> tanımından hareketle Allah’a eş koşan yani şirk olarak adlandırılan nesneler, imajlar ve kişilikleri açığa çıkarmak salih insanların maksatlarından biri değil midir? Bunların açığa çıkarılması ve teşhir edilmesi elzem bir çalışma değil midir? Kur’an ve Hz. Muhammed’in fikri ve imani olarak kalkış noktası olan İbrahimi put ve şirke dair kavrayışların bugüne izdüşümleri sorumluluk alanlarımız içinde değil midir?

Bugün belki de binlerce özne, nesne ve kurum put ve şirk kavramları ile tanımlanacak durumdadır. Eğer bu kültürün evrensellik tanımı içinde aranacak şirk var ise görmek ve göstermek sorumluluk alanlarımız içindedir elbette. Bu sadece Müslüman, Hristiyan ve Musevilerin değil, herkesin görevi olmalıydı. Şöyle ki: en uç örnek olarak Ateistleri örnek verirsek, Dünya hayatında iktidar elde etmek ve büyüklük taslamak dindarlar için şirk olarak görülmesi gerekirken, ateist bile olsa, her insanın kendi kişisel erkini toplumlar ve kişiler üzerinde kurmak çabasında olan, dini olmasa da içinde büyüdüğü kültürün verileri dolayısıyla ve onun ürünü olarak bireyler kendi erklerini ona (kişi, hükümet, kurum vb.) teslim etmelerini isteyen güce karşı koymak zorundadır. Bu bireyin özgürlüğünün en önemli koşullarından biridir. Dindar biri için ise dünyevi özgürlük halini korumanın yanında o, Allah’ın yerini alma çabasında olduğundan dolayı (yargı erki, yönetme erki ve idare etme erkini zorla ya da çeşitli yöntemlerle rızasını alarak) şirk olarak görünmek durumundadır. Uhreviyat ile alakaları olmayanlar dünyevi hallerini ve özgürlüklerini (doğal durumlarını) korumak ve kendilerine sahip çıkmak durumundadırlar. Mesele insanlığın özgürlük problemiyle doğrudan ilişkilidir.

İbn-ül Arabi’nin put ve putlara dair aşağıdaki sözleri Kur’an’ın ifade ettiği put kavramını oldukça iyi ifade etmektedir.

<quote>
<em>“Bilmelisin ki, alem <strong>ancak</strong> Allah karşısında yoksul ve hor olabilir. Öyleyse <strong>Allah’tan başkasının karşısında zelilleşen ve başkasına muhtaç olan, ona bel bağlanan ve ona eğilen herkes, putperesttir</strong>. Muhtaç olunan ise, put (vesen) diye isimlendirilirken, muhtaç olan, onu ilah diye adlandırır. <strong>Putların en görünmezi heva, arzu gücü iken en kesifi, taştır</strong>.(Putlar latiflik ve kesiflikte) bu ikisinin arasında sıralanır.”</em>[3]
</quote>

Yani put sadece görünen ve dokunulan değildir. İnsan davranış ve duruşundaki haller de put ve şirk ile tanımlanmaktadır. Allah’ın sıfatlarının bir kısmı insana yakıştırılırken bir kısmı ondan azade kılınmıştır. Örneğin: Kibriya: en kibirli olan anlamına gelirken en büyük ve her şeye gücü yeten, her yerde hazır ve nazır olana ve tüm mülkün sahibi olan vb. gibi sıfatlarla tanımlanan Bir Allah’a eş koşmak insanlar arasında yaygın bir hal halini almıştır. Bunun yanında her gün namazında ve niyazında olan, iyilik, doğruluk, ahd-e vefa, diğerkâmlık vb. meziyetlerine övgüler dizen insanlar bu putların öyle ya da böyle savunuru, hayranı ve tapınanı olmuştur. Akli ve nakli hiçbir şeyin onları uyaracak hali ve mecali kalmamış görünmekte.

Görünmez ama bir o kadar da dokunulabilen nesne olarak put; içimizde ve yaşamımıza içkin olan en önemli put para ve para kazanma hırsı olarak duruyor. Para kazanma hırsı çağdaş toplumların insanı için kritik önemdedir. Bugünün insanı bunu elde etmek maksadıyla varlığını ve şuurunu kaybetmiş durumda… Kapitalizm adı verilen düzen ulus devleti ile beraber her yerde hazır ve nazır ama elde edilmeyi bekleyen görünür-görünmez ama hayatı bütünüyle etkileyen para putunun adına inşa edilmiş bir devletin düzenidir. Uğruna ölünen ve öldürülen, insanların kendilerini feda ettikleri zıvanadan çıkmış bir hırsın elde etmek için çalıştığı bir hedeftir para. Para, hırs ve kibrin adıdır. O, devlet ve onun düzenlediği değerler sistemi ile ve suretiyle var. Bu put kapitalist toplumun – diğer toplumsal sistemleri bundan bigane kılmıyorum ama onun asıl sahibi kapitalizmdir – canıdır. Ona isteyerek ya da istemeyerek herkes tabi olmak ve çoğunluk için tapınılmak ve “mülk”ün sahibi olmak durumundadır. Para ile mülkün sahibi olunacağı sanılmaktadır. Asıl sahibinden ziyade mülk artık paranın ve “devlet”indir!

<em>“Aklın zorunlu hükmüne göre ilah etkiye konu olmaz. Onların taptığı ilah ise, kendisi ile oynanan bir ahşap veya temizlikte kullanılan bir taştır.”, “İşler Allah’ın elindedir ve işlerdeki hüküm Allah’a aittir”</em>[4] der, İbn-ül Arabi. Hiçbir etkiye konu olmamak üzerine tartışma yürüten Arabi, bilinmek isteyip ademi yaratmasıyla bir alakaya (tanınmak, bilinmek için) tabiidir. Bunu bir etki olarak adlandırmaz anlaşılan.

İnsan ve Allah arasındaki bu tanınma/bilinme ilişkisi Allah’ın kendi yerine başka ilahlar arayan insanları farklı şekillerde tanımlamasına vesile olmuş görünür. Buradaki tutarlılık dikkat çeker. Bundan dolayıdır ki, Müşrik ve Kafir arasında fark vardır. Hristiyan ve Museviler ile Müslümanlar arasındaki farka dikkat çeker ve açıklama yapar İbn-ül Arabi. Mesele gene şirk etrafında döner.

<quote>
<em>“Bilmelisin ki bütün müşrikler kafirdir. Çünkü müşrik, şirk koştuğu ve kendisini ilah edindiği şey hakkında arzusuna uyar, İlah’ın mutlak birliğinden yüz çevirir ve İlah’ın birliğine götüren ayet ve delilleri incelemekten onları gizler. İşte, zahirde ve batındaki bu örtme davranışı nedeniyle de, kafir (örten) diye isimlendirilirken Allah ile birlikte ilahlığı Allah’tan başkasına nispet ettiği için de müşrik diye isimlendirilmiştir. Böylelikle müşrik, iki nispet kabul etmiştir. İşte müşrik ile kafir arasındaki fark budur.”</em>[5]
</quote>

Böylece daha ince bir noktaya gelerek der ki:

<quote>
<em>“Hz. Peygamber bize ehl-i kitaba aykırı davranmayı emretmiştir. Bunun nedeni, onların kitabın bir kısmına inanıp bir kısmını inkar ederek bunun ortasında bir yol tutmayı istemiş olmalarıdır. Hz. Peygamber ise bize belirli hükümlerde ve zikrettiğimiz hususta [putperestliğe karşı çıkmak ve Allah’ın birliğine inanmak-b.n.-] karşı çıkmayı emretmiştir. Onlara bütün konularda karşı çıkmayı emretmiş olsaydı hiç kuşkusuz ki, bize emretmiş olduğu imana da aykırı davranmamız gerekirdi. Öyleyse ehl-i kitaba kayıtsız anlamda karşı çıkmak, geçerli değildir. İşte bu ‘ehl-i kitaba muhalefet edin’ hadisinin anlamıdır.”</em>[6]
</quote>

Anladığımız kadarıyla Kur’an ayetlerindeki en temel önerme şirk kavramı etrafında odaklanmaktadır. İnsanı insan yapan özelliklerin başında Allah’a ibadet, ona eş koşmamak gelmektedir. Bu bizce insanın doğal haline (doğal “hukuka”) uymak maksadıyladır. Bu doğal hali bozan ise gene insandır. Kur’an baştan sona doğal halin bozulmasına dair yapılan tüm girişimlere ilahi cevap amacında görünür.

*** Bir Put Olarak Devlet

Hz. İbrahim’in elindeki edilgen putlar ona atfedilen meziyetlerden ve bu meziyetlerin kendisinden-kendisine işleyen etkisinden dolayı bir anlamlandırma üzre ilahtırlar. Atfedilen meziyet atfedeni etki altına almakta ve belki de onu rahatlatmaktadır. Bu rahatlatma hali bir çeşit sağaltım etkisi de yapmaktadır. İşte bu yanılsamaya karşı Hz. İbrahim’in putlar ve İlah sorgulaması bir açıklama ve rasyonel sorularla nesneye yönelmeye karşı koyma olarak durur. Hiçbir şeye yaramayan taşın insanın ruh halini değişikliğe uğratan bir etkiden dolayıdır bu.

Katı nesnelerin insanın ruh haline etkisi böyle iken günümüz dünyasında söz, kavram, isim ve özellikle ideoloji ve dinin kendisi de bu katılıkla bir put haline gelmiş gibi görünür. Katı nesnelerde, her zaman olmasa da, katılaşan ve insanı etkisi altına alan her şeyde putlaşma/putlaştırma/tapınmaya meyyal verme hali göze çarpar. Fikir ve dinin katılaşması ideoloji haline gelerek kurumsallaşması bu katılaşmaya iyi bir örnektir. Sonuçta değişen hakikatler evreninde hiç değişmeyecek olarak görülen her şey putlaştırma olarak tanımlanabilir.

Devletin evveliyatı kurcalandığında bugünün devleti ile 100 yıl, 200 yıl ve 500 yıl öncesinin devlet adı verilen örgütlenmeleri arasında farkların olduğu ve bugünün devletinin hiç birine benzemeyecek denli hiyerarşik ve topluma nüfuz eden totaliter bir yapı olduğu fark edilebilir.

Orta Çağ denilen döneme bakıldığında otarşik köy toplulukları ve merkezi devletle kırsal alanların ilişkisinin hemen hemen olmadığı görülebilir. Bu dönemlerin kent topluluklarından bir kısmına kent komünleri diyen anarşist teorisyenler dahi mevcuttur. Bu döneme katılaşmamış devlet öncesi dönem demek çok yanlış bir tanım olmaz. Bugün insanlığın var olduğu tahmin edilen tarih 300 bin yıl önceye gitmekte. Devletin ortaya çıktığı tarihi en kaba olarak iki bin üç bin yıl önceye götürmek mümkün fakat bu devletlerin bugün devlet adını verdiğimiz devlet ile çok alakası yoktur.

Devleti yerleşiklik, nüfus yoğunlaşması ve şehirleşmeden ayrı düşünmek biraz zor görünmektedir. Tabi buna meyyal olan sadece bunlar değildir. Anarşist antropoloji çalışmaları devletin ortaya çıkışına dair bunların birbirini etkilediği çeşitli faktörleri bize sunmaktadır. H. Barclay’in <em>“Devlet’in Kökeni”</em>[7] isimli çalışması bunlardan sadece bir tanesi ve en önemlilerinden biridir. Katılaşmaya etki eden bir diğer etken ideoloji olarak ifadesini bulur. Bu ise katılaşmanın yani putlaşmanın zihniyetini ifade eder.

Bu kötüye giden süreci hiçbir etki-tepkiye uğramayan bir süreç olarak düşünmemek gerek. Bu mücadeleler tarihidir. Devletleşme mücadelesine karşı yerleşik alanlarda buna karşı doğal tepki mekanizmalarına sahip oluşumlar da mevcut olmuştur. Orta Çağ toplumlarında Kropotkin’in ifade ettiği gibi esnaf teşkilatları bu direniş alanlarından biri olarak görülür. Özellikle Orta Doğu’da ve Selçuklu-Osmanlı “sınırları” içinde Fütüvvet-Ahi teşkilatı bu esnaf teşkilatlarının başında gelmektedir. Fütüvvet-ahi örgütlenmesi tüm prensiplerini Kur’an’a dayandırmaktadır. Bir diğer tarafta ise akmakta olan, tutulamayan, mülk ve mülkiyete doğası gereği meyyal olması hiç de mümkün olmayan göçerler devletten azade var olan dikkate şayan “örgütlenmeler”dir.

Bugünün devleti eski zamanların tiranları, firavunları ve imparatorları gibi kendini tapınacak bir put – İlah – olarak sunar. O, Allah’ın yerini almaktadır. O Allah’a eş koşmaktadır. Baştan belirtelim ki Devlet şirktir. Allah’ın 99 ismine bakıldığında bu isimlerin ve sıfatların onu büyüklüğü, hakimiyeti, affediciliği, her yerde hazır ve nazır olduğu vb. gibi sıfatlarının, kendi suretinden var ettiğinden dolayı, insanda var olduğu görülür ki bunlar Kur’an vasıtasıyla ilan edilmiştir. Kur’an vasıtasıyla bu sıfatlardan hangisinin insanın yapması ve yapmaması da belirtilmiştir.

Bunlardan bir kısmı şöyledir:

<em>“Er-Rahmân: Dünyada bütün mahlükata merhamet eden, şefkat gösteren, ihsan eden.</em>

<em>El-Melik: Mülkün, kainatın sahibi, mülk ve saltanatı devamlı olan.</em>

<em>El-Cebbâr: Azamet ve kudret sahibi. Dilediğini yapan ve yaptıran.</em>

<em>El-Hâlık: Yaratan, yoktan var eden.</em>

<em>El-Kahhâr: Her şeye, her istediğini yapacak surette, galip ve hakim olan.</em>

<em>El-Alîm: Gizli açık, geçmiş, gelecek, her şeyi en ince detaylarına kadar bilen.</em>

<em>El-Hakem: Mutlak hakim, hakkı batıldan ayıran. Hikmetle hükmeden.</em>

<em>El-Adl: Mutlak adil, çok adaletli.</em>

<em>El-Celîl: Celal ve azamet sahibi olan.</em>

<em>Eş-Şehîd: Her zaman her yerde hazır ve nazır olan.</em>

<em>El-Mümît: Her canlıya ölümü tattıran.</em>

<em>El-Hayy: Ezeli ve ebedi hayat sahibi.”</em>

Bugün kendi kültürel oluşumuz içinde içselleştirdiğimiz birçok norm (adet ve gelenek vb.) ortaya çıktığı halinden aşınmalar geçirerek ve başka birçok kültürel etkilenmeleri ve farklı faktörlerle değişim geçirmiştir ama büyük bir kısmı bunun üstüne bina edilmiş görünmektedir. Devlet ve din ilişkisinde, devletin Allah’ın yapma dediği ve eş koşma ya da şirk olarak nitelediği sıfatlarını alarak tarihsel süreç içinde daha da artarak bir puta dönüştüğü, “Allah”ın sıfatlarını kendine mal ettiği aşikardır.

Devlet her yerde hazır ve nazırlığını (<em>Eş-Şehîd</em>) hayatımızın her anına müdahale etme haliyle, yargı ve hukuk yoluyla hikmetle hükmettiğini iddia edip gerekirse öldürme (<em>El-Mümît)</em>, cezalandırma, yargılama hakkını kendinde görerek, <em>El-Hakem ve El-Adl</em>; sınırlar koyarak ve topraklara ve insanlara sahiplik ettiğini iddia ederek (vatandaşlık vb.), <em>El-Melik</em>; her şeyi bilme amacı ile her alanda müdahale ederek, enformasyon toplayarak <em>El-Alîm</em>; büyük ve yıkılmaz olduğunu iddia ederek, <em>El-Hayy</em> olduğunu iddia eden şirk mekanizması ya da örgütlenmesidir.

*** Nietzsche, Din, Devlet ve Put

İlk putkırıcı olarak anılmak elbette Hz. İbrahim’e nasib olmuştur. Fakat yeni zamanların putlarına işaret etmek ve bu putların yüzlerindeki örtüyü kaldırmak Nietzsche’nin felsefeden başlayıp teoloji ve psikolojiye kadar değişik alanlara nüfuz eden fikirlerine özgü olarak durmuştur. Her ne kadar Hristiyanlık üstüne yazdıklarında Budizmi daha makul bir din olarak işaret etse de İslamı da sevdiği ve Hafız üzerinden ona referanslar verdiği bilinen bir halidir. Put kırıcılığıyla Hz. İbrahim’in çizgisinin devamcısı olarak adlandırmak çok da yanlış olmaz.

Allah’ın insanlara yapma dediklerini zorbaca elde eden tiranlar, imparatorlar ve firavunlar bir yana insanlardan yetki aldığını zor ve rıza mekanizmasını kullanarak elde eden “demokratik” devlet te şirkin yüz değiştirmiş halidir. Monarşik ya da Cumhuriyet, tüm devletlerin tek temel özelliği Allah’a eş koşmaktır. Bunun için rıza yani onaylanma için yapmayacağı şey yoktur. Her türlü manipülasyon, yalan, gözboyama ve hatta hurafeyi kullanır. Kutsal metinleri, peygamberlerin hayatını ve yolunu bozar, kendine uygun hale getirir.

Öyle ya Nietzsche boşuna seslenmedi yıllar önce. “<em>Yeni Put Üstüne</em>” adını verdiği bölümde devletten bahseder:

<quote>
<em>“</em><em>Devlet mi? O da ne? Peki! Şimdi bana kulak verin, size ulusların ölümünden söz açacağım.</em>

<em>…</em>

<em>Bütün soğuk canavarların en soğuğuna devlet denir. Soğuk soğuk yalan söyler o; ve ağzından şu yalan sürüne sürüne çıkar: ‘Ben, devlet, – ulusum ben.’”</em>

<strong><em>“Yeryüzünde benden büyüğü yoktur: düzenleyen parmağıyım ben</em></strong> <em><strong>Tanrının</strong>” — böyle böğürür o canavar. Ve yalnız uzun kulaklılar ve kısa görüşlüler değildir diz çökenler!</em>

<em>…</em>

<em>“Devlet derim ona, herkesin ağı içtiği yere, iyilerin ve kötülerin: devlet, herkesin kendini yitirdiği yer, iyilerin ve kötülerin: devlet, herkesin ağır ağır kendi canına kıymasına ‘hayat” denen yer.</em>

<em>…</em>

<em>“Bana hepsi çılgın görünür bunların ve tırmanan maymun ve azgın görünür. Burnuma kötü kokar <strong>putları</strong>, o soğuk canavar: hepsi de kötü kokar burnuma, bu <strong>putperestlerin</strong>!</em>

<em>…</em>

<em>Orda, devletin b i t t i ğ i yerde, orda başlar gereksiz olmayan insan: orda başlar gerekli kişilerin türküsü, o eşsiz, o benzersiz ezgi.</em>

<em>Oraya, devletin b i t t i ğ i yere, —oraya bak, kardeşim! Görmüyor musun gökkuşağını ve köprülerini Üstinsanın?”</em>[8]
</quote>

Sadece en “inananlar” ve Allah’a en “sadık” olanlar değildir Devlete tapanlar. Devletin kötü olduğunu bilenler de bugün ona tapmaktadır. Devletin, tahakkümün ve kötülüğün kaynağı olduğunu bilenler dahi devlete tapmakta… Allah’ın yerini almak için her türlü yöntemi kullanan bu kötü canavar ruhları da esir almıştır.

<em>“En iyimizi kendi şöleni için kızartan o eski <strong>putperest</strong> <strong>rahip</strong>, i ç i m i z d e barınır daha. Ah, kardeşlerim, ilk doğanlar nasıl kurban olmazlar!”</em>[9]

Nietzsche adeta, aynen İbn Arabi’nin Fütühat-ı Mekkiye’de bahsettiği bir hadiste işaret edildiği gibi <em>‘ehl-i kitaba muhalefet edin’</em> sözünün gereği olarak davranmakta:

<em>“ – ‘Peki sen’, dedi Zerdüşt, gezgin ve gölgeye, ‘sen kendine özgür ruh mu diyorsun, kendini özgür ruh mu sayıyorsun? Demek burda böyle putperestliğe ve papalığa giriyorsun ha?’”</em>[10]
<em>Deccal, Hristiyanlığa Lanet</em> kitabında Hristiyanlığı putperestlik olarak algılar ve “ehl-i kitaba” muhalefet eder. Hz. İsa’yı değil, Hristiyanlığı ve onun uygulamalarını, Hz. İsa’nın –aynen Kur’an’ın yaptığı gibi – putlaştırılmasını eleştirir.

Mesela şurada:

<quote>
<em>“- Geri dönüyorum, Hristiyanlığın sahici tarihini anlatıyorum. – Daha ‘Hristiyanlık’ sözcüğü bile bir yanlış anlamadır – aslında, <strong>tek bir Hristiyan vardı, o da çarmıhta öldü</strong>. “Evangelium” çarmıhta öldü. O andan başlayarak “Evangelium” adını alan her şey, daha o anda, onun yaşadığının karşıtıydı: ‘kötü haber’di, bir dysangelium’du… yalnızca Hristiyanca bir pratik, çarmıhta ölenin yaşadığı gibi yaşanmış bir yaşam, Hristiyancadır…”</em>[11]
</quote>

Bundan sonra işleyen süreci ve aslında hiyerarşik örgütlenmesiyle Tanrı ve insan arasına giren rahip ve piskoposlarından ve papalarından oluşan kilisesiyle yalana ve puta dönüşen din ve din adamları ve resmi azizleriyle başka bir devlet örgütlenmesinin oluş ve yalanlarını anlatır. Hz. Muhammed’in İslam’ı değil – çünkü İslam Allah ile kul arasına bir aracı koymaz ve bu kesindir – ama İslam ülkelerindeki benzer kurumlar ve örgütlenmeler, hatta özellikle bugünün tarikatlarının esas olarak aldığı hale benzer şekilde bir tablo çizer Nietzsche.

<quote>
<em>“ – Çevreme bakıyorum: bir zamanlar ‘hakikat’ denen şeyin tek bir sözü bile kalmamış ortada, bir rahip ‘hakikat’ sözcüğünü daha ağzına bile alınca, dayanamaz hale geliyorum. Dürüstlükle en ufak alışverişi olan kişi, bugün bilmek zorundadır ki, bir Tanrıbilimci, bir rahip, bir papa, söylediği her tümceyle, yalnızca yanılıyor değil, yalan söylüyordur, – artık elinde de değildir, ‘masumca’, ‘cahilce’ yalan söylemek. Rahip de herkes gibi bilir artık ‘Tanrı’nın[12] olmadığını, ‘günahkar’ın, ‘kurtarıcı’nın olmadığını, – ‘özgür istem’in, ‘ahlaksal dünya düzeni’nin yalanlar olduğunu : – tinin içinde bulunduğu sıkıntı, derin kendini aşma gereksinimi, artık hiç kimsenin bunları bilmemesine izin vermiyor. Kilisenin bütün kavramlarının ne olduğu artık ortaya çıkmıştır, en berbat kalpazanlıklar oldukları, doğayı, doğal değerleri değersizleştirmek amacını taşıdıkları; rahibin kendisinin de ne olduğu ortada, en tehlikeli asalak türü, yaşamın sahici zehirli örümceği… Biliyoruz, vicdanımız biliyor bugün – , rahiplerin ve Kilise’nin bu korkunç buluşlarının değerinin ne olduğunu, neye yaradıklarını, nasıl, insanlığın iğrenç bir görünüm kazanabilmesine yol açan bu kendini aşağılama durumuna ulaşılmasını sağladıklarını… Herkes biliyor bunları: ve gene de her şey eskisi gibi duruyor.”</em>[13]
</quote>

Anlattığı ve itiraz ettiği puttur. Nietzsche, İsa’nın çarmıha gerilişini anlattıktan sonra ortaya çıkan dini ve sapmaları değişik şekillerde çürütme yoluna gider. Şöyle devam eder:

<quote>
<em>“- Ve o andan başlayarak saçma bir sorun çıktı ortaya : “Nasıl olabildi de Tanrı buna izinverdi?” Buna, küçük topluluğun çarpılmış aklı bir o kadar korkunç saçmalıkta bir yanıt buldu: Tanrı günahların bağışlanması için oğlunu kurban vermişti. Nasıl da tek bir vuruşta sonu gelmişti Evangelium’un! Suça karşılık kurban düşüncesi, hem de en iğrenç, en barbarca biçimiyle: suçlunun günahları için, suçsuzun kurban edilmesi! Ne denli tüyler ürpertici bir putataparlık!İsa, oysa, ‘suç’ kavramının kendisini yok etmişti.Tanrı ile insan arasındaki uçurumu yadsımış, tanrı ile insan arasındaki o birliği, kendi “iyi.</em> <em>Haber”i olarak yaşamıştı… Kendi ayrıcalığı olarak değil! – Artık adım adım, kurtarıcı tipine eklemeler yapıldı: Yargılama ve geri-dönüş öğretisi, bir kurban ölümü olarak ölüm öğretisi, diriliş öğretisi, ki bununla, bütün bir “kutsanmışlık” yerine de ölümden sonraki bir durum koyarak!… Paulus, bu yorumu, bu pespaye yorumu, o her zamanki hahamvari arlanmazlığıyla, şöyle mantıklaştırdı: « Eğer İsa ölüyken dirilmezse, o zaman inancımız boşunadır». —Ve bir seferde, Evangelium, yerine getirilemeyecek vaatlerin en aşağılığı, arlanmaz bir kişisel ölümsüzlük öğretisi haline geldi… Paulus ayrıca bunun bir ödül olacağını öğretti!”</em>[14]
</quote>

Son olarak ifade etmeliyim ki “ehl-i kitaba muhalefet” edilmesini söyleyen Hz. Muhammed bir bozuşmaya ve putlaştırmaya itirazını dile getirir. “Ehl-i kitap” içinde olanlar zamanında Hristiyanlar, Yahudiler olarak ifade edilirken bugün buna Müslümanlar da, ehl-i kitap olduğundan dolayı, dahil edilmek durumundadır.

Puta tapar gibi nesneye (mesela paraya), sakala, türbelere, önderlere, liderler ve özellikle devlete tapanlar; soyut kavramlar olan halk, millet, milliyetçilik ve benzeri kavramlara kutsiyet atfedenler; Hakikat ve Hak’tan başkasına – Hak ve hakikat yerine – Kur’an’da ifade edildiği gibi “malınız ve çocuklarınız bir fitnedir (sınavdır)” (Teğabün Suresi, 15. Ayet) – kendi putlarına biat edenler puta tapanlardır. Allah’a eş koşmaktadırlar.

Ve son olarak, ve tekrar, bu ve yeni putları yıkmak gerekir. İçimizde ve dünyamıza sireyet etmiş putları yıkma çabası dışımızda bize hükmeden putları yıkmak için en elzem olanıdır. Kendimizden başlayarak yıkmamız gereken putların yanında en büyük putu – tahakküm putunu – yıkmadan özgürleşemeyiz. En büyük putun görünen ve kendine ad vereni ise devlettir. Ve o devlet şirktir.

[1] Elbetteki adı Kur’an’da vahiydir. Sezgiye ise kimi yerlerde ilham denitor. Söz veya yazı tarzında aktarıldığında vahiy (revelation) de denmektedir. Kavram, İslamiyet gelmeden önce de kullanılmaktaydı.
[2] Carol Bakhos. The Family of Abraham: Jewish, Christian, and Muslim Interpretations, 3. The First Monotheist
[3] İbn-ül Arabi, Futuhat-ı Mekkiye, 10. Cilt, 265. Bölüm, Musevi Makamdan, Putlardan Uzak Durma Menzilinin Bilinmesi. S. 63.(<em>Vurgular bana ait</em>)
[4] İbn-ül Arabi, Futuhat-ı Mekkiye, 10. Cilt, 265. Bölüm, Musevi Makamdan, Putlardan Uzak Durma Menzilinin Bilinmesi. S. 63
[5] İbn-ül Arabi, Futuhat-ı Mekkiye, 10. Cilt, 265. Bölüm, Musevi Makamdan, Putlardan Uzak Durma Menzilinin Bilinmesi. <em>S. 67</em>
[6] İbn-ül Arabi, Futuhat-ı Mekkiye, 10. Cilt, 265. Bölüm, Musevi Makamdan, Putlardan Uzak Durma Menzilinin Bilinmesi. S. 67 (Alıntıda köşeli parantez içindeki not bana aittir)
[7] H. Barclay’in “Devletin Kökeni” adlı kısa çalışması itaatsiz.org’da seri yazı şeklinde yayınlanmıştır. İlgilisi şu adrese bakabilir: [[https://itaatsiz.org/2019/09/15/devletin-kokeni-1-herold-barclay/]]
[8] F. Nietzsche, Böyle Buyurdu Zerdüşt, S. 64-68, T. Oflazoğlu Çevirisi, Bilgi Yayınevi, 1964 (Vurgular bana ait)
[9] F. Nietzsche, Böyle Buyurdu Zerdüşt, S. 218, T. Oflazoğlu Çevirisi, Bilgi Yayınevi, 1964 (Vurgular bana ait)
[10] F. Nietzsche, Böyle Buyurdu Zerdüşt, S. 337, T. Oflazoğlu Çevirisi, Bilgi Yayınevi, 1964
[11] Deccal, Hristiyanlığa Lanet, F. Nietzsche, Çev. Oruç Aruoba, S. 25 (Vurgular bana ait)
[12] <em>“artık ‘Tanrı’nın olmadığı”</em> şeklindeki ifade ve moda tabirle kullanılan <em>“Tanrı öldü”</em> cümlesini Nietzsche’nin özellikle din adamları ve <em>“köle ahlağına”</em> sahip olanların dünyasında çirkeflik ve yalanlara karşı bir yanıt olarak okuyorum. Oysa onun hem dili ve özellikle <em>“Böyle Buyurdu Zerdüşt”</em> kitabı adeta bir kutsal metin diline sahiptir. Tanrı’nın insanlar tarafından öldürüldüğü iddiası Tanrı’nın varolmadığı iddiası değildir. Böyle bir iddia ispatı gerektirir ki Nietzsche buna girmez. Böyle bir iddiası yoktur. Kendisinin son mektuplarından birini “Çarmıha Gerilen Dianisos” olarak imzalaması da Hz. İsa’ya dair sevgisinin ifadesidir.
[13] Deccal, Hristiyanlığa Lanet, F. Nietzsche, Çev. Oruç Aruoba, S. 24
[14] Deccal, Hristiyanlığa Lanet, F. Nietzsche, Çev. Oruç Aruoba, S. 25
