#title Neden Parlamento Bir Hiledir?
#author Andrew Flood
#SORTtopics anarşizm, anarşist bakış
#source "Why Parliament Is Fraud", Workers Solidarity Movement.
#lang tr
#pubdate 2020-06-28T15:35:19
#notes  Çeviri: Anarşist Bakış <br> Çevirenin Notu: Metine yapılan eklemeler, açıklamalar vb, [...] ile gösterilmiştir


 *Kesinti, kesinti ve daha fazla kesinti karşılığında, seçimlerde güneşin, ayın ve yıldızların söz verilmesine alıştık artık. Bunun sebebi bütün siyasetçilerin yalancı olması mı sadece, yoksa bunun daha derinlerde yatan nedenleri mi var? Seçimlere katılmamak, Bakunin'in döneminden beri anarşist bir taktik olarak süregelmiştir. Bu makalede anarşistlerin seçimlere katılmamayı veya geçersiz oy kullanmayı savunmasının nedenlerinden bazılarını ele alacağız.*

Oy kullanma hakkı, işçilerin (ve kadınların oy kullanma hakkını savunan kadınların!), son birkaç yüzyıldaki zorlu mücadeleriyle kazandıkları [haklardan] birisidir. Bir diktatörlükte yaşamaktansa, parlamenter bir demokraside yaşamanın tercih edilir olduğu gayet açık. En kusurlu demokrasiler bile, diktatörlüklerin asla kabul etmeyeceği bazı haklardan --sendikaların görece bağımsızlığı, kısıtlı gösteri hakkı, belli bir miktar ifade özgürlüğü gibi-- vazgeçmek zorunda kalmışlardır.

Ancak, sendika karşıtı yasanın 31inci maddesinden ve milliyetçilerin Belfast şehir merkezinde yürüyüş yapmalarının engellenmesinden anlaşıldığı üzere, bunların hiçbirisi mutlak [olan haklar] değildir. Özgürlüğün miktarı, patronların sistemin akışını devam ettirmek üzere vermeleri gereken [özgürlük miktarı] tarafından, artı işçilerin mücadelesi sonucunda vermek zorunda kaldıkları [özgürlük miktarı] tarafından belirlenir.

Parlamentonun gerçek amacı, tüm halkın arzularına göre ülkenin yönetilmesini sağlamak, tüm görüşleri eşit olarak değerlendirmek değildir. Parlamento, bunun yerine, ardında kapitalizmin gerçekte idare edilmesi faaliyetinin devam ettirildiği demokratik bir maske sağlar.

Goodman meselesi ve birkaç yıl önce İrlanda Sigorta Şirketi'nin kurtarılması, gerçek kararların büyük endüstri şirketlerinin yönetim ofislerinde nasıl alındığını gözler önüne serdi. Pek ihtimal dahilinde olmasa da, seçilen hükümetin patronların gözünde <em>çok ileri</em> gitmesi halinde, [patronlar hükümeti görevden] uzaklaştırmak için gerekli araçları kullanmakta oldukça çabuklardır.
<br>

*** MASKENİN ARKASINDAKİ

Bunun belki de bilinen en iyi örneği, 1972'de Şili'de demokratik bir şekilde seçilen Allende hükümetinin uzaklaştırılmasıdır. [Allende hükümeti], kısıtlı bir reform paketini çıkarmaya ve bazı büyük Amerikan sanayilerini millileştirmeye çalışmıştı. Sonuç ise CİA tarafından desteklenen bir askeri darbe oldu.

Şili'deki işçiler, kendilerini özgürleştirmek üzere seçilmiş az sayıdaki temsilciye dayanmaları nedeniyle siyasi olarak silahsızlanmışlardı. Orduya karşı örgütlü direniş çok kısıtlıydı, ve [darbenin] hemen akabinde 30.000'den fazla militan öldürüldü ve 1.000.000 kişi ülkeden kaçarak sürgüne gitti.

Ancak, kapitalizm pratikte nadiren bu tip yöntemlere gereksinim duyar; medya üstündeki kontrolleri ve siyasi partilerin finansman için büyük iş alemine dayanmaları, denetim için yeterlidir. İrlanda ve Britanya İşçi Partileri gibi örgütler, zamanlarının çoğunu Tories veya Fianna F‡il kadar iyi bir şekilde kapitalizmi idare edebileceklerini ispatlamakla geçirirler.

Onlar, kendi politikalarının grevlerden ve diğer sınıf mücadelesi biçimlerinden sakınmanın yolu olduğunu iddia ederler. Sınıf işbirliği politikalarının, lokavtlara ve sendika göçermeye dayanan katı sınıf mücadelesine göre, kapitalizm için daha etkin olduğunu söylerler.

Patronlar açısından, bu genellikle iyi bir argümandır, bazen endüstriyel barış karşılığında birkaç kırıntıdan vazgeçmeye değer. Diğer zamanlarda, ciddi bir kriz ücretlerin veya yaşam standartlarının bastırılmasını gerektirdiğinde ise, her zaman bu hükümeti kesintiler yapmaya zorlayabilirler; veya genel seçim sürecini hızlandırabilirler veya --aşırı durumlarda-- polis devletini kullanabilirler.

*** EKONOMİK VE SOSYAL İLERLEME PROGRAMI (PESP) MANTIĞI

Bu tip bir mantığın sosyalizmle hiçbir alakası yoktur. Aslında şu anki Fail/PD hükümeti --daha önceleri ise PNR--, Ekonomik ve Sosyal İlerleme Programı [<em>ing. Programme of Economic and Social Progress</em>] sayesinde aynı mantığı başarıyla uyguluyorlar. Bu anlaşmanın anlamı, artık sendika bürokratları enflasyonun altındaki bir ücret artışı karşılığında fiilen grevleri durduracak ve sabote edeceklerdir. Yani şirket kârlarının ikiye katlandığı, İrlanda ekonomisinin görece bir "patlama" dönemi yaşadığı bir zamanda, İrlandalı işçiler ücret ve istihdam anlamında gerçek kayıplara maruz kalmakta ve sosyal ücret (sağlık, eğitim vb. dahil) bağlamında ise mevzi kaybetmektedir.

İşçi ve Emek Partileri PESP'nin bazı kısımlarına itiraz edebilirler, ancak kendi hükümet stratejilerinin de bir parçası olan "toplumsal ortaklık" fikrini desteklemektedirler.

Daha radikal reformist hükümetlerin seçildiği (henüz İrlanda'da olmasa da başka ülkelerde) zamanlarda olmuştur kuşkusuz. 1936 İspanya'sı ve savaş sonrası Britanya İşçi hükümeti bunlar arasında sayılabilir. Ancak bu hükümetlerin işlevi, işçi sınıfını toplumasal bir devrim yolundan alıkoymak, aynı kazanımların parlamento yoluyla elde edilebileceğini savunmak olmuştur.

Faşist bir darbenin yaşandığı İspanya vakası test edilirse, hükümet işçi sınıfını silahlandırmak yerine faşist hükümetle müzakereye oturmayı tercih etmiştir. İspanya'da faşizme karşı ilk direniş, silahlara el koyarak, dinamit ve silahlarla faşist kışlalarına saldıran CNT'li anarşistler olmuştur.

Benzer bir örnek, Rus devriminin hemen akabinde, bütün Avrupa'da ardı ardına çeşitli ülkelerde, reformistlerin seçilmelerinin devrimi engelleyeceği argümanını işlemeleridir. Bize oy verin ve kapitalizmi kurtarın. Ne yazık ki böyle dönemlerde, bu gibi partiler sıklıkla kitlesel bir destek kazanmaktadır; işte bu nedenle, anarşistlerin reformizm etrafındaki argümanları, devrimle kaybolup gideceğini düşünmek yerine, bugünden ele almaları hayatidir.

*** İYİ LİDERLER

Bu argümanlar birçok devrimci sosyalist arasında yaygındır, ancak anarşistlerin parlamenter sürece karşı çıkmak için çok daha temel nedenleri vardır. Bu süreç, işçi sınıfı kitlesinin az sayıdaki temsilsinin parlamentoya girmesine ve onlar adına mücadele etmesine dayanır. Onların [işçi sınıfının] yegane katılımı, birkaç yılda bir oy vermeye gitmelerinden, gazete satımı veya benzeri yollarla partiye oy toplamaktan ve desteklemekten ibarettir. Neil Killock'dan Mary Robinson'a kadar fiziki bir lidere veya liderlere dayanmamız, durumu bizim açımızdan belirginleştirmektedir.

Anarşistler, herhangi gerçek bir sosyalist / anarşist toplumun az sayıdaki bireyin iyi eylemleriyle ortaya çıkabileceğine inanmazlar. Yüzyıl kadar önce Uluslararası İşçi Birliği (daha çok 'Birinci Enternasyonal' olarak bilinir) etrafında anarşist hareketin başlamasından beri, işçi sınıfının kurtuluşunun ancak işçi sınıfının kendi eylemliliğiyle başarılabileceğini savunduk.

O zaman bu tartışma Marksistlerleydi, şimdi ise Doğu Avrupa'nın çöküşünün eşiğinde pekçok ana Marksist partinin çökmesiyle beraber, bu tartışma temelde reformistlerle yapılmaktadır. Anarşist toplumu meydana getirme süreci, ya işçi kitleleriyle yapılacaktır ya da asla gerçekleşmeyecektir.

Bu fikir, tabii ki parlamenter düşüncenin tam tersidir. Kapitalizm olarak adlandırılan karmaşadan kurtulmak için, iyi, kötü veya tarafsız olan az sayıdaki lideri istemiyoruz. Aslında bu tip seçkinlerin gerekli görüldüğü her türlü fikre devamlı karşı çıkmışızdır.

Parlamenter siyaset, sizin işinizi (veya işinizin bir kısmını) yapacak insanlar için oy kullanmanıza dayanır. En iyi niyetli bireyin bile kendisini bir erk konumunda bulması, [kendi] çıkarlarının temsil ettiklerinkinden farklılaşmasına neden olur. Bu, bakanlar ve başbakan için olduğu kadar, devrimciler ve sendika bürokratları için de doğrudur.
<br>

*** ARGÜMANLAR GELİŞTİRMEK

Bu, bizi anarşistlerin parlamenter sistemle nasıl başa çıkmaları gerektiği sorusuna getirir. Herkesi oy kullanmamaya nasıl ikna edeceğiz? Belki de tüm enerjimizi seçim karşıtı kampanyalara harcamamız gerekiyor.

Aslında bu, anarşistlerin çoğu tarafından asli bir faaliyet olarak değerlendirilmez. Amacımız sadece % 10'un oy kullandığı bir seçim değildir, böyle bir şeyin kendisi anlamsız olacaktır. ABD'nde çoğu seçimlerde [seçmenlerin] yaklaşık % 30'u oy kullanmaktadır ve muhtamelen nüfüsun % 50'si oy kullanmak için kayıt bile olmamıştır. Ne var ki, ancak bir aptal bunun ABD'nin İrlanda'dan daha anarşist demek olduğunu iddia edebilir. Eğer bu % 10 veya 30 hala hükümeti seçiyorsa, bu % 99 da olabilir.

Bizim amacımız, işçi sınıfının anarşizmin fikir ve taktiklerini benimsemesiyle toplumu değiştirmektir. Bu ise, şu anda içinde yaşadığımız ekonomik sistemin (kapitalizmin) yıkılmasını ve işçilerin özyönetimi altında olan sosyalizmin [bunun] yerini almasını içerir. Oy kullanmamak basitçe bir ümitsizlik göstergesi olabilir (<em>Oy kullansak ne olur ki?</em>), biz ise işçilerin alternatif için aktif bir şekilde mücadele etmesini istiyoruz.

Seçim karşıtlığımız iki şeyi ifade etmek üzere tasarlanmıştır. İlk olarak, parlamentonun toplumdaki gerçek iktidar yeri olmadığı. İkinci olarak, anarşizmi geliştirmek görevi küçük TD grupları için değil, işçi sınıfı için olduğu.

Anarşist fikirlerin desteklenmesini sadece soyut propaganda yoluyla değil, anarşistler olarak işçi sınıfının mücadelesine katılarak, ve anarşizmin reformizme göre nasıl en iyi araçları sağladığını gün ve gün gösterek sağlayacağız.
<br>

*** REFORMİST İŞÇİLER

İşçi sınıfı içindeki aktif militanlardan çoğu reformist partileri desteklemekte, bu açık bir gerçek. Bu, pekçok devrimci grubun seçim zamanlarında '<em>hayale kapılmadan İşçi Partisine oy verin</em>' veya '<em>İşçi Partisine oy verin, ama soyalist bir alternatif kurun</em>' gibi sloganlarla işçilere seslenmesine yol açmıştır. Biz bunu yapmıyoruz.

Bu sloganlardaki problem, bunların değişimin hala küçük bir seçkinler tarafından gerçekleştirilmesi gerektiği fikrini yansıtmasıdır. Bu söylemi, bunun reformist partileri teste tabi tutarak, onları [kendi] taraftarlarına teşhir edebileceklerini söyleyerek savunurlar. Herhangi bir İrlanda sol reformist örgütlenmesine genel bir bakışın göstereceği üzere, bu saçmadır.

Reformist örgütlenmeler düzinelerce olayda testi geçememiştir. İşçiler, bu örgütlerin sosyalizmi getireceğine inandıkları için değil, onları kötünün en iyisi olarak gördükleri için oy verirler.

Bu, aynı zamanda reformist partilere oy vermek için kullanılan bir argümandır. Fianna Fail veya Fine Gael'den biraz daha iyi olsalar da, bu partileri desteklemeyi reddeden aşırı-solcular değil midir? Bunun iki cevabı vardır.

İlki, karar alınmasının asıl olarak parlamentoda değil de sanayide gerçekleştiği biliniyorken, hükümette çoğunluk oluştursalar bile bu örgütlenmeler ancak kapitalizmin onlara izin verdiği şeyleri yapabilirler. Onların tek argümanı, kapitalizmi <em>daha insancıl</em> bir şekilde örgütlemektir. Biz kapitalizme daha insancıl bir yüz vermek değil, onu ezmek istiyoruz. Ücret kesintileri yapan ve grevleri kıran bir <em>sosyalist hükümet</em> görüntüsü --tıpkı Doğu Avrupa'daki <em>sosyalist</em> polis devletlerinin varlığının yaptığı üzere, sosyalizmin işçi sınıfının gözündeki itibarını zedelemektedir .

İkincisi, bu bir enerji meselesidir. Reformist örgütleri desteklemek için harcanan bu tip çabalar, daha iyi çalışma koşulları, daha iyi ücretler için yapılan mücadelelerden alıp götürülen bir enerjidir. Seçimler, birkaç ay boyunca toplumda hiçbir değişikliğin olmadığı bir boşlukta gerçekleşmemektedir.

Binlerce işçinin katıldığı bir grev veya gösterisinin, gerçek bir değişime neden olma şansı, 20 tane İşçi veya Emek partisi TD'sinin seçilmesinden daha fazladır. Pekçok Şili'li sosyalistin fark ettiği üzere, bu tip örgütlenmeleri destekleyen devrimciler aslında gerçekte kendi mezarlarını kazmaktadırlar.

*** KURALIN İSTİSNALARI

Anarşistlerin seçimlere katılan bireyleri desteklediği durumlar da vardır. Bu, bu tip insanların belli bir mesele bağlamında ve [seçildikten sonra parlamentodan] çekileceği temelinde, [seçime] katılmaları durumunda olur. Bu, kapitalist medya tarafından büyük bir karşı kampanyayla karşılaşıldığı zamanlarda, [bu konuda] kitlesel desteği göstermenin etkili bir yolu olabilir. Diğer destekleme biçimleri aktivistlerin tehdit edilmesi, öldürülmesi gibi nedenlerle oldukça zor olabilir.

İrlanda bağlamında bu tip bir örnek, siyasi statüleri için H-Blok'un 1989'da yaptığı açlık grevidir. Bobby Sands'in Fermanagh/Güney Tyrone'dan parlamento üyesi seçilmesi ve iki H-Blok mahkumunun sınırın güneyinden TD olarak seçilmesi, açlık grevlerine gösterilen kitlesel bir destektir. Bu, sadece küçük bir azınlığın desteğine sahip olduklarını iddia eden hükümetin ve medyanın savlarını çürütmüştür.

Bu tür bir destek, işçilerin serbestçe ortaya çıkarak gösteri, grev yapmaları [için] güvenin sağlanması temelinde yapılmalıdır. Bunun kendisi bir hedef değildir, bu tip bir hareketlenmeyi sağlamak için bir taktiktir sadece.

Bunun da problemleri vardır; bu birey, seçimden önce [verdiği] eğer seçilirse çekileceği vaadinin aksine hareket edebilir. Açlık grevcilerinin [parlamentodaki] yerlerini alamadığı açlık grevi olayında dahi, bu tip bir taktiğin tehlikeleri ortadadır. Bu [destek}, Sinn Fein tarafından emperyalizm karşıtı mücadelede, seçim siyasetinin doğru bir yönelim olduğunun kanıtı olarak değerlendirilmiştir.

Açlık grevi döneminde sınırın Kuzeyi ve Güneyindeki grevlere dayanan kitlesel kampanya potansiyeli böylece kaybedilmiştir. Tek bir konuyla ilgilenen bir adayın desteklenmesi kararı, kampanyanın daha sonraki yönelimi hakkında zorlu tartışmaları içerir, ve bu hafife alınmamalıdır.

Anarşistlerin patronların seçim sürecine katılmamayı teşvik etmeyeceği başka bir durum ise referandumlardır. WSM, Boşanma Eylemi Grubu'na [<em>ing. Divorce Action Group</em>] dahildi (ve aslında hala dahildir). 1986 referandumunun ciddi kısıtlamalarına rağmen yine de EVET oyu için uğraştık.

1983'deki kürtaj karşıtı referandumda anarşistler HAYIR oyunu savundular. Tabii ki her iki referandumun sonuçlarını da nihai olarak kabul etmiyoruz. Hala boşanma hakkı; ve serbest, güvenli kürtaj talebi de dahil olmak üzere kadınların kendi üretkenliklerini kontrol etme hakkı için mücadele ediyoruz. Bu gibi hakların kendileri demokratik haklardır, karara varmak üzere çoğunluğun oy kullanacağı bir şey değildirler.

Reformist partilerdeki insanlara ne söylüyoruz? Onlar göz ardı edilemezler (ve edilmemelidirler). Hükümete katılan partinizin, veya 1981'de azınlık Fianna Fail hükümetini destekleyen İşçi Partisinin siciline bakın diyoruz.

Partinizin neyi savunduğuna bakın. Partinizin, sendika bürokrasindeki siciline bakın. Reformist partilerin diğer ülkelerde oynadığı tarihsel role bakın. Reformistler teste tabi tutuldular, ve yüzlerce kez başarısız oldular. Bırakın onları, anarşizm hakkında daha fazla öğrenin ve işçi sınıfının kendi kurtuluş kavgasına katılın.



