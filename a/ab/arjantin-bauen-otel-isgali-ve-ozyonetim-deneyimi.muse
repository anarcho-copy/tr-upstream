#title Arjantin: Bauen Otel İşgali ve Özyönetim Deneyimi
#SORTtopics işçi mücadelesi, arjantin, işgal hareketi
#date 17.03.2016
#source http://www.theguardian.com/cities/2016/mar/10/occupy-buenos-aires-argentina-workers-cooperative-movement sitesinden derlenmiştir.
#lang tr
#pubdate 2019-11-16T18:35:00




[[a-b-arjantin-bauen-otel-isgali-ve-ozyonetim-deneyi-1.jpg ]]

<quote>
Buenos Aires’de 2002 yılında bir kooperatife dönüştürülen yün-temizleme atölyesindeki bir işçi – Fotoğraf: Natacha Pisarenko / Associated Press
</quote>



1978 Yılında askeri cunta tarafından Dünya Kupası’na gelenlere beş yıldızlı konaklama sağlamak için açılan Bauen Otel, 2001 yılında iflasını açıklamış ve iki yıl sonra otelde çalışan işçiler tarafından işgal edilmişti.

Doksanlı yılların sonlarına doğru başlayan ve 2001 yılında doruğa ulaşan ekonomik kriz felaketi sonrasında Arjantin dünya tarihinin en büyük borç yüküyle karşı karşıya kalmış ve işletmeler için hazırlanan kurtarma paketleri de işe yaramamıştı.

Birbiri ardına iflas eden şirketler boşalırken, işçiler fabrikalarına geri dönerek, işyerlerinin ve makinaların mülkiyet haklarını devralmak için mahkemelere gittiler.

İşgal Hareketi başladığında işçilerin sloganı “İşgal, direniş, üretim” idi. 2016 yılına gelindiğinde işçilerin sloganı hala aynı… Buenos Aires’de yer alan pek çok başka işletme gibi Bauen Otel’de işçilerin özyönetimi altında on yıl geçirdi.

Özyönetim modelinin başarısının Buenos Aires dışında da ciddi etkileri var. Dünya genelinde, şehirlerin şirket çıkarlarına göre düzenlenmesine karşı halkların kamusal alanları halk yararına uygun olarak yeniden inşaa etmesi açısından Buenos Aires ters eğilime bir örnek oluşturuyor.

En önemlisi de, kooperatifçilik hareketinin sadece Bauen Otel’de olduğu otelcilik alanında değil restoranlar, çöp toplama merkezleri, inşaat, sağlık, eğitim, basın/yayın ve ulaşım gibi farklı pek çok alanda yaşam bulmuş olmasıdır. Şimdilerde, pekçok işgal hareketi Arjantin’in yeni sağcı Başbakanının bu modelleri rafa kaldırmasından endişe ediyor.

Armando Casado, otel restoranında çalışan 66 yaşında bir garson. O otelin hem patron hem de işçiler tarafından yönetilmesinin yakın tanıklarından. “Başlangıçta bu da diğerleri gibi bir işti,” diyen Casado “Burası daha önce olduğu kadar verimli bir işletme değil ama biz işçiler şimdi daha çok özgürüz,” diyor.

Buenos Aires Üniversitesi’nin Açık Öğretim Programına göre; Buenos Aires dünyanın ilk ve en uzun süreli işgal şehirlerinden biridir ve Arjantin’de 2014 yılı itibarıyla yaklaşık yarısı Buenos Aires merkezli olmak üzere 311 işgal işletmesi bulunuyor.

Hareketin “Ekonominin kaderciliğine ve neoliberal kapitalizm ideolojisinin” politikalarına karşı işçi isyanı olarak doğduğu ve işgal fabrikalarının ülkede ekonomik toparlanma süreci yaşanırken 2004 yılından sonra arttığı belirtiliyor. Araştırmacılar, bu modelin mali krize bir yanıt olmasının ötesinde halkın yararına bir sistem olduğunu düşünüyorlar. Şimdilerde bu sistem uygulanabilir ve sürdürülebilir bir model olarak inceleniyor.



[[a-b-arjantin-bauen-otel-isgali-ve-ozyonetim-deneyi-2.jpg ]]

<quote>
Buenos Aires’deki Ghelco çikolata fabrikası kooperatifinde şeker paketleyen işçiler – Fotoğraf: Natacha Pisarenko / AP
</quote>


Fabricas tomadas (işgal fabrikaları) tüm dünyada dikkatleri üzerine çekerken, aslında Buenos Aires’de tüm hizmetler ve üretimler işçilerin özyönetimindeki işletmelerden sağlanıyor. Bir restoranda ki galetadan, sokaktaki çöp toplama hizmetine kadar herşey Cooperativa (kooperatif)’nın bir parçası. Arjantinli işçiler, genişlemeye devam ederek on yıldan fazla bir süredir kendi işletmelerini ayakta tutmayı başardılar. Peki bunu nasıl yaptılar?

Arjantinli gazeteci Esteban Magnani, “90’lı yıllarda Arjantin’de işçiler için hiçbir alternatif yoktu, işlerini kaybetmeleri işçilerin sonu demekti. Eğer yeterince umutsuzsa insanlar, fabrikaları yeniden inşaa etmeye ve yeni bir sistem oluşturmaya o kadar hazırdırlar,” diyor.

İşçilerin içinde bulundukları umutsuzluktan çıkmak için harekete dört elle sarılmasının yanı sıra harekete yardımcı diğer bir unsurda solcu başbakanlar Nestor ve Cristina Kirchner yönetimlerinde harekete karşı düşmanca bir tavır alınmaması da etkili oldu.

Açık Öğretim Programı direktörü Andrés Ruggeri, “Kirchner hükümeti iflas kanununa, işçilerin şirketteki alacakları yerine, bir kooperatif kurmaları karşılığında çalıştıkları fabrikalarda kalmalarına izin veren bir madde eklediler,” diyor. Kirchner hükümeti onların [işgal hareketi] düşman olmadığını, onlara karşı baskı uygulanmayacağını ve tahliye işlemi gerçekleştirilmeyeceğini söyleyerek belirsiz bir şekilde hareketi desteklemişlerdir.

Ancak bu durum 2015 yılında seçilen sağcı başbakan Mauricio Macri ile değişti. Macri’nin belediye başkanı olduğu dönemlerde ki bir video kaydında kooperatiflerin iyi bir model olmadığı konusundaki söylemlerinden dolayı pek çok işçi bu modelin ortadan kaldırılmasından endişe ediyor. Üstelik bu boş bir tehdit değil.

Bauen Oteli’nde yaklaşık 130 kişi çalışıyor ancak orada bir patron bulunmuyor. İşçiler ayda ortalama 4.000 – 4.500 peso arasında maaş alıyorlar. Bu pahalı bir şehir olan Buenos Aires’de insanca bir yaşam ücreti değil. Otel çalışanlarından Patricia Guzman maaşlarının otel masrafları çıkarıldıktan sonra ellerinde kalan paraya göre belirlendiğini söylüyor. Guzman ayrıca “Bu maaşın Arjantin asgari ücret miktarının altında kaldığını biliyoruz. Otel yönetim kurulunda her 3 ayda bir baz maaş oranı olan 3.000 pesonun üzerine ilave ne artış yapılacağı konuşulur çünkü işin gidişatına göre bu oranlar değişir. Ekonomimiz zor zamanlardan geçiyor ve bizler ayakta kalmak için ücretlerimizden fedakarlık etmeye hazırız. Belki az kazanıyoruz ama ben burada çok rahatım ve başka bir iş aramıyorum,” diyor.

Bauen Otel aynı zamanda diğer kooperatif tarzında örgütlenmek isteyen işletmeler içinde model olmuş. Kendi özyönetimini kurmak isteyen işçiler Otel’e gelmiş tavsiyeler almış ve kendi modelini oluşturmuş.

Sergio Cano “Başlarda herşey çok zordu, ne yaptığımızı ve nereye doğru gittiğimizi bilmiyorduk. Şimdilerde işçilerin emekli maaşları var, sosyal güvenlikleri var. Eskiden sosyal güvenlik primleri 12 yıl üzerinden 2 yıl için ödenirdi şimdi ise benim ya da oğlumun başına bir şey gelirse sosyal güvencemiz var,” diyor.



