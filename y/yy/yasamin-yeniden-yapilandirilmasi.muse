#title Yaşamın Yeniden Yapılandırılması
#author Meydan Gazetesi
#SORTtopics anarşizm, konfederalizm, özerklik, özyönetim, yerel yönetim, rojava devrimi
#date 12.01.2016
#lang tr
#pubdate 2019-11-15T23:00:00


[[y-y-yasamin-yeniden-yapilandirilmasi-1.jpg 50 r ]]


Özerklik, konfederalizm, özyönetim gibi kavramları Rojava Devrimi sonrasında konuşuyor olmamız, Rojava’daki deneyimin yaşadığımız topraklardaki mücadelelere kattığı perspektifi ve beraberinde gelişen bir dizi toplumsal olayı anlamamız, ezilen halkların özgürlük mücadelesi açısından önem taşımaktadır.

Köy, mahalle ve şehirlerde oluşturulan halk meclislerinden demokratik özerk yönetime, günlük ihtiyaçların giderilmesi, ekonomik, sosyal ve kültürel ihtiyaçların karşılanması, güvenlik ve savunmanın halkın özörgütlülükleri tarafından savaş sürecinde dahi uygulanıyor oluşu, buradaki toplumsal işleyişin özyönetim olduğunun en açık göstergesidir.

Bu devletsiz işleyiş, çevresindeki devletli siyasal biçimleri zorlamakta, kendi siyasal kültürünü ve yapısını örgütlemeye çalışmaktadır. Kürt Özgürlük Hareketi’nin Ortadoğu’da ortaya koyduğu bu deneyim ve beraberinde şekillenen politikası, devletin siyasal alanının dışında yeni bir siyasal alanın inşa ediliyor oluşuyla ilgilidir.

Demokratik özerkliğin temeli, özyönetime dayanır; halkın kendi gerçekliğini yaratmasıdır. Devlet dışında bir siyasetin örülüyor oluşu özerklik projesi ilk ortaya konduğundan bu yana vurgulanıyor. Kürt Özgürlük Hareketi’nin bu söyleminin temelinde anarşizmin devlet eleştirilerinin iyi bir okuması, dolayısıyla iyi bir devlet çözümlemesi yatıyor.

Özünde devlet olan ya da sonucunda devleti ortaya çıkaracak siyaset türlerinin halkı temsil etmekle ilgisi olmadığı ortadadır. Kendine halkı kurtarmak üzerinden paye biçen, bunu da bir devrimle devlet aygıtını ele geçirerek gerçekleştirebileceğini düşünen <strong>devrim</strong> ve devrimcilik anlayışı iflas etmiştir. Sosyalizmin “ulusal kurtuluş mücadeleleri” tezi ile ortaya çıkan devletlerin uygulamalarında yaşanan sorunların çözülemeyeceği, Kürt Özgürlük Hareketi tarafından 2000’lerin ortalarından bu yana Abdullah Öcalan’ın birçok yazısında olduğu gibi ısrarla vurgulanmaktadır. Merkeziyetçi, bürokratik yönetim yerine özyönetimin savunulması da özgürlükçü bir çözüm arayışının sonucu olarak ortaya çıkmıştır.

Bir yanda Güney Amerika’da, Meksika devletine karşı Magonların ve Zapataların verdiği uzun süreli mücadeleler ve devamcısı konumundaki EZLN’nin özgürlük mücadelesi sonucu oluşan devletsiz deneyimler, öte yanda yanı başımızdaki Rojava Devrimi; devletsiz, özörgütlülüğe ve özyönetime dayalı bir işleyişin yakın tarihlerdeki örneklerindendir.


*** Anarşizm ve Özyönetim

Geçmişten günümüze özyönetim, farklı coğrafyalarda anarşistler tarafından deneyimlenen bir örgütlenme modelidir. Anarşizm, toplum içindeki bireylerin paylaşma ve dayanışmayı esas alan bir yaşam anlayışıyla doğrudan demokratik karar alma süreçlerini işleterek toplumsal ve ekonomik ihtiyaçları yerel, bölgesel ve mahalle düzeyindeki komün ve halk meclisleri; genelde de federasyon, konfederasyon gibi özörgütlenmeler aracılığıyla organize edilmesini esas alır. Anarşizm, insanın doğayla kurduğu ilişkide ekolojik uyuma, cinsiyet ilişkilerinde tahakkümünün ortadan kaldırılmasına, mülkiyet ve kapitalizmin dayattığı tüketim kültürü karşısında paylaşımcı bir kültürün örgütlenmesini esas alan ve kendi ekonomik ve sosyal özörgütlülüklerini oluşturan, yani tahakküme dayalı bütün ilişki biçimlerini ortadan kaldıran bir sisteme dayanır.

Bu sistemin sürdürülebilir işleyişi olarak özyönetim, devletsiz bir siyasetin toplumsal meşruluğunu sağlayabilmesi halkın karar alma süreçlerinin yaratılmasıyla ortaya çıkar. Toplumun yönetilmesi değil, kendi kendini örgütleyebilmesi amaçlanmaktadır. Bugün başta Kürdistan olmak üzere farklı coğrafyalardaki mahalle, bölge, <em>sokak</em> meclisleri, komünler, kadın ve gençlik örgütlenmeleri, meslek örgütlenmeleri üzerinden bir tartışma ve kararlaşma süreci gerekliliğini fazlaca hissettirmektedir.


*** Özyönetim ve Yerel Yönetimin Karıştırılması

Özyönetim, yerel yönetimin güçlendirilmesini savunan bir modelle karıştırılmamalıdır. Özyönetim bütünlüklü bir anlayışı esas alır; siyasal, ekonomik, sosyal, kültürel boyutlardan sadece bir kısmına önem veren işleyiş, özyönetim olmaktan uzaktır. Örnek model olarak gösterilen Avrupa’daki kanton yönetimleri, devletli siyasal işleyişten uzak kalamamış, dahası özyönetim gibi gösterilen işleyişi devlet mekanizmasının içerisine yedirmeye çalışmıştır.

Özyönetim alanları olarak mahalle, <em>sokak</em>, bölge… ve diğer yerellikler, kendi toplumsal işleyişini yaratır. Nicelik olarak bütün bireylerin karar alma sürecine ve kararla ortaya çıkan uygulamaya katılımı esas alınır. Ancak Avrupa’daki kanton yönetimleri her ne kadar özyönetime yakınmış gibi iddia edilse de böyle değildir. Yerelden genele değil, genelden yerele bir merkeziyetçi yönetim söz konusudur. Devlet kontrolündeki idari birimler, referandum gibi süreçlerle revize edilerek, çoğunluğa dayalı bir demokrasi uygulanmaktadır.

Geçtiğimiz yıllarda İsviçre kantonlarında birçok referandum gerçekleştirildi. Göçmenlere ilişkin yapılan bir referandumda ortaya çıkan sonuç %51 ile “göçmenler kalsın” oldu. Kanton demokrasileri aslında tam da bu nitelik ve nicelik sorununu gözler önüne sermektedir. Kalan %49’un “yabancı düşmanlığı” bir başka referandumda %51’e dönüştüğünde “yabancı düşmanlığı”, belki de “faşizm” demokratik bir sonuç olarak kabul edilecektir.


*** Ekonominin Özyönetimsel İnşası

Siyasal dönüşüm kadar ekonomik dönüşüm de özyönetimin işleyiş ilkelerine uygun olmalıdır. Bu uygunluk özyönetimin sürdürülebilirliğinin garantisidir. Özyönetimin ekonomik işleyişinin üretim, tüketim ve dağıtım ilişkileri, kesinlikle ve kesinlikle her bireyin gönüllü çalışma esası gözetilerek, yani sömürüsüz bir ilişki biçimiyle örgütlenmelidir.

Konfederasyon, özerklik ve özyönetimin tartışıldığı günümüzde, bu tarz bir ekonomik işleyiş, 1936 Anarşist Devrimi’nde İberya’da deneyimlendi. CNT-FAI (Ulusal Emek Konfederasyonu-İberya Anarşist Federasyonu) gibi özörgütlenmeler aracılığıyla özyönetim komiteleri sadece devletsiz bir siyasal işleyiş değil, aynı zamanda fabrika ve atölyelerin işleyiş biçimleri, toprak ve üretim araçları üzerindeki devlet ve kapitalizm etkisini yıkan bir süreci yarattı. 1936 Devrimi’nde CNT-FAI gibi özörgütlülüklerle, toplum bir yandan politikleşirken, bir yandan da ekonomik faaliyetler komünist ilkelere göre yeniden inşa edildi; üretim, tüketim ve dağıtım ilişkileri, sendika ve mesleki özörgütlenmeler yardımıyla özyönetime uygun bir şekilde yeniden oluşturuldu.

1936 İberya Devrimi, özellikle günümüzde Rojava Devrimi gibi toplumsal süreçlerde gözden kaçırılmaması, hatta irdelenerek model alınması gereken bir deneyimdir. İberya Devrimi’ndeki özyönetimin ekonomik etkilerini anlamak için dünyanın farklı bölgelerinde hala yaşanmakta olan kooperatif deneyimlere; İberya Devrimi’nin etkisiyle Franco sonrasında bile yaratılan gelenekle oluşan Katalonya’daki kooperatiflere ve Meksika’daki Zapatistlerin kooperatiflere dayalı ekonomisine bakmak gerek.

Biliyoruz ki toplumsal devrimlerin, siyasi, ekonomik, sosyal tüm boyutlarıyla inşa edilmesi zordur. Bunu, savaş koşullarında dahi bölgede çıkarı olan tüm devletlere ve kapitalistlere karşı yürüten ve yaşamı yeniden inşa etmeye çalışan Kürt Özgürlük Hareketi de bu zorlu aşamalardan geçmektedir. Kürt Özgürlük Hareketi’nin bugün vermiş olduğu bu mücadele, ancak kapitalizm karşıtı ve devletsiz bir temelde sürdürüldüğünde sadece Kürdistan’ı değil beraberinde tüm Ortadoğu’yu özgürleştirecektir.



