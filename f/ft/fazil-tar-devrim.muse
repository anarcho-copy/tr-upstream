#title Devrim
#author Fazıl Tar
#SORTtopics devrim, tekno-endüstriyel karşıtı, tüketim
#date 26.10.2011
#source 01.08.2022 tarihinde şuradan alındı: [[https://hayattabirleri.blogspot.com/2011/10/devrim.html][hayattabirleri.blogspot.com]]
#lang tr
#pubdate 2022-08-01T14:08:53



Ey ilerlemenin sahte büyüsüne kapılmış siz liberaller, müslümanlar, solcular, ideologlar, konformistler, modernistler, demokratlar, tekno-endüstriyel uygarlığı var eden tüketiciler, bu alemi kendilerine verilmiş bir hediye olarak görüp hoyratça gaspeden hakimiyetçi insanoğlu: Diktiğin her bina, satın aldığın her bilgisayar, yediğin her hamburger, giydiğin her takım elbise, yaptığın her çocuk, yarattığın her eşya, yönettiğin her şey seni kaçınılmaz bir sona daha fazla yaklaştırıyor farkında değil misin? Senin bakiyene yazılan her artı gezegenin sonlu kaynaklarını eksiltiyor, yağmalıyor, bozuyor, geri dönüşsüz acı dolu bir yıkımın puantajına dönüştürüyor anlamıyor musun? Artık ne temiz su kaynaklarına ulaşabilmenin bir yolu, ne zengin bitki florasından sağlıklı beslenmenin bir yöntemi, ne ciğerlerinle mutluluk ve zehirlenmeden soluk alabileceğin hava, ne kendine ve çevrene ayırabileceğin bolca zaman, ne de kendi kararlarını verebileceğin bir özgürlük ortamın kaldı görmüyor musun? Başkalarının belirlediği işlerde, başkalarının seni o iş için eğittiği konularda, başkalarının hesasabına, başkalarının kuralları ve dayatmalarıyla sevmediğin işlerde emekli olabilmek için binbir zorlukla ve itilip dışlanarak çalışıp durmaktan bıkmadın mı? Yasayla, polisle, askerle, vatanla, bayrakla, şehitle, toplumla, gelenekle, aileyle, gelecekle üzerinde kurulan otoritenin, hiyerarşinin, korkuların gerçek şiddet ve terör olduğunu idrak edemiyor musun? Çok geç olmadan gezegen kendinden aldıklarını geri almaya başladığı dönüşsüz yola girmeden insanlar, hayvanlar ve doğa üzerinde kurduğun tahakkümü kaldır artık. Uygarlık demek olan devletin ve onu yaratan ve yaşatan acımasız tekno-endüstriyel kapitalist mekanizmanın olmadığı bir dünya hayal et, bu baskı ve zulmün daha fazla sürmesine izin verme isyan ol devrim ol gezegeni kurtar.



