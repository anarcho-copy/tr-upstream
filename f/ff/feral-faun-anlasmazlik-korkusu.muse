#title Anlaşmazlık Korkusu
#subtitle Vahşi Devrim
#author Feral Faun
#SORTauthors Wolfi Landstreicher
#SORTtopics isyancı, anarşist, örgütlenme, birey, toplum, giriş, eleştiri, sol, sol-sonrası
#date 2004
#source 03.08.2022 tarihinde şuradan alındı: [[https://karaisyan.blogspot.com/2009/09/anlasmazlk-korkusu.html][karaisyan.blogspot.com]]
#lang tr
#pubdate 2022-08-03T20:24:39
#notes Çeviri: Uyumsuzlar Fraksiyonu <br>İngilizce Aslı: [[https://theanarchistlibrary.org/library/feral-faun-essays#toc22][Feral Revolution: Fear of Conflict]]


<quote>

Bana karşı kendinizi katılaştırmanız ve farklarınızı veya özgünlüğünüzü öne sürmeniz gerçekten içinizdeki bir zayıflık değildir: kendinize boyun eğmemeye veya kendinizi terk etmemeye ihtiyacınız var.

<right>

Max Stirner

</right>

</quote>


Ne zaman birkaç anarşistten fazlası bir araya gelirse, tartışmalar yaşanır. “Anarşist” kelimesi çoğu kez tutarsız fikir ve pratiklerin geniş dağılımını tanımlamak için kullanıldığından dolayı, bu sürpriz değil. Tek ortak payda otoriteden kurtulmuş olma arzusudur, ve onu ortadan kaldırmak için hangi metotların uygun olduğu sorusu şöyle dursun, anarşistler otoritenin ne olduğunda bile mutabık olmazlar. Bu sorular diğerlerine neden olur ve böylece tartışmalar eksik olmaz.

Tartışmalar beni rahatsız etmez. Canımı sıkan şey, bir karara varmaya çalışmaya odaklanmak. “çünkü hepimiz anarşistiz” farz edilir, hepimiz gerçekten aynı şeyi istemeliyiz; göze çarpan anlaşmazlıklarımız yalnızca ortak bir görüş bularak etraflıca konuşabileceğimiz yanlış anlaşılmalar olmalıdır. Birileri anlaşmazlıkları tartışarak bir yere oturtmayı reddettiğinde ve farklarını korumakta ısrar ettiğinde, dogmatik sayılıyorlar. Ortak bir zemin bulmaktaki bu ısrar, sık sık kendi koşullarımızda yaşamlarımızı yaratmak için eylemenin yerini alan sonu gelmeyen bu diyaloğun en önemli kaynaklarından biri olabilir. Ortak zemin bulmadaki bu girişim pek çok gerçek anlaşmazlıkların yadsınmasını kapsar.

Anlaşmazlığı yadsımakta sık sık kullanılan bir strateji, bir tartışmanın yalnızca sözcükler ve anlamları üzerindeki anlaşmazlık olduğunu iddia etmektir. Sözde insanın kullandığı sözcüklerin ve onları nasıl kullanmayı seçtiğinin, insanın fikirleri, hayalleri ve tutkuları ile bağlantısı yoktur. Yalnızca sözcükler ve anlamları hakkında çok az tartışma olduğuna eminim. Sözcükler ve anlamları hakkındaki bu tartışmalarda, ilgili bireyler açıkça ve kesinlikle neyi amaçladıklarını açıklasaydı, kolaylıkla karar verilmiş olabilirdi. Bireyler kelimeleri ve onları nasıl kullanacakları konusunda bir karara varamadığında, bu onların rüyalarının, arzularının ve düşünüş yollarının tek bir dilin öğesi olmaktan uzakta olduğunu gösterir, ortak bir dil bulamazlar. Böyle uçsuz bucaksız büyük ayrılığı sadece anlambilime indirme girişimi, gerçek anlaşmazlıkları ve ilgili bireylerin tekilliğini inkar etme girişimidir.

Anlaşmazlığın ve bireylerin tekilliğinin inkarı arta kalan solculuk veya kolektivizmden kaynaklanan birlik için saplantı gösterebilir. Birliğe her zaman sol tarafından son derece değer verilmektedir. Kendilerini soldan ayırmaya çalışmalarına rağmen, anarşistlerin çoğunun yalnızca devlet karşıtı solcular olduklarından dolayı, onlar, sadece birleşik bir cephenin bizi daima kendi seçimimiz olmayan birlikteliklerin içersine zorlayan, ve bu nedenle farklılıklarımızı n üstesinden gelmek ve “ortak nedeni” desteklemek için birleşmek zorunda olduğumuz bu toplumu yok edebileceğine inandırılırlar. Fakat kendimize “ortak neden” verdiğimiz zaman, anlayış ve mücadelenin en düşük ortak paydasını kabul etmeye zorlanılıyoruz. Bu yolda yaratılmış birliktelikler sadece ilgili bireylerin eşsiz arzularını ve tutkularını bastırarak, onları kitleye dönüştürerek büyüyen sahte birlikteliklerdir. Böyle birlikteliklerin, bir fabrikanın işleyişini elinde tutan emeğin yaratılmasından veya otoriteleri güç içersinde ve insanları çizgide tutan sosyal konsensüs birliğinden farkı yoktur. Kitle birliği, bireyin çoğunluk içersinde bir birime indirgenmesi üzerine temellendiğinden, otoritenin yok edilmesi için asla ana ilke olamaz, sadece tek biçimde veya diğerinde desteği içindir. Madem ki otoriteyi yok etmek istediğimiz için, o halde farklı bir temelden başlamalıyız.

Benim için, o temel kendim – tutkularının ve hayallerinin, arzularının, projelerinin ve karşı karşıya geldiklerinin hepsi ile birlikte yaşamım. Bu esastan, kimse ile “ortak neden” yapmam, fakat sık sık bir yakınlığa sahip olduğum bireylerle karşı karşıya gelebilirim. Pekala sizin arzularınız ve tutkularınız, hayalleriniz ve projeleriniz benimkilerle uyuşabilir. Otoritenin her şekline karşın bunları gerçekleştirme üzerine ısrarla eşlik edilmiş böyle bir yakınlık yalnızca bu bireyler arzu ettiği sürece devam eden, tekil, asi isyancı bireyler arasındaki gerçek birlik için esastır. Elbette, otorite ve toplumun yıkımı arzusu, bizi geniş ölçekli olan, fakat asla kitle hareketi gibi olmayan; onun yerine yaşamlarını kendilerinin yapmakta ısrar eden bireyler arasındaki yakınlıkların uyuşmasına ihtiyacı olacak isyancı birlik için uğraşmaya götürür. İsyanın bu türü, herkesin mutabık olabileceği en düşük ortak paydaya fikirlerimizin indirgenmesi yoluyla olamaz. Yalnızca, sadece her bireyin tekilliğinin tanınması yoluyla olabilir. Bu, ne kadar acımasız olabileceklerine bakmaksızın, bir kere yaşamlarımızı ve etkileşimlerimizi bizden çalan sosyal sistemden kendimizi kurtardığımız zaman dünyanın bize vermek zorunda olduğu etkileşimlerin insanı hayrete düşüren zenginliğinin parçası olarak, bireyler arasında var olan gerçek anlaşmazlıkları kucaklayan bir tanımadır.



