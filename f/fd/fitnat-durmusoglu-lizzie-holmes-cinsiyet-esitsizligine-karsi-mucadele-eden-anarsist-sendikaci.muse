#title Lizzie Holmes: Cinsiyet eşitsizliğine karşı mücadele eden anarşist sendikacı
#author Fitnat Durmuşoğlu
#SORTtopics anarşist feminizm, anarşist, kadın mücadelesi, anarko sendikalizm, cinsiyet, lizzie holmes, tarih, abd, haymarket, işçi mücadelesi
#date 01.02.2022
#source 18.05.2022 tarihinde şuradan alındı: [[https://www.kadinisci.org/2022/02/01/lizzie-holmes-cinsiyet-esitsizligine-karsi-mucadele-eden-anarsist-sendikaci/][kadinisci.org]]
#lang tr
#pubdate 2022-05-18T20:09:00


<quote>

Çalışan Kadınlar Sendikası’nın bünyesinde hazır giyim işçilerinin örgütlenmesi için çalıştı. Haymarket Direnişi’nde alandan sağ kurtuldu. Daha sonra İngiliz anarşisti eşi William Holmes’le birlikte New Mexico’ya taşınarak ölene kadar özgür aşk, evlilik, cinsiyet eşitsizliği, ekonomik adaletsizlik gibi konularda makaleler yazdı.

</quote>

Elizabeth Lizzie May Hunt, 21 Aralık 1850 yılında Jonathan Hunt ile radikal feminist Hannah Jackson Hunt’ın kızı olarak Lowa’da doğdu. Lizzie dört yaşındayken ailesi Ohio, Berlin Heights’ta “özgür aşk” komününe taşındı. Lizzie, ileri düzeyde bir eğitim aldı. 15 yaşına geldiğinde Ohio’da tek odalı bir okul binasında okul öğretmeni ve müzik eğitmeni olarak çalışmaya başladı. 17 yaşında Hiram J. Swank ile evlendi. Lizzie, evliliğine rağmen öğretmenliğe devam etti ve ekonomik bağımsızlıkta ısrar etti. 1868 yılında oğlu ve 1873 yılında kızı doğdu. <em>Great Railroad Strike of 1877</em> (1877 Büyük Demiryolu Grevi) hakkında bilgi sahibi olunca işçi hareketine katılmak için1879 yılında Chicago’ya gitti. Eşinden ayrıldı.

Chicago’da bir pelerin fabrikasında terzi olarak çalışmaya başladı. Müzik öğretmeni olarak iş bulamamasından duyduğu hayal kırıklığını dile getirdi, ancak terziliği kendi siyaseti açısından olumlu buluyordu ve <em>Chicago Times</em> ile yaptığı bir röportajda işçi sınıfını tanıma arzusu içinde olduğunu, <em>“tüm mücadeleleri, çalışan kızların yoksulluğunu ve angaryalarını, onların gizli gururlarını”</em> öğrendiğini anlatıyordu. Chicago’da bir yıldan fazla kaldıktan sonra, Lizzie o zamanlar <em>Socialist Labor Party of America’</em>nın (SLP) (Amerika Sosyalist İşçi Partisi) bir kolu olan <em>Working Women’s Union’a</em> (WWU) (Çalışan Kadınlar Sendikası) katıldı. O sırada Çalışan Kadınlar Sendikası sekiz saatlik çalışma günü için bir kampanya başlatıyordu ve Lizzie bu çabaya yoğun bir şekilde dâhil oldu. Daha sonra, sendikal mücadele nedeniyle terzilik yaptığı işten kovuldu, ancak sendikayla olan ilişkisinden kopmadı.

*** Emek şövalyelerine katıldı

Sendika toplantılarında yaptığı konuşmalar popülaritesini artırdı. Bir süre sonra sendika sekreteri oldu. Sekreter olarak çalıştığı süre boyunca, Çalışan Kadınlar Sendikası, o zamanlar Amerika Birleşik Devletleri’ndeki en popüler işçi sendikaları arasında yer alan <em>Knights of Labor’un</em> (Emek Şövalyeleri) üyesi oldu. Chicago’daki hazır giyim endüstrisi işçilerinin örgütlenmesinde önemli bir rol oynadı ve 1886 yılında kadın aktivistler, hazır giyim endüstrisi çalışanlarını temsil eden üç farklı Emek Şövalyesi meclisi örgütlediler. WWU sekreteri olarak Lizzie, Lucy Parsons ve İngiliz anarşist William Holmes dâhil olmak üzere diğer önde gelen aktivistlerle tanıştı. Parsons, Lizzie’i hem sosyalizm hem de anarşizmle buluşturdu. Chicago’daki radikal işçi hareketiyle olan ilişkisi ona makaleler yazması için birçok fırsat sağladı. 1880’li yıllar boyunca makalelerin ve kurgu eserlerinin üretken bir yazarı oldu, çeşitli radikal gazete ve dergilerde hikâyeler ve eğitim materyalleri yayınladı. Hazır giyim fabrikalarında yaşadığı korkunç çalışma koşullarını eleştiren çok sayıda makale yazdı, hikâyeleri <em>Anti-Monopolist, Labor Enquirer</em> ve <em>Nonconformist</em> gibi gazeteler tarafından yayınlandı.

*** Emek dergilerinde gazetecilik

Uluslararası Emekçiler Derneği’nin gazetesi <em>Alarm</em>’ın editör yardımcısı olarak görev yaptı. 28 Nisan 1885 yılında Lizzie Holmes ve Lucy Parsons, yeni inşa edilen Chicago Ticaret Kurulu binasına doğru bir yürüyüşe öncülük ederek, Chicago anarşist ve işçi hareketinin liderleri olarak öne çıktı. Bu durum onları yetkililerin onları tehlikeli teröristler olarak göstermesine yol açtı. Kasım 1885 yılında Lizzie, anarşist arkadaşı William Holmes ile evlendi. Evlendikten sonra ikisi de Chicago’dan Cenevre, Illinois’e taşındı, orada tekrar öğretmen olarak iş buldu ancak anarşist örgütlenmeden vazgeçmedi. Her ikisi de, Haymarket olayından önceki günler de Albert ve Lucy Parsons ile örgütlenme çalışmalarında bulunmak için Chicago’ya döndüler.

3 Mayıs 1886 yılında Lizzie, sekiz saatlik iş günü talep eden yüzlerce terzi kadından oluşan bir yürüyüşe öncülük etti. <em>Chicago Tribune</em> gazetesi, yürüyüşçüleri <em>“yıpranmış yüzleri ve eski püskü giysilerle, rahatsız edici bir varoluş mücadelesinin kanıtlarını taşıyanlar”</em> olarak yazdı. Yürüyüşten sonra, Lizzie Cenevre’ye döndü, ancak Lucy Parsons’tan 4 Mayıs’ta yapılması planlanan miting için Chicago’ya dönmesini isteyen bir telgraf aldı. Ertesi sabah, Lizzie Holmes Chicago’ya döndü ve o akşam anarşist mitinge katıldı. Miting Chicago polisi tarafından bastırıldı ve ardından gelen kaosun ortasında kalabalığın üzerine bir bomba atıldı. 11 kişi öldü ve onlarca kişi yaralandı. Holmes ve Lucy Parsons bombadan zarar görmedi. 5 Mayıs sabahı Lucy ve Lizzie, Chicago polisinin yağmaladığı <em>Alarm gazetesi’</em>nin ofisine gittiler. <em>Alarm</em> Chicago Polis Departmanı tarafından resmen sansürlendi. Holmes ve Parsons bombalamayı kışkırtmakla suçlanarak tutuklandı. Kanıt yetersizliğinden serbest bırakıldılar. Albert Parsons bombalamadan sonraki sabah Cenevre’ye giderek William Holmes ile kısa bir süre geçirdi, daha sonra Chicago yetkililerinden kaçmak için Wisconsin’e gitti. Ancak arkadaşları tutuklandıktan sonra, Albert Parsons Chicago’ya döndü ve yetkililere teslim oldu. Haymarket anarşistlerinin yargılanması sırasında, Lizzie Holmes, Albert Parsons adına ifade verdi ve bombanın atıldığı sırada onunla bir kafede olduğunu, bombayı atmış olamayacağını iddia etti. Holmes ayrıca anarşizmin, doğası gereği şiddet içermediğini iddia etti ve “<em>anarşi teorisinin her türlü güç fikrine karşı olduğunu</em>” ifade etti. İfadesine rağmen Parsons, asılarak ölüme mahkûm edildi.

*** New Meksika’da yaşamını yitirdi

İnfazın ardından Chicago’daki radikal örgütlenmeye yönelik baskının artmasıyla Lizzie’nin daha önce düzenlemeye yardımcı olduğu birçok yayın kuruluşu, <em>Alarm</em> da dâhil olmak üzere feshedildi. Holmes, Haymarket olayının ardından kolluk kuvvetlerinin sürekli baskısı altındaydı, ajitasyon ve anarşist örgütlenme yaptığı için Lucy Parsons ile kısa bir süre hapiste yattı. Lizzie ve kocası Chicago’dan Batı Amerika’ya gitmek üzere ayrıldı ve 1889 yılında Colorado’ya taşındılar ve 1890’lı yılların ortalarında New Mexico’ya yerleştiler.

Holmes çeşitli gazete ve dergiler için yazmaya devam etti. 1890’ların ilk yıllarında, <em>Hagar Lyndon</em> adlı bir roman yazdı. 1893 yılında <em>Lucifer, the Light-Bearer’da</em> seri olarak <em>A Woman’s Rebellion,</em> <em>Freedom</em> ve <em>Free Society</em>’de makaleleri yayınlandı. Bazı anarşist çağdaşlarının aksine, daha muhafazakâr yayın organlarında yazılarının yayınlanmasına karşı çıkmadı. <em>American Federation of Labor</em>’a (AFL) (Amerikan İşçi Federasyonu) bağlı <em>American Federationist</em> dergisinde bir dizi makale yayınladı. Özgür aşk, evlilik, cinsiyet eşitsizliği, ekonomik adaletsizlik gibi çeşitli konularda makaleler yazdı. 1908 yılına kadar çeşitli radikal gazeteler için yazmaya devam etti ve ardından sessizce kamusal hayatı terk etti. Lizzie ve kocası, 1926 yılında Santa Fe’ye taşınana kadar, son yıllarının büyük bir bölümünü Albuguergue, New Mexico’da geçirdi. Lizzie Holmes, 8 Ağustos 1926 yılında evinde öldü.

*** Kaynak

 - Lizzie Holmes – Wikipedia https://en.wikipedia.org



