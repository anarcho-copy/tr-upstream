#title Zekanın Asgari Tanımı
#subtitle Kişinin Kendi Benlik Teorisini İnşa Etmesi Üzerine Tezler
#author For Ourselves
#date 16.02.1974
#source 15.03.2023 tarihinde şuradan alındı: [[https://heimatloskolektif.wordpress.com/2023/03/11/zekanin-asgari-tanimi-for-ourselves/][heimatloskolektif.wordpress.com]]
#lang tr
#pubdate 2023-03-15T16:07:31
#topics egoizm, sitüasyonist, benlik teorisi, self determinasyon
#notes <br> **Makine çevirisi sonrası düzenleyen:**  Antarktika, Dobar, Konzept <br>  **Kullanılan makine çeviri hizmeti:** DeepL <br> **Makine çevirisiyle benzerlik oranı:** %70 <br>  **İngilizce Aslı:**  [[https://theanarchistlibrary.org/library/for-ourselves-the-minimum-definition-of-intelligence][The Minimum Definition of Intelligence: Theses on the Construction of One’s Own Self-theory]]
#centerchapter 1
#centersection 1


*** Giriş

Bu kitapçık hayatlarından memnun olmayan insanlar içindir. Şu anki varlığınızdan memnunsanız sizinle hiçbir derdimiz yok. Ancak, hayatınızın değişmesi için beklemekten…

<quote>

Samimi bir topluluk, aşk ve macera için beklemekten…

Paranın ve zorunlu işin sona ermesini beklemekten…

Vakit geçirmek için yeni aktiviteler aramaktan…

Yemyeşil, zengin bir varoluşu beklemekten…

Tüm arzularınızı gerçekleştirebileceğiniz bir durumu beklemekten…

Tüm otoritelerin, yabancılaşmaların, ideolojilerin ve ahlakların sonunu beklemekten yorulduysanız…

…o zaman aşağıdakileri oldukça kullanışlı bulacağınızı düşünüyoruz.

</quote>
*** I

Sefil ama potansiyel olarak harika olan zamanımızın en büyük sırlarından biri, düşünmenin bir zevk olabileceğidir. Bu yazı, kendi benlik-teorinizi oluşturmak için bir kılavuzdur. Kendi benlik-teorinizi inşa etmek devrimci bir zevktir, kendi devrim teorinizi inşa etmenin zevki.

Benlik-teorinizi inşa etmek aynı zamanda hem yıkıcı hem de yapıcı bir zevktir çünkü bu toplumun, yıkıcı ve yapıcı dönüşümü için pratiğin teorisini inşa edersiniz.

Benlik-teorisi bir macera teorisidir. Otantik bir devrim kadar erotik ve esprilidir.

Günümüzün ideolojileri tarafından sizin yerinize düşünülmüş olmanın bir sonucu olarak hissedilen yabancılaşma, bu yabancılaşmanın zevkli bir şekilde reddiyeti arayışına yol açabilir: Kendiniz için düşünmek. Aklınızın hakimiyetini ele geçirme zevki.

Benlik-teorisi, kendi kullanımınız için inşa ettiğiniz eleştirel düşüncenin gövdesidir. Hayatınızın neden böyle olduğunu, dünyanın neden böyle olduğunu analiz ettiğiniz an onu inşa eder ve kullanırsınız. (Ve düşünce; öznel, duygusal deneyimden geldiği için “düşünmek” ve “hissetmek” birbirlerinden ayrılamazlar.) Pratiğin bir teorisini geliştirdiğinizde benlik-teorinizi inşa etmiş olursunuz, hayatınız için arzuladığınız şeyi nasıl elde edeceğinize dair bir teori.

Teori, ya pratik bir teori olacaktır — devrimci pratiğin bir teorisi — ya da hiçbir şey olmayacaktır… dünyanın tefekküre dayalı bir yorumu, bir fikirler akvaryumundan başka hiçbir şey. İdealar alemi, gerçekleşmemiş arzunun ebedi bekleme odasıdır.

Hayatlarının arzularını gerçekleştirmenin ve dolayısıyla kendileri için savaşmanın imkansız olduğunu (genellikle bilinçsizce) varsayanlar, genellikle bunun yerine bir ideal veya dava için savaşırlar (ör. benlik-etkinliği ya da benlik-pratiği illüzyonu). Bunun yabancılaşmanın kabulü olduğunu bilenler artık tüm ideallerin ve davaların birer ideoloji olduklarını bileceklerdir.

*** II

Ne zaman bir fikir sistemi, merkezinde bir soyutlama olacak şekilde yapılandırıldığında — kendi uğruna size bir rol veya görev atayarak — bu sistem bir ideolojidir. İdeoloji, dünya ile ilişkinizde artık özne olarak işlev görmediğiniz bir yanlış bilinç sistemidir.

Çeşitli ideoloji biçimlerinin hepsi farklı soyutlamalar etrafında yapılandırılmıştır, yine de hepsi size fedakarlık, ıstırap ve temsiliyetinizde bir amaç duygusu vererek, egemen (veya egemen olmaya hevesli) bir sınıfın çıkarlarına hizmet eder.

Dini ideoloji en eski örnektir, “Tanrı” denilen fantastik projeksiyon, kainatın Yüce Öznesidir ve her insan üzerinde “O’nun” nesnesi gibi hareket eder.

Burjuva girişiminin “bilimsel” ve “demokratik” ideolojilerinde, sermaye yatırımı dünya tarihini yönlendiren “üretken” öznedir, insan gelişimine rehberlik eden “görünmez el”. Burjuvazi, bir zamanlar dini ideolojinin sahip olduğu güce saldırmak ve onu zayıflatmak zorundaydı. Teknolojik araştırmasında dini dünyanın mistifikasyonunu açığa çıkararak, kâr elde edebileceği şeylerin ve yöntemlerin alanını genişletti.

Leninizmin çeşitli türleri; kendi Partilerinin nesnesini -proletaryayı- burjuva aygıtının yerine Leninist bir aygıtı geçirme hedefine götürerek, dünya tarihini dikte etmek için haklı özne olduğu “devrimci” ideolojilerdir.

Hâkim ideolojilerin diğer birçok biçimi günlük olarak görülebilir. Yeni dindarlıkların yükselişi, toplumsal ilişkilerin egemen yapısına dolaylı yoldan hizmet eder. Günlük hayatın boşluğunun gizlenebileceği düzgün bir form sağlar ve uyuşturucular gibi onunla yaşamayı kolaylaştırır. Gönüllülük (çok çalışmamız gerek) ve determinizm (her şey yoluna girecek) dünyanın işleyişindeki gerçek yerimizi fark etmemizi engeller. Öncü ideolojide önemli olan, kendi içinde (ve kendisinde) yeniliktir. Hayatta kalmacılıkta; öznellik, yaklaşan bir dünya felaketi imgesinin çağrılması yoluyla korku tarafından engellenir.

İdeolojileri kabul ederken özne ve nesnenin tersine çevrilmesini kabul ederiz; şeyler insani bir güç ve irade kazanırken, insanlar yerlerini şeyler olarak alırlar. İdeoloji, tepetaklak bir teoridir. Günlük hayatımızın dar gerçekliği ile kavrayışımızın dışında kalan bir dünya bütünlüğü imgesi arasındaki ayrımı daha da kabulleniriz. İdeoloji, bize yalnızca bir röntgencinin bütünlükle ilişkisini sunar.

Bu ayrımda ve dava için fedakârlığın kabulünde, her ideoloji egemen toplumsal düzeni korumaya hizmet eder. Gücü bu ayrışmaya bağlı olan otoriteler, hayatta kalmak için öznelliğimizi inkar etmek zorundadırlar. Bu tür bir inkar bir çok şey için fedakârlık talep etme şeklinde ortaya çıkar: “ortak yarar”, “ulusal çıkar”, “devrim”….

*** III

Kendimize sürekli şu soruları sorarak ideolojinin at gözlüklerinden kurtuluruz: Nasıl hissediyorum?

<quote>

Keyif alıyor muyum?

Hayatım nasıl?

İstediğimi elde ediyor muyum?

Neden edemiyorum?

İstediğimi elde etmemi engelleyen nedir?

</quote>

Bu, sıradanlığın bilincine sahip olmak, kişinin günlük rutininin farkında olmasıdır. Günlük Hayatın – gerçek hayatın- varlığının, gündelik hayatın sefaleti giderek daha fazla görünür hale geldikçe, her geçen gün daha da az sır hale gelen bir kamu sırrı olduğudur.

*** IV

Benlik-teorisinin inşası; kendiniz için düşünmeye, arzuların ve onların geçerliliğini tamamen bilincinde olmaya dayanır. Radikal öznelliğin inşasıdır.

Gerçek anlamda “bilinç artırımı”, yalnızca insanların düşüncelerinin olumlu (suçluluk duymayan) benlik-bilinci düzeyine “yükseltilmesi” -her türlü ideolojiden ve dayatılmış ahlaktan arınmış olarak temel öznelliklerini geliştirmeleri- olabilir.

Pek çok solcunun, terapi tacirinin, ırkçılık bilinci eğitmeninin ve kız kardeşlerin “bilinç artırımı” olarak adlandırdıkları şeyin özü, insanları ideolojik sopalarıyla döverek bilinçsiz hale getirmeleridir.

İdeolojiden (benliğin olumsuzlanmasından) radikal öznelliğe (benliğin olumlanmasına) giden yol, nihilizmin başkenti Sıfır Noktası’ndan geçer. Bu, sosyal uzay ve zamanın rüzgarlı durgun noktasıdır: kişinin şimdiki zamanın hayattan yoksun olduğunu fark ettiği, günlük varoluşunda hayatın olmadığı sosyal araf… Bir nihilist hayatta kalmak ile yaşamak arasındaki farkı bilir.

Nihilistler, yaşamları ve dünyaya bakış açılarında bir değişimden geçerler. Onlar için arzularından, olma isteklerinden başka hiçbir şey doğru değildir. Modern kapitalist-küresel toplumdaki sefil sosyal ilişkilere duydukları nefretle tüm ideolojileri reddederler. Bu tersine çevrilmiş perspektiften; şeyleşmenin[1] baş aşağı duran dünyasını, özne ve nesnenin, soyut ve somutun tersine çevrilmesini yeni edindikleri bir netlikle görürler. Gördükleri; fetişleştirilmiş metaların, zihinsel yansımaların, ayrımların ve ideolojilerin teatral manzarasıdır: Sanat, Tanrı, şehir planlaması, etik, gülücük rozetleri, sizi sevdiklerini söyleyen radyo istasyonları ve ellerinize merhamet eden deterjanlar…

Günlük konuşmalar; “İstediğini her zaman elde edemezsin”, “Hayatın inişleri ve çıkışları vardır” ve laik hayatta kalma dininin diğer dogmaları gibi sakinleştiriciler sunar. “Sağduyu”, ortak yabancılaşmanın saçmalığından başka bir şey değildir. Her gün insanlar otantik bir yaşamdan mahrum bırakılıyor ve onun temsili geri satılıyor.

Nihilistler, kendilerini her gün yok eden sistemi sürekli olarak yok etme dürtüsünü hissederler. Oldukları gibi yaşamayazlar, çünkü akılları alev alıyor. Çok geçmeden, dünya üzerinde pratik bir etkiye sahip olacak tutarlı bir dizi taktik bulmaları gerektiği gerçeğiyle karşı karşıya kalırlar.

Ama eğer bir nihilist dünyanın dönüşümü için tarihsel olasılığı bilmiyorsa, onun öznel öfkesi bir role dönüşecektir: intihar, yalnız katil, sokak serserisi vandal, neo-dadaist, profesyonel akıl hastası… Hepsi ölü bir yaşam için tazminat istiyor.

Nihilistlerin hatası, başka nihilistlerin de olduğunu fark etmemeleridir. Sonuç olarak, ortak iletişimin ve benliğin gerçekleştirilme projesine katılımın imkansız olduğunu varsaymaktadırlar.

*** V

Kişinin yaşamana karşı “politik” bir yönelime sahip olması; hayatını, ancak dünyayı değiştirmek yoluyla yaşamın doğasının kendisini değiştirerek değiştirebileceğini bilmesidir ve dünyanın bu değişimi kolektif bir çaba gerektirir.

Bu kolektif benliğin gerçekleştirilme projesi doğru bir şekilde siyaset olarak adlandırılabilir. Fakat “siyaset”; mistifike edilmiş, ayrı insan faaliyeti haline gelmiştir. İnsan faaliyetlerinin toplumsal olarak dayatılan diğer tüm ayrımlarıyla birlikte, “siyaset” sadece bir başka ilgi alanı haline geldi. Hatta uzmanları bile var, ister siyasetçi ister politikacı olsun. Futbol, pul koleksiyonculuğu, disko müziği veya moda ile ilgilenmek (veya ilgilenmemek) mümkündür. Bugün insanların “siyaset” olarak gördükleri şey, kolektif benlik-anlayışı projesinin toplumsal olarak çarpıtılmasıdır ve bu da güç sahibi olanların işine gelmektedir.

Kolektif benlik-anlayışı, devrimci projenin kendisidir. Doğanın ve toplumsal ilişkilerin bütünlüğünün kolektif olarak ele geçirilmesi ve bunların bilinçli arzuya göre dönüştürülmesidir.

Gerçek anlamda terapi, sosyal yaşamın doğasını değiştirerek kişinin hayatını değiştirmesidir. Terapinin gerçek bir sonuç verebilmesi için sosyal olması gerekir. Sosyal terapi (toplumun iyileştirilmesi) ve bireysel terapi (bireyin iyileştirilmesi) birbirleriyle bağlantılıdır: her biri diğerini gerektirir, her biri diğerinin gerekli bir parçasıdır.

Örneğin, Gösteri Toplumunda gerçek duygularımızı bastırmamız ve bir rol yapmamız beklenir. Buna da “Toplumda bir rol oynamak” denir. (Ne kadar açıklayıcı bir cümle!) Bireyler karakter zırhlarını giyerler: doğrudan sosyal rol yapmanın sonu ile ilgili çelik benzeri bir rol yapma kıyafeti…

*** VI

Öznel düşünmek, hayatınızı – şimdi olduğu veya olmasını istediğiniz gibi – düşüncenizin merkezi olarak kullanmaktır. Bu olumlu benmerkezcilik, dışsallara karşı sürekli saldırı ile gerçekleşir: tüm sahte meseleler, sahte çatışmalar, sahte problemler, sahte kimlikler ve sahte ikilemler…

İnsanlar her ayrıntıyla ilgili fikirleri sorularak günlük varoluşun tamamını analiz etmekten alıkonulur: tüm gösterişli ıvır zıvır şeyler, düzmece tartışmalar ve yalan skandallar… Sendikalardan, güdümlü füzelerden, kimlik kartlarından yana mısın yoksa karşı mısın? Hafif uyuşturucular, yürüyüş, UFO’lar, kademeli vergilendirme hakkında ne düşünüyorsun?

Bunlar asılsız sorunlardır. Bizim için tek sorun nasıl yaşadığımızdır.

Eski bir Yahudi sözü der ki: “Yalnızca iki seçeneğin varsa, üçüncüyü seç”. Bu, öznenin sorun üzerinde yeni bir bakış açısı aramasını sağlamanın bir yolunu sunar. “Üçüncü seçeneğimizi” seçerek sahte bir çatışmanın her iki tarafını da yalanlayabiliriz: Durumu, radikal öznellik perspektifinden görmek.

Üçüncü seçeneğin bilincinde olmak, kendilerini bir durumun bütünü olarak tanımlamaya çalışan sözde zıt ama aslında denk olan iki kutup arasında seçim yapmayı reddetmektir. En basit haliyle bu bilinç, silahlı soygun suçundan yargılanan ve “Suçunuzu kabul ediyor musunuz, etmiyor musunuz?” sorusuna “İşsizim.” diye yanıt veren işçi tarafından ifade edilir. Daha teorik ama aynı derecede klasik bir örnek, “Batı”nın şirket-kapitalist yönetici sınıfları ile “Doğu”nun devlet-kapitalist yönetici sınıfları arasındaki herhangi bir köklü farkı reddetmektir. Yapmamız gereken tek şey, bir yanda ABD ve Avrupada’ki, diğer yanda SSCB ve Çin’deki temel toplumsal ilişkilere bakmak, bunların özünde aynı olduklarını görmek: Burada olduğu gibi orada da büyük çoğunluk, hem üretim araçları hem de (daha sonra onlara meta biçiminde geri satılan) ürettikleri üzerindeki kontrolden vazgeçme karşılığında bir ücret veya maaş için çalışmaya gidiyor.

“Batı” örneğinde artı değer, (işçi ücretlerinin değerinin üzerinde üretilen değer) yerel rekabet gösterisi yapan şirket yönetimlerinin mülkiyetindedir. “Doğu”da ise artı değer, ülke içi rekabete izin vermeyen ama uluslararası rekabete diğer herhangi bir kapitalist ulus kadar azgınca girişen devlet bürokrasisinin malıdır. Ne kadar da farklı(!)

Sahte bir sorunun bir örneği şu salakça “Hayat felsefeniz nedir?” sorusudur. Ortaya, kelimenin konuşmalarda sürekli geçmesine rağmen, gerçek hayatla hiçbir alakası olmayan soyut bir “Hayat” kavramı atar.

Gerçek topluluğun yokluğunda insanlar, nasıl kendileri için yaşayacaklarını unutmaları amacıyla hayatın ne olduğuna dair imgeleri tasarladıkları ve tükettikleri Gösteri’deki bireysel rollerine karşılık gelen her türlü sahte sosyal kimliğe sarılırlar. Bu sosyal kimlikler; etnik (“İtalyan”), ırksal (“Siyahi”), örgütsel (“Sendikacı”), yerleşim yeri (“New York’lu”), yönelimsel (“Gay”), kültürel (“Spor sever”) ve benzeri olabilir ancak hepsinin kökünde ortak bir bağlanma, ait olma arzusu yatar.

Apaçık olarak “siyahi” olmak bir kimlik olarak “spor sever” olmaktan çok daha gerçektir ancak belli bir noktadan sonra bu kimlikler sadece toplumdaki gerçek konumumuzu maskelemeye yarar. Yine, bizim için tek sorun nasıl yaşadığımızdır. Somut olarak bu, kişinin bir bütün olarak toplumla olan ilişkisinde yaşamının doğasının sebeplerini anlamak anlamına gelir. Bunu yapmak için bireyin tüm sahte kimliklerden, kısmi bağlantılardan kurtulması ve merkez olarak kendisiyle başlaması gerekir. Buradan yaşamın maddi temelini tüm mistifikasyonlardan arınmış bir biçimde inceleyebiliriz.

Örneğin, iş yerindeki makineden bir fincan kahve istediğimi varsayalım. Her şeyden önce kahve tarlalarında, şeker tarlalarında ve rafinerilerde, kağıt fabrikalarında vb. çalışan işçileri içeren bir fincan kahvenin kendisi var. Ardından, makinenin farklı parçalarını yapan ve monte eden tüm işçiler var. Onlardan sonra, demir cevherini ve boksiti çıkaranlar, çeliği eritenler, petrolü sondajlayanlar ve rafine edenler. Sonra, hammaddeleri ve parçaları üç kıta ve iki okyanus üzerinden taşıyan tüm işçiler. Daha sonra, üretim ve nakliyeyi koordine eden memurlar, daktilograflar ve iletişim çalışanları. Son olarak, başkalarının hayatta kalması için gerekli olan tüm şeyleri üreten bütün işçiler var. Bu bana birkaç milyon insanla, hatta dünya nüfusunun büyük çoğunluğuyla doğrudan maddi bir ilişki sağlamakta. Onlar benim hayatımı üretiyorlar ve ben de onlarınkini üretmeye yardım ediyorum. Bu ışık altında; tüm kısmi grup kimlikleri ve özel çıkarlar önemsizleşir. Bireyin, şu anda milyonlarca işçinin hüsrana uğramış yaratıcılıklarında hapsolmuş, eskimiş ve yorucu üretim yöntemleri tarafından engellenmiş, yabancılaşma tarafından boğulmuş, sermaye birikiminin akıldışı mantığı tarafından çarpıtılmış hayatının potansiyel zenginleşmesini hayal edin! Tam da burada gerçek bir sosyal kimlik keşfetmeye başlarız: Dünyanın dört bir yanında hayatlarını geri kazanmak için mücadele eden insanların içinde, kendimizi buluruz.

Bizden sürekli olarak sahte bir çatışmada iki taraf arasında seçim yapmamız bekleniyor. Hükümetler, hayır kurumları ve her türden propagandacılar, bize aslında seçenek olmayan seçenekler sunmayı severler (ör. Merkezi Elektrik Üretim Kurulu nükleer programını “Ya Nükleer Devir, ya Taş Devri” diye sundu. MEÜK, bizden bunların tek iki alternatif olduğuna inanmamızı istiyor; seçme şansımız olduğu yanılsamasına sahibiz, fakat onlar mevcut olarak algıladığımız seçenekleri kontrol ettikleri sürece, sonucu da kontrol ederler).

Yeni ahlakçılar, zengin Batı’dakilere nasıl “fedakârlık yapmaları gerektiğini”, nasıl “Üçüncü Dünya’nın aç çocuklarını sömürdüklerini” anlatmaya bayılırlar. Bize verilen seçim, kurbanlık diğerkâmlık ya da sığ bireycilik arasındadır. (Hayır kurumları, bağış kutularındaki bozuk para karşılığında bize bir şeyler yapmış olma hissi sunarak sunarak ortaya çıkan suçluluk duygusundan para kazanır.) Evet, zengin Batı’da yaşayarak Üçüncü Dünya’nın yoksullarını sömürüyoruz ama kişisel veya kasıtlı olarak değil. Hayatımızda bazı değişiklikler yapabilir, boykot edebilir, fedakârlıklarda bulunabiliriz, ancak etkileri marjinal olacaktır. Bu küresel sosyal sistem altında, biz, bireyler olarak, bizim “sömürenler” olarak küresel rolümüze kilitlendiğimiz kadar, diğerlerinin de sömürülenler olarak küresel rollerine kilitlendiğini fark ettiğimiz an, bize sunulan sahte çatışmanın bilincine varırız. Toplumda bir rolümüz var, ancak bu konuda herhangi bir şey yapma gücümüz ya çok az ya da hiç yok. “Fedakârlık ya da Bencillik” sahte seçimini, varlığı bizi bu kararı vermeye zorlayan küresel sosyal sistemin yok edilmesi çağrısında bulunarak reddediyoruz. Bu, sistemle oynamak, sembolik fedâkarlıklar sunmak, veya “biraz daha az bencillik” talebinde bulunmak değildir. Hayır kurumları ve reformcular sahte seçim alanından asla kopamazlar.

Mevcut durumun sürdürülmesinde çıkarı olanlar bizi sürekli olarak kendi sahte seçeneklerine, yani ellerindeki gücü koruyacak herhangi bir seçeneğe geri sürüklerler. “Eğer hepsini paylaşırsak, etrafta dönecek kadar kalmaz.” gibi mitlerle, başka seçeneklerin varlığını inkar etmeye ve toplumsal devrimin maddi önkoşullarının zaten var olduğu gerçeğini bizden gizlemeye çalışırlar.

*** VII

Kendini mistifikasyondan arındırmaya doğru yapılan her yolculuk, kayıp düşüncenin o iki bataklığından kaçınmalıdır: mutlakçılık ve kinizm, kendilerini öznellik çayırları olarak kamufle eden ikiz bataklıklar.

Mutlakçılık, belirli ideolojilerin, gösterilerin ve metalaştırılmaların tüm bileşenlerinin toptan kabulü veya reddidir. Bir mutlakçı, tamamen kabul veya tamamen retten başka bir seçenek göremez.

Mutlakçı, ideolojiler marketinin raflarında dolaşarak ideal metayı arar ve sonra onu satın alır, tamamıyla. Ancak ideoloji marketi -herhangi bir süpermarket gibi- yalnızca yağmalamaya uygundur. Raflar arasında gezebilir, paketleri yırtıp açabilir, orijinal ve kullanışlı görünenleri çıkarıp gerisini çöpe atabilirsek bizim için daha verimli olur.

Kinizm, ideoloji ve ahlakın baskın olduğu bir dünyaya karşı bir tepkidir. Çatışan ideolojilerle karşı karşıya kalan kinik der ki: “Her ikinizin de canı cehenneme.”. Kinik de mutlakçı kadar bir tüketici, ama ideal metayı bulma umudunu yitirmiş biridir.

*** VIII

Diyalektik düşünme süreci yapıcı düşünme eylemidir, bireyin mevcut benlik-teorisini yeni gözlemler ve benimsemelerle sürekli olarak sentezleme sürecidir, önceki teori bütünü ile yeni teorisel unsurlar arasındaki çelişkilerin çözümlenmesidir. Dolayısıyla ortaya çıkan sentez, eski ve yeninin niceliksel bir toplamı değil; bunların niteliksel olarak birbirlerinin aşımı olan yeni bir bütünlüktür.

Bir teori inşa etmenin bu sentetik / diyalektik yöntemi, ortaya çıkan çelişkilerle hiç yüzleşmeden favori ideolojilerden favori parçalarını bir araya getiren eklektik tarza karşıdır. Modern örnekler arasında genel olarak özgürlükçü kapitalizm, hristiyan marxism ve liberalizm yer alır.

Nasıl yaşamak istediğimizin sürekli bilincinde olursak, benlik-teorimizin inşasında her şeyden eleştirel bir şekilde yararlanabiliriz: ideolojiler, kültür eleştirmenleri, teknokratik uzmanlar, sosyolojik çalışmalar, mistikler ve benzeri… Eski dünyanın tüm çöpleri, onu yeniden inşa etmek isteyenler tarafından faydalı malzemeler için ayıklanabilir.

*** IX

Modern toplumun doğası, küresel ve kapitalist birliği, bize benlik-teorinizi bütünsel bir eleştiri haline getirme gerekliliğini göstermektedir. Bununla, çeşitli sosyo-ekonomik hakimiyet biçimlerinin (yani hem “özgür” dünyanın kapitalizminin hem de “komünist” dünyanın devlet-kapitalizminin) var olduğu tüm coğrafi alanların, aynı zamanda da tüm yabancılaşmaların (cinsel yokluk, zorunlu hayatta kalma, şehircilik vb.) eleştirisini kastediyoruz. Başka bir deyişle, bireyin arzularının bütünü açısından, her yerde gündelik varoluşun bütününün bir eleştirisi.

Bu projenin karşısında, bireysel arzuları, kendilerini sözde onun temsilcileri olarak belirleyen metalaşmış bir “ortak iyi”ye boyun eğdirmek için çalışan tüm politikacılar ve bürokratlar, vaizler ve gurular, şehir planlamacıları ve polisler, reformcular ve militanlar, merkezi komiteler ve sansürcüler, şirket yöneticileri ve sendika liderleri, erkek üstünlükçüler ve feminist ideologlar, psiko-sosyologlar ve korumacı kapitalistler yer almaktadır. Hepsi eski dünyanın güçleri, insanlar zihinlerinin kontrolünü ellerine alma oyununu hayatlarını geri almaya genişletirse kaybedecek bir şeyleri olan tüm patronlar, rahipler, sürüngenler…

Devrimci teori ve devrimci ideoloji birbirlerine düşmandır ve her ikisi de bunun farkındadır.{1}

*** X

Şimdiye kadar kendini mistifikasyondan arındırmanın ve kendi devrimci teorimizi inşa etmenin yabancılaşmamızı ortadan kaldırmayacağı açık olmalı, “Dünya” (sermaye ve Gösteri) her gün kendini çoğaltarak devam ediyor.

Bu kitapçığın odak noktası benlik-teorisinin inşası olsa da, asla devrimci teorinin devrimci pratikten ayrı olarak var olabileceğini ima etmeyi amaçlamadık. Sonuç verici olmak, dünyayı etkili bir şekilde yeniden inşa etmek için, pratik teorisini aramalı ve teori pratikte gerçekleştirilmelidir. Yabancılaşmadan kurtulmaya ve toplumsal ilişkilerin dönüşümüne dair devrimci beklenti, bireyin teorisinin pratiğin bir teorisinden, ne yaptığımıza ve nasıl yaşadığımıza dair bir teoriden başka bir şey olmamasını gerektirir. Aksi takdirde teori, dünyanın kifayetsiz bir yorumuna ve nihayetinde de hayatta kalma ideolojisine, günlük dünya ile kişinin kendisi arasında bir tampon görevi gören, yansıtılmış bir zihinsel sis kümesi, metalaşmış düşüncenin entelektüel zırhın statik bir gövdesi haline dönüşecektir. Ve eğer, devrimci pratik devrimci teorinin pratiği değilse, diğerkam militanizme, kişinin toplumsal görevi olarak “devrimci” faaliyete yozlaşır.

Sadece kendi içinde bir amaç olarak tutarlı olan bir teori için çabalamıyoruz. Bizim için tutarlılığın pratik kullanım değeri, tutarlı bir benlik-teorisine sahip olmanın, kişinin düşünmesini kolaylaştırmasıdır. Örneğin, günümüze kadar gelen modern sosyal kontrol ideolojileri ve teknikleri hakkında tutarlı bir anlayışa sahipseniz, sosyal kontrol altında gelecekteki gelişmeleri ele almak daha kolaydır.

Tutarlı bir teoriye sahip olmak, yaşamınız için arzularınızı gerçekleştirmeye yönelik teorik pratiği kavramanızı kolaylaştırır.

*** XI

Benlik-teorisinin inşa sürecinde, mücadele edilmesi ve kararlılıkla tespit edilmesi gereken son ideolojiler, devrimci teoriye en çok benzeyenlerdir. Bu nihai mistifikasyonlar a) Sitüasyonizm b) Konseyciliktir.

Sitüasyonist Enternasyonal (1958-1971) devrimci teoriye muazzam katkılarda bulunmuş enternasyonal devrimci bir örgüttü. Sitüasyonist teori, kişinin benlik-teorisine mal edebileceği bir eleştirel teori bütününden başka bir şey değil. Daha fazlası, sitüasyonizm olarak bilinen ideolojik suiistimaldir. SI teorisi, kişinin ölü hayatının bilmecesinin cevabı gibi görünmenin bir yoluna sahip olduğundan onu yeni keşfedenlerin ‘yıllardır aradığı cevap’ gibi gözükür. Ancak tam da bu noktada yeni bir uyanıklık ve kendine hâkimiyet gerekli hale gelir. Sitüasyonizm tam anlamıyla bir hayatta kalma ideolojisi ve günlük hayatın yıpratıcılığına karşı bir savunma mekanizması olabilir. İdeolojiye dahil olan şey ise ‘bir sitüasyonisttir’, yani radikal bir yeşim taşı ve ateşli bir ezoterik olmanın olağanüstü meta rolüdür.

Konseycilik (diğer adıyla ‘İşçi Kontrolü’ veya ‘Sendikalizm’) kapitalist üretim sisteminin yerine ‘kendi kendini yönetmeyi’ önermektedir.

Gerçek özyönetim, toplumsal üretim, dağıtım ve iletişimin işçiler ve onların toplulukları tarafından (herhangi bir ayrı liderlik tarafından aracılık edilmeksizin) doğrudan yönetilmesidir. Özyönetim hareketi, toplumsal devrim sürecinde dünyanın her yerinde tekrar tekrar ortaya çıkmıştır. Rusya 1905 ve 1917-21’de, İspanya 1936-7’de, Macaristan 1956’da, Cezayir 1960’da, Şili 1972’de ve Portekiz 1975’te. Özyönetim pratiğinde en sık yaratılan örgütlenme biçimi faaliyetlerini koordine etmek üzere yetkili delegeler seçen üreticilerin ve mahallelerin egemen genel kurulları olan işçi konseyleri olmuştur. Delegeler temsilci değildir, ancak kurulları tarafından önceden alınmış kararları uygularlar. Genel kurul, kararlarının titizlikle uygulanmadığını düşündüğü takdirde, delegeler her zaman geri çağrılabilir.

Konseycilik, bu tarihsel özyönetim pratiği ve teorisinin bir ideolojiye dönüşmüş halidir. Bu ayaklanmalara katılanlar, ücretli emeğin, meta ekonomisinin ve mübadele değerinin eleştirisiyle başlayan bir toplumsal bütünlük eleştirisi yaşarken, konseycilik kısmi bir eleştiri yapar: Tüm dünyanın özyönetimli, sürekli ve niteliksel dönüşümünü değil, dünyanın olduğu gibi statik, niceliksel özyönetimini arar. Böylece ekonomi, gündelik hayatın geri kalanından kopuk ve ona hükmeden ayrı bir alan olarak kalmaktadır. Öte yandan, genelleştirilmiş özyönetim hareketi toplumsal yaşamın tüm sektörlerinin ve tüm toplumsal ilişkilerin (üretim, cinsellik, barınma, hizmetler, iletişim vb.) dönüştürülmesini isterken, konseycilik özyönetimli bir ekonominin tek önemli şey olduğunu düşünür. Kelimenin tam anlamıyla bütün meseleyi gözden kaçırıyor: Öznellik ve yaşamın bütününü dönüştürme arzusu. İşçilerin kontrolüyle ilgili sorun, kontrol ettikleri tek şeyin iş olmasıdır.

Dünya, ancak onun neden alt üst olduğuna dair bir teori inşa edenlerin bilinçli kolektif faaliyetleriyle doğru tarafa döndürülebilir. Spontane ayaklanma ve isyancı öznellik tek başına yeterli değildir. Otantik bir devrim ancak geçmişin tüm mistifikasyonlarının bilinçli bir şekilde ortadan kaldırıldığı pratik bir hareket içinde gerçekleşebilir.

*** Son Notlar

Bu kitapçık, örgütümüz üyelerinin kolektif benlik-teorilerinin bir parçasıdır. Bizim meta-teori dediğimiz şeyin, teori yapma pratiği teorimizin ifadesidir.

Zekânın Asgari Tanımı’nın hazırlanışı ve dağıtımı, yaptığımız diğer her şeyle aynı nedenle gerçekleştirilmektedir çünkü yabancılaşmanın mevcut statik düzenini, gerçekleşen hayallerin hareketli manzarasına dönüştürecek bir toplumsal devrime ilham vermek istiyoruz. İstediğimiz hayatları ancak diğer herkesin istedikleri yaşamları yaratma sürecinde yaratabileceğimizi biliyoruz. Bizler devrimciyiz çünkü arzularımız toplumsal bir devrimi gerektiriyor.

Dünya ancak, neden baş aşağı olduğuna dair bir teori inşa edenlerin bilinçli kolektif faaliyeti ile doğru tarafa döndürülebilir. Spontane isyan ve direnişçi öznellik tek başına tek başına yeterli değildir. Gerçek bir devrim yalnızca geçmişin tüm mistifikasyonlarının silinip süpürüldüğü pratik bir hareket içinde gerçekleşebilir.

*** Ek olarak: Kurucu Anlaşmaların Önsözü

Hayatlarımızın yaşanmaz hale geldiğine uyanmış durumdayız. Sıkıcı, anlamsız işlerden, hayatta kalma payımızı almak için kuyruklarda, masalarda ve tezgahlarda sonsuza kadar beklemenin aşağılanmasına, hapishane benzeri okullardan, tekrarlayan akılsız “eğlence”ye, ıssız ve suç dolu sokaklardan evin boğucu izolasyonuna kadar, günlerimiz sadece aynı yerde kalmak için daha ve daha hızlı koştuğumuz bir koşu bandıdır.

Nüfusun büyük bir çoğunluğu gibi biz de hayatlarımızın nasıl kullanılacağı üzerinde hiçbir kontrole sahip değiliz, bizler çalışma kapasitemizden başka satacak bir şeyleri olmayan insanlarız. Bir araya geldik çünkü artık var olmaya zorlandığımız şekle tahammül edemiyoruz, yalnızca her geçen gün daha yabancı ve çirkin hale gelen bir dünya yaratılması uğruna enerjilerimizin sıkıştırılarak tüketilip çöpe atılmasına daha fazla tahammül edemiyoruz.

Sermaye sistemi, ister “Batılı” özel şirket ister “Doğulu” bürokratik devlet biçimiyle olsun, yükselişi sırasında bile acımasız ve sömürücüydü şimdi de -çürümekte olduğu yerde- havayı ve suyu kirletiyor, kalitesi düşen mal ve hizmetler satıyor ve bizi kendi yararına bile giderek daha az kullanabilir hale geliyor. Birikim ve rekabet mantığı, kaçınılmaz olarak kendi çöküşüne yol açıyor. Dünyadaki tüm insanları geniş bir üretim ve tüketim ağında birbirine bağlarken bile, bizi birbirimizden izole ediyor; teknoloji ve üretim gücünde giderek daha büyük ilerlemeleri teşvik ederken bile, kendisini bunları kullanmakta yetersiz buluyor. İnsanın kendini gerçekleştirme olanaklarını çoğaltırken bile, kendimizi suçluluk, korku ve kendini küçümseme katmanlarında boğulmuş buluyoruz.

Ancak gücümüz, zekamız, yaratıcılığımız, tutkularımız ile en muazzam üretken güç <em>biz kendimiziz</em><em>.</em> Dünyayı olduğu gibi, Sermaye’nin suretinde üreten ve çoğaltan bizleriz; aile, okul, kilise ve medyanın koşullandırmasını, bizi köle olarak tutan koşullandırmayı birbirimizde pekiştiren bizleriz. Sefaletimizi sona erdirmeye, hayatlarımızı kendi ellerimize almaya birlikte karar verdiğimizde, dünyayı istediğimiz şekilde yeniden yaratabiliriz. Eski sistem altında geliştirilen teknik kaynaklar ve dünya çapındaki üretim ağı bize bu araçları sağlıyor, bu sistemin krizi ve süregelen çöküşü bize bu şansı ve acil ihtiyacı veriyor.

Dünya süper güçlerinin hakim ideolojileri, iç içe geçmiş yalanlarıyla bize yalnızca “Komünizm”e karşı “Kapitalizm” gibi sahte bir seçenek sunmaktadır. Fakat bu yüzyıldaki devrim tarihinde (Rusya 1905, Almanya 1919-20, İspanya 1936-37, Macaristan 1956) kendi yaşamlarımız üzerindeki iktidarı geri alabileceğimiz genel biçimi keşfettik: İşçi Konseyleri. En yüksek anlarında bu konseyler, işyerleri ve topluluklardaki kitle meclisleriydi; meclisleri tarafından <em>önceden alınmış</em> kararları uygulayan ve herhangi bir zamanda onlar tarafından geri çağrılabilen, sıkı bir şekilde denetlenen delegeler aracılığıyla bir araya geliyorlardı. Konseyler kendi savunmalarını örgütlediler ve kendi yönetimleri altında üretimi yeniden başlattılar. Artık modern telekomünikasyon ve veri işlemeyi kullanarak yerel, bölgesel ve küresel düzeyde bir konseyler sistemi aracılığıyla dünya üretimini koordine edip planlayabilir ve kendi yakın çevremizi şekillendirmekte özgür olabiliriz. Sözde “Komünist” ülkelere iyi bir bakışın göstereceği gibi, bürokrasi ve resmi hiyerarşi ile herhangi bir uzlaşma, işçi konseylerinin mutlak gücü dışında herhangi bir şey, sadece sefalet ve yabancılaşmayı yeni bir biçimde tekrar üretecektir. Bu nedenle, hiçbir siyasi parti devrimci hareketi temsil edemez veya “onun adına” gücü ele geçiremez çünkü bu sadece egemen sınıfların değişmesi anlamına gelir, ortadan kaldırılması değil. Bir araya gelmiş özgür üreticilerin planı, devlet ve şirket üretiminin dayatıcı planına mutlak bir karşıtlık içindedir. <em>Bizim için neyin en iyi olduğuna ancak hepimiz birlikte karar verebiliriz</em>.

Bu sebeplerden dolayı, sizi ve hem sizin hem de bizim gibi yüz milyonlarca insanı, yaşamın her alanının devrimci dönüşümünde bize katılmaya çağırıyoruz. Ücretli ve maaşlı emek sistemini, meta değişim değerini ve kârı, şirket ve bürokratik iktidar sistemini ortadan kaldırmak istiyoruz. Yaptığımız her şeyin doğasına ve koşullarına karar vermek, tüm toplumsal yaşamı kolektif ve demokratik olarak yönetmek istiyoruz. Keyifli yaratıcı faaliyetler için tüm yeteneklerimizi devreye sokarak, zihinsel işlerin el işlerinden ve “boş” zamanların çalışma zamanlarından ayrılmasına son vermek istiyoruz. Günlerimiz merak, öğrenme ve zevk ile dolması için tüm dünyanın bizim bilinçli benlik eserimiz olmasını istiyoruz. <em>Daha azını değil</em>.

Bu asgari programı belirlerken ne gerçekliğe bir ideal dayatmaya çalışıyoruz ne de istediğimizi istemekte yalnızız. Fikirlerimiz bilinçli ya da bilinçsiz olarak zaten herkesin zihnindedir çünkü bunlar gezegenin her yerinde var olan <em>gerçek hareketin</em> ifadesinden başka bir şey değildir. Ancak bu hareketin kazanabilmesi için kendisini, amaçlarını <em>ve düşmanlarını</em> daha önce hiç olmadığı kadar iyi tanıması gerekmektedir.

Biz bu hareket adına değil, bu hareketin bir parçası olarak kendimiz için konuşuyoruz. Kendimizin üzerinde ve üstünde hiçbir Dava tanımıyoruz. Ancak benliklerimiz zaten toplumsaldır {2}: tüm insan ırkı, her bir üyesinin yaşamını üretir, hatta şimdi her zamankinden daha fazla. Amacımız basitçe bu süreci ilk kez bilinçli hale getirmek, insan yaşamının üretimine bir sanat eserinin yaratıcı yoğunluğunu kazandırmaktır.

İşte bu anlayışla sizleri, bizim yaptığımız gibi, çalıştığınız ve yaşadığınız yerlerde örgütlenmeye, toplumu birlikte nasıl yönetebileceğimizi planlamayı başlamaya, hepimize dayatılan ve giderek derinleşen sefalete karşı kendinizi savunmaya çağırıyoruz. Sizi, dünya etrafımızda parçalanırken herkesi olduğu yerde donduran yalanlara, korkudan doğan kendini kandırmalara aktif olarak saldırmaya çağırıyoruz. Sizi bizimle ve aynı şeyi yapmakta olan diğerleriyle bağlantı kurmaya çağırıyoruz. Her şeyden önce, sizi kendinizi ve arzularınızı ciddiye almaya, kendi yaşamlarınıza hükmetme gücünüzün farkına varmaya çağırıyoruz.

Ya şimdi ya hiç. Eğer bir geleceğimiz olacaksa, o gelecek biz kendimiz olmalıyız.

<strong>KENDİMİZ İÇİN!</strong>

<right>
16 Şubat, 1974
</right>

[1] Şeyleşme (Reification) — İnsanları, soyut kavramları vb. şeylere (mesela metalara) dönüştürme eylemi.

{1} Buradaki bu cümle Guy Debord’ un [[/library/guy-debord-gosteri-toplumu#toc129][Gösteri Toplumu’ nun 124. Tezinden]] alıntıdır. Debord’un bu tezi ise Marx’ ın “[[https://www.marxists.org/archive/marx/works/1844/manuscripts/comm.htm][Komünizm tarihin çözülmüş bilmecesidir ve kendisinin bu çözüm olduğunu bilir]].” cümlesinin “aşırılmasıdır”. (ç.n.)
{2} bkz. [[https://www.marxists.org/archive/marx/works/1845/theses/theses.htm][Feuerbach üzerine tezler 6.tez]] (ç.n.)


