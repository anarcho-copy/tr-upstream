#title Ted Kaczynski ile Röportaj
#author Blackfoot Valley Dispatch, Theodore Kaczynski
#SORTauthors J. Alienus Rychalski, Theodore Kaczynski
#SORTtopics röportaj, teknoloji, ilkelcilik, eleştiri, mektup, tekno-endüstriyel karşıtı
#date 2001
#source 07.05.2022 tarihinde şuradan alındı: [[https://vahsikaracam.blogspot.com/2020/02/ted-ile-roportaj.html][vahsikaracam.blogspot.com]]
#lang tr
#pubdate 2022-05-07T16:49:57
#notes Çeviri: Karaçam <br>İngilizce Aslı: [[https://theanarchistlibrary.org/library/ted-kaczynski-an-interview-with-ted-kaczynski][An Interview with Ted Kaczynski]] <br>Çeviriye Esas Alınan Metin: <em>Technological Slavery, Feral House, 2010 (An Interview with Ted, sayfa. 392-408)</em> <br>Röportajı Yapanın Notu: <em>Kaczynski’nin saklı kulübesinin bulunduğunu iddia eden yayınların aksine bu kulübe bulunamamıştır.</em> <br> Röportajı Gerçekleştiren: <em>J. Alienus Rychalski, Blackfoot Valley Dispatch’in (Lincoln, Montana) Özel Muhabiri</em>


*** Birinci Bölüm

<em>1999 yılında Theodore J. Kaczynski’den</em> Blackfoot Valley Dispatch <em>için bir röportaj talebinde bulundum. Bu teklifi büyük bir incelikle kabul etti. Röportaj aynı yıl Florence, Colorado’daki Yüksek Güvenlikli hapishanede gerçekleşti.</em>

<strong>BVD:</strong> Evet...

<strong>TJK:</strong> Evet...

<strong>BVD:</strong> Evet, neden Berkeley’deki işinden ve matematikteki kariyerinden ayrıldın?

TJK: Berkeley’deki işi kabul ettiğim zaman, bunu zaten en fazla iki yıl boyunca yapacağıma ve sonrasında gidip ormanda yaşayacağıma önceden karar vermiştim. Hayatımı yalnızca bir matematikçi olarak geçirmek fikri beni hiçbir zaman tatmin etmemişti. Çocukluğumdan beri medeniyetten kaçmayı hayal etmiştim – ıssız bir adada ya da başka bir vahşi yerde yaşamak gibi.

Fakat sorun bunun nasıl yapılacağını bilmememdi ve medeniyet ile ilgili düşünceleri bir kenara bırakıp, cesaretimi toplayarak ormanda yaşamaya gitmek oldukça zordu. Bu çok zor bir şeydir. Çünkü yaptığımız seçimlerin ne kadar büyük bir miktarının çevremizdeki insanların beklentileri ile şekillendiğini fark etmeyiz ve gidip diğer insanların bizi çılgın olarak değerlendireceği bir şey yapmak—bu çok zor bir şeydir. Dahası gerçekte nereyi gideceğimi de bilmiyordum.

Fakat Michigan Üniversitesi’ndeki son yılımın başlangıcında bir krizin içinden geçtim. Bizi topluma bağlayan psikolojik zincirlerin benim için koptuğunu söyleyebilirim. Bundan sonra sistemden kopmak ve vahşi bir yere gidip orada yaşamaya çalışmak için gerekli olan cesarete sahip olduğumun bilincindeydim. Berkeley’e gittiğimde, hiçbir zaman orada sonsuza kadar durmak gibi bir amacım yoktu. Berkeley’deki işi yalnızca bir yer satın almak üzere başlangıç için para biriktirmek amaçlı kabul etmiştim.

<strong>BVD:</strong> Çocukluk yıllarında vahşi bir yere gidip orada yaşamak gibi bir hayalin olduğunu söyledin. Bu tarz hayaller kurmana sebep olan şeyin ne olduğunu hatırlıyor musun? Gördüğün ya da tecrübe ettiğin bir şey?

<strong>TJK:</strong> Tabii ki okuduğum şeyler beni o doğrultuda yönlendirmişti. Robinson Crusoe bunlardan birisiydi. Ve sonrasında 11 ya da 12 yaşlarındayken, Neandertaller ve nasıl yaşadıkları hakkında spekülasyonlarda bulunan antropoloji kitapları okudum. Bu tarz şeyleri okumak konusunda müthiş bir ilgi duymaya başladım ve bir noktada neden sürekli bu tarz şeyler okumak istediğimi kendime sordum. Ve fark ettim ki, gerçekten yapmak istediğim şey aslında bu konular hakkında daha fazla kitap okumak değildi; o anlatılan tarzda yaşamaktı.

<strong>BVD:</strong> Bunların seni bu yönde eyleme geçecek kadar etkilemeleri çok ilginç. Crusoe ya da Neanderthal’lerin hayatında sana çekici gelen şeylerin neler olduğunu düşünüyorsun?

<strong>TJK:</strong> O zamanlar bu hayat tarzına yönelik bende oluşan ilginin sebebini bildiğimi sanmıyorum. Şimdi bunun büyük oranda özgürlük ve şahsi otonomi ile alakalı olduğunu düşünüyorum.

<strong>BVD:</strong> Bu tarz şeyler çoğu insana çekici geliyordur. Peki neden herkes ... ?

<strong>TJK:</strong> Birçok insanın bu tarz şeylere ilgisinin olduğunu düşünüyorum; fakat gerçek anlamda bağlarını koparıp, fiili olarak gidip böyle bir şey yapmaya kararlı değiller. Robinson Crusoe’nun şimdiye kadar yazılmış en çok okunan kitaplardan biri olduğu söyleniyor. Yani birçok kişiye çekici geldiği açık. Benim davamla ilgilenen bir araştırmacı, Montana’da sürdüğüm hayat tarzının çok ilgisini çektiğini ve davam hakkında konuştuğu başka birçok kişinin de bu yaşam tarzını oldukça merak ettiklerini söyledi. Ve bu araştırmacının konuştuğu birçok kişi bana imrendiklerini söylüyorlarmış. Aslına bakarsan, beni tutuklayan FBI ajanlarından birisi bana “Buralardaki yaşamına çok özeniyorum.” demişti. Yani bu şekilde hisseden çok sayıda insan var fakat akıntıya kapılıp giderler ve kopuş noktasına gelmezler.

<strong>BVD:</strong> Sen bu kopuşu yaşadığın zaman Lincoln, Montana’ya gittin. Neden Lincoln?

<strong>TJK:</strong> Aslında ilk önce Britanya Kolumbiyası’ndaki bir kraliyet arazisine başvurmuştum. Fakat sanırım bir yıl sonra bunu reddettiler. Bundan sonraki kışı, 1970-1971 kışını, Lombard, Illinois’daki ebeveynlerimin evinde geçirdim. Bu arada kardeşim, Montana Great Falls’ta yaşamaya gitti ve orada Anaconda Şirketi’nin döküm fabrikasında işe girdi. Bu kış sırasında anneme yazdığı bir mektupta, eğer ülkenin o bölgesinde bir yer almak istiyorsam benimle yarı yarıya ortak olabileceğini söyledi. Böylece o ilkbahar Great Falls’a gittim, kardeşimi buldum ve teklifini kabul ettim. Karakteristik bir pasiflik ile yer bulma işini bana bıraktı.

Ne yapacağımı bilmediğim için, 200. otoyoldan, sanırım o zamanlar 20. otoyoldu, batıya doğru gidip orada neler olduğuna bakmaya karar verdim. Lincoln’dan geçerken orada küçük bir kabin gördüm. Yolun kenarında küçük bir kiosk gibiydi ve üzerinde emlakçı tabelası vardı. Durup ismi Ray Jensen olan yaşlı emlakçıya bana ücra bir yerde bir toprak parçası gösterip gösteremeyeceğini sordum. Bana Stample Pass Road’un oralarda bir yer gösterdi. Orayı beğendim. Kardeşimi de getirip gösterdim o da beğendi ve böylece orayı aldık. 2100$ -20$’lık banknotlarla- peşin ödemeyi sahibine, Cliff Gehring’e yaptık.

<strong>BVD:</strong> Yani aslında herhangi bir yer olabilirdi.

<strong>TJK:</strong> Evet.

*** İkinci Bölüm

<strong>BVD:</strong> Oraya ilk taşındığında Lincoln nasıl bir yerdi?

<strong>TJK:</strong> Kasabanın kendisi bana o kadar farklı gelmiyor. Çok fazla değişiklik olduğunu görmüyorum. Fakat yeni okul, kütüphane ve açılan birkaç yeni iş yeri gibi bazı değişiklikler vardı. Belki de kasaba ile ilgilenseydim oradaki değişiklikleri daha iyi fark edebilirdim fakat ilgilenmediğim için değişiklikleri çok fazla fark etmedim.

Benim ilgilendiğim şey çevredeki kırsal alandı. Ve bu alan yalnızca kerestecilik ve yol yapımı sebebiyle değil, çok sayıda insan bu bölgeye taşındığı için de büyük bir değişikliği uğradı. Mesela Stample Pass Road. Stample Pass Road’da çok daha az sayıda yerleşim yeri vardı ve çoğu yalnızca kulübeydi. Modern kulübeler değil, fakat on yıllar on yıllar önce yapılmış kabinler. Ve devamlı olarak orada yaşayan az sayıdaki insan gerçek eski topraktı, başka kültüre mensup modern olmayan insanlar. O zamanlarda Stample Pass Road eski vahşi batı günlerinden kalmış gibiydi.

Stample Pass Road boyunca şimdi bir gezintiye çıksanız çevredeki ormanlık alan ile uyumsuz cafcaflı, abartılı modern yapıları görürsünüz. Fakat eskiden orada olan birkaç kabin cafcaflı değildi. Modern değillerdi. Ve ailem 1970’lerin başında bana ziyarete geldiğinde Stample Pass Road’da bir gezintiye çıkmıştık ve kendi geçmişine rağmen köküne kadar burjuva olan annem aşağılayıcı bir şekilde şöyle sormuştu: “Bu yerlerde yaşayan bu insanlar kimler? Evsiz falan mı bunlar?” Evsiz falan değillerdi, eski toprak emeklilerdi. Statüleri ve evlerinin görünüşlerini kafaya takmıyorlardı. Evlerinin orta-sınıf saygınlığına uygun gözüküp gözükmediğini önemsemeyecek kadar eski kafalıydılar. Yani annemin standartları ile evleri salaş gözüyordu.

Stample Pass Road’un nasıl değiştiğini görebilirsin ve bu değişiklik bence Lincoln çevresinde gerçekleşen değişikliği karakterize ediyor. Çünkü eskiden kulübe olmayan birçok yerde şu anda kulübeler var.

<strong>BVD:</strong> Senin kulüben çevredeki ormanlık ile uyum içindeydi. Onu yapmana yardımcı olması için bazı planlar kullandın mı yoksa kulübenin planını da kendin mi yapmıştın?

<strong>TJK:</strong> Kendim yaptım.

<strong>BVD:</strong> Kabini de kendin mi inşa ettin?

<strong>TJK:</strong> Kardeşimden az, ama çok az bir yardım aldım. Bana verdiği yardım bahsetmeye değmez. Çoğunlukla kendim yaptım.

<strong>BVD:</strong> Kulübeyi yapman ne kadar sürdü?

<strong>TJK:</strong> Sanırım 1971 Temmuz’unun başlarından o yılın Kasım’ının sonlarına kadar sürdü. Fakat yaptığım işe, Great Falls’a çeşitli sebepler dolayısıyla yaptığım yolculuklar yüzünden ara vermek zorunda kaldım. En uzun arayı ayağımı yaktığım zaman vermek zorunda kaldım. 1 Ağustos 1971 tarihinde kaynayan bir çorbaya çarpacak kadar sakar davrandım. Çorba spor ayakkabımın üzerine döküldü ve ayağımı fena bir şekilde yaktı. Doktorların talimatıyla 5, 5.5 hafta hiçbir şey yapamadım.

<strong>BVD:</strong> Kulübende ışık olup olmadığını merak ediyorum. Kulüben yeteri kadar aydınlık mıydı?

<strong>TJK:</strong> Kış aylarında mı?

<strong>BVD:</strong> Herhangi bir zamanda.

<strong>TJK:</strong> Evet yeteri kadar ışık vardı. Tabii ki havanın karardığı zamanlar dışında.

<strong>BVD:</strong> Lincoln’e geldiğinde tanıştığın ilk insanlar kimlerdi, komşuların kimlerdi?

<strong>TJK:</strong> Tabii ki, ilk olarak emlakçı. Fakat oraya yerleştiğimde sosyal anlamda tanıştığım ilk kişiler, hala bana komşu bulunan kulübenin sahibi olan Glen ve Dolores Williams’tı. Orada asla sürekli olarak yaşamadılar. Orası onlar için tatilde kullandıkları bir evdi. Onlarla her zaman dostça ilişkilerim olmuştur, fakat hiçbir zaman çok yakın olmadık. Ve Irene Preston ve Kenny Lee. Onlar renkli diye adlandırdığımız kişiliklere sahiplerdi. Kenny ilginç hikayeler anlatırdı...

<strong>BVD:</strong> Peki Lundberg’ler ile ne zaman tanıştın?

<strong>TJK:</strong> Sanırım Dick Lundberg ile ilk 1975 yılında tanıştım. Çünkü o zamana kadar bir arabam, daha sonra da eski bir pikap kamyonetim vardı. Fakat 1975 yılından sonra çalışan motorlu bir aracım yoktu ve ara sıra Helena’ya Dick ile beraber gitmeye başlamıştım. Sanırım Eilenn ile 70’lerin sonunda ya da 80’lerin başında tanıştım.

<strong>BVD:</strong> Yani bu tanıştığın insanlar senin yakınında yaşayan insanlardı.

<strong>TJK:</strong> Evet. Glenn ve eşi bildiğin gibi benim hemen aşağımda oturuyorlardı ve aynı zamanda Bill Hull ve ailesinin birkaç üyesi ile de tanıştım. Dükkanlarda çalışan tezgahtarlar haricinde 80’lere kadar tanıdığım tek insanlar bunlardı. Sherri (Wood) kütüphaneyi devraldığında onunla da tanıştım. Sonrasında Theresa ve Garlands’larla da tanıştım. Onlarla dükkanlarına gittiğim için tanışmıştım. Yani orada olduğumun ilk 10 yılında insanlar ile çok fazla tanışmadım.

<strong>BVD:</strong> Ya Chris Waits?

<strong>TJK:</strong> Onunla ilk tanışmam sanırım 80’lerin ortalarındaydı. Tam olarak hatırlamıyorum. Bazen onunla yolda karşılaşırdım. Belki birkaç kere beni arabasıyla bir yerlere bırakmıştır – fakat böyle bir şeyin olup olmadığından emin değilim. Yolda karşılaştığımızı ve merhabalaştığımızı biliyorum ama onunla olan tanışıklığım bundan ibaretti. Bir kez Leora Hall’un, bahçesinde yaptığı satışta karşılaştık ve o zaman orada onunla ayak üstü konuşmuştuk. Gördüğün gibi genellikle vaktimi ormanda ve kendi başıma geçirdim, yani bana yakın yerlerde oturanlardan başka kimseyi tanımak için çok fazla fırsatım olmadı.

<strong>BVD:</strong> Anlıyorum. O sana çok da yakın olan bir yerde yaşamıyordu. Waist kitabında, onunla ayak üstü konuştuğun Leora Hall’un bahçe satışından bahsediyor ve gümüş ya da gümüş kaplama çatal bıçak aldığından bahsediyor. Fakat Laura Hall senin kesinlikle gümüş ya da gümüş kaplama hiçbir şey almadığını çünkü böyle bir şey satmadığını söylüyor. Fakat seni orada gördüğünü ve hatta aldığın şeyleri de hatırlıyor. Bununla ilgili bir yorum yapmak ister misin?

<strong>TJK:</strong> Ne Leora Hall’dan ne de başka birisinden gümüş hiçbir şey almadım.

<strong>BVD:</strong> Tamam, o zaman başka konulara geçelim. Hayatında takip ettiğin rutinler var mıydı?

<strong>TJK:</strong> Gerçekten belirli rutinlerim yoktu, fakat bazı faaliyetler –yemek pişirmek ya da ateş için odun toplamak gibi– belirli bazı rutinlere denk geliyordu.

<strong>BVD:</strong> Lincoln'de geçirdiğin ortalama bir gün nasıldı?

<strong>TJK:</strong> Bu cevaplaması çok zor bir soru çünkü ortalama bir gün yaşayıp yaşamadığımdan emin değilim. Yaptığım şeyler içinde bulunduğum mevsime ve o gün yapmak zorunda olduğum işlere göre epey değişiyordu. Fakat temsili bir günü anlatmaya çalışacağım...

*** Üçüncü Bölüm

<strong>TJK:</strong> ... Ocak ayından bir günü alalım ve sabaha karşı üçte uyandığımı ve karın yağmakta olduğunu gördüğümü varsayalım. Sobamı yakıyorum ve üzerine bir tencere su koyuyorum. Su kaynamaya başladığında üzerine yulaf döküyorum ve pişene kadar birkaç dakika karıştırıyorum. Sonra tencereyi sobanın üzerinden alıp içine birkaç kaşık şeker ve toz süt koyuyorum. Yulaflar soğurken haşlanmış soğuk tavşan eti yiyorum. Sonra yulafları yiyorum, birkaç dakika açık sobanın önünde oturup ateşin sönüşünü seyrediyorum, sonra üzerimdekileri çıkarıp tekrar yatağa ve uyumaya dönüyorum. Uyandığımda güneş yeni doğmakta oluyor. Yataktan çıkıp hemen giyiniyorum çünkü kulübenin içi soğuk. Giyinmeyi tamamladığımda ortalık biraz daha aydınlanıyor ve karın artık yağmadığını ve gökyüzünün açık olduğunu görüyorum. Kar yeni yağmış olduğundan tavşan avı için iyi bir gün olacak. Eski, emektar tek atımlık 22 mm tüfeğimi duvardan alıyorum. Küçük ahşap 16’lık mermi kutusunu, ateş yakmam gerekebileceği için birkaç kibriti cebime atıyorum ve kemerime bir bıçak bağlıyorum. Sonra kar ayakkabılarımı giyip yola çıkıyorum. İlk olarak tepenin üzerine çıkmak için sert bir yokuş ve sonra avlanmak istediğim kontorta çamlarına kadar düz bir yürüyüş var. Çamlarda biraz ilerledikten sonra bir kar tavşanının izlerine rastlıyorum. Dönüp giden izleri bir saat civarı takip ediyorum. Sonra bir anda, diğer her yeri beyaz olan kar tavşanının siyah gözlerini ve kulağının siyah ucunu görüyorum. Genelde ilk önce gördüğünüz, gözler ve kulağının siyah ucudur. Tavşan beni dalların ve çam ağaçlarından yeni düşmüş yeşil iğnelerin arkasından izler. Tavşan 12 metre ötemdedir ve dikkatli bir şekilde beni izliyordur, o yüzden daha fazla yaklaşmaya çalışmam. Fakat yine de daha iyi bir açıdan vurmak için yerimi değiştirmem gerekir – çünkü en küçük bir dal parçası dahi 22’lik bir mermiyi ıskaya yol açacak şekilde yolundan saptırır. İyi bir vuruş yapmak için yere rahatsız bir pozisyonda uzanmam ve dizimi tüfeğin kabzası için destek olarak kullanmam gerekir. Tavşanın başının üzerine, gözünün tam arkasındaki bir noktaya doğru nişan alırım ... sabit dur ... bum! Tavşan başından vurulmuştur. Böyle bir vuruş tavşanı anında öldürür fakat tavşanın arka ayakları birkaç dakika boyunca şiddetli bir şekilde sallanır, bu yüzden tavşan karın üzerinde seker. Tavşanın sallanması bittiğinde ona doğru giderim ve tamamı ile ölmüş olduğunu görürüm. Yüksek sesle: “Teşekkürler Tavşan Dede” derim. Tavşan Dede tüm kar tavşanlarının ruhları için icat ettiğim bir tür yarı-tanrıdır. Birkaç dakika boyunca tamamı ile beyaz olan kara ve çam ağaçlarından süzülen ışığa bakarım. Sessizliği ve yalnızlığı hissederim. Burada olmak güzeldir. Bazen tepenin yamaçlarında kar motoru izleri görürüm, fakat şu anda içinde bulunduğum ormanda, büyük hayvanlar için olan av sezonu kapandığında, burada bulunduğum tüm yıllar boyunca tek bir ayak izine bile rastlamamışımdır. Cebimden düğümlü iplerden birini çıkarırım. Tavşanın boynuna ipi geçirip diğer ucunu elime dolarım. Daha sonra başka bir tavşan izi bulmak için bakınmaya başlarım. Üç tane tavşan avladığımda eve doğru yola çıkarım. Eve vardığımda altı yedi saat dışarıda geçirmiş olurum. İlk işim tavşanların derisini yüzmek ve iç organlarını çıkarmaktır. Karaciğerleri, kalpleri, beyinleri ve başka bağlantılı parçaları bir kabın içine koyarım. Karkasları tentenin altına asar ve birkaç patates ve yabani havuç almak için kökleri sakladığım kilerime giderim. Bunları yıkadıktan sonra ve başka bir takım işlerden sonra –odun kırmak ya da içme suyu için eritmek üzere kar toplamak– tencereyi kaynamak üzere sobanın üzerine koyarım ve uygun zamanda, kurutulmuş yabani otları, yaban havuçlarını, patatesleri ve tavşanların karaciğerlerini ve diğer iç organlarını tencereye eklerim. Tam olarak piştiklerinde hava kararmaya başlamıştır. Güvecimi kerosen lambamın ışığında yerim. Ya da tasarruf yapmak istiyorsam, sobamın kapağını açıp ateşin başında yerim. Bir avuç kuru üzümle yemeğimi bitiririm. Yorulmuşumdur fakat huzurluyumdur. Sobanın açık kapağı önünde bir süre daha durarak oturmaya devam ederim. Biraz okurum belki. Ya da yatağımda yatar ve bir süre sobanın açık kapısından yanmakta olan ateşi izlerim. Ya da yatağımda uzanıp duvara vuran ışıkları izlerim. Uykum geldiğinde elbiselerimi çıkarıp, battaniyenin altına girer ve uykuya dalarım.

<strong>BVD:</strong> Ben de seni kıskanıyorum ... Kulağa muhteşem geliyor. Özgürlük ve otonomi. Yetişmek için bir saat yok, hem gerçek, hem mecaz anlamda. Fakat konuyu değiştirmeme izin ver. Uykudan bahsettin. Yatağın rahat mıydı?

<strong>TJK:</strong> Benim için yeterince rahattı.

<strong>BVD:</strong> Tavşan Dede’ye teşekkür etmene saygı duyuyorum ve bunu taktir ediyorum. Yemekten önce şükretme geleneğinin gerçek kökenlerini hatırlattı bana. Kendini feda etmenin, başka bir hayatın devam etmesi için başka bir hayatın kendini feda etmesi ... Kadere inanır mısın?

<strong>TJK:</strong> Hayır.

<strong>BVD:</strong> Tanrıya inanıyor musun?

<strong>TJK:</strong> Hayır. Ya sen?

<strong>BVD:</strong> Tanrıya mı kadere mi?

<strong>TJK:</strong> İkisine de.

<strong>BVD:</strong> Belki... Ebeveynlerinin ateist olduğunu ve senin de ateist bir evde yetiştirildiğini okuduğumu hatırlıyorum.

<strong>TJK:</strong> Doğru.

<strong>BVD:</strong> Anne babanın tanrıdan bahsettiklerini hatırlıyor musun? “Bazı insanlar buna inanıyorlar...” gibi şeyler söylediklerini hatırlıyor musun?

<strong>TJK:</strong> Evet biraz böyle şeyler yaptılar. Mesela annem bana bir kitap okuyorsa ve orada tanrı ile ilgili bir şey geçiyorsa durumu şöyle açıklardı: “Bazı insanlar böyle şeylere inanıyorlar fakat biz inanmıyoruz.”

<strong>BVD:</strong> Anlıyorum. Temsili gününe geri dönersek... yediğin şeylerden bahsetmiştin. Genel olarak yediğin şeyler nelerden oluşuyordu? Ortalama bir günde neler yiyordun?

<strong>TJK:</strong> Bu mevsime göre oldukça fazla değişim gösteren bir şey.... 1975 ve 1983 arasında kış için un, pirinç, yulaf, şeker, mısır, yemeklik yağ ve toz süt ve konserve meyve, domates alırdım. Soğuk mevsimler boyunca her gün bir konserve kutusu yerdim. Az bir miktar konserve balık ve kurutulmuş meyve yerdim. Bunlar dışında yediğim her şey yabani ya da bahçemde yetiştirdiğim şeylerdi. Karaca geyik, kanada geyiği, kar tavşanı, çam sincabı, üç çeşit orman tavuğu, porcupine ve kimi zamanlar ördek, dağ sıçanı, misk sıçanı, gelincik, çakal, yanlışlıkla öldürdüğüm bir baykuş –hiçbir zaman kasıtlı olarak baykuş öldürmezdim– geyik faresi ve çekirgeler, yaban mersini, sabun eriği, amerikan hanımeli, yaban üzümü, iki çeşit siyah havuç, ahududu, çilek, Oregon üzümleri, acı kiraz ve kuşburnu. Yediğim nişastalı kökler ise cama, yampa, acıkök, Lomatium ve aynı zamanda spring beauty kökleri... Başka birkaç çeşit kök ve yabani ot da yedim... Mayıs ve haziran aylarında her yemekten önce salata, baya büyük bir salata yerdim. Arazimde gezinip onu bunu koparıp ağzıma atardım. Birkaç sefer de, yenebilir kökleri ezip ekmek yapmıştım. Fakat onları ezmek inanılmaz derecede zaman alıcı idi. Elle çekilen değirmenim yoktu bu sebeple onları taşta öğütmüştüm. Bahçemde patates, yabani havuç, pancar, soğan, iki çeşit havuç, ıspanak, turp, brokoli, bazı zamanlar karapazı, kudüs enginarı yetiştirirdim.

Yabani otları ve bahçe meyvelerini ve bazen de yaban meyvelerini kışın kullanmak üzere kuruturdum. Fakat nişastalı yiyeceklerim için genelde patates ve dükkandan alınan un, pirinç ve benzerleri gibi kuru gıdaları tüketirdim. Yabani nişastalı kökler yükseklerde nadirdir. Acıkök ve cama kökleri daha aşağılarda düzlüklerde daha bol bulunur ve buralar genellikle özel arazilerdir ve muhtemelen buralardaki rençperler bu besinlere ulaşmak için arazilerini kazmamı istemezler. Kış aylarında Douglas köknarının iğne yapraklarından yaptığım bir çayı C vitamini ihtiyacım için içerdim.

1995-1996’da, Montana’daki son kışımda, biraz zor zamanlar geçiriyordum. Fakat sistemin sunduğu şeylerden bir kez vazgeçtiğinizde kendi başınıza doğaçlama ne kadar iyi idare edebileceğiniz şaşırtıcıdır. Taze, kurutulmuş ya da konserve hiçbir ticari meyve ya da sebzeye sahip değildim; fakat kendi kuruttuğum meyvelerden bir hayli vardı. Bir miktar kurutulmuş kara havuç ve ışgınım vardı ve et için sincap ve tavşanlar vardı. Kullandığım ticari ürünler yalnızca un –tam ve beyaz un– pişirme yağı, şekerdi. Ve sanırım az biraz pirincim vardı. Yulaf ya da mısırım olup olmadığını hatırlamıyorum. Süt tozum bitmişti ve kalsiyum kaynağı olarak diş alçısı kullanıyordum. Bu bittiğinde ya toz haline getirilmiş tavşan kemiği ya da kalsit kullanmayı düşünüyordum. Fakat yine de iyi durumdaydım, yemeklerden hoşlanıyordum ve güzel bir kış oluyordu.

<strong>BVD:</strong> En sevdiğin yabani yiyecek neydi?

<strong>TJK:</strong> Sanırım Lincoln civarındaki en lezzetli yabani besin keklik üzümüydü. Vaccinium’un yükseklerde yetişen küçük bir türüdür. Üzümler öylesine küçüktür ki bir kap doldurmak için saatler gerekir fakat tadı mükemmeldir. Bunlar haricinde en sevdiğim yiyecekler yaban mersinleri, yampalar ve geyik karaciğeri, kar tavşanı ve porcupinelerdi.

<strong>BVD:</strong> Hazırladığın favori bir tarifin var mıydı?

TJK: Standart hazırladığım yemekler yoktu. Çünkü belirli bir zamanda ne bulunuyorsa onu yiyordum. Genel olarak en iyi yemeklerim, içerisinde et, sebze ve patates, pirinç, erişte ya da yampa kökleri gibi nişastalı yiyecekler bulunan güveçlerdi.

<strong>BVD:</strong> Yemekleri dışarıda mı yerdin?

<strong>TJK:</strong> Bunu nadiren yapardım. Genelde içeride, kulübemdeki masamda yerdim... Yemeği bitirdiğimde bazen sandalyemde oturup ayaklarımı masaya uzatır ve bir süre pencereden dışarı bakardım...

<strong>BVD:</strong> Pencereden dışarıyı görebiliyor muydun?

<strong>TJK:</strong> Efendim?

<strong>BVD:</strong> Pencereden baktığında dışarıyı görebiliyor muydun?

<strong>TJK:</strong> Evet. Pencereler bunun için yapılır...

*** Dördüncü Bölüm

<strong>BVD:</strong> Hangi bitkilerin yenilebileceğini ve hazırlanışlarını nasıl öğrendin?

<strong>TJK:</strong> Berkeley’i terk etmemden yıllar önce doğaya meraklıydım ve yenilebilir yabani otları ayırt etme ve benzeri yetenekleri öğreniyordum. Onları tanımayı konu üzerine yazılmış kitaplardan öğrendim. Fernald ve Kinsey’in <em>Edible Wild Plants of Eastern America</em> kitabı ve Donald Kirk’in <em>Edible Wild Plants of Western America</em> kitapları. Kitaplar bu otların nasıl hazırlanabileceği ile ilgili tarifler içeriyorlar fakat ben çoğunlukla deneme yanılma yolu ile öğrendim. Bazı yenilebilir otları deneme yolu ile öğrendim. Bazı bitki aileleri ile deneme yapmak tehlikeli olabilir. Havuç ve zambak aileleri gibi. Çünkü bunlar ölümcül derecede zehirli türler içerebilirler. Fakat hardal otu ailesi, kır çiçekleri ve pancarlar ile deneme yapmak güvenlidir. Bildiğim kadarıyla ölümcül düzeyde zehir içermezler fakat bazı türleri belirli derecede zehirli olabilir. Hardal otu ailesinden, bitkilerin adını öğrenmeden kullandıklarım olmuştu. Kır çiçeklerinden adını öğrenmeden yıllarca yediğim bir tür vardı. Bu yalancı karahindabıymış. Ve sıklıkla yediğim bir pancar türü vardı fakat bunun adını hiç öğrenmedim.

<strong>BVD:</strong> Kendi kendine yeterli miydin?

<strong>TJK:</strong> Tamamı ile değil. Mağazalardan alınan un, pirinç, yulaf ve pişirme yağı gibi besinlere ihtiyaç duyuyordum. Kıyafetlerimin çoğunu satın alıyordum. Gerçi kendi yaptıklarım da olmuştu. İlk başlarda, tam anlamı ile kendine yeterlilik, sonunda ulaşmak istediğim bir hedefti. Fakat çevremdeki vahşi doğanın daralması ve bölgenin kalabalıklaşması ile bunu yapmak için bir sebep kalmadığı hissine kapıldım ve ilgilerim başka yönlere gitmeye başladı.

<strong>BVD:</strong> Yaşama tarzın rüyalarını, arzularını ve ilk başta sahip olduğun motivasyonları nasıl tatmin etti? Yani gençken sahip olduğun düş ve Berkeley’den ayrılmak ile ilgili kararın ve planın. Ve Lincoln’deki yaşamının en tatmin edici yanı neydi?

<strong>TJK:</strong> Ormandaki yaşamımda şahsi özgürlük, bağımsızlık ve bir miktar macera ve düşük stresli bir yaşam gibi bulmayı umduğum tatminleri buldum.

Fakat aynı zamanda daha önce tam olarak anlayamadığım ve tahmin etmediğim ya da hatta tamamen sürpriz bir şekilde gerçekleşen tatminler yaşadım.

Doğa ile ne kadar daha yakın hale gelirseniz onun güzelliğini o kadar fazla taktir ediyorsunuz. Bu yalnızca manzaralardan ve seslerden oluşan bir güzellik değil fakat bir bütün olarak doğanın uyandırdığı taktir duygusu... Bunu nasıl ifade edebileceğimi bilmiyorum. Çarpıcı olan şey, yalnızca orayı ziyaret etmekten farklı olarak ormanda sürekli yaşadığınızda, güzelliğin dışarıdan baktığınız bir şey olmaktan ziyade hayatınızın bir parçası haline gelmesidir.

Bununla bağlantılı olarak doğa ile kurduğunuz yakınlık, duyularınızın keskinleşmesine sebep oluyor. Duyma ya da görme kabiliyetiniz artmıyor, fakat çevrenizdeki şeyleri daha çok fark ediyorsunuz. Şehir yaşamında içeriye dönme eğiliminiz vardır. Çevreniz, alakasız sesler ve görüntüler ile doludur ve bunların çoğunu, bilincinize ulaşmaması için engellemek üzerine koşullanmışsınızdır. Ormanda dikkatiniz dışarıya, çevrenize yöneliktir; çünkü çevrenizde olanların çok daha fazla bilincindesinizdir. Mesela yerdeki yenilebilir bitkiler ya da hayvan izleri gibi çok küçük şeyleri fark edersiniz. Eğer bir insan geçip çok küçük bir iz bıraktıysa bunu fark edersiniz. Kulağınıza gelen seslerin ne olduğunu bilirsiniz. Bu bir kuş sesidir; bu uçan, bir at sineğidir; bu kaçan, korkan bir geyiktir; bu bir sincabın kestiği kozalaktır ve yerdeki odunların üzerine düşmüştür. Eğer ne olduğunu bilemediğiniz bir ses duyarsanız, duyulabilecek en küçük şiddette dahi olsa, hemen onu fark edersiniz. Benim için bu farkındalık ya da duyuların bu şekilde genişlemesi, doğaya yakın yaşamanın en büyük lükslerinden birisiydi. Bu duyguyu aynı şeyi tecrübe etmeden anlayamazsın.

Öğrendiğim diğer bir şey, belirli bir amacı olan bir iş yapmanın önemiydi. Burada bahsettiğim gerçekten bir amacı olan, ölüm kalım meselelerinden oluşan işlerdir. Ormanda yaşamanın tam olarak ne anlama geldiğine, ekonomik durumumun yemek yiyebilmek için avlanmak, bitki toplamak ve bahçede bitki yetiştirmemi zorunlu kılmasına sebep olan bir duruma gelmesi ile farkına vardım. Lincoln’de yaşadığım zamanın bir bölümünde, özellikle 1975’ten 1978’e kadar, avlanmakta başarısız olursam yiyecek et bulamazdım. Onları kendim toplamaz ya da yetiştirmezsem yiyecek bitki bulamazdım. Bu tarz bir kendine yeterliliğin getirdiği kendine güven ve başarı duygusunun verdiğinden daha tatmin edici bir şey yoktur. Bununla bağlantılı olarak kişi ölüm korkusunu kaybeder.

Doğanın yakınında yaşayarak fark ettiğin şey, mutluluğun, zevki artırmak ile bir alakası olmadığıdır. Mutluluk genelde huzur ile alakalıdır. Huzuru yeterli uzunlukta tecrübe edersen çok güçlü zevkler sana itici gelmeye başlar—aşırı zevk erişmiş olduğun huzuru bozacaktır.

Son olarak can sıkıntısının medeniyetin bir hastalığı olduğunu öğreniyorsun. Sıkıntı denilen şey çoğunlukla insanların sürekli olarak kendilerini eğlendirme ve meşgul tutma ihtiyacında olmaları ve bunu yapamadıklarında çeşitli endişelerin, sıkıntıların, huzursuzlukların ve benzerlerinin yüzeye çıkmaya başlaması ve insanları rahatsız etmesidir. Ormandaki yaşama bir kez alıştığında can sıkıntısı denen şey ortadan kalkar. Eğer yapılacak herhangi bir işin yoksa hiçbir şey yapmadan saatlerce oturabilirsin; yalnızca kuşları, rüzgarı ya da sessizliği dinlersin, güneş hareket ettikçe oluşan gölgeleri izlersin, ya da yalnızca tanıdığın nesnelere bakarsın. Ve sıkılmazsın. Yalnızca huzur içindesindir.

<strong>BVD:</strong> Lincoln’deki yaşamının en zor yanı neydi?

<strong>TJK:</strong> Ormanda yaşadığım hayatın en kötü yanı modern medeniyetin önlenemez bir şekilde yaklaşmasıydı. Stample Pass Road’da ve başka yerlerde her zaman daha fazla ev vardı. Ormanın içerisine daha fazla yol yapılıyordu, daha fazla ağaç kesiliyordu ve daha fazla uçak üzerimden uçuyordu. Geyiklerin üzerindeki alıcılar, tarım ilacı sıkılması ve buna benzer bir sürü şey.

<strong>BVD:</strong> Ormandaki yaşamın ile ilgili en güzel anıların neler?

<strong>TJK:</strong> .... Baharın ilk günlerinde karlar bunu mümkün kılacak kadar eridiğinde tepelerde uzun yürüyüşlere çıkardım, artık kar ayakkabısı giymek zorunda olmamanın verdiği fiziksel özgürlüğün tadını çıkarırdım. Ve eve yabani soğanlar, karahindiba, ve Lomatium gibi bir sürü taze ve genç yabani sebze ile ve bir kaç tane orman tavuğu ile gelirdim. Bunları yasa dışı olarak avlıyordum itiraf etmem gerekir. Sabahın erken saatlerinde bahçemde çalışmak. Kışın kar tavşanı avlamak. Kış aylarında gizli kulübemde geçirdiğim zamanlar. Bahar, yaz ve sonbaharda kamp yaptığım bazı yerler. Geyik eti, patates ve bahçemdeki diğer sebzeler ile yaptığım sonbahar güveçleri. Hiçbir şey yapmadan hatta düşünmeden, huzur içinde oturduğum ve uzandığım anlar.

<strong>BVD:</strong> Teşekkür ederim...

<strong>TJK:</strong> Rica ederim.



