#title Sistemin En İnce Oyunu
#author Theodore Kaczynski
#SORTtopics aktivizm, giriş, devrim, reformizm, tekno-endüstriyel karşıtı
#date 2008
#source 25.05.2022 tarihinde şuradan alındı: [[https://gayrnesriyat.substack.com/p/sistemin-en-ince-oyunu-ted-kaczynski][gayrnesriyat.substack.com]]
#lang tr
#pubdate 2022-05-25T07:43:45
#notes Çeviri: Gayr Neşriyatı <br>İngilizce Aslı: [[https://theanarchistlibrary.org/library/ted-kaczynski-the-system-s-neatest-trick][The System’s Neatest Trick]] - [[https://ia800300.us.archive.org/21/items/tk-Technological-Slavery/tk-Technological-Slavery.pdf][Technological Slavery]] <br>Bu Metnin Başka Bir Çevirisi: [[/library/theodore-kaczynski-sistemin-en-etkileyici-numarasi][Sistemin En Etkileyici Numarası]]
#publisher


Sistem, zamanımızın sözde devrimcileri ve isyancıları üzerinde bir oyun oynadı. Bu oyun o kadar tatlı ki bilinçli planlanmış olsaydı, matematiksel zarifliğinden dolayı planlayanlara hayran olurduk.

<br>

*** 1. Sistem Ne Değildir?

Olaya sistemin ne olmadığını açıklığa kavuşturarak başlayalım. Sistem, George W. Bush, danışmanları ve atadığı kişiler falan gibi bir şey değildir, sistem protestoculara kötü davranan kolluk kuvvetleri de değildir, çokuluslu şirketlerin CEO’ları değildir, canlıların genlerini laboratuvarda kurcalayan deliler de değildir. Bütün bu insanlar sistemin hizmetkarlarıdır ve kendi başlarına sistemi oluşturmazlar. Özellikle, bu kişilerden herhangi bir tanesinin kişisel değerleri, tutumları, inançları ve davranışları, sistemin ihtiyaçları ile ciddi şekilde çelişebilir.

Bir örnekle açıklamak gerekirse, sistem mülkiyet haklarına saygı gösterilmesini gerektirir, ancak CEO’lar, polisler, bilim adamları ve politikacılar bazen bunun dışına çıkar ve hırsızlık yaparlar. (Hırsızlıktan bahsederken, kendimizi fiziksel nesnelerin kaçırılması durumlarıyla sınırlandırmak zorunda değiliz. Gelir vergisinde hile yapmak, rüşvet almak ve diğer her türlü -hile ve yolsuzluk gibi- mülk edinilebilen yasa dışı her yolu buna dahil edebiliriz.

Politikacılar, polisler veya CEO'lar tarafından bireysel olarak işlenen yasadışı eylemler (hırsızlık, rüşvet ve yolsuzluk vb.) ne olursa olsun, sistemin bir parçası değiller, sistemin hastalıklarıdırlar. Hırsızlık ne kadar az olursa, sistem o kadar iyi işler ve bu nedenle, sistemin hizmetkarları ve destekçileri, bazen kendi özellerinde kanunları çiğnemeyi uygun bulsalar bile konu halk olunca kanuna itaati savunurlar.

Başka bir örnek daha vereyim. Polis kurumu, sistemin uygulayıcısı olmasına rağmen, polis vahşeti sistemin bir parçası değildir. Polisler, bir şüphelinin ağzını burnunu kırdığında sisteme hizmet etmiş olmuyorlar, sadece kendi öfkelerini ve düşmanlıklarını ortaya çıkarmış oluyorlar. Sistemin amacı gaddarlık ya da öfkenin dışavurumu değildir. Polis söz konusu olduğunda, sistemin amacı, kurallara uyulmasını zorlamak ve bunu mümkün olan en az miktarda aksama, şiddet ve gücü genel kötüye kullanım ile yapmaktır. Dolayısıyla, sistem açısından ideal polis, asla sinirlenmeyen, asla gereğinden fazla şiddet kullanmayan ve insanları kontrol altında tutmak için var olan bir güçten ziyade manipülasyon yeteneği gelişmiş polistir. Polis vahşeti, sistemin bir parçası değil, onun bir hastalığıdır.

Bunun doğruluğunu görmek için medyanın tutumunu inceleyebilirsiniz. Ana akım medya dünyanın neredeyse her yerinde polis vahşetini kınıyor. Ve tabii ki, ana akım medyanın tutumu ise sistem için neyin iyi olduğuna karar veren güçlü sınıflar arasındaki fikir birliğinin temsilidir.

Hırsızlık, rüşvet ve polis vahşeti hakkında söylediklerimiz, ırkçılık, cinsiyetçilik, homofobi, yoksulluk ve fahişeler gibi ayrımcılık ve mağduriyet söz konusu olabilen durumlarda da geçerlidir. Bunların hepsi sistem için kötü aslında. Örneğin, siyah insanlar kendilerini ne kadar aşağılanmış veya dışlanmış hissederlerse, suça yönelme olasılıkları o kadar artar ve kendilerini sistem için faydalı kılacak olası kariyerlerden uzaklaşmış bulurlar.

Çok hızlı gerçekleşebilen şehirlerarası ulaşım yollarını bizlere getiren modern teknoloji, geleneksel yaşam biçimlerinin bozulmasıyla, nüfusun birbirine karışmasına yol açmış, böylece günümüzde farklı ırk, millet, kültür ve dinlerden insanlar yan yana yaşamak ve çalışmak zorunda kalmışlardır. İnsanlar eğer ırk, etnik köken, din, cinsel tercih vb. temelinde birbirlerinden nefret eder veya birbirlerini reddederlerse ortaya çıkacak çatışmalar sistemin işleyişine bir engeldir. Jesse Helms gibi eski dinozorları saymazsak sistemin liderleri bunu çok iyi biliyorlar ve bu yüzden okulda ve medya aracılığıyla ırkçılığın, cinsiyetçiliğin, homofobinin ve benzerlerinin ortadan kaldırılması gereken sosyal sorunlar olduğuna inanmamızı dayatıyorlar.

Hiç şüphe yok ki sistemin bazı liderleri, bazı politikacılar, bilim adamları ve CEO'lar kendi özellerinde bir kadının yerinin ev olduğunu ya da eşcinsellik ve ırklararası evliliğin iğrenç olduğunu düşünüyor. Ancak çoğunluk bu şekilde hissetseydi bile ırkçılık, cinsiyetçilik ve homofobi gibi duruşların sistemin bir parçası olduğu söylenemezdi — liderler özelinde gerçekleşen hırsızlık miktarından fazlası gözlemlenirse çalmanın sistemin bir parçası olduğu anlamına gelir. Sistemin kendi güvenliği uğruna hukuka ve mülke saygıyı teşvik etmesi gerektiği gibi, aynı nedenle ırkçılığı ve diğer mağduriyet biçimlerini de caydırmalıdır. Bu nedenle sistem, elitler özelinde gerçekleşen sapmalara rağmen temel olarak ayrımcılığı ve mağduriyeti bastırmayı taahhüt etmektedir.

Bunun doğruluğunu görmek için ana akım medyanın tutumuna tekrar bakabilirsiniz. Daha cüretkâr ve gerici yorumculardan birkaçının ara sıra çekingen ola da olsa aksini belirtmelerinin dışında, medya ezici bir şekilde ırksal ve toplumsal cinsiyet eşitliğini, eşcinselliğin ve ırklararası evliliğin kabulünü desteklemektedir.

Sistemin uysal, şiddetten uzak yaşayan, evcilleştirilmiş, yumuşak başlı ve itaatkâr bir nüfusa ihtiyacı var. Sistemin, sosyal makinenin düzenli işleyişine müdahale edebilecek herhangi bir çatışma veya aksamadan kaçınması gerekir. Irksal, etnik, dini ve diğer grupsal düşmanlıkları bastırmanın yanı sıra, maçoluk, saldırgan dürtüler ve şiddete yönelik her türlü eğilim gibi aksamaya veya düzensizliğe yol açabilecek diğer tüm eğilimleri de kendi lehine bastırmak veya bir noktada kullanmak zorundadır.

Doğal olarak, geleneksel ırksal ve etnik karşıtlıklar bir anda değil yavaş yavaş ortadan kalkar, maçoluk, saldırganlık ve şiddet içeren dürtüler kolayca bastırılmaz ve cinsiyete ve sex kimliğine yönelik tutumlar bir gecede değişmez. Dolayısıyla bu değişimlere direnen birçok birey var ve sistem bu direnişlerinin üstesinden gelmekte sorun yaşıyor.

*** 2. Sistem İsyan Etme Dürtüsünden Nasıl Faydalanıyor

Modern toplumda yaşayan bizler yoğun bir kurallar ve düzenlemeler ağı tarafından kuşatıldık. Şirketler, hükümetler, işçi sendikaları, üniversiteler, kiliseler ve siyasi partiler gibi büyük örgütlerin insafına kaldık ve sonuç olarak güçsüz düştük. Sistemin bizi düşürdüğü kulluk konumu, güçsüzlük ve diğer aşağılamaların bir sonucu olarak, isyan etme dürtüsüne yol açan yaygın bir hayal kırıklığı var. Ve Sistemin en düzgün oyununu oynadığı yer burasıdır: Parlak bir el çabukluğu sayesinde isyanı kendi avantajına çevirecek.

Birçok insan kendi hayal kırıklıklarının köklerini anlamıyor, bu nedenle isyanları yönsüz. İsyan etmek istediklerini biliyorlar ama neye karşı isyan etmek istediklerini bilmiyorlar. Neyse ki, Sistem onlara isyan etmek adına standart ve basmakalıp şikayetlerin bir listesini sunarak ihtiyaçlarını karşılayabiliyor: ırkçılık, homofobi, kadın sorunları, yoksulluk, rezalet durumdaki işyerleri ... ve tüm boktan "aktivizm" konuları.

Çok sayıda potansiyel asi burada yemi yutuyor. Irkçılık, cinsiyetçilik vb. ile mücadelede. Sadece sistemin işini yapıyorlar. Buna rağmen Sisteme isyan ettiklerini hayal ediyorlar. Bu nasıl mümkün olabilir?

Birincisi, 50 yıl önce sistem henüz siyah insanlar, kadınlar ve eşcinseller için eşitliğe bağlı değildi, bu yüzden bu isyan motivasyonları gerçekten bir isyan biçimiydi. Sonuç olarak, bu nedenler geleneksel olan isyancı nedenler olarak kabul edilebilir. Bugün ise bu statüyü sadece bir gelenek meselesi olarak korumuş oldular; yani, her isyancı nesil önceki nesilleri taklit ediyor.

İkincisi, daha önce de belirttiğim gibi, sistemin gerektirdiği toplumsal değişimlere direnen hala önemli sayıda insan var ve bu insanlardan bazıları yeri geliyor polisler, hakimler veya politikacılar gibi otorite figürleri bile olabiliyor. Bu direnişçiler potansiyel isyancılar için bir hedef oluşturuyor, onlara karşı isyan edebilecekleri biri. Rush Limbaugh gibi yorumcular, aktivistler aleyhinde konuşarak bu sürece yardımcı oluyorlar: Birisini kızdırdıklarını görmek, aktivistlerin isyan yanılsamalarını daha da besliyor.

Üçüncüsü, sistemin talep ettiği toplumsal değişimleri tam anlamıyla kabul eden sistem liderlerinin çoğunluğuyla bile kendilerini çatışmaya sokmak için, potansiyel isyancılar, sistem liderlerinin ihtiyatlı gördüklerinden daha ileri giden çözümlerde ısrar ediyorlar ve önemsiz meseleler üzerinde abartılı bir öfke gösteriyorlar. Örneğin, siyah insanlara tazminat ödenmesini talep ediyorlar ve ne kadar temkinli ve makul olursa olsun, bir azınlık grubunun herhangi bir eleştirisine sık sık öfkeleniyorlar.

Böylece eylemciler Sisteme isyan ettikleri yanılsamasını sürdürebiliyorlar. Ama yanılsama çok saçma. Irkçılığa, cinsiyetçiliğe, homofobiye ve benzerlerine karşı ajitasyon, siyasi hile ve yolsuzluğa karşı ajitasyondan sistemin kendisine karşı bir isyan çıkmaz. Hile ve yolsuzluğa karşı çalışanlar isyan etmiyor, sistemin uygulayıcısı gibi davranıyorlar: Politikacıların sistemin kurallarına uymalarına yardımcı oluyorlar. Irkçılığa, cinsiyetçiliğe ve homofobiye karşı çalışanlar da benzer şekilde sistemlerin uygulayıcısı olarak hareket ediyorlar: Sistemin, sistem için sorunlara neden olan sapkın ırkçı, cinsiyetçi ve homofobik tutumları bastırmasına yardımcı oluyorlar.

Ancak eylemciler sadece sistemin uygulayıcısı olarak hareket etmiyorlar. Ayrıca, halkın kızgınlığını sistemden ve kurumlarından uzaklaştırarak sistemi koruyan bir tür paratoner görevi de görüyorlar. Örneğin, kadınları evden çıkarıp işyerlerine sokmanın sistemin yararına olmasının birkaç nedeni vardı. Elli yıl önce, hükümet ya da medya tarafından temsil edilen sistem, kadınların yaşamlarını ev yerine kariyerlerine odaklamalarını toplumsal olarak kabul edilebilir kılmak için tasarlanmış bir propaganda başlatmış olsaydı, değişime karşı doğal insan direnci halkta yaygın bir kızgınlığa neden olurdu. Gerçekte olan şey, değişikliklere, sistem kurumlarının güvenli bir mesafeden takip ettiği radikal feministlerin öncülük etmesiydi. Toplumun daha muhafazakâr üyelerinin kini, sistem ve kurumlarına değil, radikal feministlere yöneltildi çünkü sistemin desteklediği değişimler feministlerin savunduğu daha radikal çözümlere kıyasla yavaş ve ılımlı görünüyordu ve bu nispeten yavaş değişimler bile radikallerin baskısıyla sisteme zorla dayatılmış olarak görülüyordu.

*** 3. Sistemin En İnce Oyunu

Özetlemek gerekirse, sistemin oynadığı oyun şudur:

 1. Sistemin kendi verimliliği ve güvenliği için teknolojik ilerlemeden kaynaklanan değişen koşullara uyumlu, derin ve radikal toplumsal değişimler getirmesi gerekiyor.

 1. Sistemin dayattığı koşullar altında yaşamın hayal kırıklığı isyankâr dürtülere yol açıyor.

 1. Asi dürtüler, gereken toplumsal değişimlerin hizmetinde sistem tarafından seçilir; eylemciler artık sisteme faydası olmayan eski ve modası geçmiş değerlere karşı ve sistemin kabul etmemiz gereken yeni değerlerden yana "isyan ederler".

 1. Bu şekilde, aksi takdirde sistem için tehlikeli olabilecek asi dürtülere, sadece sisteme zararsız değil, aynı zamanda onun için yararlı olabilecek bir zemin verilir.

 1. Toplumsal değişimlerin dayatılmasından kaynaklanan kamusal kızgınlığın büyük bir kısmı sistemden ve kurumlarından uzaklaşmakta ve bunun yerine toplumsal değişimlere öncülük eden radikallere yönelmektedir.

Tabii oyun, bir oyun oynandığının bilincinde olmayan sistem liderleri tarafından önceden planlanmamıştı. Şu şekilde bir çalışma mekanizmasından söz edilebilir:

Herhangi bir konuda hangi pozisyonun alınacağına karar verirken, medyanın editörleri, yayıncıları ve sahipleri bilinçli veya bilinçsiz olarak çeşitli faktörleri dengelemelidir. Okuyucularının veya izleyicilerinin konuyla ilgili yazdırdıkları veya yayınladıkları şeylere nasıl tepki vereceğini düşünmeli, sonra da reklamverenlerinin, medyadaki meslektaşlarının ve diğer güçlü kişilerin nasıl tepki vereceğini düşünmeli ve yazdırdıkları veya yayınladıklarının sistemin güvenliği üzerindeki etkisini düşünmelidirler.

Bu pratik düşünceler genellikle konuyla ilgili sahip olabilecekleri kişisel duygularından daha ağır basacaktır. Medya liderlerinin, reklamverenlerinin ve diğer güçlü kişilerin kişisel duyguları çok çeşitli olabilir. Liberal veya muhafazakâr, dindar veya ateist olabilirler. Liderler arasındaki tek evrensel ortak zemin, sisteme, sistemin güvenliğine ve gücüne olan bağlılıklarıdır. Bu nedenle, kamuoyunun kabul etmeye istekli olduğu sınırlar içinde medyanın yayacağı tutumları belirleyen temel faktör, medya liderleri ve diğer güçlü insanlar arasında sistem için neyin iyi olduğu konusundaki bir fikir birliği olacaktır.

Bu nedenle, bir editör veya başka bir medya lideri, bir harekete veya davaya karşı hangi tutumun alınacağına karar vermek için yola çıktığında, ilk düşüncesi, hareketin sistem için iyi veya kötü neler içerdiğini belirlemektir. Belki de kendi kendisine kararının ahlaki, felsefi veya dini gerekçelere dayandığını söyler, ancak pratikte sistemin güvenliğinin medyanın tutumunu belirlemede diğer tüm faktörlerden öncelikli olduğu gözlemlenebilir bir gerçektir.

Örneğin, bir haber dergisi editörü milis hareketine bakarsa, bazı şikayetlerine ve hedeflerine kişisel olarak sempati duyabilir veya duymayabilir, ancak aynı zamanda reklamverenleri ve medyadaki meslektaşları arasında milis hareketinin Sistem için potansiyel olarak tehlikeli olduğu konusunda güçlü bir fikir birliği olacağını da görür ve bunun ışığında yapılması gerekeni yapar. Bu şartlar altında, dergisinin milis hareketine karşı olumsuz bir tutum sergilemesinin daha iyi olduğunu bilir. Medyanın olumsuz tutumu muhtemelen milis hareketinin sönmesinin nedenlerinden biri olmuştur.

Aynı editör radikal feminizme baktığında, daha aşırıya kaçan çözümlerinden bazılarının sistem için tehlikeli olacağını görür, ancak feminizmin sistem için yararlı olan birçok şey barındırdığını da gözden kaçırmaz. Kadınların iş ve teknik dünyaya katılımı onları ve ailelerini sisteme daha iyi entegre eder. Yetenekleri iş hayatında ve teknik konularda sisteme hizmet eder. Aile içi şiddet ve tecavüzün sona erdirilmesine yönelik feminist vurgu da sistemin ihtiyaçlarına hizmet eder çünkü tecavüz ve istismar, diğer şiddet biçimleri gibi, sistem için tehlikelidir. Belki de en önemlisi, editör, modern ev işlerinin değersizliğinin ve anlamsızlığının, modern ev hanımının sosyal izolasyonunun birçok kadını hayal kırıklığına uğratabileceğinin farkına varmasıdır -kadınların iş hayatında ve teknik dünyada kariyer basamaklarını tırmanmalarına izin verilmediği sürece sistem için sorunlar yaratabilecek bir hayal kırıklığı.

Bu editör, kişisel olarak alt pozisyondaki kadınlar söz konusu olduğunda daha rahat hisseden maço bir tip olsa bile, feminizmin, en azından nispeten ılımlı bir biçimde, Sistem için iyi olduğunu biliyor. Editoryal duruşunun ılımlı feminizme karşı olumlu olması gerektiğini biliyor, aksi takdirde reklamverenlerinin ve diğer güçlü insanların buna karşı çıkmasıyla karşı karşıya kalacak. Bu nedenle ana akım medyanın tutumu genel olarak ılımlı feminizmi destekliyor, radikal feminizme karşı değişken ve yalnızca en uç feminist konumlara karşı sürekli düşmanca davranıyor.

Bu tür bir süreçle sistem için tehlikeli olan isyancı hareketler olumsuz propagandaya maruz kalırken, sistem için yararlı olduğuna inanılan isyancı hareketlere medyada temkinli teşvik veriliyor. Medya propagandasının bilinçsizce emilmesi, potansiyel isyancıları sistemin çıkarlarına hizmet edecek şekilde "isyan etmek" için yönlendiriyor.

Sistemin bu oyununu oynamasında üniversite aydınları da önemli bir rol oynamaktadır. Her ne kadar kendileri bağımsız düşünürlerden hoşlanıyor olsalar da aydınlar (bireysel istisnalara izin veriyorlar) bugün Amerika'daki en sosyalleşmiş, en konformist, en terbiyeli ve en evcilleştirilmiş, en şımartılmış, bağımlı ve omurgasız gruptur. Sonuç olarak, isyan etme dürtüleri özellikle güçlüdür. Ancak, bağımsız düşünceden aciz oldukları için, gerçek isyan onlar için imkansızdır. Sonuç olarak onlar, sistemin temel değerlerine meydan okumak zorunda kalmadan insanları rahatsız etmelerine ve isyan yanılsamasının tadını çıkarmalarına izin veren sistemin oyununun enayileridirler.

Üniversite aydınları, gençlerin öğretmenleri oldukları için, sistemin gençlere oyun oynamasına yardımcı olacak bir konumdalar; bunu, gençlerin isyankâr dürtülerini standart, kalıplaşmış hedeflere doğru yönlendirerek yapıyorlar: ırkçılık, sömürgecilik, kadın sorunları vb. Üniversite öğrencisi olmayan gençler, öğrencilerin isyan ettiği "sosyal adalet" konularını medya aracılığıyla ya da kişisel temas yoluyla öğreniyor ve öğrencileri taklit ediyorlar. Böylece, saç stilleri, giyim stilleri ve diğer tuhaflıklar taklit yoluyla yayıldığı gibi, akranların taklit edilmesiyle yayılan klişeleşmiş bir isyan tarzının olduğu bir gençlik kültürü gelişiyor.

*** 4. Oyun Kusursuz Değil

Tabii sistemin oyunu da mükemmel çalışmıyor. "Aktivist" topluluk tarafından benimsenen pozisyonların tümü sistemin ihtiyaçları ile tutarlı değildir. Bu bağlamda sistemin karşısına çıkan en önemli zorluklardan bazıları, sistemin kullanması gereken iki farklı propaganda türü olan entegrasyon propagandası ve ajitasyon propagandası arasındaki çatışmayla ilgilidir.

Entegrasyon propagandası, modern toplumda sosyalleşmenin temel mekanizmasıdır. Sistemin güvenli ve yararlı araçları olabilmek için insanlara sahip olmaları gereken tutum, inanç, değer ve alışkanlıkları aşılamak için tasarlanmış propagandadır. İnsanlara, sistem için tehlikeli olan duygusal dürtüleri kalıcı olarak bastırmayı veya yüceltmeyi öğretir. Odak noktası, belirli, güncel konulara yönelik tutumlardan ziyade, uzun vadeli tutumlara ve geniş uygulanabilirliğin köklü değerlerine odaklanmaktadır.

Ajitasyon propagandası, belirli, mevcut durumlarda belirli tutum veya davranışları ortaya çıkarmak için insanların duyguları üzerinde oynanır. İnsanlara tehlikeli duygusal dürtüleri bastırmayı öğretmek yerine, zaman içinde yerelleştirilmiş iyi tanımlanmış amaçlar için belirli duyguları teşvik etmeyi amaçlar.

Sistemin düzenli, uysal, işbirlikçi, pasif, bağımlı bir nüfusa ihtiyacı var. Her şeyden önce şiddet içermeyen bir nüfusa ihtiyaç duyuyor, çünkü hükümetin fiziksel güç kullanımı konusunda tekele sahip olması gerekiyor. Bu nedenle entegrasyon propagandası bize şiddetten dehşete düşmemizi, korkmamızı öğretmek zorundadır, böylece çok kızgın olduğumuzda bile onu kullanmaya özendirilemiyor olacağız. ("Şiddet" derken, insanlara yapılan fiziksel saldırıları kastediyorum.) Daha genel olarak, entegrasyon propagandası bize pasiflik, karşılıklı yardım ve işbirliğini vurgulayan yumuşak, sevimli değerler öğretmek zorundadır.

Öte yandan, belirli bağlamlarda sistemin kendisi, kendi hedeflerine ulaşmak için acımasız, saldırgan yöntemlere başvurmayı yararlı veya gerekli bulmaktadır. Bu tür yöntemlere en belirgin örnek savaştır. Savaş zamanında sistem ajitasyon propagandasına sarılır: Halkın askeri harekatın onayını kazanmak için, insanların gerçek ya da sözde düşmanlarına karşı korkmuş ve kızgın hissetmelerini sağlamak için duygularla oynar.

Bu durumda entegrasyon propagandası ile ajitasyon propagandası arasında bir çatışma var. Şiddetten nefret ettirilen, bunun entegrasyon propagandasının öğrettiği yumuşak, sevimli değerlere aykırı olduğunu düşünecek bu insanlar kanlı bir askeri operasyonu onaylamaya kolayca ikna edilemezler.

Burada sistemin oyunu bir dereceye kadar geri tepiyor. Entegrasyon propagandasının değerleri lehine başından beri "isyan eden" eylemciler savaş zamanında da bunu yapmaya devam ediyorlar. Savaş çabalarına sadece şiddet olduğu için değil, "ırkçı", "sömürgeci", "emperyalist" vb. oldukları için de karşı çıkıyorlar.

Sistemin oyunu, hayvanların tedavisi söz konusu olduğunda da geri tepiyor. Kaçınılmaz olarak, birçok insan hayvanlara karşı da insanlar için uygulamaları öğretilen yumuşak ve barışçıl tavırları sergiler. Hayvanların et için katledilmesinden ve tavukların küçük kafeslerde tutulan yumurtlama makinelerine indirgenmesi veya hayvanların bilimsel deneylerde kullanılması gibi hayvanlara zarar veren uygulamalar görünce dehşete düşerler. Hayvanların kötü muamelesine karşı ortaya çıkan muhalefet sistem için bir noktaya kadar yararlı olabilir: Vegan bir diyet, kaynak kullanımı açısından etçil bir diyetten daha verimli olduğu için, veganlık, yaygın olarak benimsenirse, insan nüfusunun artışının Dünya'nın sınırlı kaynaklarına getirdiği yükü hafifletmeye yardımcı olacaktır. Ancak aktivistlerin bilimsel deneylerde hayvan kullanımına son verme konusundaki ısrarı, sistemin ihtiyaçları ile doğrudan çelişmektedir, çünkü öngörülebilir gelecekte, araştırma konusu olarak yaşayan hayvanlar için bir ikame bulunması muhtemel değildir.

Yine de sistemin oyununun böyle birkaç yerde geri tepmesi, onun isyancı dürtüleri sistemin lehine çevirmek için kullanılan etkili bir araç olduğu gerçeğini değiştirmez.

Burada anlatılan oyunun, toplumumuzdaki isyankâr dürtülerin yönünü belirleyen tek faktör olmadığı da kabul edilmelidir. Bugün birçok insan kendini zayıf ve güçsüz hissediyor (sistemin bizi gerçekten zayıf ve güçsüz kılmasının çok iyi bir nedeni var) ve bu nedenle saplantılı bir şekilde kurbanlarla, zayıflarla ve ezilenlerle özdeşleşiyor. Irkçılık, cinsiyetçilik, homofobi ve yeni sömürgecilik gibi mağduriyet konularının standart eylemci meseleleri haline gelmesinin bir nedeni de budur.

*** 5. Bir Örnek

Yanımda bir antropoloji ders kitabı var ve burada üniversite entelektüellerinin, uyumu modern toplumun eleştirisi adı altında saklayarak sistemin oyunlarına nasıl yardım ettiklerinin birkaç güzel örneğini fark ettim. Bu örneklerin en tatlısı, yazarın, interseks bir kişi (yani hem erkek hem de kadın fiziksel özellikleriyle doğmuş bir kişi) olan Rhonda Kay Williamson'ın bir makalesinden "uyarlanmış" biçimde alıntı yaptığı 132-36. sayfalarda bulunur.

Williamson, Amerikan Kızılderililerinin sadece interseks kişileri kabul etmekle kalmayıp, onlara özellikle değer verdiklerini belirtmektedir. Bu tavrı, kendi ebeveynlerinin kendisine karşı benimsediği tavırla eşitlediği Avrupa-Amerikan tavrıyla karşılaştırıyor.

Williamson'ın ailesi ona zalimce davrandı. Onu interseks durumu nedeniyle hor gördüler. Ona "lanetli olduğunu ve şeytana teslim edildiğini" söylediler ve "şeytan"ı ondan kovmak için onu şaşalı kiliselere götürdüler. Hatta "içindeki iblisi kusması" için poşetler bile verildi.

Ancak bunu modern Avrupa-Amerikan tutumuyla eşitlemek açıkçası gülünçtür. 150 yıl önceki Avrupa-Amerikan tutumuna yakın olabilir, ancak günümüzde hemen hemen her Amerikan eğitimci psikoloğu veya ana akım din adamı, interseks bir kişiye bu tür bir muamele yapıldığını görse dehşete düşer. Medya böyle bir muameleyi olumlu bir ışık altında tasvir etmeyi asla hayal edemezdi. Bugün ortalama orta sınıf Amerikalılar, interseks olma durumunu Kızılderililer kadar kabul etmeyebilir, ancak çok az kişi Williamson'a yapılan muamelenin zulüm olmadığını söylerdi.

Williamson'ın ebeveynleri açıkçası sapkınlardı, tutumları ve inançları sistemin değerleriyle bağdaşmayan dindarlardı. Böylece, Williamson, modern Avrupa-Amerikan toplumunu eleştirir bir gösteri sergilerken, gerçekten de yalnızca sapkın azınlıklara ve günümüz Amerika'sının egemen değerlerine henüz adapte olmamış kültürel geri kalmışlara saldırıyor.

Kitabın yazarı Haviland, 12. sayfada kültürel antropolojiyi modern Batı toplumunun varsayımlarına meydan okuyan ikonoklastik olarak tasvir ediyor. Bu, gerçeğe o kadar aykırı ki, o kadar komik ve acınası ki. Modern Amerikan antropolojisinin ana akımı, sistemin değerlerine ve varsayımlarına sefilce boyun eğmektedir. Günümüzün antropologları toplumlarının değerlerine meydan okuyormuş gibi yaptıklarında, tipik olarak yalnızca geçmişin değerlerine meydan okurlar- sistemin bizden talep ettiği kültürel değişimlere ayak uyduramayan sapkınlar ve geri kalmışlardan başka kimsenin sahip olmadığı eskimiş ve modası geçmiş değerler.

Haviland'ın Williamson'ın makalesini kullanması bunu çok iyi açıklar ve Haviland'ın kitabının genel eğilimini bunlar temsil eder. Haviland, okuyucularına politik olarak doğru dersleri öğreten etnografik gerçekleri gösterir, ancak politik olarak yanlış olan etnografik gerçekleri hafife alır veya tamamen atlar. Bu nedenle, Williamson'ın, Kızılderililerin interseks kişileri kabul ettiklerini vurgulamak için anlattıklarını aktarırken, örneğin, birçok Kızılderili kabilesi arasında zina yapan kadınların burunlarının kesildiğinden bahsetmiyor; ya da Karga Kızılderilileri arasında bir yabancı tarafından vurulan bir savaşçının suçluyu derhal öldürmek zorunda olduğunu, aksi takdirde kabilesinin gözünde geri dönülmez bir şekilde küçük düşürüldüğünü; Haviland da Kızılderililerin alışılmış işkence yöntemini tartışmıyor. Tabii ki, bu tür gerçekler şiddeti, maçoluğu ve cinsiyet ayrımcılığını temsil eder, bu nedenle sistemin günümüz değerleriyle tutarsızdırlar ve politik olarak yanlış olarak sansürlenme eğilimindedirler.

Yine de Haviland'ın antropologların Batı toplumunun varsayımlarına meydan okuduğu inancında tamamen samimi olduğundan şüphem yok. Üniversite aydınlarımızın kendini kandırma kapasitesi kolaylıkla o dereceye düşebilir.

Sonuç olarak, açıkça belirtmek isterim ki, zina için burunları kesmenin iyi bir şey olduğunu söylemediğim veya kadınların başka herhangi bir şekilde istismar edilmesine müsamaha gösterilmesini önermediğim gibi, interseks oldukları için ya da ırkları, dinleri, cinsel yönelimleri vb. nedeniyle kimsenin küçümsendiğini veya reddedildiğini de görmek istemem. Ama bugün toplumumuzda bu konular olsa olsa reform konularıdır. Sistemin en ince oyunu, aksi takdirde devrimci bir yön almış olabilecek güçlü isyankâr dürtüleri bu mütevazı reformların hizmetine çevirmiş olmasıdır.

<br>



