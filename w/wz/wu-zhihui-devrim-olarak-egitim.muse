#title Devrim Olarak Eğitim
#author Wu Zhihui
#SORTtopics çin
#date 1908
#source Yeni Çağ No.65,19 Eylül 1908
#lang tr
#pubdate 2020-01-13T23:00:00



<em>Wu Zhihui (1869-1953) Yeni Çağ’ı yayınlayan Paris grubunun etkili bir üyesiydi. Eğitimi anarşist toplumsal devrimin özellikle önemli bir parçası olarak düşündü, daha sonra bazı fikirlerinin uygulamaya konulduğu Çin’deki Emek Üniversitesi’nin kurulmasına yardım etti, kısa bir süre için bu üniversite, anarşist hareketi ezmeye başlamasından önce, Guomindang hükümetinin amaçlarına hizmet etti. Aşağıdaki pasajlar, kendisinin, ilk olarak Eylül 1908’de Yeni Çağ’da yayımlanan “Devrim Olarak Eğitimin Anarşist Savunusu Üzerine” adlı makalesinden alınmıştır. Makale İngilizceye, Oregon Üniversitesi Tarih Bölümü’nden Guannan Li tarafından çevrilmiştir.</em>

POLİTİK DEVRİMİN AMACI, haklar için mücadele etmektir. Politik devrim, kamu ahlakını ulusal egemenlik ile eşitler. Bu yüzden, politik devrim gerçekleştiğinde, çok kolay biçimde devrimci partinin sürü eylemine dönüşür. İlk önce, kralların elinden iktidarı alırlar. Ardından birbirleri ile savaşır, fark gözetmeyerek katliam yapar ve birbirlerini baskı altında tutarlar. Açıkça ihlal etmeye cüret etmedikleri tek şey anavatan ve ulusal egemenliktir.

….Politik devrimi savunanlar, hakları duyguları tahrik etmek için katalizör olarak kullanır. Bu durumda haklar, kamu ahlakına karşıdır. Burada, devrim ve eğitim iki ayrı şey olarak görünür. Bu yüzden kötü etkiler kaçınılmazdır.

Politik devrimde anayasalcılığa çağrı, bilhassa aşağılıkcadır… politik iktidarı ele geçirmiş olsalar bile, zorlayıcı emperyal ev varoluşuna devam edecektir.

Anarşist Devrim bütünüyle farklıdır. Anarşistler kamu ahlakını harekete geçirmeyi hedefler, birey ile toplum arasındaki karşılıklı ilişkiyle ilgilenirler ve kolektif mutluluğun peşinden gitmek için tüm kişisel haklardan feragat etmeye hazırdırlar. Bu, gerçekten de eğitime vurgu yapar, devrime değil… Eğitim yaygınlaştığında, herkes eski alışkanlıklarını terk eder ve yeni bir yaşama başlar. Devrim, bu nedenle, bu dönüşümün tam da kesin sonucudur. Bu sonuçlar açısından, devrime ön hazırlık yapan devrimci bir eğitimi, devrimden önce tesis etme arayışında olan devrimci bir savunuda, yanlış olan bir şey yoktur.

Dolayısıyla, anarşistlerin devrimi politik devrim değildir; anarşistlerin devrimi eğitimdir… Eğitim devrimdir. Gündelik eğitim gündelik devrimdir. Eğitimin daha küçük sonuçları, toplumsal adaletlerdeki küçük değişimlerdir. Buna küçük devrim denir… Eğitimin sonuçları, toplumun bütünündeki eski adetlerin ansızın dönüşümüdür, buna büyük devrim denir… Gerçek devrim için aslında tamamlanış yoktur. Gerçek ve adalet her gün ilerler. Eğitim durmadığı sürece, devrimde durmaz…

Anarşist eğitim hayırseverlik, bağımsızlık ve özgürlük vb. gibi gerçekleri ve kamu ahlakını içeren ahlaktan ve deneysel bilimler gibi gerçekleri ve kamu ahlakını içeren bilgiden oluşur. Bunların dışında eğitim yoktur.

Bazı akılsız insanlar, eğitimin ve devrimin ayrı şeyler olduklarını düşünür. Onların devrimi, isyan etmek için duyguları harekete geçirmeyi amaçlar. Onların zihinlerindeki eğitim, alt düzeyde metodun ve köle eğitiminin hakim olduğu yerler olan okullarda, pedagoglar tarafından yürütülen eğitimdir. Kamu ahlakını geliştirmek için, bilinç reformunu savunuyorlar. Fakat, ne kadar geliştirirlerse, o kadar yolundan şaşacaklar. Böylece, kamu ahlakının gelişimi nihayetinde başarısız olur ve kısa ömürlü devrim; düşünce kabiliyeti olmayan isyan tarafından sonlandırılır. Çünkü eğitimin devrim ve devrimin de ahlakının ortaya serilmesi olduğunu hiç kabul etmiyorlar. Sadece ilerici anarşist-eğitimciler kamu ahlakının devrim olduğuna dair bütünsel bir görüşe sahipler.



