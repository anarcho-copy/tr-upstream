#title Mahnovşçina ve Anarşizm, 1921
#author Peter Arşinov
#SORTtopics anarşizm, ukrayna, mahnovist, mahnovşçina, ukrayna devrimci isyan ordusu, rus devrimi
#date 03.01.2019
#lang tr
#pubdate 2020-07-01T13:10:29




[[m-v-mahnovscina-ve-anarsizm-1.png f]]


Peter Arshinov (1887-1937) devrimci bir sosyalistti, 1905 Rus devrimi süresince Bolşevik Partinin bir üyesiydi. 1906’da anarşist oldu. Doğum yeri olan Ukrayna’ya döndüğünde, bir polis merkezinin bombalanması olayına karıştı ve birçok işçinin eziyet çekmesinden ve ölümünden sorumlu bir demiryolu patronuna suikast düzenledi. Hapsedildi ve idam cezasına çarptırılınca, 1907’de kaçtı. Birkaç yıl Avrupa’da dolaştıktan sonra devrimci eylemlerine yeniden başlamakmak için 1909’da Rusya’ya geri döndü. 1910’da tekrar tutuklandı ve yirmi yıl hapse mahkum oldu. Moskova’daki Butyrki hapishanesine gönderildi, orada yerel anarşist bir grubun bir polis şefine düzenlediği suikaste karışmış olmaktan müebbet hapse mahkum olmuş Nestor Makhno ile tanıştı. Mart 1917’de, Şubat Devrimi’nin neticesinde, Arshinov ve Makhno hapishaneden salıverildiler. Makhno derhal Ukrayna’ya dönerken, Arshinov bir süre Moskova’da kalıp Maskova Anarşist Gruplar Federasyonuyla birlikte çalıştı. 1918’de Moskova’da yeniden buluştular, ancak Makhno 1919’da bir kitle isyanı başlattıktan sonra Arshinov Ukrayna’ya dönüp Mahnovist harekete (Mahnovişçina) katıldı ve 1921’deki yenilgisine dek orada kaldı. Aşağıdaki pasajlar Arshinov’un 1923’de basılan Lorraine ve Fredy Perlman tarafından tercüme edilmiş History of the Makhnovist Movement* (1918-1921) adlı kitabından alınmıştır.

ANARŞİST İDEAL, ÇEŞİTLİLİK bakımından geniş ve zengindir. Yine de, anarşistlerin kitlelerin toplumsal mücadelesi içerisindeki rolü son derece alçakgönüllüdür. Görevleri, mücadelede ve yeni toplumun kuruluşunda kitlelerin doğru yolda olmalarına yardım etmektir. Eğer kitle hareketi kararlı bir çarpışma aşamasına girmemişse, anarşistlerin görevi, kitlelerin önlerindeki mücadelenin önemi, görevleri ve amaçları konusunda aydınlanmasına yardımcı olmaktır; onların görevi gerekli askeri hazırlıkları yapmasında ve güçlerini örgütlemesinde kitlelere yardımcı olmaktır. Eğer hareket nihai çarpışma aşamasına zaten girmiş bulunuyorsa, anarşistler vakit kaybetmeksizin harekete dahil olmalıdır; kitlelerin yanlış sapmalardan kendilerini özgürleştirmesine yardım etmeli, onların ilk yaratıcı çabaları desteklemeli, entelektüel olarak onlara yardımcı olmalı, ve hareketi işçilerin gerçek hedeflerine yönelen yörüngede tutmak için yardım etmeye daima çabalamalıdır. Bu, devrimin ilk aşamasında, anarşistlerin temel ve aslında tek görevidir. Bir kez mücadelede uzmanlaşan ve mücadelenin toplumsal inşasına başlayan işçi sınıfı, yaratıcı çalışmada inisiyatifi artık kimseye bırakmayacaktır. O zaman işçi sınıfı kendisini kendi düşünceleriyle yönlendirecektir; bu da onun planları doğrultusunda bir toplum yaratacaktır. Bu anarşist bir plan olsun ya da olmasın, bu planın yanı sıra bu plana dayalı toplum da, kendi düşüncesi ve iradesi üzerinden şekillenmiş ve düzenlenmiş olan özgürleşmiş emeğin derinliklerinden gün yüzüne çıkacaktır.

Mahnovişçina’yı incelediğimizde hemen bu hareketin iki temel durumunun farkına varırız: 1)Toplumun en aşağı katmanlarının bir halk hareketi olarak gerçekten proleter kökeni: Hareket aşağıdan başlamıştır ve hareket başından sonuna dek bizzat halk kitleleri tarafından desteklenmiş, geliştirilmiş ve yönlendirilmiştir; 2) Hareket, en başından beri, kasıtlı olarak tartışmasız bir şekilde anarşist ilkelere dayanmıştır: (a) işçilerin tam inisiyatif hakkı, (b) işçilerin ekonomik ve toplumsal öz yönetim hakları, (c) toplumsal inşada devletsizlik ilkesi…

Mahnovişçina da, tam olarak fark edilmeyen, tamamen belirginleşmemiş. ama anarşist ideal için mücadele eden ve anarşist yolda yürüyen emekçi kitlelerin anarşist bir hareketini görürüz.

Ancak tam da, bu hareket kitlelerin derinliklerinden hasıl olduğu için, gerekli teorik güçlere, herhangi bir yaygın hareket için vazgeçilmez olan genelleme yapma güçlerine sahip değildi. Bu noksanlık, genel durum karşısında hareketin fikirlerini ve sloganlarını geliştirmekte ya da teorinin katı ve işlevsel biçimlerini hazırlamakta başarısız oluşunda kendisini gösterdi. İşte bu yüzdendir ki hareket, özellikle her taraftan ona saldıran sayısız düşman karşısında, yavaş ve acılı bir biçimde gelişti…

Hareketin temel eksikliği son iki yılı boyunca genel olarak askeri etkinliğe yoğunlaşmış olmasında yatar. Bu hareketin kendisinin organik bir kusuru olmaktan ziyade, Ukrayna’daki durumun ona dayattığı şansızlığıdır.

Ara vermeksizin üç yıl süren iç savaşlar güney Ukrayna’yı kalıcı bir savaş alanı haline getirdi. Maddi, toplumsal ve ahlaki yıkıma uğratarak köylülerden öç alan çeşitli partilerin sayısız orduları bölgeyi o baştan bu başa kat ediyordu. Bu, köylüleri tüketip bitirdi. Bu, işçilerin özyönetim alanındaki ilk tecrübelerini yok etti. Onların toplumsal yaratım ruhunu ezildi. Bu koşullar Mahnovişçina’yı sağlıklı dayanaklarından, kitleler arasında toplumsal yaratıcı çalışma yapmaktan ayırdı ve onu savaşa yoğunlaşmaya zorladı -doğru, devrimci savaşa, ama yine de savaşa…

Mahnovişçina toplumsal devrimi doğru kavramaktadır. Devrimin zaferi ve güçlenmesinin ve onu takip edebilecek refahın gelişiminin, şehirlerdeki emekçilerle kırsaldaki emekçiler arasında yakın bir ittifak olmadan gerçekleştirilmeyeceğinin farkındadır. Köylüler, kent işçileri ve güçlü endüstriyel girişimler olmadan, toplumsal devrimin mümkün kılacağı faydaların birçoğundan mahrum kalacaklarının farkındadır. Dahası, kent işçilerini kardeşleri olarak görmekte, onları aynı emekçi ailesinin üyeleri saymaktadırlar.

Hiç şüphe yok ki, zafer ve toplumsal devrim anlarında, köylüler işçilere tam desteklerini sunacaktır. Bu, doğrudan kent proletaryasına verilen gönüllü ve gerçek devrimci destek olacaktır. Şimdiki durumda, köylülerden zor kullanılarak alınan ekmek, asıl olarak devasa hükümet makinesini beslemektedir. Köylüler bu pahalı bürokratik makineye ne kendilerinin ne de işçilerin gereksinim duyduğunu, hükümetin işçilere yönelik tavrının, bir hapishane yönetiminin mahkumlara yönelik tavrına benzediğini çok iyi bir şekilde görmekte ve anlamaktadırlar. Bu nedenledir ki, köylüler ekmeklerini gönüllü olarak devlete vermek yönünde en ufak bir istek bile taşımamaktadırlar. Günümüz vergi toplayıcıları -komiserler ve Devletin farklı ikmal organları- ile ilişkilerinde bu yüzden bu denli saldırganlardır.

Ancak, köylüler daima kent işçileriyle doğrudan ilişkiye girmeye çabalar. Bu sorun köylü kongrelerinde birden çok kez ortaya atıldı ve köylüler daima bunu devrimci ve olumlu bir şekilde çözdüler. Toplumsal devrim anlarında, şehirli proleter kitleler tamamen bağımsız olduklarında ve kendi örgütlenmeleri üzerinden köylülerle doğrudan ilişkiye geçtiklerinde, köylüler, yakın gelecekte işçilerin bütün devasa endüstri güçlerini kentteki ve kırsaldaki işçilerin ihtiyaçlarının hizmetine uygun olarak konumlandıracağını bilerek, çok gerekli olan gıda maddesini ve hammaddeleri tedarik edeceklerdir…

Kitlelerin sadece eskiyi yok edebileceğini, sadece imhaya giriştiklerinde yüce ve kahraman olduğunu ve yaratıcı çalışmada onların atıl ve bayağı olduğunu öne sürerken devletçiler yalan söyler. Yaratıcı aktivite alanında, günlük çalışma alanında, kitleler büyük başarılar ve kahramanlıklar gösterebilir. Fakat bunun için ayaklarının altında sağlam bir temel hissetmeleri gerekir; tamamen özgür olduklarını hissetmelilerdir: yaptıkları işin kendilerinin olduğunu bilmelilerdir; benimsenen her toplumsal kanunda kriterin kendi iradesinin, umutlarının ve arzularının tezahürünü görmelidirler. Kısacası, kelimenin en geniş anlamıyla kitleler kendi kendilerini idare etmelidir…

Dünya proleterleri, kendi varlığınızın derinliklerine bakın, gerçeği arayın ve onu kendiniz gerçekleştirin; gerçeği başka hiçbir yerde bulamazsınız.

Bu Rus devriminin şiarıdır.



