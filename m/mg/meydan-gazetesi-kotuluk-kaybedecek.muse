#title Kötülük Kaybedecek
#author Meydan Gazetesi
#SORTtopics kötülük, polis, direniş
#date 21.09.2017
#lang tr
#pubdate 2019-12-19T11:29:43


Kötülük doğmaz, doğurulmaz. Bir çiçekten toprağa düşüp, bahar gelince de baş vermez. Kimilerinin dediği gibi bir rüzgar gibi esmez. Ani bir yağmur gibi boşalmaz. Kötülük üretilir. Bu işin ustası olmuş ellerce hazırlanır, paketlenir ve her gün yürüdüğümüz sokakların, meydanların dibine, evlerimizin kuytu köşelerine, tezgahların kenarına, çalışma masalarının köşesine sinsice yerleştirilir.

Bazen kötülük bomba olur patlar bir meydanda. Yüz insan, sanki hiç anıları, dostları, gülümseyen yüzleri, tatlı telaşları ve korkuları yokmuş gibi ölüverir bir anda. Bazen anlamsız ve iğrenç bir açlık olur, kendinden güçsüz olana her şeyi yapabileceğini düşünür. Dokuz yaşındaki bir çocuğa tecavüz eder. Ayrılmak isteyen karısını sokağın ortasında kırk altı defa bıçaklar. Bazen zehirli bir su gibidir. Bu sudan içenler çıldırmış gibi sağa sola saldırır. Devlet, millet diye diye, bir annenin cenazesini mezardan çıkartır. Başka bir cenazeyi arabanın arkasına bağlayıp sürükler, başka birinin kulağını keser anahtarlık yapar. Kendi işine gelmeyen, kendine biat etmeyenin ne dirisine ne ölüsüne tahammülü vardır.

Üniforma giydiğine çok rastlanır, inandıkları için, her şeye rağmen direnmeyi seçtikleri için tereddütsüz bir şekilde açlığa yatan iki insanın üzerine gevrek gevrek, aptal aptal sırıtarak elindeki gaz tüpünü boşaltır. Kalp atışları yavaşlayan, günden güne eriyen bu iki insanın başına gidip, sanki hiç çocuğunun başını okşamamış, kimseyle dostça el sıkışmamış gibi büyük bir soğukkanlılıkla “öldünüz mü lan?” diye sorar.

Kendi uygarlığını, kendi krallığını üretmek için binlerce ağacı kesiverir bir gecede. Ya da nedensiz yere bir köpeği tekmeleyiverir. Bir Caretta Caretta’nın kafasına 10 kurşun sıkabilir mesela. Daha da büyüsün daha da semirsin diye açtığı devasa fabrikalar, santraller, madenler, alışveriş merkezlerinin dünyayı zehirlemesine aldırmaz. Dünya yavaş yavaş ölümün eşiğine gelirken kasırgalar fırtınalar seller ve tufanlarla sarsılırken, hiçbir şey olmamış gibi işine devam edebilir.

Laboratuvar ortamında iktidarlar tarafından üretilmiş bir çeşit virüstür kötülük. Manipülasyon ve korkuyla beraber enjekte edilir. Bu her şeyi normalleştirir. Zamanla her şey o kadar normal gözükür ki yabancı olan şey silikleşir, kötülük kavranamaz; anlaşılamaz hale gelir.

Aslında her şey itaat etmekle başlar. Dostça bir gülümsemeye karşılık vermediğimiz yerde ortaya çıkar, ötekine yardım etmeyi reddettiğimizde ya da bir adaletsizliğe kayıtsız kaldığımızda bizi sarmaya başlar. Ve nihayetinde başka bir canlıya nefretle yaklaştığımızda bizleri ele geçirir.

Ama dedik ya başta, kötülük doğamaz, yapılsa üretilse bile kök salamaz diye. O yüzden bizden değildir kötülük. İşte Kropotkin de tam olarak bundan bahseder. İyi olan, hayatta kalma çabasıdır. Dayanışma ve karşılıklı yardımlaşma hayatta kalmanın yegane yoludur. Yaşamın akışını kesen şey kötüdür, bu da bencilikle ve rekabetle ilişkilidir. Bunların sonucu olan iktidar ve tüm merkezi yapılarla beraber devlet kötüdür. Kötülüğün somutlaşmış halidir.

Bu kötülüğü sağaltmak, bu kökleri tekrar canlandırmakla mümkün olur ancak; itaat etmeyi reddetmekle başlar. Dayanışma ve duygudaşlıkla beraber büyür. Ötekine karşı duyulan nefretin, iktidara karşı öfkeye dönüşmesiyle yenilir kötülük!

İtirazın, isyanın, duygudaşlığın ve alçak gönüllüğün tekrar hayat bulduğu yerde kötülük ölmeye başlıyordur! Yaşamın tekrar akmaya başladığı yerde kötülük kaybetmeye mahkûmdur!

Kötülük Kaybedecek; Biz Kazanacağız!



