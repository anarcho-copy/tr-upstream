#title Bakunin Yoldaş 206 Yaşında
#author Meydan Gazetesi
#SORTtopics mikhail bakunin, anarşist, tarih
#date 30.05.2020
#source 24.06.2020 tarihinde şuradan alındı: [[https://meydan.org/2020/05/30/bakunin-yoldas-206-yasinda/][meydan.org]]
#lang tr
#pubdate 2020-05-29T22:00:00



<em>“…Bizler tüm resmi iktidarların keskin düşmanlarıyız; bu iktidarlar son derece devrimci olsalar bile. Bizler alenen tanımlanmış her türlü diktatörlüğün düşmanıyız; biz devrimci anarşistleriz…”</em> sözleriyle, tüm iktidarlara karşı mücadelesini belirginleştiren Mikhail Bakunin, 141 yıl önce bugün, 30 Mayıs 1814’te doğdu.

Mikhail Bakunin’in yaşamı ve mücadelesinin anlatıldığı, Karala Dergisi’nin 1. sayısında yayınlanan yazıyı, Bakunin’in 206. doğum yıl dönümünde paylaşıyoruz.

<em>Bir devrimci anarşistin hayatı nasıl anlatılır? Doğumu, yaşayışı ve ölümü üzerine kurulu bir şablondan değil elbette. Özgürlük, eğer düşündüğünü eyleyebilmekse; devrimci bir anarşisti anlatırken düşünceleri önemli bir yerde durur. Çünkü o, düşüncelerini içinde bulunduğu anda gerçekleştirebilmenin çabası içerisindedir.</em>

<em>Söz konusu anarşist Bakunin ise, yaşamı üzerine bir şeyler yazabilmek daha da zordur. Üzerinde dikkatli çalışmayı gerektirir. Yazdıklarının ve eylediklerinin nelere yol açtığı, sadece içinde bulunduğu zamandan anlaşılmaz. Öngörülerinin haklılığı sonraki yüzyıllarda da kendini göstermiştir.</em>

<em>Bakunin, tarihte anarşist düşüncenin pratiğe dökülmesindeki en büyük isimlerden biri değildir sadece. Hareket insanı gibi gösterilmesine karşın, anarşist düşüncenin en önemli tartışmalarını vermiş; düşüncenin geliştirilmesine büyük katkılarda bulunmuş bir anarşisttir.</em>

*** Bakunin’in Eylemi

<em>1840’ta doğduğu topraklardan; Rusya’dan ayrıldıktan sonra Polonya’dan Bohemya’ya; İtalya’dan Fransa’ya, Avrupa’da devrimci düşünceyi ve hareketi gittiği her yere götürdü. Avrupa’nın dört bir yanında köylü isyanlarının en ön saflarında, işçi örgütlenmelerinin kuruluşunda, ezilenlerin özgürlüğü önünde engel olan bütün güçlere karşı savaştı. Bunu yaparken ezilenlerin örgütlenme gerekliliğini anlattı. İktidarlara karşı verilecek mücadelenin, yıkıcı ve güçlü yanını tam da buraya koydu. <strong>“Özgürlük ancak özgürlükle, halkın topyekün isyanıyla ve halkın tabandan gönüllü örgütlenmesiye yaratılabilir.</strong></em>

<em>19.yüzyılda Bakunin’in Avrupa’daki etkisinin ne olduğunu anlamak açısından, Çarlık Rusya’ya karşı mücadeledeki en etkili isimlerden Alexander Herzen’in anekdotu değerlidir. “Bakunin Paris’ten Prag’a giderken Alman köylülerinin isyanıyla karşılaşır. Köylüler kalenin etrafında ne yapacaklarını bilmez halde dolanırken, Bakunin arabadan iner ve isyanın neden olduğunu sormakla zaman kaybetmeden, köylüleri örgütler. Arabasına geri dönüp yola koyulduğunda kale çoktan dört tarafından yükselen alevler içindedir.”</em>

<em>1847’de, Polonya Ayaklanması’nı anmak için Paris’te yaptığı bir konuşmadan dolayı Fransa’dan sınırdışı edilir. Çarlık Rusya’nın, Bakunin’i yakalamak için tüm çabalarına rağmen, Bakunin Avrupa’nın farklı şehirlerinde gizlenmeyi başarır. Buralarda boş durmaz, ayaklanmalar örgütler. Özellikle Dresden ayaklanmasıyla ismi, otorite ve baskıyla karşılaşan tüm kesimlerin dilinde dolaşmaya başlar. Sadece Slavların değil, farklı imparatorlukların zulmüne maruz kalan halkların verdiği özgürlük mücadelelerinde, Bakunin’in düşünceleri ve eylemleri bir dayanak noktası haline gelir.</em>

<em>1850’den 1861’deki hapishaneden kaçışına kadarki süre içerisinde Çarlık Rusyası tarafından tutsak edilir. Önce Japonya’ya, sonra ABD’ye oradan da İngiltere’ye geçerek mücadelesine kaldığı yerden devam eder. Özellikle hapisten kaçtıktan sonraki bu süreçte, Uluslararası Kardeşlik İttifakı’nın altyapısı oluşturulmaya başlanır. Gittiği farklı yerlerdeki Kardeşlik Örgütlenmeleri, Floransa Kardeşliği örneğinde olduğu gibi Enternasyonal’in yerel birimlerine dönüşür. 1869’da (sadece işçilerin uluslararası en büyük örgütlenmesi değil, ezilen halkların da bir örgütlenmesi konumunda bulunan) Enternasyonal’in Basel Kongresi’ne katıldığında, sadece Enternasyonal için yeni bir dönem başlamaz, sosyalizmin içerisindeki iki ekol anarşizm ve marksizm arasındaki temel ayrılıklar şekillenir. 1873’te Karl Marks’ın örgütlediği karalama kampanyalarıyla, ayak oyunlarıyla Bakunin ve grubu Enternasyonal’den ayrılır. Dolayısıyla İsviçre, Fransa, İspanya, İtalya ve Belçika seksiyonları da… Sonrasında St. Imier’deki Kara Enternasyonal’in yerellerini oluşturacak bu seksiyonlar, bulundukları bölgelerdeki anarşist hareketin ve düşüncenin gelişimi noktasında önemli bir yere sahip olmuşlardır.</em>

<em>1870’de Lyon Ayaklanması’ndadır. Ayaklanma, Paris Komünü açısından önemlidir. Çünkü ayaklanma komünün ilk adımıdır. Lyon’daki düşünce Paris’i komünleştirir. Merkeziyetçiliğe karşı özörgütlülük temelinde inşa edilen ayaklanma özellikle Paris Komünü’ndeki anarşistler tarafından ilham alınarak uygulanır. Bir yıl öncesindeki ayaklanmadaki tutuklanma kararından dolayı, Marsilya’da saklanmak zorundadır. Ölümünden iki yıl önce Bologna’da örgütlemeye çalıştığı isyan, onun son çabası olarak anarşist tarihteki yerini alır.</em>

*** Bakunin’in Düşüncesi

<em>Mücadeleyle bu kadar iç içe geçen bir yaşamının, Bakunin’i yazınsal anlamda verimli olmasına müsade etmediği gibi yaygın bir kanı vardır. Bu yaygın kanının aksine, Bakunin düşünsel anlamda anarşist ideolojiye katkısı oldukça yüksektir. Mücadele alanındaki deneyimler onun yazdıklarına oldukça etki edebilmiş ve dönemin (ve sonraki dönemlerin) temel tartışmalarına anarşist bir perspektif oluşturmuştur.</em>

<em>Almanya’da Gericilik; Slavlara Çağrı; Federalizm, Sosyalizm ve Anti-teolojizm; Tanrı ve Devlet; Paris Komünü ve Devlet Düşüncesi, Özgürlük ve Devlet gibi yapıtları, yazdıklarının sadece bir kısmını oluşturmaktadır. Yazdıklarında, genel olarak içerisinde bulunulan durum ya da sistemi irdelemeye çalışan Bakunin, sadece tespit yapmaz. Durumun ya da sistemin nasıl üstesinden gelineceğine ilişkin de yazar.</em>

<em>Hapishane yıllarından önceki süreçte, federalizm, halkların özgürlüğü, devlet karşıtlığı gibi konular üzerine en fazla yazdığı, konuştuğu ve tartıştığı konulardır. Enternasyonal’e dahil olduğu süreçte, Marks ve ekibine karşı giriştiği otoriter sosyalizm eleştirileri, sadece anarşist düşünce açısından değil, devrimci perspektif açısından da önemlidir. Sosyalizmin sonraki pratiklerinde devletin ve otoritenin rolünün ne olduğu, bu deneyimlerin nasıl olumsuzluklara yol açtığı düşünüldüğünde Enternasyonal’deki tartışmalar önemini bir kez daha ortaya koyar. Paris Komünü gibi farklı toplumsal devrim süreçlerine ilişkin değerlendirmeleri ayrıca önemlidir. Farklı deneyimlerin bizzat içerisinde yer almış bir devrimcinin değerlendirmeleri olması açısından, Bakunin’in tüm çabaları özgürlük düşüncesinin coğrafyalar arası pratikleme çabasıdır.</em>

*** Düşünce ve Eylemin Uyumu

 <em>“Komünizm teoriden değil, pratik içgüdüden türer.”</em>

<em>Düşünce ve eylem arasındaki dengeli bir uyumdur Bakunin’in yaşamı. Nasıl ki, anarşizm sadece bir kuram ve düşünceler bütünü değilse; düşüncelerin inşasına dayanan bir eylem ve hareketse, Bakunin de anarşizmin bu özelliğini kendisine ilke edinmiştir. Bir yandan düşüncelerini dünyanın farklı yerlerindeki özgürlük mücadeleleriyle şekillendirmiş, bir yandan da anarşist ideolojinin somutlanması için Jura Federasyonu’ndan Floransa Kardeşliği’ne özörgütlülükler oluşturmuştur.</em>

<em>Bakunin, yaşamını özgürlüğün önünde dikilen her iktidarı yıkmaya adamış devrimci bir anarşist. Yaşamın yaratıcı kaynağı olduğu için yıkıcı tutkusunu dizginlemeden…</em>



