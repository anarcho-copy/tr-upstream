#title Ferguson’da Her Yerde Bütün Polisler Katildir
#author Meydan Gazetesi, Scott
#SORTauthors  Meydan Gazetesi, Scott
#SORTtopics anti faşist, faşizm, abd
#date 20.09.2014

#lang tr
#pubdate 2020-04-14T17:02:43



<quote>

9 Ağustos günü ABD’nin Ferguson şehrinde, siyahi bir genç, silahsız olduğu halde ve insanların gözü önünde polis kurşunuyla katledildi. Ferguson’da başlayan protestolar on gün süren bir isyana dönüştü ve ABD’nin bir çok yerine yayıldı. Bölgede isyana katılan anarşist örgütten Scott ile yaptığımız röportajı yayınlıyoruz. Bölgedeki yoldaşların diğer yazıları için http://antistatestl.noblogs.org/ ‘a bakabilirsiniz.
</quote>



<strong>Meydan: St. Louis ve Ferguson’da polis şiddeti genel olarak hangi düzeyde? Son zamanlarda bir artış oldu mu?</strong>

<strong>Scott:</strong> Polis şiddeti burada ABD’deki birçok yerden daha fazla, ama herhangi bir orta-batı şehrinde durum farklı değil. Hayır, arttığını düşünmüyorum. Bence buna karşı tahammülsüzlük arttı. ABD’deki birçok yerde olduğu gibi, siyahileri taciz ederek, suçlu duruma düşürerek, katlederek, cezaevlerine hapsederek ya da belli mahallelerden sürekli uzak tutarak “hadlerini” bildirmek için ciddi bir çaba var.

Polis her yerde bir işgalci güç olarak var ve St. Louis’in siyahi gettolarında durum farklı değil. Polis orada sınıflı topluma karşı yaratılacak herhangi bir hareketi bastırmak için duruyor ve bunun bir parçası olarak ırklar arasındaki yapay ayrımları dayatıyor. Beyazların bölgesinde bir siyah olmak şüphelidir ve sizi durdurup sizinle bayağı uğraşırlar. Siyahların mahallesinde beyazsanız sizi durdururlar ve tehlikeli mahallede olduğunuz için azarlanırsınız. O mahallede yaşadığınızı söylediğinizde polis çoğu zaman “buralar beyazlar için tehlikeli” derler. Ne zaman siyahi biri bu uyarılara karşı gelse bir sürü polis bölgeye gönderilir.

<strong>Obama yönetimi iktidara geldikten sonra devlet politikasında bir değişim oldu mu? Başkanın rengi, Mike Brown’un katledilmesi karşısındaki davranışları etkiledi mi?</strong>

Tabii k: Obama’nın seçilmesi birçok insan için tarihi bir andı, ama sadece renk-körlüğü ve demokrasi yalanını güçlendirmeye yaradı.

Bazı insanlarda seslerinin gittikçe daha çok duyulacağı ve politik sistem dahilinde çalışmanın daha mümkün olduğu algısı var. İsyanlar ve yağmalar sırasında bu tip insanlar sükunet çağrısı yapıyor ve sistemden umudu kesmememizi söylüyorlar, işyeri yağmalarını istemiyorlar. Ortalığı karıştıranların kökünü kazımak için polisle işbirliği yapıyorlardı. İsyanın bitmesini istiyorlardı çünkü normal, saygın, politik anlayışa sığmıyordu. Adalet sisteminin her ırktan insana hizmet edeceğine gerçekten inanıyorlar ve bu bence kısmen Obama yüzünden böyle.

Elbette gerçek şu ki, hala güce sahip olanlar ve olmayanlar var—ve gücü yeni elde edenler ümitsizce durumu değiştirmeye çalışıyor, ya da delicesine görmezden geliyor ve bilerek bu gerçeği gizliyor. Obama’yı iyiye doğru bir adım olarak görenler var, ama bizim gibi, ırkı ne olursa olsun, hayatları değişmediği için politik sisteme baştan beri inanmayan birçok insan var.

<strong>İsyanla birlikte toplumsal algıda değişimler oldu mu? İsyandan sonra (özellikle anti-kapitalist ve devlet karşıtı) toplumsal hareketler arttı mı?</strong>

Açıkça anti-kapitalist ya da devlet karşıtı hareketler olmadı. İsyanın içindeki birçok kişi eylemleri ve sözleriyle, çoğu zaman doğası gereği bu tavrı gösterdi. Polisle yüzleşmek, bulvarı dönüştürerek özgürleştirilmiş bir bölge yaratmak gibi.

Özel mülkiyete ya da polise saygı duyulmayacağını ve birlikte yaptıklarında bundan zarar görmeyeceklerini öğrenen birçok insan olduğunu düşünüyorum, ya da umuyorum. Umarım isyan, insanlara daha çok işgal, polisle ve onun tarafındakilerle daha çok kavga ile onları ezen düzeni yıkma cesaretini verir.

<strong>İsyana sürükleyen öfkeyi, ekonomik ve sınıf çelişkilerine bağlayabilir miyiz?</strong>

İdari para cezaları ve fiziksel cezalar (yenilenmemiş trafik sigortası, aşırı hız, bozuk sinyal lambası, vs.), stratejik ve ırkçı biçimde kullanılıyor ve bence bunun öfkeye katkısı büyük. Irkçı polislerin bireysel olarak siyahileri trafikte durdurması yüksek bir olasılıktır. Çoğu insan cezaları ödemekte zorlanıyor ve arama emri çıkartılıyor. St. Louis içinde 80 tane kasaba var ve çoğunun kendi polis gücü var. Dolayısıyla bir kişinin bölgede birbirinden bağımsız birçok tutuklama emri olabilir. Birisi yakalandığında, bir cezaevinde yatar, diğer transfer edilir, sonra bir başkasına ve böyle sürüp gider. İnsanların hayatları paramparça oluyor: İçeride kaldıkça iş bulamıyor, kirayı ya da temel ihtiyaçlarını karşılayamıyor, çocuklarına bakamıyor, vb. Böylece bir yandan güçsüzlük algısı, diğer yandan öfke birikiyor. Tüm bunlar, polis cinayeti, cevap olarak isyana dönüşen protestolar, hepsi anlaşılabiliyor. Hayret verici olan tek şey, bunun neden daha sık olmadığı.

<strong>Anladığımız kadarıyla bu tip polis cinayetleri ABD’de neredeyse olağan. Bu durumda Mike Brown’un öldürülmesi neden isyana sürükledi?</strong>

Söylemesi zor. İsyanın basit bir formülü yok. İsyanı ateşleyen şey Ferguson polisinin cinayeti ele alış biçimi olabilir. Belki de halkın Brown’u hevesli bir genç—üniversiteye gidip büyük işler yapacak biri— olarak görmesi, polisi kışkırtıp layığını bulan bir suçlu olarak görmemesi yüzündendir. Brown’un katledildiği mahallenin birbirine sıkı bağlı olması, birçok insanın birbirini tanıması ve bu yüzden öfkeyi beraber hissedebilmesi yüzünden de olmuş olabilir. Bir sürü insan vurulduğunu bizzat gördü. Cansız bedeni dört saat boyunca sokakta bırakıldığı için etrafında büyük bir kalabalık toplandı.

<strong>Anarşistler isyana hangi seviyede katıldı? İsyanın karakterini nasıl etkilediniz?</strong>

Anarşistler oradaydı, ama çoğumuz anarşist kimliğimizle orada değildik. Bayraklarımız yoktu. Bildiri dağıtmadık. Bazılarımız olaylar çerçevesinde sokaklarda grafiti yaptı. Bazılarımızın çatışma deneyimi daha fazlaydı ve bu becerilerimizin, biber gazına ve plastik mermilere karşı koyarken diğerlerine de faydalı olabileceğini düşündük. İlk başlarda çeşitli nedenlerle bu yaklaşıma karar verildi.

Oradaki birçok insanın polise olan öfkesi ve kini bizimki ile örtüşüyordu, hatta çoğu kez bizi aşıyordu. Anarşistler, liderlik derdindeki diğer bazı devrimci grupların aksine etkin bir şekilde polisle çatıştı, yağmaya, vb. yasadışı eylemlere katıldılar. Anarşistler ön saflarda etkin olan insanlarla buluşup onların yanında kavgaya girdiler ve onları sadece piyon olarak görmediler. Bu işe yaradı çünkü ırksal farklılıklar biraz olsun kalktı ve birçok insan için polis (beyaz ya da siyah) ortak düşman haline geldi.

Anarşistler insanların etkin bir şekilde yüzlerini polis ve medyanın dikizleyen gözlerinden saklamalarını sağladı ve bence böyle bir etkimiz olmuş olabilir çünkü bazı geceler bir çok insanın yüzü maskeliydi. Anarşistler ayrıca, önlem olarak (polisin tüm kayıtlarını izlediği) medyanın yasadışı faaliyetleri kaydetmesini engelledi. Medya çoğu zaman kulak asmadığı için daha ciddi önlemler alındı. Anlatılanlara göre anarşist olmayan bir yağmacı, bir yoldaşın yardımına koşup, çekimi durdurmayan kameramana bıçak çekmiş.

Kalabalığı kendi tarafına çekmeye çalışan bir avuç politik grup vardı ve çok ayrıcalıklı ve yabancılaşmış bir haldeydiler. Kalabalığın büyük kısmı, liderlik talep edenleri dinlemek istemiyordu. Yeni Kara Panter Partisi, Siyahi Mücadele Örgütü, Örgütlü Reform ve Güçlendirme için Missouri’liler, İslam Milleti, Scientology Kilisesi, Devrimci Komünist Parti, seçilmiş idareciler, vb., hepsi kitleleri yönlendirmek istiyordu: bazıları açıkça, bazıları daha gizliden. Karşıt gruplar, kalabalık onlarla ilgilenmezken kendi aralarında megafonlarıyla tartışmaya girerek birçok kez komik ve gereksiz görüntüler oluşturdular. Bu maskaraya anarşistler olarak dahil olmamız için hiçbir neden yoktu. Bu isyana aktif katılımcılar olarak girmek, insanlarla tanışmak ve beraber direnmeyi öğrenmek daha iyiydi.

İftira söz konusu olunca en çok anarşistler hedef olurlar. İktidarı istemediğimiz ve direnişlere liderlik etmek istemediğimiz için, iktidar isteyenler, başaramadıklarında bizi suçlarlar. İsyan sırasında diğer politik gruplar genelde bize iftira attılar çünkü kendi programları kitleler tarafından duyulmuyordu. Bazıları şüpheli anarşistlerin fotoğraflarını internette yayınlayacak kadar ileri gittiler. İsyan ve kontrol edilemeyen kalabalıklar için bizi suçladılar, sanki politik olmayan siyahi insanlar anarşistler olmadan kendilerini koruyamazmış ya da onları çevreleyen sistemi deviremezlermiş gibi. İşin garibi fotoğraflardaki şüphelilerin çoğu anarşist değil, RCP’li komünistlerdi.

<strong>Sizce isyan sırasında kalıcı ilişkiler kuruldu mu?</strong>

Daha çok yapmadığım için üzüldüğüm tek şey bu: yeni ilişkiler kurmak, ama belki de tamamen bizim hatamız değildir. İnsanlarla beraber militanca çatışmak garip bir şey ve bir kaç gün sonra bu isyan durumu ortadan kalkıyor. Bütün bu güzel, isyankar insanlar nereye gitti diye merakla bakınıyorsunuz. Olayın sıcaklığı içinde sokaklardakilerle anında arkadaş olduk. Mucizevi biçimde birbirimizin arkasını kolladık. Karşı koyarken günlük hayattan bahsettik, birbirimizi korumak için yollar düşündük. O kadar çok şey, o kadar coşku ve şimdi bir sessizlik var.

Orada birlikte çatıştığımız çoğu insan kolayca ulaşılabilir değiller. Ya çalışıyorlar, ya da yaşamak için suç işliyorlar, aile geçindiriyorlar.

<strong>Eklemek istediğiniz ya da Meydan okurlarıyla paylaşmak istediğiniz başka bir şey var mı?</strong>

Bu röportaj çoğunlukla tek kişinin cevaplarıyla oluştu. Bu yüzden Ferguson’daki olaylarla ilgili kapsamlı bir perspektif veremez. Ferguson’da hiçbir zaman duyulmayacak bir sürü deneyim ve perspektif oluştu. Lütfen bunu dikkate alın çünkü bu deneyim hala sindiriliyor ve işleniyor. Ve bitmedi. Eğer mahkeme katilleri suçlu bulmazsa tekrar başlayabilir, hatta bu sefer daha yoğun bir şekilde.



