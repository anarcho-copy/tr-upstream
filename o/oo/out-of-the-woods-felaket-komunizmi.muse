#title Felaket Komünizmi
#author Out of the Woods
#date 08.05.2014 - 22.05.2014
#source 18.02.2023 tarihinde şuradan alındı: [[https://heimatloskolektif.wordpress.com/2023/02/13/felaket-komunizmi-bolum-1-felaket-topluluklari-out-of-the-woods/][heimatloskolektif.wordpress.com]]<sup>1</sup>, [[https://heimatloskolektif.wordpress.com/2023/02/15/felaket-komunizmi-bolum-2-komunizasyon-ve-betondan-utopya-out-of-the-woods/][heimatloskolektif.wordpress.com]]<sup>2</sup>, [[https://heimatloskolektif.wordpress.com/2023/02/18/felaket-komunizmi-bolum-3-lojistik-yeniden-tasarim-brikolaj-out-of-the-woods/][heimatloskolektif.wordpress.com]]<sup>3</sup>
#lang tr
#pubdate 2023-02-18T22:26:02
#topics afet, felaket komünizmi
#notes Çeviri: Konzept, Antarktika, Dobar, Kostenurka <br>İngilizce Aslı:
[[https://libcom.org/article/disaster-communism-part-1-disaster-communities][Disaster communism part 1]], [[https://libcom.org/article/disaster-communism-part-2-communisation-and-concrete-utopia][Disaster communism part 2]], [[https://libcom.org/article/disaster-communism-part-3-logistics-repurposing-bricolage][Disaster communism part 3]]

** Bölüm 1: Felaket Toplulukları

Bu üç bölümlük makalenin birinci bölümünde, felaket komünizmi kavramını, felaket durumlarında tipik olarak oluşan dayanışma ve karşılıklı yardımlaşma topluluklarıyla ilişkili olarak ele alıyoruz.

<quote>
“On binlerce insan bir şeyleri başarmak için sermayeye ya da hükümetlere ihtiyacımız olmadığını gösterdi. İnsanların birbirlerini rahatlatmaya, kendi geleceklerini yeniden inşa etmeye, yaratmaya ve şekillendirmeye katılma iradesini gösterdiler.”
</quote>

Bu alıntı [[https://revoltsnow.wordpress.com/2011/04/10/166/][Revolts Now]] adlı bir blogdan. Libcom okuyucuları bu tür ilhamları genellikle grevlerde veya ayaklanmalarda, işçi sınıfının direksiyonu ele geçirdiği veya frene bastığı anlarda (istediğiniz deyimi seçin) görürler. Revolts Now [[https://en.wikipedia.org/wiki/2010%E2%80%932011_Queensland_floods][Queensland’deki sel]] felaketinin ardından yaşananlardan bahsediyordu. Şöyle yazdılar:

<quote>
“…felaketten etkilenen toplulukların devleti beklemeyen ya da sermayenin inisiyatif almasına izin vermeyen, bunun yerine ‘elleriyle müzakere eden’, kendi topluluklarını yeniden inşa eden ve ‘kendilerini iyileştirerek’ daha güçlü topluluklar ortaya çıkaran çabaları. Ben bu çabaları felaket komünizmi olarak adlandırıyorum.”
</quote>

Biz, felaket komünizminin iklim değişikliğini değerlendirmek için faydalı bir kavram olduğunu düşünüyoruz. Pek yaygın olmasa da, bu terimin en az iki farklı anlamını tanımlayabiliriz. İlk anlam, felaket durumlarına kolektif, kendi kendine örgütlenmiş yanıtlardır. İkincisi ise iklim kaosu karşısında insan ihtiyaçlarına dayalı ekolojik bir toplum beklentisi ya da başka bir deyişle [[https://en.wikipedia.org/wiki/Anthropocene][Antroposen]][1]‘de komünizmin olasılığı ile ilgilidir. Bu ilk anlamı ‘felaket toplulukları’, ikincisini ise ‘felaket komünizasyonu’ olarak adlandırabilir ve bunların her ikisini de daha geniş bir felaket komünizmi sorunsalının anları olarak görebiliriz.

*** Felaket Toplulukları

Rebecca Solnit, felaket toplulukları fikrini <em>Cehennemde İnşa Edilmiş Bir Cennet</em> adlı kitabında popülerleştirmiştir. Solnit, felaketlerde devletin amacının genellikle hayatta kalanlara yardım etmekten ziyade ‘düzeni’ yeniden tesis etmek olduğuna dikkat çekiyor. 1906 San Francisco depreminde ordu, deprem bölgesine gönderilmişti, 50 ila 500 kişi hayatını kaybetmişti ve kendi kendine organize olan arama, kurtarma ve yangın söndürme çalışmaları sekteye uğramıştı.[2]

<quote>
“Yangınlar ve gürültülü patlamalar üç gün boyunca devam etti. Sanki bir savaş gibiydi. İşleri bittiğinde şehrin yarısı kül ve moloz yığınına dönmüş, yirmi sekiz binden fazla bina yıkılmış ve dört yüz binlik nüfusun yarısından fazlası evsiz kalmıştı. Nob Hill’ın tepesindeki konaklar yandı; Market Street’in güneyindeki gecekondu bölgesi neredeyse yeryüzünden silinmişti. Bu felaket, çoğu zaman olduğu gibi, karışık tepkilere yol açtı: Vatandaşların çoğu arasında cömertlik ve dayanışma vardı, aynı zamanda bu boyun eğdirilmemiş vatandaşlar güruhudan korkan ve onları kontrol etmeye çalışanlar da günyüzündeydi, [Tuğgeneral] Funston’ın sözleriyle, bu güruh ‘cezalandırılmamış bir çeteydi’.(s.35)”
</quote>

Solnit’e göre, mevcut toplumsal düzeni sürdürmek için sürekli çaba sarf etmek gerekmekteydi. Bunu bir elektrik lambasına ve felaketleri de elektrik kesintisine benzetmekteydi. Gerçek ya da mecazi anlamda elektrikler kesildiğinde, kendiliğinden <em>“doğaçlama, işbirlikçi, kooperatif ve yerel topluma dönüş (s.10)”</em> yaşanır. Devletin baskıcı eylemleri,1906 San Francisco’sunda olduğu kadar 2005 Katrina’sında da, devlet iktidarını ve kapitalist normalliği yeniden dayatmakla ilgilidir.

Devlet, yerelleşmiş öz-örgütlenmeyi, işbirliğini ve karşılıklı yardımı ezilmesi gereken bir tehdit olarak görmektedir. İşte bu yüzden devlet kendi vatandaşlarına tatlı sudan ziyade sıcak kurşun sağlamakta daha hızlı davranıyor: düzen hüküm sürmek zorundadır. Solnit, çok sayıda felaketi inceleyen ve bencillik, anti-sosyal bireycilik ve saldırganlık klişelerinin tamamen kanıtsız olduğunu ortaya koyan [[https://udspace.udel.edu/items/0e4bf49b-f7a0-4feb-916d-8ada6367431b][Charles Fritz’in çığır açıcı çalışmasından]] yararlanıyor.[3] Gerçekten de bunun tam tersi doğrudur:

“Felaketzedeler nadiren histerik davranışlar sergiler, bir tür şok geçirme ya da sersemleme davranışı daha yaygın bir ilk tepkidir. En kötü felaket koşullarında bile insanlar kendi kontrollerini korur ya da hızla yeniden kazanır ve başkalarının iyiliği için endişelenmeye başlarlar. İlk arama, kurtarma ve yardım faaliyetlerinin çoğu, organize dış yardım gelmeden önce felaketzedeler tarafından üstlenilir. Felaketlerde yağmalama raporları oldukça abartılıdır, hırsızlık ve soygun oranları aslında felaketlerde düşer ve çalınandan çok daha fazlası başkalarına verilir. Başkalarına karşı saldırganlık ve belli kişileri günah keçisi ilan etme gibi diğer anti-sosyal davranış biçimleri nadirdir ya da hiç yoktur. Bunun yerine çoğu felaket, felakete uğrayan halk arasında sosyal dayanışmada büyük bir artışa neden olur ve bu yeni yaratılan dayanışma, çoğu kişisel ve sosyal patoloji türünün görülme sıklığını azaltma eğilimindedir. (Fritz, s.10)”

Fritz ayrıca, felaketler ve ‘normallik’ arasındaki ayrımın “günlük yaşamın doğasında bulunan birçok stres, gerginlik, çatışma ve memnuniyetsizlik kaynağını rahatlıkla göz ardı edebileceğini” belirtiyor.[4] Aradaki fark, felaket durumlarının kurumsal düzeni askıya alarak değişime uygun yapılandırılmamış bir durum yaratmasıdır. Böylece felakette hissedilen sefaletin yanı sıra günlük yaşamın stres ve sıkıntıları da kolektif olarak ele alınabilir. Bu sosyal yaşamı insan ihtiyaçları etrafında yeniden yapılandırmak için hem psikolojik destek hem de kolektif güç sağlar.[5]

*** Sosyal dönüşüm için bir fırsat?

<quote>
“İnsanlar, eski sistem altında gizli kalmış ve gerçekleştirilememiş bazı isteklerini gerçekleştirme fırsatını görmektedir. Kendileri için yaratabilecekleri yeni roller görüyorlar. Eski eşitsizlikleri ve adaletsizlikleri ortadan kaldırma olasılığını görüyorlar. Kültürde bu değişiklikleri gerçekleştirme fırsatı, normalde diğer kriz türlerinde bulunmayan felaketlere olumlu bir boyut kazandırır. (Fritz, p.57)”
</quote>

Daha da önemlisi, felaket toplulukları kasıtlı topluluklar, terk edilmiş komünler veya aktivistlerin geçici özerk bölgeleri değildir. Bunlar, olumsuz koşullar altında kendi kendini örgütleyen, piyasa dışı, devletçi olmayan sosyal yeniden üretimlerdir, kapitalizmden gönüllü bir biçimde ayrılma girişimi değildir. Ancak yine de bu tür projelerin bazı eksikliklerinden muzdariptirler. Her şeyden önce, deneyim katılımcıları ömür boyu değiştirse bile, genellikle kısa ömürlüdürler. Fritz, pratikte bu tür toplulukların bir tür temel toplumsal işleyiş ve istikrar yeniden sağlanana kadar devam ettiğini, bu sürenin barış zamanındaki felaketlerde genellikle haftalar ya da aylar, savaş zamanında ya da kronik veya seri felaketlerde ise birkaç yıl olduğunu belirtmektedir.

Bu, akıllı bir devletin neden baskıdan daha fazla seçeneğe sahip olduğunu ve dolayısıyla ABD İç Güvenlik Bakanlığı’nın neden kendi kendine örgütlenen ve anarşistlerden etkilenen Occupy Sandy yardım çabalarını [[https://truthout.org/articles/dhs-study-praises-occupy-sandy-with-murky-intentions/][övebileceğini]] açıklamaya yardımcı olur. Kendi kendine örgütlenen felaket toplulukları, felaketlere müdahale etme konusunda devlet kurumlarından ve piyasa güçlerinden daha etkili olduğundan, devlet arkasına yaslanıp insanların acı çekmesine izin verebilir ve topluluk dağılıp normale döndüğünde kendini yeniden gösterebilir. Devletin ‘dayanıklılığa’ olan ilgisi, proleterleri felakete maruz bırakmak, onları kendi çabalarıyla hayatta kalmaya terk etmek ve felaket anı geçtikten sonra yeniden inşa ve soylulaştırmanın ‘felaket kapitalizmi’ ile harekete geçmektir.[6]

O halde, felaket toplulukları tek başlarına kapitalist toplumsal düzen için devrimci bir tehdit oluşturmazlar – ve hatta kapitalist normalliği yeniden tesis etmek için düşük maliyetli bir araç olarak iyileştirilebilirler. Eğer komünist olarak adlandırılabilirlerse, bu David Graeber tarafından herhangi bir sosyal düzeni (kapitalizm dahil) mümkün kılan temel sosyalliği ve özgür işbirliğini tanımlamak için kullanılan bir terim olan ‘temel komünizm’ anlamında olacaktır. Bu makalenin ikinci bölümünde, felaket komünizminin daha geniş bir devrimci, anti-kapitalist dinamikle ilişkili olarak ne anlama geldiğine bakacağız.

<right>
8 Mayıs 2014
</right>

** Bölüm 2: Komünizasyon ve Somut Ütopya

*** Felaket Komünizasyonu

Bağlı olduğumuz liberteryen komünist çevrelerde son zamanlarda anti kapitalist bir devrimin neye benzeyeceğine dair tartışmaların çoğu ‘komünizasyon teorisi’[7] başlığı altında gerçekleşti. Bildiğimiz kadarıyla bu tartışmaların çok az bir kısmı doğrudan iklim değişikliğiyle ilgiliydi. [[https://libcom.org/library/what-are-we-do-endnotes][Endnotes tarafından verilen şu tanım]], felaket komünizmi hakkında düşünmek için yararlı bir çıkış noktası olarak hizmet etmektedir:

<quote>
“Komünizasyon, bütünlük düzeyinde, bütünlüğün ortadan kaldırıldığı bir harekettir. (…) Bireysel bir eylemin ‘komünizeleştirici’ olarak belirlenmesi, eylemin kendisinden değil, yalnızca parçası olduğu genel hareketten kaynaklanır ve bu nedenle devrimi, sanki gereken tek şey bu tür eylemlerin kritik bir noktaya ulaşıncaya kadar belirli bir birikimiymiş gibi komünizeleştirici eylemlerin toplamı açısından düşünmek yanlış olacaktır. Devrimin böyle bir birikim olarak kavranması, niteliksel bir dönüşümün tetikleyeceği beklenen niceliksel bir genişlemeye dayanır. (…) Bu tip lineer devrim anlayışlarının aksine komünizasyon, sınıf mücadelesinin kendi dinamiği içindeki niteliksel bir değişimin ürünüdür.
</quote>

Bu pasaj muhtemelen isimsiz muhaliflerini karikatürize ediyor ancak burada bahsedilen mantık felaket komünizmi hakkında önemli bir kanıya varmamızı sağlamaktadır, hiçbir felaket topluluğunun devrime yol açmayacağına. Devrim ancak felaket topluluklarının kendi kendini örgütleyen sosyal yeniden üretim; mevcut mülkiyet ilişkileri, devlet vb. ile çatışmaya girdiğinde ve bu sınırların üstesinden geldiğinde gerçekleşecektir. Bunu da farklı felaket topluluklarının, sınıf mücadelelerinin ve sosyal hareketlerin genişlemesi olmadan ve birbirlerine bağlanmadan hayal etmek zordur.

Felaket toplulukları tipik olarak kısa ömürlüdür ve kapitalist normalliğe geri dönme eğilimindedir. Bu topluluklar kendilerini hakim sosyal düzene karşıt olarak kurgulamadıkça ve diğer mücadelelerle bağlantı kurmadıkça, izole kalacak ve de (ya baskı, iyileşme yoluyla ya da sadece oluşum koşullarını geride bırakarak) dağılacaklardır. Hem yoğun yön (bir mücadele içindeki sınırların aşılması) hem de kapsamlı yön (yayılma ve bağlantı kurma) önemlidir çünkü hiçbir yerel mücadele genişleme olmadan kendi iç sınırlarını aşamaz. İyileştirici bir ufuktan dönüştürücü bir ufka niteliksel bir geçiş olmaksızın hiçbir yaygın hareket devrimci hale gelmeyecektir.

Bu düşünce tarzı aynı zamanda her türlü katastrofist ‘ne kadar kötü o kadar iyi’ yaklaşımını da dışlamaktadır. Kemer sıkma politikaları kaçınılmaz olarak devrime yol açmayacağı gibi, felaketlerin de sosyal dönüşüme yol açacağını düşünmek için hiçbir neden yoktur. Lakin iklim değişikliği devrim için parametreleri değiştirmektedir. Artan gıda ve enerji maliyetleri, kitlesel yerinden edilme ve su kıtlığı gibi şeyler, proleterlerin mevcut sosyal ilişkiler içinde kendilerini yeniden üretme kapasitelerini giderek daha fazla zorlayacaktır. Örneğin, açlık mutlak kıtlığı değil gelir dağılımını yansıtır ve bu olay tarımsal üretkenlikte iklim kaynaklı önemli düşüşler olsa bile doğru olmaya devam edecektir, dolayısıyla sosyal mülkiyet ilişkileri biyofiziksel yeniden üretimle giderek daha fazla çatışacaktır.

Endnotes’un da belirttiği gibi, bir faaliyet ancak bütünsel düzeyde gerçekleşiyorsa, yani komünist sosyal ilişkiler yaratma biçiminde kapitalizme yönelik sınıfsal ve sosyal sistem çapında bir saldırının parçası ise komünizasyondur. Eğer bunun bir parçası değilse, o zaman faaliyet kapitalist sosyal ilişkiler bütününün ve bu bütünün yeniden üretiminin bir parçasıdır (izole felaket topluluklarında gördüğümüz gibi). Kapitalist sınıf ve onun hükümetleri de bir dereceye kadar bunun farkındadır. Felaketlere verdikleri tepkiler yalnızca kısa vadeli durumla ilgili değil, aynı zamanda uzun vadeyle de ilgilidir.

Harry Cleaver, [[https://en.wikipedia.org/wiki/1985_Mexico_City_earthquake][Mexico City depreminin]] ardından [[https://heimatloskolektif.wordpress.com/2023/02/11/depremin-islevleri-harry-cleaver/][yazdığı makalede]], toprak sahipleri ve emlak spekülatörlerinin depremi, uzun zamandır kurtulmak istedikleri insanları tahliye etmek, depremde yıkılan evlerini yıkmak ve pahalı yüksek apartman daireleri inşa etmek için bir fırsat olarak gördüklerini yazıyor. Meksikalı işçi sınıfı buna başarıyla karşı koydu:

<quote>
“…Binlerce kiracı örgütlendi ve hasarlı mülklerin hükümet tarafından kamulaştırılması ve nihai olarak mevcut kiracılarına satılması talebiyle başkanlık sarayına yürüdü. Hükümet hala felç olmuş durumdayken inisiyatif alarak, yaklaşık 7,000 mülke el konulmasını başarıyla sağladılar.”
</quote>

Cleaver bunu mümkün kılan iki koşulu şöyle tanımlamaktadır: Depremden önceki mücadele tarihi ve “depremin hükümetin hem idari kapasitelerinde hem de otoritesinde bir çöküşe yol açması”. Bunlardan ilki, devlet iktidarına meydan okuyabilecek ya da kendi çıkarları doğrultusunda doğrudan eyleme geçebilecek felaket topluluklarının ortaya çıkış koşullarını anlamamıza yardımcı olması açısından önemlidir. İkincisi ise felaketlerin, toplumu kapitalist tutmaya çalışan devlet ve sermaye güçlerini nasıl sınırlayabileceğini anlamamıza yardımcı olması açısından önemlidir.

*** Felaket Komünizminin iki anı

Felaket topluluklarının görünürdeki evrenselliği, ister felaket ister sosyal antagonizma nedeniyle olsun, kapitalist normalliğin bozulduğu her yerde kendi kendini örgütleyen sosyal yeniden üretimin ortaya çıkacağına inanmak için güçlü gerekçeler sunuyor. Endnotes’un iddiasına karşı bu tamamen olumsuz hükümlerle sınırlı olmadığımız anlamına geliyor:

<em>Endnotes</em>

<quote>
“[Komünizasyon teorisinin] verebileceği tavsiyeler öncelikle olumsuzdur: Kapitalist sınıf ilişkisinin yeniden üretimine dahil olan sosyal biçimler, ortadan kaldırılacak olanın bir parçası oldukları için devrimin araçları olmayacaklardır.”
</quote>

Aynı fikirde değiliz. Felaket topluluklarının, kapitalist olmayan sosyal yeniden üretimin anormal koşullar altında neye benzeyebileceğine dair bir fikir verdiğini düşünüyoruz. Devrimci bir hareket tanımı gereği anormal olduğu için felaket topluluklarını yok saymak kadar onların kendi başlarına yeterli olduğunu iddia etmek de hata olur. Bu felaketlerin basit bir niceliksel birikiminin komünizme yol açtığı anlamına gelmez, sadece felaket topluluklarında kapitalist olmayan sosyal ilişkilere dair işaretler olduğu anlamına gelir. Nitekim, bir noktada değer ve sermaye birikiminden en azından kısmen farklı bir mantıkla hareket etmemiş olsalardı, felaket topluluklarının kapitalist normalliğe geri dönmesini açıklamak imkansız olurdu. Bunun devlet ya da piyasa aracılığı olmaksızın insan ihtiyaçları için kendi kendini örgütleyen üretim ve dağıtımın komünist bir mantığı olduğunu savunuyoruz.

Dahası, kapitalist sosyal biçimlerin (ücretler, değer, metalar…) kapitalist olmayan sosyal yeniden üretimin temelini oluşturamayacağı doğru olsa da, sosyal biçimler mevcut dünyanın içeriğini tüketmez. Örneğin, David Harvey [[http://s-usih.org/2011/09/david-harveys-mental-conceptions.html][yedi ‘faaliyet alanı’]] tanımlar:

<quote>
 1. Teknolojiler ve organizasyon biçimleri

 1. Sosyal ilişkiler

 1. Kurumsal ve idari düzenlemeler

 1. Üretim ve emek süreçleri

 1. Doğayla ilişkiler

 1. Günlük yaşamın ve türlerin yeniden üretimi

 1. Dünyanın zihinsel kavramları
</quote>

Endnotes’un yaptığı hata, kapitalizmin bütünselleştirici eğilimlerini zaten bütünselleşmiş bir kapitalizm olarak ele almaktır (örneğin: “Olduğumuz şey en derin düzeyde bu [sınıf] ilişkisi tarafından oluşturulur”).[8] Gördüğümüz herhangi bir devrimin, bu yedi unsurun bazılarını ortadan kaldırması ve/veya tamamen yeni sosyal biçimlerle değiştirmesi, diğerlerini yeniden düzenlemesi ve yeniden yapılandırması, ayrıca yeni fikirleri, biçimleri, teknolojileri ortaya çıkarması vb. şekillerle her birini dönüştürmesini umarız.

*** Somut Ütopya

[[https://libcom.org/article/murray-bookchins-libertarian-technics][Murray Bookchin’in]] “enkazdan kurtarabildiğimiz her ganimetle kaçmalıyız (…) harabelerin kendileri madendir” sözünü ciddiye alırsak, o zaman [[https://en.wikipedia.org/wiki/Apophatic_theology][apofatik komünizmle]] sınırlı kalmayız. Elbette ‘ne yapılacağını’ önceden tam olarak belirleyemeyiz, belirlemek de istemeyiz. Hareket geliştikçe bunun katılımcılar tarafından çözülmesi gerekiyor. Ancak bu, kapitalist sosyal ilişkiler altında gerçekleştirilemeyen bazı kısıtlamaları, olasılıkları ve örtük potansiyelleri tanımlayamayacağımız anlamına gelmez.

Dağıtılmış yenilenebilir enerji üretiminin, liberteryen komünist bir toplumla merkezi fosil yakıt enerji üretiminden daha uyumlu olduğunu söylemekle çok da ileri gitmiş olmayız. Bu ‘doğası gereği’ komünist olduğu ya da komünizmin habercisi olduğu anlamına gelmiyor – şehirlerimizin çatılarında beliren güneş panelleri bunun aksini göstermektedir. Benzer şekilde, tarım söz konusu olduğunda, mümkün olanı kısıtlayan biyofiziksel parametreler vardır (karbon, nitrojen ve su döngüleri gibi). Tarımın komünizasyonunun neye benzeyeceğini kesin olarak söyleyemeyiz ancak en azından bazı kısıtlamaları ve olasılıkları belirleyebilir ve hatta bunların nasıl sonuçlanabileceğine dair spekülasyon yapabiliriz.

Felaket toplulukları; hem mevcut teknolojilerin, bilgi birikiminin ve altyapının insan ihtiyaçlarını karşılamak için nasıl hızla yeniden tasarlanabileceğini hem de ortaya çıkan bu yeniliklerin nasıl çözülebileceğini ve kapitalist normalliğe nasıl yeniden dahil edilebileceğini göstermesi açısından bilgilendiricidir.[9] Daha da ileri gidebilir ve somut bir ütopyacılığın yeniden keşfedilmesi gerektiği konusunda ısrar edebiliriz. Giderek artan bir şekilde soyut ütopyalara dayanan sermayenin kendisidir – örneğin, var olmayan karbon yakalama teknolojisi için geniş boş salonları olan yeni ‘temiz’ kömür santralleri inşa etmek. Buna karşılık olarak somut bir ütopyacılık, mevcut sosyal ilişkiler tarafından engellenen ve halihazırda var olan olasılıklara bakar.[10]

İşgücü tasarrufu sağlayan teknoloji her yerde var ancak hızlanma ve işsizlik olarak deneyimleniyor. Endüstriyel ekoloji, değerlerin yönettiği bir dünyada büyük ölçüde kurumsal bir sosyal sorumluluk hilesiyle sınırlıdır. İşbirliğine dayalı, kendi kendini örgütleyen ve kooperatif üretim biçimlerine öncülük ediliyor ancak bunlar genellikle kendi kendini yöneten, güvencesiz sömürü olarak deneyimleniyor. Uygulanabilir, sürdürülebilir ve düşük verimli tarım uygulamaları mevcuttur ancak enerjiye aç dünya pazarında marjinalleştirilmiştir. [[https://www.biophiliccities.org/][Biyofilik kentler]] ve [[https://en.wikipedia.org/wiki/Regenerative_design][rejeneratif tasarım]], büyük ölçüde izole edilmiş gösteri projeleri veya varlıklı kesimler için soylulaştırıcı kentsel alanlarla sınırlandırılmıştır ve potansiyelleri sınıf ilişkileri tarafından kısıtlanmıştır.

Endnotes ile birlikte, “bu potansiyellerin ‘komünizeleştirici’ olarak belirlenmesi, şeylerin kendilerinden değil, yalnızca parçası oldukları genel hareketten kaynaklanır” diyebiliriz.[11] Endnotes’a karşı olarak bunun en azından felaket komünizmine olumlu bir içerik kazandırdığı konusunda ısrar edebiliriz, sadece yeni başlamış olan belirsiz ama somut ütopik potansiyellerin geniş bir taslağı olsa bile.

Üçüncü bölümde çağdaş lojistik örneği üzerinden felaket topluluklarının mikro düzeyini felaket komünizasyonunun makro düzeyine bağlamaya çalışacağız.

Out of the Woods

<right>
14 Mayıs 2014
</right>

** Bölüm 3: Lojistik, Yeniden Tasarım, Brikolaj

Kapitalist lojistik konusundaki son tartışmalara bir bakış, felaket komünizmi hakkındaki üç bölümlük tartışmamızı tamamlıyor ve bizi insan ihtiyaçlarını karşılamak için altyapının yeniden kullanımına bakarken başladığımız noktaya geri getiriyor.

*** Lojistik Tartışması

İkinci bölümde tartışılan komünizme yönelik tamamen olumsuz yaklaşım, diğerlerinin yanı sıra Alberto Toscano tarafından da eleştirilmiştir.[12] Bu eleştiri, kapitalist lojistiğin – gemicilik, limanlar, depolar, tam zamanında üretim, stok kontrol algoritmalarından oluşan küresel ağ – politikasına ilişkin bir tartışma biçimini almıştır. Toscano, çağdaş lojistiğin açıkça kapitalist bir yaratım olduğunu savunmakla birlikte, sabotaj ve abluka gibi tamamen olumsuz bir yaklaşımın, en azından post-kapitalist bir topluma geçiş dönemi için onu ele geçirme potansiyelini, hatta gerekliliğini göz ardı ettiği konusunda ısrar ediyor. Lojistik, genel olarak mevcut üretim ve dolaşım altyapısı için bir vaka çalışması olarak dururken, tartışmanın gerçek özü budur.

<quote>
“Toscano
Materyalizm ve strateji, bizi kolektif eylem kapasitelerimizden ayıran yapıların ve akışların sadece durdurulmak yerine nasıl farklı amaçlara dönüştürülebileceğine dair acil eleştirel ve gerçekçi soruyu reddediyor gibi görünen etik olanın program karşıtı bir iddiasıyla bertaraf edilmektedir.”
</quote>

Bu, Endnotes tarafından öne sürülen tamamen olumsuz tavsiyelere yönelik eleştirilerimizi yansıtıyor gibi görünmektedir. Bununla birlikte, ortaya koymaya değer bazı önemli farklılıklar vardır. Toscano, David Harvey’i onaylayarak ondan şu şekilde alıntı yapmaktadır:

<quote>
“Bu nedenle, oluşturulmuş ortamların doğru yönetimi (ve buna uzun vadeli sosyalist veya ekolojik dönüşümleri tamamen farklı bir şeye dönüştürmeyi de dahil ediyorum), hem ekolojistlerin hem de sosyalistlerin hoşuna gitmeyecek geçiş dönemi siyasi kurumlarını, güç ilişkileri hiyerarşilerini ve yönetişim sistemlerini gerektirebilir.”
</quote>

Harvey’in buradaki yanılgısı, devrimci bir hareketin boş bir sayfa değil, eski dünyayı miras aldığı şeklindeki (doğru) önermeden, ‘doğru yönetimin’ burnumuzu tutmak ve belirsiz bir geçiş dönemi boyunca eski dünyaya çok benzeyen hiyerarşilere ve yönetime katlanmak anlamına geldiği şeklindeki yersiz sonuca varmaktır. Eğer bu size tanıdık geliyorsa, nedeni bunun en azından İkinci Enternasyonal’den (1889-1916) beri sol yöneticilerin temel kinayesi olmasıdır. İşçiler! Büyüklerinizin sözünü dinleyin! Emirler sizin iyiliğiniz içindir!

Bu mecazın özünde, işçilerin öz-örgütlenmesine karşı duyulan derin güvensizlik ve karmaşıklığın çözümünün hiyerarşik komuta olduğuna dair refleksif bir inanç yatmaktadır. David Harvey bu argümanı nükleer enerji ve hava trafik kontrolü ile ilgili olarak açıkça ortaya koymuştur. Harvey’in argümanları büyük ölçüde strawmanlere dayanmaktadır (“ya siz uçaktayken hava trafik kontrolörleri sonsuz bir fikir birliği toplantısı yapsaydı!!!”) ve bu argümanı [[https://libcom.org/article/i-wouldnt-want-my-anarchist-friends-be-charge-nuclear-power-station-david-harvey-anarchism][burada]] ikna edici bir şekilde çürütülmektedir.

Öte yandan, Endnotes’tan Jasper Bernes tarafından Toscano’ya verilen bir yanıt, öz yönetime çok farklı bir itiraz sunmaktadır.[13] Sorun, işçilerin teknokratlara kıyasla beceriksiz olmaları değil, işçilerin fazlasıyla becerikli olmasıdır. Bu, yapısal olarak kendi ihtiyaçlarına düşman bir altyapıyı kendi kendine yönetmek anlamına gelecektir:

<quote>
“İşçilerin lojistiğin sunduğu üstünlükleri ele geçirmeleri – başka bir deyişle, küresel fabrikanın kontrol panelini ele geçirmeleri – kendilerine ve ihtiyaçlarına yapısal olarak düşman olan bir sistemi yönetmeleri, aşırı ücret farklılıklarının bizzat altyapının içine yerleştirildiği bir sistemi denetlemeleri anlamına gelecektir.”
</quote>

Endnotes’un makalesi, lojistik altyapısını ele geçirmenin arzu edilen (ya da söz konusu işçiler tarafından istenen) bir şey olmadığı -amacının merkez ve periferi bölgeler arasındaki ücret farklılıklarını istismar etmek olduğu- ve muhtemelen mümkün bile olmadığı konusunda ikna edici şu argümanı sunuyor: Lojistik ağlar tam da grev, işgal ya da doğal afet gibi aksaklıkları atlatmak üzere tasarlandığından, herhangi bir düğümü ele geçirmek o düğümün lojistik ağdan kopmasına neden olacaktır.[14] Bir tam zamanında (JIT){1} depoyu ele geçirirseniz, boş bir depoyu ele geçirmiş olursunuz. Sandro Mezzadra ve Brett Nielson’ın bir makalesinde belirttiği gibi, “Sermaye; finans, lojistik ve ekstrasyon sistemlerine esneklik ve ‘hata toleransı’ inşa ederek bu rahatsızlıkların etrafından dolaşmaya çalışır”.[15]

Buradaki anlaşmazlık, ‘lojistiğin’ yekpare bir bütün (felsefi terimlerle, bir ‘bütünlük [totality]’) olarak ele alınmasına odaklanıyor gibi görünüyor. Soru daha sonra “bunu ele geçirebilir miyiz ve geçirmeli miyiz?” şeklinde ortaya atılmaktadır. Bu çıkmaza yönelik bir çözümün ipucu ancak Endnotes makalesinin son paragrafında verilmektedir:

<quote>
“Bu, yakın çevremizde karşılaştığımız şeylerin envanterini çıkaran, küresel bütünlük açısından ustalığı hayal etmeyen, daha ziyade belirli, zor durumdaki konumlardan savaşmak zorunda kalacaklarını ve savaşlarını bir kerede değil de art arda kazanmak zorunda kalacaklarını bilen partizan fraksiyonlar açısından bir <strong>brikolaj süreci</strong> olacaktır. Bunların hiçbiri mücadelelerin yürütülmesi için bir plan, bir geçiş programı oluşturmak anlamına gelmez. Daha ziyade, geçmiş mücadelelerin deneyiminin zaten talep ettiği ve gelecek mücadelelerin muhtemelen yararlı bulacağı bilgiyi üretmek anlamına gelir.”
</quote>

*** Brikolaj olarak yeniden kullanım

Brikolaj kavramı, felaket topluluklarının yerelleştirilmiş karşılıklı yardımlaşmasını küresel felaket komünizasyonu sorunsalıyla birleştiriyor gibi göründüğünden bu yeniden kullanım kavramının üzerinde durmak istiyoruz. Bu terim 1962 yılında antropolog Claude Lévi-Strauss tarafından sosyal teoriye kazandırılmış ve diğerlerinin yanı sıra Gilles Deleuze ve Felix Guattari tarafından geliştirilmiştir:

<quote>
“Brikolaj (…) oldukça kapsamlı ve aynı zamanda sınırlı bir malzeme stokuna ya da temel kurallara sahip olma; parçaları sürekli olarak yeni ve farklı kalıplar ya da konfigürasyonlarda yeniden düzenleme becerisi.”
</quote>

Deleuze ve Guattari, psikanalitik şapkalarıyla, burada şizofrenik bilişi detaylandırmakla ilgileniyorlar: görünüşte ilgisiz kelimelerin, kavramların, nesnelerin durmaksızın birbirine bağlanması ve yeniden bağlanması. Alıntılanan pasajın çevirmen notu daha kullanışlı ve açık bir tanım sunuyor: “brikolaj: (…) Elde olanla yetinme sanatı”. Felaket komünizminin mantığı da tam olarak budur.

Bu nedenle Toscano, “artık değerin egemen olmadığı bir dünyada yer kabuğunu dolduran ölü emeklerden nasıl bir fayda sağlanabileceği sorusunun, sermayenin lojistik ağını bozmaktan çok daha radikal bir soru olduğu” konusunda ısrar etmekte haklıdır. Ancak sonuç olarak hiyerarşik ‘uygun yönetimi’ gerekli bir ‘geçiş’ önlemi olarak onaylamakla yanılıyor. Bölüm 1’deki felaket toplulukları örnekleri bu noktayı yeterince açıklamaktadır: ‘uygun (hiyerarşik) yönetim’ öz-örgütlenmenin etkinliği karşısında sönük kalmaktadır.

Bu etkinlik eldeki her şeyin pragmatik ve doğaçlama bir şekilde yeniden kullanımına dayanır; yani brikolaja. Bu da lojistiğin -ve buna bağlı bir şekilde genel olarak mevcut altyapının- organik bir yapı (bir bütünlük) olarak ele alınmasının gerekmediğini varsaymaktadır.

“Günümüzde organik bütünlüklere karşı temel teorik alternatif olarak filozof Gilles Deleuze’ün <em>asamblajlar</em>{2} olarak adlandırdığı, dışsallık ilişkileriyle karakterize edilen bütünler dediği şeydir. Bu ilişkiler, her şeyden önce, bir asamblajın bileşen parçasının ondan ayrılabileceğini ve etkileşimlerinin farklı olduğu farklı bir asamblajın içine yerleştirilebileceğini ima eder.[16]”

Bu yalın bir ifadeyle ne anlama geliyor? Basitçe, lojistik bir bütün olarak (Bernes/Endnotes’un iddia ettiği gibi) iflah olmaz bir şekilde kapitalist olsa da çeşitli ölçeklerdeki sayısız bileşenlerden oluşur: gemiler, kamyonlar ve trenler; limanlar, yollar ve demiryolları; bilgisayarlar, algoritmalar ve fiber optik kablolar; atomlar, moleküller ve alaşımlar; ve unutmamak gerekir ki insanlar. Bu parçaların mevcut organizasyonunun sermayenin valorizasyonu için optimize edilmiş olması, başka optimizasyonlara sahip başka konfigürasyonların olamayacağı anlamına gelmez. Aslında, olası konfigürasyonlar pratikte sonsuzdur. Bu yeniden yapılandırma potansiyeli kabul edildiği sürece, bu bütünlerin ‘totalite’ ya da ‘asamblaj’ olarak kabul edilmesinin çok fazla önemi yoktur. Yeni bir konfigürasyonun lojistiğe benzemesi için hiçbir neden yoktur.

Açık bir şekilde, depolar, kamyonlar ve trenler başka amaçlarla kullanılabilir, gemiler de aynı şekilde – ve sadece bariz olanlar ile sınırlı da değil. Mevcut dünya ticaret hacmi, küresel ücret farklılıklarından faydalanılmadan muhtemelen bir anlam ifade etmeyecektir. Ancak gemiler, insanları taşımaktan, mercan kayalıklarının oluşumunu başlatmak için batırılmak, soyulmak veya eritilip başka ürünlere dönüştürülmeye kadar başka amaçlara da hizmet edebilir.[17] İletişim altyapısının çok amaçlı olduğu aşikârdır ve stok kontrol algoritmaları bile hacklenip yeniden tasarlanarak kamuya açık hale getirildiğinde potansiyel kullanım alanlarına sahip olabilir.

Kamyonların açlara yiyecek dağıtmak için mi, elektrik motorlarıyla güçlendirilmek için mi, parçalara ayrılmak için mi ve/veya barikat işlevi mi göreceğini önceden belirlemek kesinlikle mümkün değildir. Felaket toplulukları bize, yerel, acil üretimin en olumsuz koşullar altında bile insan ihtiyaçlarını verimli bir şekilde karşılayabileceğine inanmamız için bolca sebep veriyor. Ancak şeylerin doğasının potansiyel olarak yeniden yapılandırılabilir olduğunu vurgulamak -ve onları yeniden yapılandırmak için öz örgütlenmenin yeterliliğini vurgulamak- aynı zamanda felaket komünizasyonunun daha geniş sorunsalını da aydınlatır. Bu şekilde soru, bir bütün olarak ele alındığında “ele geçirmek mi yoksa terk etmek mi?” değil, ne şekilde parçalara ayrılacağı ve bileşenlerinin yeni amaçlar için ne şekilde yeniden tasarlanacağıdır: Sermayenin sonsuz valorizasyonu değil, insan ihtiyaçlarının ekolojik olarak karşılanması.

<right>
22 Mayıs 2014
</right>

[1] [[https://jasonwmoore.wordpress.com/2013/05/13/anthropocene-or-capitalocene/][Jason Moore]], “sera gazı emisyonları ve iklim değişikliğinin yarattığı önemli -ve giderek büyüyen- sorunu anlatmak için bir metafor olarak Antroposen’nin memnuniyetle karşılanması gerektiğini”, ancak sorunu belirli sosyal örgütlenme biçimleri -yani sermaye- yerine ‘Antroposen’e -yani insanlığa- bağlayarak sorunu doğallaştırdığını ve neo-Malthusçu varsayımları gizlediğini savunuyor.

[2] Bu bize Chicago Belediye Başkanı Richard Daley’in polis baskısını savunurken yaptığı ünlü Freudyen sürçmeyi hatırlatıyor: “Polis düzensizlik yaratmak için burada değildir. Polis düzensizliği korumak için buradadır.”

[3] İnsanların melek olduğunu iddia etmiyoruz, sadece kanıtlar sürekli olarak işbirliğine dayalı, toplum yanlısı davranışın baskın tepki olduğunu gösteriyor. Ancak bu dayanışmaya kimlik aracılık ediyor ve bu da ırkın kimin yaşayıp kimin öleceği konusunda önemli bir faktör olduğu anlamına geliyor. Medya, devlet düzeninin bozulduğu her yerde Hobbesçu bir anomi anlatısına uymak için istisnai vakalara odaklanmayı seviyor (örneğin [[https://www.dailymail.co.uk/news/article-2226332/Superstorm-Sandy-Mother-boys-swept-arms-left-screaming-street-12-hours.html][bu Daily Mail haberine bakın]]), ancak bunun gibi vakalar belki de ırksal ötekileştirmenin etkisi olarak daha iyi anlaşılabilir – siyah bir kişi yardım istemek için kapıyı çaldığında, beyaz insanlar mutlaka cevap vermezler ve hatta belki de emin olmak için [[https://edition.cnn.com/2013/11/07/us/michigan-woman-shot/][onu vurarak öldürürler]].

[4] Örneğin, çağdaş ‘uyarılma toplumunda’ kaygı/sinirliliğin baskın duygusal durum olduğunu savunan Sometimes Explode’un [[https://libcom.org/article/nervousness-politics][bu bloguna]] bakın.

[5] [[https://www.theguardian.com/theguardian/2008/mar/01/scienceofclimatechange.climatechange][James Lovelock]], kaygıyı fırtına öncesi bir tür sakinliğe bağlayarak, ancak kaçınılmaz olan gerçekleştiğinde çözülebilecek olan kaygıyı bu çizgide tartışmaktadır: “İnsanlık tam da 1938-9 gibi bir dönemde,’ diye açıklıyor, ‘hepimiz korkunç bir şey olacağını biliyorduk ama bu konuda ne yapacağımızı bilmiyorduk’. Ancak ikinci dünya savaşı başladığında, ‘herkes heyecanlanmıştı, yapabilecekleri şeylere bayılmıştılar, upuzun bir bayram gibiydi… bu yüzden şimdi yaklaşan krizi düşündüğümde, bu terimlerle düşünüyorum. Bir amaç duygusu – insanlar bunu istiyor.” Savaş zamanı nostaljisini paylaşamayız, ancak yaklaşan kıyamet duygusu kesinlikle çağdaş kültüre yayılmış durumda.

[6] [[https://endnotes.org.uk/articles/21][Endnotes dergisindeki bir makalenin]] yorumladığı gibi, “Esneklik sadece görünüşte muhafazakar bir ilkedir; istikrarı esnek olmamakta değil, sürekli kendi kendini dengeleyen uyarlanabilirlikte bulur.” Felaket topluluklarında ne devlet gücü ne de sözde girişimci ‘deha’ bu uyarlanabilir öz-örgütlenmeyi yaratabilir, daha ziyade durum istikrara kavuştuktan sonra harekete geçerler.

[7] [[https://libcom.org/forums/theory/request-communisation-theory-dummies-10042014][Bu forum konusunda]] olduğu gibi.

[8] Bu noktayı Facebook’ta bir tartışmadaki arkadaşımdan ödünç aldım. Bu olay [[https://www.marxists.org/archive/marx/works/1867-c1/p1.htm][Marx’ın Kapital’deki]] “burada bireyler <strong>yalnızca</strong> ekonomik kategorilerin kişileştirilmesi, belirli sınıf ilişkilerinin ve sınıf çıkarlarının somutlaştırılması oldukları ölçüde ele alınmaktadır” görüşüyle karşılaştırılabilir. Komünizasyon argümanı, ‘gerçek sübvansiyon’un daha sonra Marx’ın ‘yalnızca’ ayrıntısının tartışmalı hale getirildiği noktaya kadar ilerlemiş olmasıdır. Buna katılmıyoruz ve bu ayrıntının kapitalizmin herhangi bir teorik analizi için hayati önem taşıdığını düşünüyoruz.

[9] Komünist hareket bir anlamda sermayeyi yansıtmaktadır – ya büyümek ya ölmek zorundadır.

[10] Somut ve soyut ütopyalar arasındaki ayrım, Marx’ın itirazlarına rağmen Marx’ın aslında en büyük ütopyacı düşünür olduğunu göstermeye çalışan Ernst Bloch’tan gelmektedir. Marx’ın eleştirdiği ütopyacı sosyalistler yalnızca geleceğin toplumlarının soyut planlarını ortaya koyarken, Marx ütopyayı halihazırda mevcut olan somut eğilimlerin ve gizli potansiyellerin ayrıntılı analizi yoluyla aramıştır.

[11] Muhtemelen Endnotes burada klasik Marx’ı basitçe yorumluyor: “Komünizm, mevcut durumu ortadan kaldıran gerçek harekettir.”

[12] Alberto Toscano, [[https://www.metamute.org/editorial/articles/logistics-and-opposition][Logistics and opposition]], Mute.

[13] Jasper Bernes, [[https://libcom.org/library/logistics-counterlogistics-communist-prospect-jasper-bernes][Logistics, counterlogistics and the communist prospect]], Endnotes 3rd Issue.

[14] Ancak Ashok Kumar’ın Novara için kaleme aldığı [[http://wire.novaramedia.com/2014/04/5-reasons-the-strike-in-china-is-terrifying-to-transnational-capitalism/][şu yazıya]] göre: “Büyük tedarikçiler tedarik zinciri boyunca yatay olarak genişleyerek depolama, lojistik ve hatta perakendeyi de kapsar hale geldi. Bu gelişme, yarı tedarikçi tekelleşmesinin ortaya çıkmasına yol açarak tedarik zincirinin alt kısmında daha fazla değer elde edilmesine neden oldu (…) Adidas ve Nike gibi şirketler için Pou Chen gibi büyük ölçekli tedarikçilerden kaçmak artık son derece maliyetli.”

[15] Sandro Mezzadra & Brett Nielson, [[https://www.radicalphilosophy.com/article/extraction-logistics-finance][Extraction, logistics, finance: global crisis and the politics of operations]], Radical Philosophy. Bu yazı Endnotes’taki yazıyı tamamlıyor ve onunla birlikte okunmaya değer. ‘Karşı-operasyonlar’ öneren sonuç, Endnotes’un ‘karşı-lojistik’ savunusunu yankılamaktadır. Birincisi, sadece kesinti amacıyla bilişsel haritalamayı değil, aynı zamanda küresel lojistik-ekstraksiyon ağı boyunca mücadelelerin, ittifakların ve öznelliklerin oluşumunu vurgulayarak tartışmasız daha zengin bir kavram sunuyor.

[16] Manuel De Landa, A new philosophy of society: assemblage theory and social complexity, Continuum, s.10-11. Mezzadra ve Neilson’a katılıyoruz: “Herhangi bir sosyal varlığı ya da biçimi oluşturan çoklu ve değişken ilişkilerin izini sürmekte ısrar eden bu ağ ve asamblaj yaklaşımlarına sempati duymuyor değiliz. Ancak bu tür yaklaşımlar sermaye kategorisinin analitik geçerliliğini inkar edecek şekilde kullanıldığında ihtiyatlı davranıyoruz.”

[17] Örneğin, bir TV programı yakın zamanda bir Airbus A320’nin tamamını dönüştürmeye çalışmıştı. (Orijinal yazıdaki bağlantıya girilemiyor ve arşiv sitelerinde de arşivlenmiş bağlantılar bir yere çıkmıyor; ancak bulmaya çalışmak istiyorsanız yazının tam adı şu: “Kevin McCloud oversees three designers attempting to upcycle an entire Airbus A320 in Supersized Salvage | Unreality TV”)

{1} Tam zamanında depo (JIT depo), depoda bulundurulan envanter miktarını minimalize etmek için tasarlanan; müşterilerden gelen siparişlerin gerçek zamanlı olarak işlendiği bir depo yönetim modelidir. (ç.n.)

{2} Deleuze’e göre asamblaj, sürekli gelişen ve birbirine bağlı varlıklar, fikirler ve süreçler zinciri olarak dünya kavramını ifade eder. Deleuze, dünyanın birbirinden ayrı, izole varlıklardan oluşmadığını, bunun yerine sürekli değişen ve birbirine uyum sağlayan asamblajlardan veya “makinelerden” oluştuğunu savunmuştur. Asamblajlar, fiziksel nesneleri, insanları, fikirleri ve sosyal uygulamaları içerebilen unsurların karmaşık düzenlemeleridir. (ç.n.)

