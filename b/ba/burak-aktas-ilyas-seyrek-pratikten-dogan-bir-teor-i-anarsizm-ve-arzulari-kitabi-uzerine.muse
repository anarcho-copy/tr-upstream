#title Pratikten Doğan Bir Teori
#subtitle "Anarşizm ve Arzuları" Kitabı Üzerine
#author Burak Aktaş, İlyas Seyrek
#SORTtopics kitap, anarşizm
#date 02.09.2020
#source 13.09.2020 tarihinde şuradan alındı: [[https://meydan.org/2020/09/02/pratikten-dogan-bir-teori-anarsizm-ve-arzulari-kitabi-uzerine/][meydan.org]]
#lang tr
#pubdate 2020-09-13T10:02:27


<em>Anarşizm ve Arzuları</em>, Sümer Yayıncılık’tan Tuba Demirci tarafından çevrilerek Ağustos 2020’de Türkçe anarşizm literatürüne kazandırıldı.

Cindy Milstein kitabı “anarşizme giriş” niteliğinde bir kitap olduğu iddiasıyla yazıyor. Belirtmek gerekir ki herhangi bir konuda başlangıç kitabı yazmak her zaman zordur. Konu anarşizm olunca bu zorluk katlanarak artıyor çünkü anarşizm diğer ideolojiler, felsefi akımlar gibi masa başında değil sokakta yazıldı ve yazılmaya devam ediyor. Haliyle başlangıcının ne zaman olduğu konusunda net bir tarih vermek veya tarihten bir noktayı seçip anarşizmin doğuşu ilan etmek oldukça zorlaşıyor.

Giriş kitabı olarak kullanılabilecek Bakunin, Berkman, Goldman, Kropotkin gibi anarşistlerin yazdığı klasik dönem kitaplarının bugünün sorunlarına cevap olmadığı ve güncel gerçekliklerle örtüşmediği konusunda iddialar da bu kitapta mevcut. Bütün bunları göz önüne aldığımızda Milstein’in bu girişiminin oldukça cesur bir deneyim olduğunu söyleyebiliriz.

Bununla birlikte eserin yazım şeklinin anarşizmin temel ilkelerinden beslendiğini söylemek yanlış olmaz. Kitabın içerisindeki bölümler genel olarak bazı eylemlerde kullanılan bildirilerin, açıklamaların gözden geçirilmesiyle oluşturulmuş. Bu anlamda pratikten teoriye doğru bir yazım macerasından söz edebiliriz. Yazarın da ifade ettiği bu durum, özellikle Seattle Çatışmaları ve Katrina Kasırgası sonrası deneyimlerden derinden etkilenmiş. Küreselleşme karşıtı hareketin ve eylemlerin de eserin merkezinde olduğunu söyleyebiliriz. Kitabın teşekkür kısmının kalabalıklığı ve Milstein’in ifadeleri yazım ve basım aşamalarının kolektif yapısını ve niteliğini açıkça ortaya koyuyor, bu sebeple sadece içeriği değil şekli itibariyle de bu kitaba anarşizme dair bir kitap demek doğru olacaktır.

Yazıldığı politik ortamı anlamak ve neden böyle bir esere ihtiyaç duyulduğunu anlayabilmek için metnin yazıldığı dönemden hemen sonra Taksim İsyanı ile de benzer kökenli olduğu iddia edilen Occupy hareketinin yaygınlaşması ve o dönemdeki eylemliliklerde kendini güçlü bir şekilde var eden örgütlü anarşizmi de düşünmek gerekir. Kitap o dönemki güncel “Anarşizme Giriş” eseri boşluğunu doldurması ve anarşizmi yükseltmesi açısından da değerli bir metin olma özelliği gösteriyor.

İçerik kısmına geçmeden önce Türkçe çeviri için de birkaç söz söylemek gerekli. Giriş kitabı olarak yazılan bu eserin çevirisinde yer yer yapay bir dil kullanılmış. Bazen herkes anlasın diye yapılan “aşırı” Türkçeleştirmeden bazen de karmaşık cümle yapılarından ötürü bu konularla yeni ilgilenen biri için çok da okunaklı olmayabilir.

Pek çok anarşist düşünüre göre anarşizmin ve etiğin simbiyotik (iki canlının da faydasına olan yaşam formu, birlikte var olabilme ilişkisi) bir ilişkisi var. Milstein de bu geleneğe saygı duyuyor ve kitapta anarşizmin etikle örülmesi gerektiğinden bahsediyor. Burada kullandığı etik genel anlamda kullanılan “ahlak” değil Kropotkinci anlamda bir “değerler bütünü”dür. Bunu kitapta da alıntılanan şu bölümden anlayabiliriz:

<quote>
“Uyum… Profesyonel ve bölgesel olarak farklılaşmış gruplar arasında üretim ve tüketim yararına, uygar varlığın bağımsız olarak kurulmuş, aynı zamanda sonsuz çeşitlilikte ihtiyaç ve beklentileri için varılan özgür fikir birliği ve uzlaşmalarla yakalanır”
-Peter Kropotkin, “Anarşizm”, 1910
</quote>

Giriş kitabı olma iddiası sebebiyle bazı kavramlar oldukça karikatürize edilerek kullanılmış. Örneğin hâlâ anarşizm ile ilgili yanlış anlaşılan “Liberalizm” ve “Sosyalizm” sentezi bir ideoloji tanımı neredeyse birebir aktarılıyor. Anarşizmin hem birey hem de toplumu aynı anda dönüştürme ve düşleme ideali çok basite indirgenerek liberalizmin en iyi yanları ve sosyalizmin en iyi yanlarını birleştirirsek anarşizmi elde ederiz gibi iyi niyetlerle yazılmış ancak yanlış anlaşılmalara ve kavramsal tahribatlara yol açabilecek bazı bölümler görüyoruz.

Eserde tarihsel ve önemli bilgilere rastlamak da mümkün. Enternasyonal ve anarşistlerin ayrılışı gibi temel bazı kırılım noktalarına değiniliyor. Yazar bu konuda, yaşadığı coğrafya sebebiyle Bakunin ve Marks ikiliğinden çıkıp kendini özgürlükçü diye tanımlayan marksistlerle yan yana mücadelenin genel geçer bir yaklaşım olması gerektiğini belirtiyor. Fakat anarşizmin ve anarşist mücadelenin her coğrafyada farklılık gösterdiği ve özgünlüğünü koruduğunu vurgulamak gerekiyor. Kuzey Amerika coğrafyasında Marksist gelenek çok güçsüzken anarşistler bütün hareketliliklerde ön plandalar. Geçmişte de durum bundan farklı değildi. Amerika İşçi ve Emek Mücadelesi tarihi aynı zamanda o coğrafyadaki anarşizmin de tarihidir. Marksistler ise neredeyse hiçbir zaman böyle bir örgütlülüğe ve deneyime erişemediler. Bu sebeple Kuzey Amerika’da “özgürlükçü” Marksistler ile yakınlık göstermek stratejik anlamda doğru olabilir. Güncel örneklerden biri olan Belarus sürecine baktığımızda ise çoğu marksist ve sosyalistin ülkenin diktatörünü desteklediğini anarşistlerin ise sokakta ön saflarda mücadele ettiğini görüyoruz. Haliyle Belarus’ta böyle bir yakınlaşmanın olma ihtimali oldukça düşük. Bu sebeple kitapta coğrafyaya göre değişen taktik ve pratiklerin anarşizmin ilke ve prensipleriyle karıştırılabilme ihtimalini doğuran bir yaklaşım söz konusu.

Sonrasında anarşizmin antikapitalist kökenlerinden bahsedilip bu kökenleri Proudhon’a ve Godwin’e bağlayan bir bölümde, otonomcular, Zapatistalar, nükleer karşıtı eylemler, işgal evleri gibi deneyimlerden de bahsediliyor. Bu hareketlerin anarşist prensiplerle olan yakın ilişkisine vurgu yapılıyor fakat burada da örgütsüz anarşizm ve alt kültür anarşizmi anlayışının kabulü tehlikesi yatıyor.

Kitapta ayrıca siyasi yöntem olarak doğrudan demokrasi vurgusunun yapıldığını ve şu an içinde bulunduğumuz yapılar yerine doğrudan demokrasiye dayalı yapılar kurma fikrini sıklıkla görüyoruz. Bunlar da Milstein’in de kabul ettiği gibi Bookchin’den oldukça etkilenilmiş bölümler. Ayrıca kitapta Rousseau etkisini de oldukça net bir şekilde görebiliyoruz.

Toparlayacak olursak, kitabın özellikle Kuzey Amerika anarşizmini merak edenler için oldukça iyi bir kaynak olduğunun altını çizmek ve anarşist hareketin güncel sorunlara karşı radikal ve özgürlükçü pratikler geliştirebilmesinin teorik arka planını göstermeye çalıştığını belirtmek gereklidir. Fakat bunlarla birlikte daha önce de belirttiğimiz gibi “Anarşizme Giriş” için içerik olarak eksik bir kitap olduğunu ve dil olarak da zorlayıcı olduğunu düşünüyoruz.



