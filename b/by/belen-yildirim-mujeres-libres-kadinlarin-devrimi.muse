#title Mujeres Libres: Kadınların Devrimi
#author Belen Yıldırım
#SORTtopics kadın, kadın mücadelesi, ispanya, cnt fai, mujeres libres, anarşist feminizm
#date 24.01.2013
#source Meydan Gazetesi
#lang tr
#pubdate 2021-12-26T01:02:36


*1936’da İspanya’da anarşist devrimin kök saldığı topraklarda tarihte derin izler bırakacak olan bir kadın hareketi yeşeriyordu: “Mujeres Libres”*


Özgürlüğün tarifsiz hissi İspanya’yı devrim, devrimi anarşizm olarak görenlerin tarihinde saklıdır. 1930’larda İspanya’da anarşist bir devrim gerçekleşiyordu. İspanya’nın sokaklarında, caddelerinde, meydanlarında çocuk, kadın, erkek, genç, yaşlı hep bir ağızdan aynı şarkı söyleniyordu; herkes için özgürlük… Toplumsal devrim İspanya’nın topraklarını sarmalamış, mücadele beraberlik ruhunu pekiştirmişti. Herkes için özgürlük istiyorlardı çünkü anarşist devrimin kök saldığı bu topraklarda tarihte derin izler bırakacak olan bir kadın hareketi yeşeriyordu: “Mujeres Libres”

*** “Birlik olmaksızın, yaşam olmaz”

İspanya’da her yaştan, her kesimden kadın kendi kurtuluşlarını kendi hayalleriyle, akıllarıyla ve bedenleriyle kazanabileceklerinin farkındaydı. Onlar evlerinde yama dikenler, hastanelerde bakıcılık yapanlar, fabrikalarda işçiler, öğretmenler, fahişeler ve düşmüşlerdi. Düşünme, yaratma ve eyleme geçmek için hazırlanmış bekleyerek, kendi gerçeklikleriyle yan yana gelmişlerdi. Toplumsal devrimin hayatlarını değiştireceğine inanıyorlardı. Onlara göre özgürlük, temel olarak toplumsal bir üründü. Bireysellik ve yaratıcılığın tam ifadesi ancak topluluk içinde ve onun sayesinde gerçekleştirilebilirdi. Pilar Grangel’in (Mujeres Libres’te de aktif olan bir öğretmen) bireysellik ve topluluk arasındaki ilişkiyi betimlerken yazdığı gibi, “Ben ve benim doğrum; ben ve benim inancım. Ve ben senin için, ancak kendim olmaktan asla vazgeçmeyerek ki böylece sen de her zaman kendin olabilesin. Senin varlığın olmadan ben var olmadığım için, ama benim varlığım seninki için vazgeçilmez olduğu için.”

Onlar, hayatta kalmak için toplumsal yaşamın, karşıtlar arasındaki saldırgan bir çatışmayla değil, karşılıklı yardımlaşmayla düzenlendiği Kropotkinci anlayışa sık sık göndermeler yaptılar: “Birlik olmaksızın, yaşam olamaz.” Çok kısa sürede sayıları binleri buldu, örgütlendiler. Ekonomik sınıf hiyerarşilerinden, politik ve cinsel ayrıcalıklardan arınmış, adaletli bir toplumda, herkesin mümkün olanı gerçekleştirmekte özgür olacağına inanıyorlardı.

Ancak anarşist de olsa özgürlük koşullarında dahi kadın erkek arasındaki farklılıklar göz ardı edilebilirdi. Özellikle cinsiyete dayalı işbölümüyle biçimlenmiş bir toplumda, kadınlar için ciddi sıkıntıların ortaya çıkması kaçınılmazdı. İspanya’da da benzer sorunlar açığa çıktı. Anarşistler toplumsal örgütlenmenin ana ilkesinin politik olmaktan ziyade ekonomik olduğunda ısrar ettiler ve çoğunlukla ekonomik ilişkileri fikirlerinin merkezine yerleştirdiler. Ve ekonomik olarak tasarlanan bu odaklanma, cinsiyete dayalı işbölümüyle biçimlendirilmiş bir toplumda, kadınlar için ciddi sorunlar doğurdu. Kadınlar işe nasıl dâhil edileceklerdi? Özgür toplum, cinsiyete dayalı işbölümüne meydan okuyacak ve onun üstesinden gelebilecek miydi? Veya işbölümünü olduğu gibi bırakacak ve kadınlar için bir çeşit “farklı ancak eşit” statü gerçekleştirmeye mi uğraşacaktı?

İspanya’da 1910’dan itibaren CNT (Ulusal Emek Konfederasyonu) içerisindeki anarko sendikalistler, toplumun tabanına sendikaları koydular. Sendikalar, her sendikanın bir delege aracılığıyla ilişkilenen federasyonlar yoluyla, yerel ve endüstriyel olarak, koordine edileceklerdi. Ancak, bu anlayış (çocuklar, işsizler, yaşlılar, engelliler ve çalışmayan anneler dâhil olmak üzere) işçi olmayanların toplumsal kararlara katılımı için çok az fırsat yaratıyordu.

Anarko sendikalistlerin dışında, “Patronlar olduğu için işçiler vardır. İşçicilik kapitalizmle, sendikalizm de ücretlerle birlikte ortadan kaybolacaktır.” diyerek, İspanya’da uzun bir geçmişe sahip olan başka bir gelenek, “Municipio Libre” (Özgür Komün) hareketi sürüyordu. “Geçici anlamda dahi sendikalist çözümün uygun olmadığı, özellikle de tarımsal köylerde, toprağın ve tüm üretim araçlarının toplumsallaştırılması, üreticilerin ellerine verilmesi temelinde bütün İspanya’da özgür komünleri ilan ettiğimiz andan itibaren devrimin peşine düşme hakkımı saklı tutuyorum.” Bu sözler, sendikaların kapitalizmin ürünleri olduklarını, dönüştürülmüş bir ekonomide örgütlenme ve koordinasyonun temelleri olacaklarını varsaymanın anlamlı olmadığını düşünen iki anarşist kadına, Soledad Gustavo ve Federica Montseny’e aittir. İspanya’da kent sanayisi içerisinde ve kırsal alanda yaşam süren kadınlar anarşizm ve kadının konumu konusunda farklı görüşler ortaya atmışlardı.

*** “Hem kalplerde hem fabrikalarda sömürülüyoruz”

İspanya’da kadınların konumunun belirlenmesi noktasında iki görüş oldukça belirgindi. Biri CNT’nin sahiplendiği görüş olan, kadının üretici konumunda yer alarak erkekle eşit düzeyde ücretli emek gücüne dâhil olması. Diğeri ise Proudhon’cu görüş olan kadının evdeki rolüyle topluma katkı sunan üretici konumunda yer alması. Kadın böylece aktifleşecek, erkekle eşitlenecek ve özgürleşecekti. Bu iki görüş dışında kadının evde aktifleşmesinin ve CNT gibi sendikalara örgütlemenin kendi başına yeterli olmayacağına inananlar da vardı. Onlar için, kadınların ezilmesinin kaynakları işyerindeki sömürüden daha geniş ve daha derindi. Kadınların ezilmesinin ekonomik olduğu kadar kültürel olduğunu, kadınların ve onların aşağılanmasının otoriter ve hiyerarşik mekanizmalar aile, kilise gibi ahlakçı kurumlar aracılığıyla geliştirildiğini öne sürüyorlardı.

1930’lar boyunca süren, kadının konumuna ilişkin bu görüşler, Mujeres Libres’in kurulmasında etkili oldu. Hareket, İspanya anarşist devriminde bağımsız bir kadın hareketi olarak kendisini örgütlemiş ve cephe savaşlarında yer almıştır. Cinsel özgürlük, hareketin mücadele alanlarından biriydi. Aynı zamanda evlilik, doğum kontrolü ve aile planlaması alanında yürütülen ciddi bir mücadeleden bahsedebiliriz. Kadınların “hem kalplerde hem de fabrikalarda sömürülmesi”ne karşı koyan Mujeres Libres, hızla büyüyerek çok kısa bir zamanda otuz binlere yaklaşan kadın sayısına ulaştı. Toplumsal cinsiyet rollerinden ve erkek egemen davranış biçimlerinden tamamen kopamamış pek çok anarko sendikalist, sendikalarda yani iş yerlerinde ve ev yaşantılarında kadın erkek arasındaki eşitsizlik kalıplarını yeniden üretiyorlardı.

Mujeres Libres buna karşı çıktı. Mujeres Libres’in bir üyesi, yaşadığı deneyimden şöyle söz ediyordu:


<quote>
“Mujeres Libres düşüncesine katılmıyordum. Mücadelenin hem erkekleri hem de kadınları etkilediğini düşünüyordum. Birlikte daha iyi bir toplum için savaşıyoruz. Neden ayrı bir örgütlenme olsun ki? Bir gün, Juventudes’den (genç erkeklerden oluşan bir örgütlenme) bir grupla beraberken, Mujeres Libres’in Juventudes merkezinde ki ofislerinde düzenlediği bir toplantıya iştirak ettik. Genç erkekler kadın konuşmacılarla dalga geçmeye başladılar, bu beni başından itibaren sinirlendirdi. Kadının konuşması bittiğinde, erkekler peşi sıra sorular sormaya başladılar ve zaten hiçbir şey yapamayacaklarını düşündükleri kadınların ayrı örgütlenmesinin de anlamsız olduğunu söylediler. Tartışma coşkuluydu ancak yorumlarının tonu beni tiksindirmişti, birden Mujeres Libres’i savunmaya başlamıştım.”
</quote>


*** Sadece “kadın” oldukları için…

Mujeres Libres, dünyadaki birçok kadın hareketine ilham kaynağı olmuştur. Kadınlar ne sadece işçidirler, ne sadece köylü, ne sadece eş, ne sadece anne, ne de sadece anarşist. Kadınlar biçimlendirilmiş bu toplumsal roller dışında, öncelikle kadındırlar. Sadece kadın oldukları için ezilmekte olduklarını fark etiklerinde ise kendi kurtuluşlarını kendilerinin sağlayabileceklerini anlamışlardır. Onlar İspanya’da içsel devrimin, yani kendi devrimlerinin, peşinden yürümüşlerdir. Böylelikle İspanya’da yaşanmış anarşist devrim, aynı zamanda kadınların kurtuluşuna varacak yeni bir hareketin başlangıcı, kadının özgürleşmesine adanmış bir mücadelenin hafızalarımıza kazınan adı olmuştur; Mujeres Libres.



