#title Tohumda Sömürü
#author Nergis Şen
#SORTtopics ekoloji
#date 11.07.2019
#source 04.05.2020 tarihinde şuradan alındı: [[http://patikaekoloji.org/tohumda-somuru-nergis-sen/][patikaekoloji.org]]
#lang tr
#pubdate 2020-05-04T22:43:10



Bundan yaklaşık 350 milyon yıl önce… Tam olarak Karbonifer dönem içerisinde bitkiler soylarını sürdürmenin ve daha büyük alanlarda var olmanın bir yolunu buldular: Embriyolarını bir kılıf içine gizlemek. Sert kılıf içerideki döllenmiş yumurtayı dış etkenlere karşı koruyacak, bitkinin kendisi uygun koşulları bulup kabuğunu kırıncaya dek onu koruyacaktı. O bir tohumdu!

Bu tohumlar ve tohumlu bitkiler milyonlarca yıl içerisinde değişti dönüştü ve tüm dünyaya yayıldı. Bu geçen zaman içerisinde yeryüzünden bir çok tür geldi geçti. Toprağa düşen tohumlara, kabuğunu kıran filizlere, boy veren bitkilere eşlik ettiler.

*** İnsanlık ve Tohum

Bu türlerden bir tanesi yaklaşık 200.000 yıl önce Afrika’da boy gösterdi ve vakit kaybetmeden bütün dünyaya yayılmaya başladı. Mezopotamya’dan Çin’e, Çin’den Avrupa’ya kadar ayak basmadık yer bırakmadılar.

Muazzam bir gözlem gücüne sahip olan bu türün bireyleri, hayatlarını avcılık ve toplayıcılık yaparak sağlıyorlardı. Fakat bir gün Mezopotamya dolaylarında bir insan ya da insan topluluğu tohumun kudretini fark etti. Bitkiden toprağa düşen tohum milyonlarca yıllık evrimin getirdiği kolaylıklarla rahatlıkla doğuyor büyüyor ve bir şekilde hayatta kalıyordu. Hayatta kalan bu bitki ise doğadaki diğer türlerin hayatta kalmasını sağlıyordu.

İnsan bu döngüyü fark etti ve bitkinin yaptığı şeyi o yapmaya başladı. Bundan yaklaşık 10.000 yıl önce insan eliyle toprağa bırakılan ilk tohum -kendisine benzer karmaşık birçok nedenle beraber dinin ortaya çıkışı gibi- toplumların hayatını kökünden değiştirdi ve toplumları bugüne kadar taşıyan olaylar silsilesini başlattı.

İlk önce birkaç küçük deneme yapıldı. Sonra göç güzergahlarında kurulan geçiçi bahçeler kalıcı bahçelere dönüşmeye başladı. Kalıcı bahçeler yavaş yavaş tarlarara dönüşürken komplike sulama sistemleri geliştirildi. Bu arada kurumsallaşmış dinler, rahipler, kolluk kuvvetleri ve devletler geriye kalan insanların tepesinde büyüyüp serpilmeye meylettiler.

Toprağa tohumu ilk defa bırakan çiftçinin torunları ve torunlarının torunları o tohumları melezleyerek bitkileri daha verimli hale getirdi. Yüzyıllar hatta bin yıllar boyunca -her ne kadar sırtlarında yukarıda saydığımız kamburları taşımak zorunda kalsa da- çiftçi topraktan toprak çiftçiden öğrenmeye devam etti.

Aradan binlerce yıl geçti. Günümüze kadar emekleyerek daha fazla üretim, daha az zaman şiarı hakim olmaya başladı. Sanayide kullanılan makineler, tarlalara transfer olurken zenginlerin zaman ve mal hırsı kimya sanayisini gübre ve ilaç olarak toprakla buluşturuyordu. Bu buluşmadan kimileri için zenginlik kimileri için de zehir çıkıyordu.


*** Tohumun Devletler ve Şirketler Tarafından Kontrolü

Bir toplumu kontrol etmek ve yönlendirmek gıda temelli başlar ve gıdayı kontrol etmek de tohumu kontrol etmekle… Tarımsal üretimi en başından beri kontrol altına alabilmek ve dönüştürmek, çiftçinin elinde bulunan yerel tohumu yok etmeyi amaçlar. Bu gıda iktidarını elinde bulundurmak için dünyanın genelinde şirketler ve devletler türlü stratejiler geliştirmişlerdir.

1950-1970 yılları arasında, ABD ve kapitalist devletlerin çoğunun yaptığı, tarımda kimyasalların üretimi, kullanımı ve tohum üzerindeki genetik araştırmalar elde edilen mahsulün artışına yol açmıştır. Bu süreç, “birinci yeşil devrim” süreci olarak adlandırılır.

Dünya Savaşı sonrasında tarım olarak verimi çok yüksek bölgelere yönelik çalışmalarla “ikinci yeşil devrim” süreci başlatılmıştır. Bu süreçlerle tohum yetiştiriciliğinin merkezileşmesi sistematik bir şekilde başlamış bulunmaktadır.

İlk olarak Nelson Rockefeller ve ABD’nin eski tarım bakanı Henry Wallace tarafından Meksika bölgesinde, Meksika devletinin de desteğiyle gerçekleşmiştir. Buğday üzerinden işleyen bu süreçte Meksika, buğdayda kendine yeterli bir bölge haline gelmesinin yanı sıra ihracat yapmaya da başlamıştır. “Yeşil devrim”in uygulandığı bir sonraki bölge ise çok verimli toprakları ve kalabalık nüfusuyla Hindistan’dır. Burada da Ford Vakfı ve Hindistan devletiyle birlikte pirinç üretimi arttırılmıştır. Devamında ise Filipinler, Pakistan ve Çin’deki tahıl üretiminde artış yakalanmıştır.

Bu olayların etkisiyle; 60’lı yıllarda, Filipinler’de ekimi yapılan 3000’nin üzerinde pirinç türü varken yirmi sene sonra bölgenin %98’inde iki tür pirinç kaldı. Yani; devletler ve şirketler birbiriyle anlaşmalı bir şekilde, yerel tohumların tükenmesi ve geleneksel tarım yöntemlerinin dönüştürülmesi için üretimini kontrol altına aldıkları, hibrit veya GDO’lu tohumların ekimini dayatmaktaydı.

Hibrit tohumlardan, yerel tohumlara göre bir ekimde daha fazla mahsül elde edilebilmektedir. Ancak bu traktörlerle, sulama araçlarıyla, kimyasal gübrelerle, aşılarla ve ilaçlarla sağlanmaktadır. Hal böyle olunca, toprağın verimi aşırı düşüş yaşamaktadır. Bir başka dezavantajı ise sonraki dönem için tekrar tohum elde edilememesidir. Hibrit tohumlar çiftçiyi sürekli şirketlere mahkum eden bir pozisyonda bırakarak, ekonomik açıdan oldukça zorlamaktadır ve çiftçilerin kendi arasındaki dayanışma ilişkisini de sonlandırmayı amaçlar.

Monsanto, DuPont Pioneer, Syngenta, Dow, Land O’Lakes ve Bayer; dünya çapındaki bu şirketler dünyadaki tohumların yarısından fazlasını kontrol ediyor. Tohumun yarısından fazlasını kontrol etmelerine rağmen daha fazla kar hırsıyla, daha fazlasını elde etmek için birbirlerini desteklemekten geri kalmıyorlar. Syngenta, 2000 yılında ilaç şirketi olan Novartis’in tarım bölümü ile tarım kimyasalları şirketi Zeneca’nın birleşmesi sonucu oluşmuştur. Birbirlerini desteklediklerini gösteren şirketlerden bir diğeri ise Monsanto ve Bayer’dir. Bu iki dev şirket 2016 senesinde birleşerek genel olarak tohum meselesinde birinciliği garanti altına almışlardır. Bu birleşme öncesinde 2013 verilerine göz attığımızda; tohum satışlarında %26 pay ile birinciliği Monsanto elinde bulunduruyorken Bayer ise %3 pay ile altıncı sırada yer alıyor. Aynı yıl tarım ilaçlarına ait verilerde birincilik %20 payla Syngenta bulunmaktadır. Bu sıralamada Bayer %18 payla ikinci, Monsanto ise %8 payla beşincilikte. Bu tablonun ardından Bayer, Monsanto’yu satın alarak hem tohumda hemde tarım ilaçlarında birinciliği garantilemiş bulunuyor. Yani kendisi genetiği bozulmuş tohumlarını üretiyor, tohumun verimini arttırmak için ilaç üretiyor; bu kimyasal müdahalelerle insan sağlığı bozuluyor; bozulan insan sağlığının tedavisi için ilaç üretiyor. Tam bir kapitalist strateji; tam bir “kazan-kazan”.

Tohumu kendi tekellerinde bulunduran bu şirketler, bulunduğu bölgeye ait tohumları oralarda bulunan çiftçilerden toplamıştır. Buradaki amaçsa bu tohumları saklayıp,ıslah etmek, tohum üzerinde araştırmalar yapabilmek ve belki de en can alıcısı gelecekte kendilerinin yani şirketlerin iktidarını yaratacağı bir düzenin planları doğrultusunda kullanabilmektir. Bu tohumları saklamak içinse tohum şirketleri tarafından tohum bankalarını oluşturulmuştur. Devletler ve şirketler ellerinde birçok tohumu bulundurarak tohumlar üzerinden endüstriyel patentlere sahip olmayı amaçlamaktadırlar.

*** Patent Hırsızlıktır

Patent hakkı bundan 30 yıl kadar önce sadece makinalar -cansız varlıklar- için kullanılıyorken, günümüzde genetik yapısı değiştirilmiş organizmalar yeni bir tür olarak tanımlanıp patentlenmektedirler. İlk olarak 1980’de genetiği değiştirilmiş bir mikrop ABD tarafından patentlenmiş ardından ise 1982’de Münih Avrupa Pantent Bürosu “insan eli değmiş her şey patentlenebilir” diyerek genetiği değiştirilmiş mikroorganizmaları patentlemiştir. 1985 yılında bitkiler, 1988 yılında hayvanlar, 2000 yılındaysa ilk defa insan embriyosu patent altına alınmıştır. Özellikle; çok yoğun oranda ekimi gerçekleşen buğday, mısır, pirinç gibi temel ihtiyaçların genetik oynamalarıyla yeni bir ürün olduklarının düzeni yapılarak şirketler bu ürünlerin patentlerini almışlardır. Monsanto, Washinton’da bulunan patent kurumundan 1989 yılından 2005’e kadar 647 adet patent hakkı aldığı verileri bulunmaktadır. Böylesi bir durumda çiftçi kendi yaşadığı bölgeye ait olan yani kendisinin olan tohuma ulaşmak için o tohumun patentlendiği şirketlere bağlı bir duruma gelmiş bulunuyordu.

İlk olarak Meksika ve Hindistan’da başlatılan yeşil devrim süreci devletlerin ve şirketlerin yararına olduğundan dünyada yayılmaktaydı ve bu süreç Türkiye’yi de içine çekmişti. Bu süreçte Türkiye’de ilk olarak “Sonora 64” isimli buğdayın ekimi gerçekleşti. Rockefeller Vakfı’nın kuruluşunda aktif olarak yer aldığı Meksika’da bulunan bir araştırma enstitüsünde geliştirilen bu hibrit olan buğdayla, Türkiye’deki tarım süreci de artık temizlenmesi zor ve sonlandırılamayacak bir yola girdi. Hibrit tohumlar artık çeşit çeşit ekilip biçilmeye, aynı zamanda çiftçiyi ekonomik açıdan dara sokmaya başladı.

2006 yılında 5553 sayılı Tohumculuk Kanunu’nun çıkmasıyla çiftçinin artık yerli tohumu satması yasaklanmıştı. Ayrıca yerli tohumla üretim yapan çiftçi ise desteklenmeyecekti. Bu kanunla birlikte tohum şirketlerinin birliği olan Türkiye Tohumcular Birliği kurulmuş ve devlet tarafından tohumdaki kontrol bu birliğe ait olmuştu. Bu yapılanlar sonucunda artık temiz gıda da iyice ortadan kalkma riskiyle karşı karşıya kaldı.

Bir takım çiftçi hibrit tohumlarla üretim yaparken bir takım çiftçi ise tüm yasaklamalara rağmen bir şekilde devletlerden ve şirketlerden saklayarak ellerinde bulunan yerel tohumla üretimlerine devam etti. Tabi yerel tohumlarla elde edilen gıdaların piyasadaki satışının yasaklanma durumuyla ekonomik varlıkları zora giren çiftçiler, son senelerde adlarını sıkça duyduğumuz gıda topluluklarıyla, kolektiflerle ve kooperatiflerle tanışmış oldu. Çiftçiler için hem ekonomik bir alternatif yol olan gıda toplulukları, kolektifler ve kooperatifler hem yerel tohumun dilden dile aktarılmasının, unutulmamasının önünü açmış bulunuyorlar.

19 Ekim 2018 tarihinde Tarım Bakanlığı tarafından “Yerel Çeşitlerin Kayıt Altına Alınması, Üretilmesi ve Pazarlanmasına Dair” yeni bir yönetmelik yürürlüğe girdi. Yönetmelikle birlikte çiftçiler ellerinde bulunan yerel tohumlara dair patent almak zorunda bırakılıyor. Bunun sonucunda da tohumlar patentlenerek şirketlerin malı haline gelmişken bireylerin de mülkü olabilecek ancak devlet talep ettiğinde vermek zorunda olacaktı.

*** Tohum Yaşamdır, Yaşamı Savunacağız!

Yerel tohumlardan elde edilen gıdalarla beslenmek gıdanın temizliğinden ve lezzetinden dolayı diğer tür tohumlara göre oldukça üstündür. Yerel tohumun doğayla ilişkisi de daha az su kullanılmasından, suni olmayan ve az gübre kullanılmasından, kimyasal ilaçlar olmamasından ve toprağı verimsiz bırakmamasından dolayı oldukça uyumludur.

İnsanların yaşamlarını sürdürebilmesi için ihtiyaç duydukları gıdayı kendi kontrollerinde bulundurmak isteyen devletler ve şirketler insanları üretim alanında büyük oranda kendilerine bağımlı hale getirmeye çalışıyor. Bu da ekonominin her alanında (üretim, tüketim, dağıtım) devletlerin ve şirketlerin iktidarlarını büyütmesi demek.

Tohuma devletlerin ve şirketlerin böylesine saldırıları varken bazı çiftçiler veya kendilerince balkonlarında, bahçelerinde ufak tefek üretim yapanlar bu saldırılara karşı direniş gösteriyorlar. Tohumun hiçbir şekilde patentlenemeyeceğini, yaşam olduğu savunanlar; kendi bulundukları bölgelerde veya düzenlenen festivallerde kendi aralarında tohum takası yaparak yerel tohumun varlığını sürdürmeye devam ediyorlar. Bizler, doğasına ve yaşam alanlarına yönelik saldırılara karşı direnmiş olanlar, üretim faaliyetlerimizin temelini oluşturan tohumlara karşı gerçekleştirilen saldırılara da direniş göstermeye, alternatifler yaratmaya devam edeceğiz. Ve bu tohumu -yaşamı- korumaya, üretmeye ve nesilden nesile aktarmaya, bu geleneği her daim sürdürmeye kararlıyız.



