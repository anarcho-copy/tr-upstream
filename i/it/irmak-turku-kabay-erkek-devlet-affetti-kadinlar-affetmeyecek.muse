#title Erkek Devlet Affetti Kadınlar Affetmeyecek
#author Irmak Türkü Kabay
#SORTtopics kadın, kadın mücadelesi, kadın cinayeti
#date 04.05.2020
#source Meydan Gazetesi
#lang tr
#pubdate 2020-05-04T18:31:35

Mayıs ayındayız…

11 Mart’tan 31 Mart’a kadar geçen 20 günlük süreçte 21 kadın katledildi. Evde kalmak “korunmak” için ilk koşul olarak gösterilirken geçtiğimiz günlerde Dilek Kaya kendi evinde karantinadayken erkek sevgilisi tarafından katledildi. Devletin bu süreçte başlattığı “hayat eve sığar” kampanyası, kadınların hayatında karşılığını bulmadı. Korona virüs tedbiri adı altında çıkarılan her yasa ve üretilen her politika da öncekiler gibi, kadınların erkek şiddetine terk edilmesiyle sonuçlanmaya devam ediyor.

2014 yılında boşandıktan sonra bile şiddete uğrayan ve en az yüz kere karakola başvuran Zeliha Erdemir, geçtiğimiz haftalarda mecliste onaylanan infaz yasasındaki değişiklikler kapsamında birçok hapishanede tahliyeler sürerken bu yasanın tehdit ettiği binlerce kadından biri.

Bundan 4 yıl önce, boşandığı erkek Cem Kara tarafından tehdit edilen Zeliha Erdemir, evlendikleri 2011 yılından beri sistematik olarak şiddete maruz kalıyordu. Zeliha can güvenliğinin olmayışını şu sözlerle anlatmıştı:

<em>“Çocuğumun, ailemin, benim can güvenliğimiz yok. 2011’den beri bana sistematik olarak fiziksel şiddet, hakaret ve tehditlerde bulunuyor. Didim Adliyesi’nde 46 şikayetime soruşturma açıldı ve herhangi bir sonuç alamadık. 2016’dan beri süren şiddet davam var. Hayatım altüst.”</em>

Zeliha’nın sesini sosyal medyadan “ölmek istemiyorum” diyerek sesini duyurmasıyla ve kadın örgütlerinin de tepkisiyle bu erkek tutuklanmış, Zeliha çocuğuyla beraber yeni bir hayata başlamıştı.

Bu erkek daha önce Zeliha’nın 8 yaşındaki çocuğunu kaçırdığı için 2 ay boyunca tutuklu kalmıştı, yetmedi. Serbest bırakıldıktan hemen sonra Zeliha’ya defalarca şiddet uyguladı. Zeliha ise ısrarla şiddet karşısında susmadı. Bu erkeğin yargılandığı dava 17 ay 20 gün hapis cezasıyla sonuçlanmışken değiştirilen infaz yasasıyla 2 Mayıs günü onun da tutukluluğunun sona ereceği öğrenildi. Yani yine bir tehdit ve yine aynı erkek…

Zeliha 2 Mayıs gelmeden sesini duyurmak için çeşitli kanalları kullanırken infaz yasasıyla ilgili <em>“Kimse o erkeği benim yerime affedemez. Başıma bir şey gelirse sorumlusu devlettir!”</em> diyor.

Peki Zeliha, devleti sorumlu tutarak ne demek istiyor?

Kendisini katleden erkeği, katledilmeden önce 24 kere şikâyet ettikten sonra sokak ortasında öldürülen Ayşe Tuba Arslan’ın ve şiddet gördüğü için karakola başvurduğunda <em>“Bir şey olmaz.”</em> denilerek eve gönderilen nice kadının sorumlusunun devlet olduğunu söylüyor Zeliha. Devlet, uyguladığı politikalarla kadınların sadece varlığını değil yok oluşunu da görmezden geliyor.

Yine geçtiğimiz günlerde, evli olduğu Rukiye’yi bıçakladığı için tutuklanan ve 2019 yılında serbest bırakılan bir erkeğin 9 yaşındaki kızını döverek katlettiği ortaya çıktı. Kadına ve çocuğa şiddet uygulayanları serbest bırakarak bunu meşrulaştıran, hiçbir sorun yokmuş gibi aynı politikaları sürdüren devlet, tüm kadınlar ve çocuklar karşısında suçlu değil midir?

Devlet de erkektir ve erkekler için adalet demek, kadınlar için adaletsizlik demektir… Bu erkekleri affeden devletken aynı sorunların tekrar yaşanması an meselesidir. Hapisten çıkarak “yarım bıraktığı işi tamamlamak” isteyecek erkekleri koşulsuz şartsız serbest bırakmak, <em>“Kadınlar katledilsin”</em>! demektir.

Bu durumda <em>“Kim kimi affediyor?”</em> sorusunu sormalıyız.

Vladimit Jankélévitch “Pardonner?” adlı kitabında, Nazilerin ölüm kamplarında yaptıkları katliamların, hayatta kalanlar tarafından -ölenler adına- affedilmesinin mümkün olmadığını vurgular. Suçlunun affedilmeyi isteyebilmesi için, öncelikle hiçbir çekince koymadan, suçuna bahaneler bulmadan, suçluluğunu kabul etmesi gerektiğini belirtiyordu. Ve böyle bir durumda bile affetmek yalnızca bir kandırmacadan ibarettir. Affetmek, çoğunlukla o eylemi yok saymaya ve unutmaya çalışmak olarak çıkar karşımıza. Bir kadının uğradığı şiddeti affetmesi, o şiddetin izlerini tamamen silmesi mümkün olmaz çoğu zaman. Bu izler bedenden silinse de psikolojik etkileri sürebilir.

Zeliha’nın <em>“Kimse benim yerime onu affedemez.”</em> demesi bu yüzden anlamlıdır. Çünkü Zeliha, kendisine yeni baştan bir hayat kurmaya çalışırken devlet onun adına bir şiddet failini affederek serbest bırakacak. Yani Zeliha’nın kurtulmaya çalıştığı şiddet tekrar hayatında yerini alacak ya da Zeliha’nın sesi duyulacak ve yalnızca Zeliha’nın değil birçok kadının yaşamı kurtulacak.

Devlet, kadınları evlere kapatsa ve seslerini duymazdan gelmeye çalışsa da kadınların isyanı evlere sığmaz! Buna güvenelim. Öfkesini isyan eyleyen, özgürlük için mücadele eden biz kadınlar devletin baskısına, bizi yok etmeye çalışan politikalarına karşı yalnız ve güçsüz değiliz, bize şiddet uygulayan erkekleri ve onların şiddetini görünmez ve meşru kılmaya çalışan devleti affetmiyoruz.

“Kadınlar birlikte güçlü!” diyerek birbirimizin sesini duymayı sürdürelim ve birbirimizin sesini duydukça mücadelemizi büyütelim.



